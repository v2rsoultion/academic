<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading wow bounceUpLeft">
 				<h1 class="wow bounceUpLeft">Company</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home"> Home / </a> </span>   Company</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid we-provied ">
 	<div class="container padding_zero padding-all">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 col-xs-12 company-contant wow bounceInLeft">
 				<h1> We provide  best school management </h1>

 		       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 		       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 		       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 		       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 		       cillum dolore eu fugiat nulla pariatur.  </p>

 		        <p> ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 		       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 		       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 		       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 		       cillum dolore eu fugiat nulla pariatur. 
 		        Duis aute irure dolor in reprehenderit in voluptate velit esse
 		       cillum dolore eu fugiat nulla pariatur. </p>

		     </div>
 	     	<div class="col-md-6 col-sm-6 col-xs-12 company-contant wow bounceInRight">
 		       	<img src="images/team.png" class="img-responsive team-img">
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal ">
	<div class="container padding_zero padding-all">
		<div class="row">
			
 	     	<div class="col-md-6 col-sm-6 col-xs-12 company-main-block wow bounceInLeft">
 	     		<h1> our Vision </h1>
 	     		<p>
 	     		   Lipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 	     			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 	     			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 	     			consequat. 
 	     		</p>
 	     		<div class="clear"></div>

 		       	<ul class="company-list-contant wow fadeInLeft" type="none">
 					<li><span></span>
 						<p>Multiple Platform - Web, Android, IOS</p> 
 					</li>
 					<li><span></span>
 						<p>15+ Modules for School ERP</p> 
 					</li>
 					<li><span></span>
 						<p>10+ System Roles of School</p> 
 					</li>
 					<li><span></span>
 						<p>12 Months of Pre-Development Research</p> 
 					</li>
 				</ul>
	 	     	
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-6 col-sm-6 col-xs-12 feature-main-block feature-main-xs wow bounceInRight">
 	     				<img src="images/vision3.png" class="img-responsive vision3">
 	     	</div>
 	     	<div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

  <!-- section MIssion start -->
<div class="container-fulid our-mission ">
	<div class="container padding_zero padding-all">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12  feature-main-xs wow bounceInUp">
 	     			<img src="images/team-work.png" class="img-responsive team-png ">
 	     	</div>
 	     	<div class="col-md-6 col-sm-6 col-xs-12 company-main-block wow bounceInUp">
 	     		<h1> our Mission </h1>
 	     		<p>
 	     		   Lipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 	     			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 	     			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 	     			consequat. 
 	     		</p>
 	     		<div class="clear"></div>

 		       	<ul class="company-list-contant wow fadeInLeft" type="none">
 					<li><span></span>
 						<p>Multiple Platform - Web, Android, IOS</p> 
 					</li>
 					<li><span></span><p>15+ Modules for School ERP</p></li>
 					<li><span></span><p>10+ System Roles of School</p> </li>
 					<li><span></span><p>12 Months of Pre-Development Research</p></li>
 				</ul>
	 	     	
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	
 	     	<div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

  <!-- section admin portal start -->
<div class="container-fulid meet-our-team">
	<div class="container padding_zero padding-all">
		<div class="row ">
			<div class="col-md-12 col-sm-12 col-xs-12 team-our-main-block wow fadeInUp">
 		       	<h1>Meet out team </h1>
 	     	</div>
 	     	<div class="clear"></div>
 	     	<div class="col-md-4 col-sm-4 col-xs-12 our-team-block ">
 	     		<div class="our-team wow fadeInUp">
	 	     		<div class="circle-block-team">
	 	     			<img src="images/2.png" class="img-responsive wow rotateIn">
	 	     		</div>
	 	     		<h3>  Team name</h3>
	 	     		<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, seddo 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-4 col-sm-4 col-xs-12 our-team-block ">
 	     		<div class="our-team wow fadeInUp">
	 	     		<div class="circle-block-team">
	 	     			<img src="images/2.png" class="img-responsive wow rotateIn">
	 	     		</div>
	 	     		<h3>  Team name</h3>
	 	     		<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, seddo 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-4 col-sm-4 col-xs-12 our-team-block ">
 	     		<div class="our-team wow fadeInUp">
	 	     		<div class="circle-block-team">
	 	     			<img src="images/2.png" class="img-responsive wow rotateIn">
	 	     		</div>
	 	     		<h3> Team name</h3>
	 	     		<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, seddo 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	
		</div>
	</div>
</div>
 <!-- end -->


 <!-- section why us start -->
  <!-- section admin portal start -->
<div class="container-fulid Our-values">
	<div class="container padding_zero padding-all">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 company-vlalue-block wow fadeInUp">
 		       	<h1>Our values </h1>
 	     	</div>
 	     	<div class="clear"></div>
 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/client.png" class="img-responsive " >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> client focus</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amt, consectet adipisicing elit, semeid eiusmod. 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/quality.png" class="img-responsive quality-img" >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> quality</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amt, consectet adipisicing elit, semeid eiusmod.  
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/integity.png" class="img-responsive" >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> integrity</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amt, consectet adipisicing elit, semeid eiusmod. 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/team-work-icon.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> team work</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amt, consectet adipisicing elit, semeid eiusmod. 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>
		</div>
	</div>
</div>
 <!-- end -->

 
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

