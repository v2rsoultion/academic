<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Student and Staff Information System</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>Student and Staff Information System</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Student and Staff Information System</h1>
 		       	<p>Academic Eye provides 2 different modules for Staff and Student information management system, as it is very important for school to store required data and information about student and staff so we have designed this modules to eradicate the manual paper based system and store accurate information which can be corrected anytime if needed and accessed anytime need, This system provides efficient database and it eliminates paperwork and extra effort utilized by the management team.</p><br>
 		       	<p>Student Information System provide complete student profile which stores complete information of studying student as well as left student, school administration can check student’s entire information such as student’s personal details, parents, address, academic, medical and discipline records.</p><br>
 		       	<p>Staff Information System provide detailed information about school’s staff and closely integrated with payroll module so that attendance and leaves provide accurate for salary generation. This module stores details of staff such as personal, education, experience, class-subject allotted in school, payroll, attendance, leaves management, roles of staff in school.</p><br>
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Staff and Student Information system both modules serves same purpose of managing their details in the system.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/fees.png" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Complete Profile view</h1>
 	     				<p>Staff and Student entire information in a profile view is available to school.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/report-analicy.png" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Ease to Store information</h1>
 	     				<p>Information is categorized which makes it easy to store data for staff and student.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/offlin-fess.png" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Easy to get Individual Report</h1>
 	     				<p>School can easily view and export individual staff or student information in pdf format.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/report.png" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Faster Access to details</h1>
 	     				<p>These modules store information in proper format which facilitates faster access to only required of particular staff or student.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Student and Staff Information System</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/cheque.png" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Attendance </a></h1>
					<p>Student Information system is provided to high authorities to view student’s attendance,
Staff Information system manage staff attendance with actual timing with biometric machines.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/cheque.png" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Leave Application</a></h1>
					<p>All the leave application applied by parents for student will be displayed in student information system, All the leave application applied by staff and balance of leaves will be displayed in staff information system.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/cheque.png" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Student’s Academic Information</a> </h1>
					<p>In Student information system apart of from basic and guardian details, school can check details of academics i.e, percentage achieved in previous classes.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/cheque.png" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Staff Performance</a></h1>
					<p>In Staff Information system, school can check staff performance on the basis of increase/decrease of student’s marks from previous test so that teacher evaluation can be done easily.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/cheque.png" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Staff Roles for Access</a> </h1>
					<p>In Staff Information system, Roles and Permission are assigned to staff, so that each staff has its own work area and responsibilities in the system.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Reports</a> </h1>
					<p>School can get easy reports of previous or existing staff or students in these modules, these reports can be print or can be export as PDF file.</p>
				</div> 				
 			</div>
 			
 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

