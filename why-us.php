<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading wow bounceUpLeft">
 				<h1 class="wow bounceUpLeft">Why Us</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="index"> Home  / </a> </span>  Why Us</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->


  <!-- section admin portal start -->
<div class="container-fulid why-us-page">
	<div class="container padding_zero padding-all">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 company-vlalue-block wow fadeInUp">
 		       	<h1>Why Us </h1>
 	     	</div>
 	     	<div class="clear"></div>
 	     	

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/friendly-user.png" class="img-responsive quality-img" >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">User Friendly</h3>
	 	     		<p class="wow fadeInUp">Academic Eye is complete Icon-based system which is completely understandable user without any help of technical person for guidance, User Interface is well-designed, interactive, progressive and self explanatory.</p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/easy-us2.png" class="img-responsive" >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Easy to Use</h3>
	 	     		<p class="wow fadeInUp">Academic Eye is easy to use web and mobile application as it is easy to understand, easy to get familiar, provides them access anywhere, anytime which doesn’t troubles the user as compare to traditional system for school management.


	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/productive.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Productive and Efficient </h3>
	 	     		<p class="wow fadeInUp">Academic Eye provides perfect solutions for the school’s management requirement, by providing such solutions it reduces the time and efforts of management hence it increase efficiency of management which increases productivity of staff and school.

	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/affordable.png" class="img-responsive Affordable">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> Affordable Price</h3>
	 	     		<p class="wow fadeInUp">Academic Eye is inexpensive software application which is designed to provide efficient solution on affordable prices to all the types of education institute from large to small, and school only need to pay for their usage requirement.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/why1.png" class="img-responsive Web-base">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Web Application</h3>
	 	     		<p class="wow fadeInUp">Web Based Application are the best solution for school management, Academic Eye is a complete web based application which can be run on Web Browsers hence it gives the advantage of using the system anytime and anywhere, reduce local infrastructure to run the software.

	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/why3.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Mobile Application</h3>
	 	     		<p class="wow fadeInUp">Academic Eye is available on Google Play Store and Apple App Store which provides complete solution to operate various functionalities from mobile application for all users from Principal to Security Guard, Parent, Student. The school doesn’t even need to pay extra for mobile application.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/why2.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Cloud Based ERP</h3>
	 	     		<p class="wow fadeInUp">Academic Eye covers all the benefits of cloud technology, as it is cloud based application it store the data on the cloud servers which ensure data backup and data security, School Management and Parent/Student can access 24x7 from anywhere.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/coustomztion.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Customization Ready</h3>
	 	     		<p class="wow fadeInUp">Academic Eye can be easily customized as per requirement of schools, if it doesn’t fulfill the complete requirement of school, we have dedicated teams of experts developers which are constantly working on research and development of project.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/why4.png" class="img-responsive Access Access2">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Secure Access to Portal</h3>
	 	     		<p class="wow fadeInUp">Academic Eye concerns about complete security, for all portal access for  Administration, Staff, Parent, Student is only possible with correct login credentials of all users, It is Role Based and Permission Based software which provides limited access to functionality and data to users.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/why5.png" class="img-responsive ssl ss2">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Web SSL Security</h3>
	 	     		<p class="wow fadeInUp">We are taking care of institutes data and information with best security, Academic Eye provides complete Web Based Security by using Secure Protocol i.e, Secure Socket Layer which encrypts the connection between your webserver and visitors' web browser.  
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/data-backup.png" class="img-responsive data-backup">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Data Backup</h3>
	 	     		<p class="wow fadeInUp">To ensure institutes from problem of data loss, we have provided Automatic Data Backup facility which takes daily data backup so that management doesn’t need to worry about loss or failure of data, backups are saved online so anytime school can restore their data.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/privacy-data.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> Data Privacy </h3>
	 	     		<p class="wow fadeInUp">Privacy of Data is important for all the schools, Data Leakage can cause spam or marketing emails, sms, calls to school staff, students and their parents, school processes delicate data of fees collection and accounting management on software such data must be private to schools, Academic Eye gives protection to school data and also provide agreement of data privacy.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/multi-institue.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Administrative Portal</h3>
	 	     		<p class="wow fadeInUp">Academic Eye provides separate portal for High Level Authorities. As they are Managing Director/Trustees/Principal of School they can view their multiple school in a single portal, and also can manage all the system and dedicated reports is created for all the Trustee/Director of school.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/school-staff.png" class="img-responsive school-staff">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Staff Portal</h3>
	 	     		<p class="wow fadeInUp">Academic Eye is complete Role based and Permission based system, entire school staff will get their login credentials to use portal, all the staff can view their details in the portal and also able to see the module for which they are responsible to work, There are more than 10 Roles in the system which can be assigned to any staff.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/family.png" class="img-responsive Parents	">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> Parents Portal</h3>
	 	     		<p class="wow fadeInUp">Apart from School Administration and Staff, we provide Parents portal because nowadays parents getting involved in students academics performance so they need to get information about their wards, Academic Eye provides web and app portal for parents with SMS/Email/Notification for alerts and Interactive Voice Response to get complete ward academic information.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/stuendt-login.png" class="img-responsive Student">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Student Portal</h3>
	 	     		<p class="wow fadeInUp">Academic Eye provide student portal so that they can get their all academic details like daily homework ,daily attendance, exams marks, exams schedule and more on the school app and website. Students can also check Notice Board and Dairy Remarks in their portal. Students can also get messages in their group, task from principal.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/excellent-support2.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Best Client Support</h3>
	 	     		<p class="wow fadeInUp">Academic Eye aims to provide best support to the clients, we are always a one call away from the client for the solution of their problems, Our software support teams will always resolve all client’s problems on call, or on remote desktop, if the problem resist they will note your query and provide you solution as soon as possible.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/pps.png" class="img-responsive ppu">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Pay as you Go</h3>
	 	     		<p class="wow fadeInUp">Academic Eye gives more than 18 modules for managing school requirement, but as per requirement may be school have less resource or less requirement, so we didn’t charge for the complete software at a time, school have various options to choose so they need to only pay as per their usage.

	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/professional.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Experienced Developers</h3>
	 	     		<p class="wow fadeInUp">We have team of experienced developers and designers which always consider project from Client point of view, which is the most important part of any development, our team is always designing and developing all the module of software while considering all technical glitches and its usability for the clients.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/frequnce.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Great Updation</h3>
	 	     		<p class="wow fadeInUp">We always consider that system must be up to date, so our team always ready to update the system as per client and external (Govt laws or reports) requirements and we always take feedbacks from clients so that we can try to update our system as per client needs and provide high usability to the clients.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/graphic.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> Efficient Communication</h3>
	 	     		<p class="wow fadeInUp">Academic Eye provides better communication of school and parents, school staff and parents can chat with each other in the app or web portal, which reduces burden of staff and parents to meet and discuss about child performance
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/report2..png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> SMS / Email / Notification</h3>
	 	     		<p class="wow fadeInUp">Academic Eye send alerts to the users with various integration, Alerts for Attendance, Fees dues, Homework, Task to students, Communication messages, Notice boards, Recruitment, Admission, Enquiry.  School can predefine message template, system will automatically send the message when it is required.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/virous-view.png" class="img-responsive Various">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Hardware Integration</h3>
	 	     		<p class="wow fadeInUp">Academic Eye supports various Hardware to manage school efficiently with technology they are GPS tracker, RFID for Attendance, Biometric Machines for Attendance, Barcode Scanner for Libraries.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-4 col-xs-12 why-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/why6.png" class="img-responsive " >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp">Reports </h3>
	 	     		<p class="wow fadeInUp">Academic Eye can generate 100+ Reports in the system, which helps the management to get best reporting system for all the areas of school from Admission to Accounting. Reports generated can be download easily in various formats.
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>
 	     	
		</div>
	</div>
</div>
 <!-- end -->
 <div class="clear"></div>

  <!-- tablate icon section start -->
<div class="container-fulid whybg-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12" style="margin-bottom: 40px;">
				<!-- <div class="image11 wow fadeInLeftBig"><img src="images/blue-iphone.png" alt="img loading" class="img-responsive"> </div>
				<div class="image22 wow fadeInUpBig"><img src="images/mac2.png" alt="img loading" class="img-responsive"></div>
				<div class="image33 wow fadeInRightBig"><img src="images/i-phoneblue.png" alt="img loading" class="img-responsive"></div> -->
				<div class="confiu-img"><img src="images/acadamicimg.png" alt="img loading"class="img-responsive" ></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

