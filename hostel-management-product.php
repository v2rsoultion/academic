<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Hostel Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>Hostel Management</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Hostel Management</h1>
 		       	<p>Hostel management offers complete management for the hostel facility to the students of school, this module can easy maintain a list of hostels, blocks, rooms, students registered for hostels, fees collection of hostellers, room reservations, allocation and other facilities.This module helps school in keeping records related to a hostel like fees register, due fees, advance fees, etc. This module involves efficiently managing various aspects for hostels and it requires minimal staff to perform this action without any expert training.</p><br>
 		       	<p>Hostel Management module is basic need of the school allowing the hostel facility for the students, this module completely manage the need of the school, keep track of complete records of students, their fees, hostel’s room. Also it reduces the manual processing to manage the multiple hostel of the school which increase the productivity of school efficiently. Schools can easily takes care of everything about their hostel with modules, This module easily produces various reports for the school director and principal.</p><br>
 		       	
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Hostel Management ensures school to easily the manage all the activities in their in their hostels, from student registration to student termination of facility.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/fees.png" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Simplified the Process</h1>
 	     				<p>Hostel module makes it really simple to register student, allocate room, transfer student etc.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/report-analicy.png" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Manage multiple hostels</h1>
 	     				<p>Hostel module easy manage multiple hostel of the school in the single portal.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/offlin-fess.png" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Improved Reporting</h1>
 	     				<p>Hostel module provides reporting about hostel room, fees and students register in a single click.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/report.png" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Reduce Burden</h1>
 	     				<p>Hostel Module eradicate manual paper based system, which helps staff to manage records easily.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Hostel Management</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/cheque.png" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Hostel and Blocks</a></h1>
					<p>This module allows school to manage multiple hostel, its multiple block and multiple floors of each block with details of hostel warden.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/cheque.png" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Categorization of Room</a></h1>
					<p>Hostel module provide facility to categorize the each room of floor in blocks of hostel as per the facility given in the room.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/cheque.png" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Dynamic Fees System</a> </h1>
					<p>Hostel Module provides facility to charge fees dynamically as per room, which means fees of all rooms can be different.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/cheque.png" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Registration of Student</a></h1>
					<p>In this module, school can easily register student for Hostel facility and also complete details of hostel room,floor and block can be recorded.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/cheque.png" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Transfer Student</a> </h1>
					<p>In this module, school can also transfer student from allocated room to another room easily and swapping of students can also be done easily.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Student Leaving Room</a> </h1>
					<p>Hostel module can easily manage the deallocation of room for the student.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Reports</a> </h1>
					<p>Hostel Module provides various reports to school - Registered Students List, Hostel Fees due report, Unoccupied Rooms, Occupied Rooms, Room Details, Transferred Student Details, Left Students Details.</p>
				</div> 				
 			</div>
 			
 			
 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

