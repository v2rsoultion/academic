<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>School Fees Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>    Product</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>About Product </h1>
 		       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><br>
 		       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 		       	 		       	<br>
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/tab.jpg" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/tab.jpg" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/fees.png" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Fees Reconciliation</h1>
 	     				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid dolorem</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product-two "> <a href="#" title="Reports & Analytics"> <img src="images/report-analicy.png" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Reports & Analytics</h1>
 	     				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid dolorem</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product-three"> <a href="#" title="Offline Fees Collection"> <img src="images/offlin-fess.png" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Offline Fees Collection</h1>
 	     				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid dolorem</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/report.png" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Fees Scheduling</h1>
 	     				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid dolorem</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>School Fees Management Simplified</h1>
 		         	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p>
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block">
				  <a href="#" class="imge" title="Account Report">	<img src="images/account-report.png" alt="Account Report"></a>
					<h1><a href="#" title="Account Report"> Account Report</a></h1>
					<p>Lorem ipsum dolor sit amt, consectet op adi
						pisicing elit, semeid do eiusmod porara incid
						idunt lorem opsum damy!</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/fees.png" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees"> Facilities Fees</a></h1>
					<p>Lorem ipsum dolor sit amt, consectet op adi
						pisicing elit, semeid do eiusmod porara incid
						idunt lorem opsum damy!</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/prepaid-account.png" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Prepaid Account</a> </h1>
					<p>Lorem ipsum dolor sit amt, consectet op adi
						pisicing elit, semeid do eiusmod porara incid
						idunt lorem opsum damy!</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/rtr.png" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">RTE Collection </a></h1>
					<p>Lorem ipsum dolor sit amt, consectet op adi
						pisicing elit, semeid do eiusmod porara incid
						idunt lorem opsum damy!</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/fess-account.png" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Fee Counter</a> </h1>
					<p>Lorem ipsum dolor sit amt, consectet op adi
						pisicing elit, semeid do eiusmod porara incid
						idunt lorem opsum damy!</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Cheque Details</a> </h1>
					<p>Lorem ipsum dolor sit amt, consectet op adi
						pisicing elit, semeid do eiusmod porara incid
						idunt lorem opsum damy!</p>
				</div> 				
 			</div>

 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

