<!-- header include -->
<?php include('header.php'); ?>
<!-- header close -->
<!-- Send Mail Start -->
<?php
error_reporting(0);

if (isset($_POST['contact_name']) && isset($_POST['contact_email']) && isset($_POST['contact_subject']) && isset($_POST['contact_message'])) {
  $msg = "Thank you for filling the form. We will review this and get back to you shortly.";
  $send_mail = 'info@academiceye.com';
          $to = $send_mail;
  
          $subject    = 'Contact';
          
          $sender     = 'Academic EYE';
          
          $user_name  = $_REQUEST["contact_name"];
          
          $user_email    = $_REQUEST["contact_email"];
  
  
          $user_subject  = $_REQUEST["contact_subject"];
  
          $user_message  = $_REQUEST["contact_message"];
  
          $message = '
          <html>
  
  <head>
      <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Academic Eye</title>
  </head>
  
  <body>
      <div style="width:700px; border:1px solid #ccc; padding:0px 0px; margin:20px auto;">
          <div style="padding:10px 15px; text-align:center;">
              <a href="http://www.academiceye.com/" target="_blank" title="academiceye">
                  <img src="http://www.academiceye.com/images/logo.png" alt="" width="15%" height="100px" alt="academiceye" /></a>
          </div>
          <div style="background:#2d3864; padding:10px; text-align:center;">
              <!-- <a style="color:#fff; text-decoration:none; font-family:arial;" href="http://www.academiceye.com/" title="academiceye.com" target="_blank">www.academiceye.com</a> -->
          </div>
          <div style="padding:20px; font-family:arial;">
            <div style="font-size:14px; color:#333; line-height:24px;">
                  <p> <b>Hello,</b> </p>
                  
              </div>
  
  
              <div>
                <b style="width: 15px; display: block; font-size: 18px; float: left; line-height:25px; padding:10px 0px;">Name:</b> <span style="font-size: 16px;padding:10px 0px 10px 70px; display: block; float: left;">'.$user_name.'</span>
                 <div style="clear: both;"></div>
              </div>
              <div>
                <b style="width: 15px; display: block; font-size: 18px; float: left; line-height:25px; padding:10px 0px;">Email:</b>
                 <span style="font-size: 16px; padding:10px 0px 10px 70px; display: block; float:left;">'.$user_email.'</span>
                 <div style="clear: both;"></div>
              </div>
              <div>
              <b style="width: 15px; display: block; font-size: 18px; float: left; line-height:25px; padding:10px 0px;">Subject:</b>
                <span style="font-size: 16px;display: block; padding:10px 0px 10px 70px; float: left;">'.$user_subject.'</span>
                 <div style="clear: both;"></div>
              </div>
              <div>
              <b style="width: 15px; display: block; font-size: 18px; float: left; line-height:25px; padding:10px 0px;">Message:</b>
                <span style="font-size: 16px;display: block; padding:10px 0px 10px 70px; float: left;">'.$user_message.'</span>
                 <div style="clear: both;"></div>
              </div>
  
          </div>
  
         <div style="background:#2d3864; padding:10px; text-align:center;">
              <a style="color:#fff; text-decoration:none; font-family:arial;" href="http://www.academiceye.com/" title="academiceye.com" target="_blank">www.academiceye.com</a>
          </div>
  
      </div>
  </body>
  
  </html> ';
      
              $headers  = 'MIME-Version: 1.0' . "\r\n";
              $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
              
              // Additional headers
              $headers .= 'To: '.SITE_TITLE.' <'.$to.'>' . "\r\n";
              $headers .= 'From: '.SITE_TITLE.' <'.$to.'>' . "\r\n";
      
          $mail = mail($to, $subject, $message, $headers);
}
else
{
  $msg = ' ';
}
  ?>

<!-- End -->
<!-- end -->
<!-- second section start -->
<div class="container-fulid inner-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
        <h1> Contact us</h1>
        <div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home"> Home / </a></span> Contact us</div>
      </div>
    </div>
  </div>
</div>
<!-- end -->
<!-- map section -->
<div class="container-fluid  map wow fadeInDown" data-wow-duration='.5s' data-wow-delay='.5s'  id="googleMap">
</div>
<!-- end map section -->
<!-- section admin portal start -->
<div class="container-fluid wow fadeInDown form-fluid " data-wow-duration='.5s'  data-wow-delay='.5s'>
  <div class="container ">
    <div class="row testimonail_row_bottom">
      <div class="contact_form wow fadeInDown" data-wow-duration='.5s' data-wow-delay='.6s'>
        <div class="contact_left hidden-xs">
          <div class="col-md-12 col-sm-12 col-xs-12 contact_left_block ">
            <ul class="wow fadeInDown" data-wow-duration='.5s' data-wow-delay='.8s'>
              <li>Contact info </li>
              <li>
                <span class="spaadderss spaadderss2">
                <i class="fa fa-map-marker" style="margin-right: 10px; "></i>  </span> 
                <div class="address-div">E-42, Opp. ICAI Institute,Kalpataru Shopping Center,Jodhpur (Raj.)</div>
              </li>
              <a href="mailto:info@wscubetech.com" title="info.Acedemiceye@gmail.com">
                <li><span class="spaadderss"> <i class="fa fa-envelope" style="margin-right: 10px; "></i>  </span> info.acedemiceye@gmail.com</li>
              </a>
              <a href="tel:+91 123 456 987  ">
                <li><span class="spaadderss"> <i class="fa fa-phone" style="margin-right: 10px; "></i>  </span> 8306017707  </li>
              </a>
            </ul>
            <div class="clear"></div>
            <div class="follow2 wow rotateInDownLeft" data-wow-duration='.5s' data-wow-delay='.7s'> follow us </div>
            <div class="icon_ALL class=" wow zoomInDown ">
              <div class="google google1 wow zoomInDown " style="color:#3f5aa2; ">
                <a href="https://www.facebook.com/academiceye/" title="facebook" target="_blank"> <i class="fa fa-facebook " ></i></a>  
              </div>
              <div class="google google2 wow zoomInDown " style="color:#50abf1; "> 
                <a href="https://twitter.com/ " title="Twitter " target="_blank "> <i class="fa fa-twitter "></i></a> 
              </div>
              <div class="google google5 wow zoomInDown " style="color:#dd5144; "> 
                <a href="https://plus.google.com/" title="Google Plus " target="_blank "><i class="fa fa-google-plus "></i></a>  
              </div>
              <div class="google google3 wow zoomInDown " style="color:#007ab9; "> 
                <a href="https://www.linkedin.com/" title="Linkedin " target="_blank " > <i class="fa fa-linkedin "></i> </a>
              </div>
              <!-- <div class="google google4 wow zoomInDown " style="color:#f61c0d; "> 
                <a href="https://www.youtube.com/c/wscubetechjodhpur " title="YouTube " target="_blank "> <i class="fa fa-youtube "></i></a>  
                </div> -->
              <div class="clear "></div>
            </div>
            <div class="clear "></div>
          </div>
          <div class="clear "></div>
        </div>
        <div class="contact_right">
          <div class="success-msg-color"><?php echo $msg; ?></div>
          <form method="post"  id="contact-us-form" action="">
            <div class="contant_head_contant wow fadeInDown " data-wow-duration='.5s'  data-wow-delay='.7s'>Get in touch with us</div>
            <div class="input_block ">
              <label id="response" style="display: none;"> </label>
              <div class="clear"></div>
            </div>
            <div class="input_block ">
              <div class="input_block_left">  
                <div class="form-group">
                  <input type="text" name="contact_name" placeholder="Your Name" class="input wow rotateInDownLeft" data-wow-duration='.6s'  data-wow-delay='.8s' >
                </div> 
                <div class="clear"></div>
              </div>
              <div class="input_block_left">
                <div class="form-group">
                  <input type="text" name="contact_email" placeholder="Your Email" class="input wow rotateInDownLeft" data-wow-duration='.6s'  data-wow-delay='.8s' > 
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
            <!-- Subject -->
            <div class="input_block">
            <div class="form-group">
                <input type="text" name="contact_subject" placeholder="Subject" class="input wow bounceInRight" data-wow-duration='.6s'  data-wow-delay='.8s' > 
              </div>
              <div class="clear"></div>
            </div>
            <!-- Subject -->
            <div class="input_block ">
              <div class="form-group ">
                <textarea class="address" name="contact_message" rows="5" placeholder="Message" class="wow bounceInRight" data-wow-duration='.6s'  data-wow-delay='.8s'></textarea>
              </div>
              <div class="clear"></div>
            </div>
            <div class="input_block">
              <div class="input_block_submit-block">
                <input type="submit"  value="Send Message" name="submit" class="input_block_submit-button" id=""> 
                <!-- input_block_submit-button -->
                <div class="clear "></div>
              </div>
              <div class="clear "></div>
            </div>
          </form>
        </div>
        <div class="clear "></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>
<!-- end -->
<!-- section why us start -->
<div class="container-fulid">
  <div class="container">
  </div>
</div>
<div class="clear"></div>
<!-- end -->
<!-- footer  section start -->
<?php include('footer.php'); ?>
<!-- <script src="js/bootstrapvalidator.min.js"></script>