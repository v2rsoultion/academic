<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Admission Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span> Admission Management </div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Admission Management  </h1>
 		       	<p>Academic Eye encourages school to use online admission facilities with Admission Module as it saves a lot time of parent and school staff which is consumed in manual paper processing of admission and have a lot more benefits for the school.</p><br>
 		       	<p>Admission Module is integrated with Online Payment method to get admission form fees online, school can also upload E-Brochure on this module for the parents. Enquiry can be also managed in this school, all the online enquiries form filled by parents will be displayed for communication. This Module makes admission process easy to the schools as it only ask some basic configuration for the admission and designed the online form for the schools, which can be shareable via URL on social platforms, It is completely integrated with Sms/Email and auto send Sms/Email to students, also it is integrated with Accounting Module when online fees payment is collected in admissions.</p>
 		       	 		       	<br>
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Admission modules is efficient and effective solution over manual admission and enquiry processing system.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/productimages/admission management/Online Admission Process.svg" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Online Admission Process </h1>
 	     				<p>Easy to create online form and use generated url on website to get admission forms.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/productimages/admission management/Efficient Enquiries Management.svg" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Efficient Enquiries Management</h1>
 	     				<p>Enquiries forms can be created and managed easily in the system to convert it into admission.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/productimages/admission management/Easy to Reach new parents.svg" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Easy to Reach new parents</h1>
 	     				<p>Admission, Enquiries and Brochure publish on site will help school to reach new parents.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/productimages/admission management/Supports OnlineOffline forms.svg" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Supports Online/Offline forms</h1>
 	     				<p>Schools can easily generate forms on the system and use it online as well as offline.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Admission Management</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/productimages/admission management/admission-enquiries.svg" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Admission and Enquiries</a></h1>
					<p>Admission module allows school to create both types of forms, i.e, Admission form and Enquiry form which helps to school to details of interested parents/students.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/productimages/admission management/dynamic-form.svg" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Dynamic form </a></h1>
					<p>School can easily create a form without having any technical knowledge all the basic to extended information fields of students is available to use in forms.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/productimages/admission management/brochure.svg" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Brochure </a> </h1>
					<p>Now a days E-brochure are helping schools to showcase their facilities, education quality and detail information, this module allows you to upload brochure for new parents.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/productimages/admission management/online-paymentI.svg" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Online Payment API</a></h1>
					<p>Academic Eye provides facility to takes form fees online before getting form filled and for this we provide various Online Payment API to collect fees online.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/productimages/admission management/setup-details- students.svg" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Setup details of new students</a> </h1>
					<p>Students have various choices in higher classes and primary classes to opt for particular subject, we can easily manage subject list of each students.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/admission management/smsemails-parents.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">SMS/Emails to Parents</a> </h1>
					<p>This module auto send sms/emails to parents on successful payment, completing form filling process, receipt of payment and for getting admission in school.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/admission management/print-forms-offline.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Print Forms for Offline Use</a> </h1>
					<p>If school don’t want to go for online form for some reason, they can easily design form and print form for offline use, so we are always ready for all the cases.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/admission management/social-media-existence.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Social Media Existence</a> </h1>
					<p>Schools can share the forms link directly thru app on social media platform, which increase their existence on the social media helps to get new admission.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/admission management/Easy to Reach new parents.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Easy to Reach Parents</a> </h1>
					<p>Online admission system allows schools to get form filled online and thru sharing their links to admission form, more parents will able to know about admission opening.</p>
				</div> 				
 			</div>

 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

