<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Transport Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>Transport Management</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Transport Management</h1>
 		       	<p>Transport Management is developed to manage the owned or contracted vehicles efficiently, this module takes cares of complete details related to transport facility of school such Vehicles details, driver and conductors details, Bus routes, Fees of routes.To increase security of transportation of school we have provide GPS tracker hardware integration with Attendance of student in Bus using RFID hardware integration which helps parents to track current location of the bus in the mobile application.</p><br>
 		       	<p>Transportation module is an efficient route planning which makes the day to day task for transport very simple as its main focus is on the safety of students. This module tracks all vehicle details along with driver details which improve safety and exact location can be easily tracked. This module also improves communication with parents as they will get alerts regarding delay of the bus through SMS. It offers the best solution to manage transportation route with basic information of vehicle along with student pickup and drop-off stations. School can easily maintain a database for students or staff who have opted for transportation along with charges for one way and two way are predefined.</p><br>
 		       	
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Transport Management provides reliable and secure way to manage vehicles with GPS and RFID technology.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/productimages/transport-management/lt.svg" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Location Tracking</h1>
 	     				<p>This module supports GPS Tracker integration which provide exact location of bus.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/productimages/transport-management/stl.svg" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Student Tracking</h1>
 	     				<p>This module supports RFID for attendance so that parents and bus in charge can track student attendance.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/productimages/transport-management/emr.svg" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Easy to manage Routes</h1>
 	     				<p>This module easily manage routes of the bus from picking location from Google Map.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/productimages/transport-management/ims.svg" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Improves Security</h1>
 	     				<p>Transport Module integrates various hardware for security and alerts system for any mishappening.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Transport Management</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/productimages/transport-management/vsd.svg" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Vehicle & Staff Details</a></h1>
					<p>This module keep track of vehicle details , and bus, conductor details with daily attendance for payroll management.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/productimages/transport-management/pdl.svg" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Pick up - Drop location</a></h1>
					<p>Transport module allows school to manage routes with Pick up/Drop with Estimate time of Arrival at stop.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/productimages/transport-management/mostu.svg" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Mapping of Student</a> </h1>
					<p>In this module, school can easily register student for transport facility and also map them to bus with pick up and drop location.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/productimages/transport-management/mosataffa.svg" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Mapping of Staff</a></h1>
					<p>In this module, school can also map staff to bus with pick up and drop location and manage the deduction in payroll if required.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/productimages/transport-management/stuata.svg" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Student’s Attendance</a> </h1>
					<p>RFID hardware can be easily integrate with this module, which can easily track student’s attendance and give notification to parents mobile apps.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/transport-management/feesys.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Fees System</a> </h1>
					<p>Schools can easily designed Kilometer range based fees system, will be auto calculated for particular Pick up, Drop locations.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/transport-management/histra.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">History Tracking</a> </h1>
					<p>This module also allows to check previous routes tracking of the bus from the start point to stop point with accurate timing reports.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/transport-management/atopar.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Alerts to Parent</a> </h1>
					<p>Transport Module sends alerts to parents about delay of the bus, bus arrival at their pick up, drop stops, student attendance alert.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/transport-management/atoscho.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Alerts to School</a> </h1>
					<p>Transport module send alerts to school about when driver/conductor wants to alert the school about any mishappening.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/transport-management/reports.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Reports </a> </h1>
					<p>This Module generate various reports students attendance report, staff attendance report, stop wise reports, fees register of students.</p>
				</div> 				
 			</div>
 			
 			
 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

