<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1> Examination and Report Card</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>  Examination and Report Card</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Examination and Report Card</h1>
 		       	<p>Managing examination and generating report card is one of the primary task of school and most difficult task when it comes to paper based manual system, it is challenging for teachers to create manual tabsheet and report card entries and it also may need to verify and validate so many time before printing the report cards, so to reduce this entire problem we have a dedicated module.</p><br>
 		       	<p>Examination and Report Card Module simplifies the most complicated work of school, it is created to manage all the exams from class test to annual examination in easy way to reduce less the burden of teachers who need to manually manage these things. This Module store entire session exam-marks of students and helps school to view marks entry thru various reports. Exam Schedule is created in this module which notify parents/student and staff. Report Card Generation is progressive step by step process it validates and verify all the inputs required to generate final marksheet and any problem occur will be displayed to staff. Academic Eye provides multiple templates of report card and school can also get template of report card as per their needs.</p>
 		       	 		       	<br>
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Examination and Report Card Module helps school to manage the examination and result process easier and faster than manual system.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/productimages/examination and report card/Effective Exam Management.svg" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Effective Exam Management</h1>
 	     				<p>No more manual processing and calculating for examination and report cards.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/productimages/examination and report card/Ease to Marks Entry.svg" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Ease to Marks Entry</h1>
 	     				<p>Staff can easily enter marks of student subject & exam-wise, also anytime can edit it.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/productimages/examination and report card/Customise Report Cards.svg" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Customise Report Cards</h1>
 	     				<p>Reports card are customized by our team, so school doesn’t need to worry about design and printing</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/productimages/examination and report card/008-calculator.svg" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Accurate Calculation</h1>
 	     				<p>System will auto calculate exam marks and percentage with checking of missing entries.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About  Examination and Report Card</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/productimages/examination and report card/Terms and Exams.svg" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Terms and Exams </a></h1>
					<p>Easy to manage Terms and its exams for each class, after that mapping subject of classes for the exams, this flow is too easy to understand by staff.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/productimages/examination and report card/Grade Scheme.svg" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Grade Scheme</a></h1>
					<p>Grade Schemes for Academic and Co-Scholastic subject can be created easily with remark for the grade, it will auto calculate and display according to grade definition.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/productimages/examination and report card/Marks Criteria.svg" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Marks Criteria</a> </h1>
					<p>Marks Criteria for Academic Subject is created for calculating pass and fail as per criteria definition and obtained marks of the subject.</p>
				</div> 				
 			</div>
 			
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/productimages/examination and report card/Easy to Generate Report Card.svg" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Easy to Generate Report Card</a></h1>
					<p>Generating Report Card is 3 Step process in our system firstly it checks any Marks Entries is missing, Calculate Pass-Fail & Percentage and Rank Generation.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/productimages/examination and report card/Auto-Check Missing Entries.svg" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Auto-Check Missing Entries</a> </h1>
					<p>Academic Eye checks missing entries of student and ask teacher to either give it zero or mark as absent, such features makes the academic eye more productive and effective.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/examination and report card/Reduce Burden.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Reduce Burden</a> </h1>
					<p>Staff can easily upload Excel Sheet of marks of students exam-wise, which helps to reduce work overhead of staff entering marks and checking it again and again.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/examination and report card/Exam Schedule.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Exam Schedule</a> </h1>
					<p>This module provides school to create class-section wise exam-schedule for all exams, and school can mention room no and roll no range in exam schedule.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/examination and report card/Results thru SMSEmail.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Results thru SMS/Email</a> </h1>
					<p>Results will be sent to parent thru sms/email, subject-wise marks and final result is sent to sms, email.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/examination and report card/Accurate Reports.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Accurate Reports</a> </h1>
					<p>School can get error free and auto calculated reports for all exams i.e, Tabsheet, Student Pass & Fail reports, Student Rank Reports, Student Toppers list.</p>
				</div> 				
 			</div>

 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

