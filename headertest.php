<!DOCTYPE>
<html>
<head>
	<title>Academic Eye</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- <link href="images/fav.svg" rel="shortcut icon" type="image/svg" /> -->
	<link rel="shortcut icon" type="image/svg" href="images/favicon.png"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Poppins:300,400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
     <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" href="css/style.unav.css">


</head>
<body>
<!-- header part start -->
 <div class="container-fluid" id="myHeader" class="myHeader header_fixed">
	<div class="container padding_zero header " >
		<div class="row">
			<div class="col-md-5 col-sm-3 col-xs-6">
				<div class="logo-back  logo-scroll">
					<a href="index.php" title="Acadmeic Eye"> <img src="images/logo.png" class="logo logo_scroll" id="img" class=""></a>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-4  hidden-lg hidden-md  hidden-sm  book-a-demo" style="padding: 0px;"><a role="button" onclick="showdata()"> book a demo </a> 
			<div class="clear"></div> </div>
			<div class="col-md-7 col-sm-9 hidden-xs ">
				<nav class="navbar ">
				    <ul class="nav navbar-nav hidden-xs navbar-right equal_to">
				      <li><a href="index.php" title="Home">Home</a></li>
				      <li class="product-menu dropdown dropdown-large"><a href="product.php" title="Product">Product</a>
				      	<ul class="dropdown-menu dropdown-menu-large row ul_width  ul_width3 wow fadeIn">
				      		<li class="col-md-4">
                                 <ul>
                                    <li class="firstlist">
                                       <ul class="ul_one">
                                          <li><a href="academic-management-product.php">Academics Management </a> </li>
				      		<li><a href="admission-and-enquiries-product.php"> Admission Management </a> </li>
				      		<li><a href="examination-and-report-card-product.php"> Examination And Report Card </a> </li>
				      		<li><a href="fees-management-product.php"> Fees Management </a> </li>
                                       </ul>
                                    </li>  
                                 </ul>
                              </li> 
                              <li class="col-md-4">
                                 <ul>
                                    <li class="firstlist">
                                       <ul class="ul_one">
                                          <li><a href="student-and-staff-information-system-product.php">Student and Staff Information System </a> </li>
				      		<li><a href="recruitment-and-substitute-management-product.php">Recruitment and Substitute Management</a> </li>
				      		<li><a href="staff-payroll-management-product.php">Staff Payroll Management</a> </li>
				      		<li><a href="inventory-management-product.php">Inventory Management</a> </li>
                                       </ul>
                                    </li>  
                                 </ul>
                              </li> 
                              <li class="col-md-4">
                                 <ul>
                                    <li class="firstlist">
                                       <ul class="ul_one">
                                          <li><a href="accounting-and-finance-product.php">Accounting and Finance</a> </li>
				      		<li><a href="transport-management-product.php">Transport Management</a> </li>
				      		<li><a href="hostel-management-product.php">Hostel Management</a> </li>
				      		<li><a href="library-management-product.php">Library Management</a> </li>
                                       </ul>
                                    </li>  
                                 </ul>
                              </li> 
				      	</ul>
				      </li>
				      <li><a href="company.php" title="Company">company</a></li>
				      <li><a href="contact-us.php" title="Contact Us">contact us</a></li>
				      <li><a role="button" title="Book a Demo" > <span  class="hidden-xs myBtn" onclick="showdata()"> Book a Demo </span></a></li>
				    </ul>
				</nav>
			</div>
			<!-- mobile menu code start -->

			<div class="col-md-3 col-sm-3  col-xs-3  hidden-lg hidden-md hidden-sm mega-button ">
				<div id="navigation">
					<button>
						<span>Toggle menu</span>
					</button>
					<nav>
						<a class="menu-heading" href="index.php"><img src="images/logo3.svg"  style="width: 40%;"> </a>
						<a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> &nbsp;&nbsp;  Home</a>
						<div><a href="product.php"><i class="fa fa-product-hunt" aria-hidden="true"></i> &nbsp;&nbsp; Product</a>
							<!-- <a href="#">Link 3</a> -->
							<div>
								<a href="academic-management-product.php">Academics Management </a>
								<a href="admission-and-enquiries-product.php"> Admission Management </a>
								<a href="examination-and-report-card-product.php"> Examination And Report Card </a>
								<a href="fees-management-product.php"> Fees Management </a>
								<a href="student-and-staff-information-system-product.php">Student and Staff Information System </a>
								<a href="recruitment-and-substitute-management-product.php">Recruitment and Substitute Management</a>
								<a href="staff-payroll-management-product.php">Staff Payroll Management</a>
								<a href="inventory-management-product.php">Inventory Management</a>
								<a href="accounting-and-finance-product.php">Accounting and Finance</a>
								<a href="transport-management-product.php">Transport Management</a>
								<a href="hostel-management-product.php">Hostel Management</a>
								<a href="library-management-product.php">Library Management</a>
							</div>
						</div>
						<a href="company.php"><i class="fa fa-building"></i> &nbsp;&nbsp; company</a>
						<a href="contact-us.php"><img src="images/php.png">  &nbsp;&nbsp;contact us</a>
						<a href="why-us.php"><img src="images/contact.png"> &nbsp;&nbsp;Why Us</a>
						<a href="became-our-partner.php"><img src="images/bap.png"> &nbsp;&nbsp;Became our Partner</a>
					</nav>
				</div>
			</div>
		<!-- mobile menu code close -->
		</div>
	</div>
</div>	
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
  	<div class="model-header">
    <span class="close">&times;</span>
       <p>Book a Demo</p>
       <div class="new-school">( for new school only )</div>
       <div class="new-school-contant">parents , please contact your school for username / Password</div>
       <br>
     </div>
   <div class="form-book">
   	    <p>complete the form below to see a demo of acadmeicapp want to speak with us right now? Call<span> +91 898989898 </span></p>
   	    <div id="response" style="display: none;"></div>
		<form id="model-user">
			 <div class="form-group">
			    <input type="text" class="form-control" placeholder="Name" name="name">
			  </div>
			  <div class="form-group">
			    <input type="email" class="form-control" placeholder="Email Address" name="email">
			  </div>
			  <div class="form-group">
			    <input type="text" class="form-control" placeholder="School Name" name="pwd">
			  </div>
			  <div class="form-group">
			    <input type="number" class="form-control" placeholder="Phone Number" name="PhoneNumber">
			  </div>
			  <div class="checkbox">
		     	  	<textarea class="form-control form-control2" placeholder="Message"></textarea>
			  </div>
		  <button type="submit" class="Submit-btn">Submit</button>
		</form>
   </div>
  </div>

</div>

		
<!-- end -->


               
