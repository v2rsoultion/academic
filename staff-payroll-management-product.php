<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Staff Payroll Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>Staff Payroll Management</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Staff Payroll Management</h1>
 		       	<p>Staff payroll is the process of generating salaries of employees. The entire process of payroll includes calculation of salaries, deductions, additions of any kind of benefit and final payment of salaries done either by cash/or cheque. Most of the small school use outsourcing for calculation of salaries as some government policies like PF, ESIC such reports are complicated to calculate as it require proper knowledge, so to reduce this burden from school, Payroll Module is included in Academic Eye.</p><br>
 		       	<p>This module helps to reduce burden of schools as it eradicate paperwork to maintain records and also requires less time for generation of payment and reports. Payroll module will store the data and will help to generate salary along with perks accurately. Payroll maintains records for leave status (casual leave, half day leave, full day leave, etc) for employees and department wise. This module helps you to take care of monthly pay and allowances by calculating the number of days present in the office as well as authorized leave is taken by the staff.</p><br>
 		       	
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Staff Payroll modules helps school to configure and generate the staff salary as per their basic pay, allowance and deductions.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/fees.png" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Auto Calculation of Payroll</h1>
 	     				<p>Payroll system calculates salary automatically with no of attendance and no of loss of pay leaves.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/report-analicy.png" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Fast and efficient system</h1>
 	     				<p>Payroll system takes less time to generate salary with proper calculation in comparison of manual system.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/offlin-fess.png" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Government Policies Integrated</h1>
 	     				<p>Payroll module consider Government Policies i.e, PF, ESIC calculation reports .</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/report.png" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Reduce Burden and Error</h1>
 	     				<p>Easy to generate payroll with proper calculation of allowance and deduction, with multiple reports and register.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Staff Payroll Management</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/cheque.png" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Staff’s configuration & details </a></h1>
					<p>Staff configuration such as Department, Grades, Designation, Salary Heads, Bank details are more will be configured easily in the system, and salary will be generated on the basis of this configuration.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/cheque.png" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Creating Salary Structure</a></h1>
					<p>Academic Eye allows school to create multiple Salary Structure for each department, grades which consist of allowance and deduction of Basic Pay to employee.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/cheque.png" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Calculation of Basic-Pay </a> </h1>
					<p>Payroll module takes the staff attendance, which helps to calculate basic salary on the basis of their attendance marked and which avoid any duplicity of any benefit or deduction done for staff.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/cheque.png" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Creating Leave Structure</a></h1>
					<p>Payroll module allows schools to define leave structure for staff like casual leaves, half day leaves, etc and also define a total number of leaves allotted for a particular session like 10 casual leaves for 1 academic year.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/cheque.png" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Loans and Advance</a> </h1>
					<p>Payroll module allows school to provide loans and advance to employee and every-time before salary generation it will to ask to deduct loan emi & advance amount.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Overtime and Arrears</a> </h1>
					<p>Payroll module auto calculate overtime from substitute management, HR can also feed details of overtime in this module and Arrears can also be given to employee, if required.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Reports and Register</a> </h1>
					<p>This module generates various report and payroll register i.e, Employee Register, Monthly Salary Register, Arrear Register, Advance and Loan Register, PF (Monthly PF, Register, Form 3A, 6A, 12A 5, 10), ESI Register (Month wise summary, Form 5), Monthly TDS register. </p>
				</div> 				
 			</div>
 			
 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

