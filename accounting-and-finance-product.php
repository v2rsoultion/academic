<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Accounting and Finance</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>Accounting and Finance</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Accounting and Finance</h1>
 		       	<p>Accounting module is the most difficult and important task to perform for schools as it shows final position of school in the end of accounting year. This module provides an efficient and effective solution as it helps in managing the various day to day expenses, also it stores and manages all data regarding the bank transactions as well as student fee collection or any expenses.</p><br>
 		       	<p>Academic Eye provides reliable and powerful accounting module which provides best solution that school doesn’t need to use any other accounting software, This module can track all transaction and it reflects in books of accounts automatically.All the reports like Ledger, Bank Book, Cash Book, Trial Balance, Income & Expenditure Account and Balance Sheet are available. Accounting module keeps track of various expenses made by the school. It also has the facility to generate and print vouchers, which are an essential document for accounts.</p><br>
 		       	
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Accounting and Finance is reliable and user-friendly software for school’s staff who usually works on Tally Accounting Software.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/productimages/accounting- finance/easy-accounting.svg" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Easy to Accounting</h1>
 	     				<p>This module is easy to use as it has user friendly interface and provides all the functionalities required for accounting.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/productimages/accounting- finance/easyunderstandflow.svg" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Easy to Understand Flow</h1>
 	     				<p>The flow of accounts module is easy to understand for the person having knowledge of accounts as it is based on Tally.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/productimages/accounting- finance/mrecords.svg" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Maintain Records</h1>
 	     				<p>This module keep tracks of all accounting transaction in details.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/productimages/accounting- finance/a-r.svg" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Accounting Reports</h1>
 	     				<p>This module generates various accounting books and final a/c reports very easily.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Accounting and Finance</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/productimages/accounting- finance/agh.svg" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Accounts Group and Heads</a></h1>
					<p>Accounting module allows to add group name (Ledgers group a/c)  and head name (Ledgers a/c) under sub heads and main heads (Assets,Liability,Income and Expenses) these are pre-define in the system.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/productimages/accounting- finance/acv.svg" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Accounting vouchers</a></h1>
					<p>Accounting management allows to feed all the voucher entries in the system - Journal Entries, Payment and Receipt Entries, Contra entries.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/productimages/accounting- finance/ceam.svg" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Complete Asset Management</a> </h1>
					<p>Accounting module allows to manage complete assets of school whether it is on loan or fully owned by the school, with depreciation entries.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/productimages/accounting- finance/Iwsm.svg" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Integrated with Stock Module</a></h1>
					<p>This module is integrated with Stock and Inventory module - all the sale and purchase transaction, tax paid & received, discount paid & received  and available stock directly reflect the respective ledgers.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/productimages/accounting- finance/iwfm.svg" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Integrated with Fees Module</a> </h1>
					<p>This module is integrated with Fees collection module - all the paid transaction, discount/concession entries, imprest a/c, wallet a/c, RTE transaction, due fees etc directly reflect the respective ledgers.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/accounting- finance/Iwpm.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Integrated with Payroll Module</a> </h1>
					<p>This module is integrated with Staff Payroll module - all the salary paid, loan deducted, advance deducted, payable taxes etc directly reflect the respective ledgers.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/accounting- finance/reports.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Reports</a> </h1>
					<p>This module gives complete reporting about day to day expenses and incomes, school can check daily, monthly and date ranges about their expenses and incomes and Ledgers Report and Trial Balance Report.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/accounting- finance/fbr.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Final Books Reports</a> </h1>
					<p>Accounting Module provide Final Books of accounts - Balance sheet, Profit & Loss A/C, Income/ expenditure statement.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/accounting- finance/books-reports.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Books of A/c Reports</a> </h1>
					<p>Accounting Module provide Books of accounts - Cash/bank book, Day book, Journal, Sale register, Purchase register.</p>
				</div> 				
 			</div>
 			
 			
 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

