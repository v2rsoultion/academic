<div class="container-fulid footer wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.20s"">
  <div class="container tab-res">
    <div class="row">
      <div class="col-md-5 col-xs-12 col-sm-5  hidden-xs footer-img  " >
        <div class="col-md-6 col-xs-12 col-sm-6  footer-img  padding_left" >
          <a href="index.php" title="Acadmeic Eye"> <img src="images/Untitled-1.png"></a>
        </div>
        <div class="col-md-6  col-sm-6  footer-heading footer-heading-other   hidden-xs padding-all-remove ">
            <h1>USEFUL LINKS</h1>
          <ul type="none">
            <li><a href="why-us.php" title="Why Us">
                  <i class="fa fa-angle-right"></i> 
                  <p>Why Us</p>   
                </a> </li>
            <li><a href="became-a-partner.php" title="Became A Partner">
                  <i class="fa fa-angle-right"></i> 
                  <p>Became A Partner </p>
                </a>  
            </li>
            <li><a href="terms-condition.php" title="T&C"> 
                  <i class="fa fa-angle-right"></i> 
                  <p>T&C </p>
                </a>
            </li>
            <li><a href="privacy-policy.php" title="Privacy Policy">
                  <i class="fa fa-angle-right"></i> 
                  <p>Privacy Policy </p>
                </a> 
            </li>
          </ul>
         </div>
      </div>
      <div class="col-md-7 col-xs-12 col-sm-7 footer-img padding-all-remove " >
        <div class="col-md-6  col-sm-6 footer-heading footer-heading-other  hidden-xs padding-all-remove">
            <h1>FEATURES</h1>
          <ul type="none">
            
            <li><a href="examination-and-report-card-product.php" title="Examination And Report Card">
                  <i class="fa fa-angle-right"></i>  
                  <p>Examination And Report Card</p>   
                </a>  
            </li>
            <li><a href="fees-management-product.php" title="Fees Management">
                  <i class="fa fa-angle-right"></i>  
                  <p>Fees Management</p>  
                </a>  
            </li>
            <li><a href="accounting-and-finance-product.php" title="Accounting and Finance">
                  <i class="fa fa-angle-right"></i>  
                  <p>Accounting and Finance</p> 
                </a> 
            </li>
            <li><a href="transport-management-product.php" title="Transport Management">
                  <i class="fa fa-angle-right"></i> 
                  <p>Transport Management</p>  
                </a> 
            </li>
          </ul>
         </div>
          <div class="col-md-6 col-xs-12 col-sm-5 footer-heading footer-heading-last  footer-heading-other  tab-res">
            <h1>GET IN TOUCH</h1>
          <ul type="none" class="getintouch">
            <li>
              <i class="fa fa-map-marker" aria-hidden="true" style="font-size: 20px;" ></i> 
              <p>E-42, Opp. ICAI Institute,Kalpataru Shopping Center,Jodhpur (Raj.)</p></a> 
              <div class="clear"></div>
            </li>

            <li><a href="tel:+91 123 456 987" >
                  <i class="fa fa-phone" style="font-size: 20px;"></i>
                  <p>8306017707</p>  
               </a>
               <div class="clear"></div>
            </li>

            <li><a href="mailto:info.academic@gmail.com" title="info.academiceye@gmail.com" >
                <i class="fa fa-envelope" style="font-size: 15px; padding-top: 2px;"></i>
                <p> info.academiceye@gmail.com </p> </a>
                <div class="clear"></div> 
            </li>
          </ul>
         </div>
         <div class="clear"></div>
      </div>
    </div>
  </div>
</div>

<div class="container-fulid last-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-xs-12 col-sm-6 All-Rights"> <span class="res-xs"> <a href=""> Privacy Policy</a> |  <a href=""> Terms Of Use </a></span> &copy; 2018 academic Eye. All Rights Reserved </div>
      <div class="col-md-6 col-xs-12 col-sm-6 padding_right">
        <div class="all-icon-block">
            <div class="icon-block"><a href="https://www.facebook.com/academiceye/" target="__blank" title="Facebook"> <i class="fa fa-facebook-f"></i></a> </div>
            <div class="icon-block"><a href="https://twitter.com/" target="__blank" title="Twitter"> <i class="fa fa-twitter"></i></a>  </div>
            <div class="icon-block"><a href="https://plus.google.com/" target="__blank" title="Google-plus"> <i class="fa fa-google-plus"></i></a>  </div>
            <div class="icon-block"><a href="https://www.linkedin.com/" target="__blank" title="Linkedin"> <i class="fa fa-linkedin"></i></a>  </div>
            <div class="clear"></div>
           </div> 
       </div>
    </div>
  </div>
</div>
<!-- end -->

<!-- jquery -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery-v3.2.1.min.js"></script>
  <script src="js/jquery.validate.js"></script>
  <script src="js/additional-methods.js"></script>
 <!--  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
<!-- Minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="js/bootstrapvalidator.min.js"></script>
 <script type="text/javascript" src="js/owl.carousel.js"></script>
  <script type="text/javascript" src="js/jquery.unav.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
<!-- mobile code start responsive -->

  <script>
    $(function(){
      $('#navigation').uNav();
    });
  </script>
 <!-- close mobile -->
<!-- fixed header -->
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction()
 {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");

  } else {
    header.classList.remove("sticky");
  }
}
</script>

<script type="text/javascript"> 
  $(document).ready(function() 
     { $(window).bind('scroll',  function() 
        { if ($(window).scrollTop() >84) { 
                $(".logo-back").css("width","80px");
                $(".logo-back").css("height","70px");
                $(".logo-back").css("top","11px");
                 $(".logo-back").css("transition","1s");
                $(".logo-back>a>img").css("width","90%");
                
              
             
         } else { 
                  $(".logo-back").css("width","110px");
                $(".logo-back").css("height","110px");
                $(".logo-back").css("top","15px");
                $(".logo-back>a>img").css("width","90%");
                
        }
     }); 
    });
</script>


<!-- fixed header code close -->
<!-- wow js -->
<script src="js/wow.js"></script>
  <script>
     wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
    // document.getElementById('moar').onclick = function() {
    //   var section = document.createElement('section');
    //   section.className = 'section--purple wow fadeInDown';
    //   this.parentNode.insertBefore(section, this);
    // };
   
   
  </script>
  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                
                autoplay: false,
                autoplayTimeout: 2000,
                autoplayHoverPause: true
                
              });
        })
   </script>
         <script>
            var map;
              function initMap() {
            map = new google.maps.Map(document.getElementById('googleMap'), {
              center: {lat: 26.2725474, lng: 73.0021332},
              zoom: 18

        });

              var map = new google.maps.Map(document.getElementById('googleMap'),
               {
                 zoom: 18,
                  center: {lat: 26.27359, lng: 73.0283063},
                 });
                var marker = new google.maps.Marker({
                    position: map.getCenter(),
                    map: map,
            });
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhebMfMgeOpa2NzuBKRtk8dKqgd2pJIfI&callback=initMap"
    async defer></script> 

    <!-- book a demo start -->
  <!-- <script type="text/javascript">
  $(document).ready(function() {
    $('#model-user').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
    submitButton: '$model-user input[type="submit"]',
    submitHandler: function(validator, form, submitButton) {
       $.ajax({
               url: 'http://localhost/academic/form.php?action=contact',
               type: "POST",
               data: $(form).serialize(),
               success: function(data)
               {
              
                if (data == 'fail')
                {
                   
                    $("#response").css('display','block');
                    $("#response").css('text-align','center');
                    $("#response").css('color','red');
                    $("#response").css('padding','5px');
                    $("#response").html('Please check the the captcha form');
                    
                }
                if (data == 'success')
                {
                    $("#response").css('display','block');
                    $("#response").css('text-align','center');
                    $("#response").css('color','green');
                    $("#response").css('padding','5px');
                    $("#response").html('Your Mail send successfully');
                   $('input[type="text"], textarea').val('');
                }
              }
                                  
           });
              return false;
        },
        feedbackIcons: {
            
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        
                        notEmpty: {
                        message: 'Please enter your name'
                    }
                }
            },
             
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email'
                    },
                    emailAddress: {
                        message: 'Please enter your email'
                    }
                }
            },
           
             pwd: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please enter school name'
                    }
                 }
                },
            
            
                PhoneNumber: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please enter your phone number'
                    },
                    stringLength: {
                        max: 10,
                        min: 10,
                        message: 'Please enter valid phone number'
                    }
                }
            },
           
            }
        })
});
</script> -->
<!-- end -->
<!-- popup model code -->

<!-- Became A Partner form code  start-->
 <!-- <script type="text/javascript">
  $(document).ready(function() {
    $('#became-our').bootstrapValidator({
        feedbackIcons: {
            
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            uname: {
                validators: {
                        notEmpty: {
                        message: 'Please enter your name'
                    }
                }
            },
            ucompany: {
                validators: {
                        notEmpty: {
                        message: 'Please enter your company name'
                    }
                }
            },
            uemail: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email'
                    }
                }
            },
            uphone: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please enter your phone number'
                    }
                }
            },
           }
        })
});
</script> -->


<!-- Became A Partner form code  end -->

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

$(function(){
$('.numeric').on('input', function (event) { 
this.value = this.value.replace(/[^0-9]/g, '');
});
});

// When the user clicks the button, open the modal 


function showdata(){
  $("#myModal").fadeIn();
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<script type="text/javascript">
  jQuery(document).ready(function () {
    $("#contact-us-form").validate({

    /* @validation states + elements 
    ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

        rules: {
          contact_name: {
          required: true,
        },
        contact_email: {
          required: true,
          email: true,
        },
        contact_subject: {
          required: true,
        },
        contact_message: {
        required: true,
        },
  },

    /* @validation highlighting + error placement 
    ---------------------------------------------------- */
    highlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
      $(element).closest('.field').addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
      $(element).closest('.field').removeClass(errorClass).addClass(validClass);
    },

    errorPlacement: function (error, element) {
      if (element.is(":radio") || element.is(":checkbox")) {
        element.closest('.option-group').after(error);
      } else {
        element.closest('.form-group').after(error);
        // error.insertAfter(element.parent());
      }
    }
  });

});

  jQuery(document).ready(function () {
    $("#became-our").validate({

    /* @validation states + elements 
    ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

        rules: {
          uname: {
          required: true,
        },
        ucompany: {
          required: true,
        },
        uemail: {
          required: true,
          email: true
        },
        uphone: {
        required: true,
        phoneUS: true
        },
  },

    /* @validation highlighting + error placement 
    ---------------------------------------------------- */
    highlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
      $(element).closest('.field').addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
      $(element).closest('.field').removeClass(errorClass).addClass(validClass);
    },

    errorPlacement: function (error, element) {
      if (element.is(":radio") || element.is(":checkbox")) {
        element.closest('.option-group').after(error);
      } else {
        element.closest('.form-group').after(error);
        // error.insertAfter(element.parent());
      }
    }
  });

});

  jQuery(document).ready(function () {
    $("#model-user").validate({

    /* @validation states + elements 
    ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        name: {
          required: true,
        },
        email: {
          required: true,
          email: true
        },
        sname: {
          required: true,
        },
        phonenumber: {
          required: true,
          phoneUS: true
        },
        message: {
          required: true,
        }
  },

    /* @validation highlighting + error placement 
    ---------------------------------------------------- */
    highlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
      $(element).closest('.field').addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
      $(element).closest('.field').removeClass(errorClass).addClass(validClass);
    },

    errorPlacement: function (error, element) {
      if (element.is(":radio") || element.is(":checkbox")) {
        element.closest('.option-group').after(error);
      } else {
        element.closest('.form-group').after(error);
        // error.insertAfter(element.parent());
      }
    }
  });

});
</script>

<!-- contact us from start -->
<!-- <script type="text/javascript">
  $(document).ready(function() {
    $('#contact-us-form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later  
        submitButton: '$model-user input[type="submit"]',
    submitHandler: function(validator, form, submitButton) { 
        },
        feedbackIcons: {
            
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            contact_name: {
                validators: {
                        
                        notEmpty: {
                        message: 'Please enter your name'
                    }
                }
            },
             
            contact_email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email'
                    },
                    emailAddress: {
                        message: 'Please enter your email'
                    }
                }
            },
           
             contact_subject: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please enter subject'
                    }
                 }
                },
            
            
                contact_message: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please enter your message'
                    }
                }
            },
           
            }
        })
});


</script> -->
<!-- end -->

<!-- contact from   start -->
<!-- <script type="text/javascript">
  $(document).ready(function() {
    $('#message-user').bootstrapValidator({
       
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
             conname: {
                validators: {
                        
                        notEmpty: {
                        message: 'Please enter your name'
                    }
                }
            },
            conemail: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email'
                    },
                    emailAddress: {
                        message: 'Please enter your email'
                    }
                }
            },
            consubject: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please enter subject'
                    }
                 }
                },
            conmessage: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please enter your message'
                    }
                }
            },

        }
    });
});
</script> -->
<!-- contact from end -->
</body>
</html>