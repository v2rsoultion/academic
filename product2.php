<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>School Fees Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>    Product</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Academics Management </h1>
 		       	<p>Academic Eye provides extended solution to the school for managing complete details of Classes, Section, Subject and their Mapping, as we know basic setup of school is important because it describes the school into the system anything missing in this makes the software uneffective and also will not generate the proper reports required for the school.</p><br>
 		       	<p>Academics Module is all about managing Classes, its Section and Subjects, Class teacher, Virtual classes, Competition & its certificates, TimeTable Management. This module setup details for session and easily imported in new session. We designed a simple solution for the school having multiple Board of Education, Mediums, Streams for the classes, school can manage such things in a single portal. Module can easily manage the subjects opted as per choice of student in the higher classes. Certificates for school competition can be also be easily managed in this module</p>
 		       	 		       	<br>
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Academics Modules provides various features to the schools who aims to manage their entire schools definition in one place.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/tab.jpg" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/tab.jpg" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/fees.png" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Complete Solution </h1>
 	     				<p>Manage Multiple Board of Education, Mediums and Streams for higher classes.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Reports & Analytics"> <img src="images/report-analicy.png" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>TimeTable Management</h1>
 	     				<p>Fast Time Table creation module for all class section with multiple period at same time slot.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Offline Fees Collection"> <img src="images/offlin-fess.png" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Student’s Subjects</h1>
 	     				<p>Student’s optional subject can be easily mapped in the system with proper validation in exams and marksheet.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/report.png" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Certificates Generation</h1>
 	     				<p>Schools can easily generate certificates of TC, CC competition i.e, Certificate of Participation and Winners.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>School Fees Management Simplified</h1>
 		         	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p>
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block">
				  <a href="#" class="imge" title="Account Report">	<img src="images/cheque.png" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Class-Section Management </a></h1>
					<p>Creating class and section with multiple option is too easy in system. School can set various configuration of section i.e, No of Intake, Shift of Section, Stream of Section.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/cheque.png" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Class Teacher Allocation</a></h1>
					<p>Class Teacher to each section can be allocated in this module and all the privilege related to that section will be given to that teacher.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/cheque.png" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Virtual Class</a> </h1>
					<p>Design for a special classes for Dancing, Music, Acting and more in which students from all the classes are included, For the purpose of tracking of students taking classes.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/cheque.png" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Academic Subject  </a></h1>
					<p>Subjects can be managed easily as per requirement of the class, easy to setup a subject as academic subject for a class and calculate exam report including this subject</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/cheque.png" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Co-scholastic Subject</a> </h1>
					<p>Co-scholastic types and its subject can be created in system with subject description to display in report card and grades for this subject managed automatically in results.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Map Subjects to Class-Section</a> </h1>
					<p>Subjects are independent entity in the school, School needs to Map subjects to class and section, By this we can easily manage subjects for particular sections</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Map Subjects to Students</a> </h1>
					<p>Students have various choices in higher classes and primary classes to opt for particular subject, we can easily manage subject list of each students.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Map Subjects to Teachers</a> </h1>
					<p>Subject as per section is mapped to teacher to know which subject is going to be taught by whom, also privileges related to subject for students is managed by this teacher.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Certificates Management</a> </h1>
					<p>Managing Student - Transfer Certificate, Character Certificate and Competition Certificates. Templates are available and also as per school it can be customized.</p>
				</div> 				
 			</div>

 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

