 <!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading wow bounceUpLeft">
 				<h1 class="wow bounceUpLeft">School Fess Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="index"> Home / </a> </span>   Became A Partner</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<!-- section -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Became A Partner </h1>
 		       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><br>
 		       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 	     	</div>
 	     	<div class="col-md-6 col-sm-6 col-xs-12 team-partner wow fadeInUp">
 	     		<img src="images/partner-3.png" class="img-responsive team-partner-img">
 	     	</div>
 	     	<div class="col-md-6 col-sm-6 col-xs-12  form-became-partner-block wow fadeInUp">
 	     		<form id="became-our" method="post" action="">
 	     		<div class="form-became-partner">
 	     			<h2>became a partner</h2>
 	     			<div class="input-block form-group ">
 	     				<div class="font-icon-block">
 	     					<img src="images/user.png">
 	     				</div>
 	     				<input type="text" name="uname" class="input-box form-control" placeholder="Name">
 	     			<div class="clear"></div>
 	     			</div>

 	     			<div class="input-block form-group ">
 	     				<div class="font-icon-block">
 	     					<img src="images/company-icon.png" class="img-responsive company-icon">
 	     				</div>
 	     				<input type="text" name="ucompany" class="input-box form-control companyname" placeholder="Company Name">
 	     			<div class="clear"></div>
 	     			</div>

 	     			<div class="input-block form-group ">
 	     				<div class="font-icon-block">
 	     					<img src="images/user-env.png">
 	     				</div>
 	     				<input type="email" name="uemail" class="input-box form-control" placeholder="Your Email (Will Contact to This Mail Id)">
 	     			<div class="clear"></div>
 	     			</div>

 	     			<div class="input-block form-group ">
 	     				<div class="font-icon-block">
 	     					<img src="images/phone-icon.png" class="img-responsive phone-icon">
 	     				</div>
 	     				<input type="number" name="uphone" class="input-box form-control" placeholder="Your Phone Number">
 	     			  <div class="clear"></div>
 	     		   </div>

 	     		   <div class="">
 	     				<input type="submit" name="submit" class="submit-partner">

 	     		   </div>
 	     		   <div class="clear"></div>
 	     		</div>
 	     		</form>
 	     	</div>
 	     </div>	
 	</div>
 </div>
<!-- end -->

<!-- section admin portal start -->
<div class="container-fulid Our-values">
	<div class="container padding_zero padding-all">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 company-vlalue-block wow fadeInUp">
 		       	<h1>Why Academic Eye </h1>
 		       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
 	     	</div>
 	     	<div class="clear"></div>
 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/client.png" class="img-responsive " >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> client focus</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, seddo 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/quality.png" class="img-responsive quality-img" >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> quality</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, seddo 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/integity.png" class="img-responsive" >
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> integrity</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, seddo 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>

 	     	<div class="col-md-3 col-sm-3 col-xs-12 our-value-block ">
 	     		<div class="our-value">
	 	     		<div class="circle-block wow rotateIn">
	 	     			<img src="images/team-work-icon.png" class="img-responsive">
	 	     		</div>
	 	     		<h3 class="wow fadeInUp"> team work</h3>
	 	     		<p class="wow fadeInUp"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, seddo 
	 	     		 </p>
		 	     
	 	   	   	   <div class="clear"></div>
 	   	   	   </div>
 	   	   	    <div class="clear"></div>
 	     	</div>
		</div>
	</div>
</div>
 <!-- end -->

 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->



