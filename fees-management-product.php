<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Fees Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>Fees Management</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Fees Management</h1>
 		       	<p>Fees management in the school is the most important activity of school because it is main source of income of the school so it is required to be accurate and collection must be done on time and issue receipts to them. Academic Eye delivers a complete fees collection software which ease the work of collection, reminders to fees due and generate fees receipts with proper reporting.</p><br>
 		       	<p>Fees Collection software is designed to save a lot of time of staff engaged in fees collection and receipt generation, also the system send alerts to parents via sms and app notification about fees dues, partial payment facility, balance adjustment facility, fee relaxation facility, percent discount, optional fees, concession, hostel fees system, transportation fees and separate conveyance facility.</p><br>
 		       	<p>Fees configuration is defined and applied on admission portal for new student, for old student its is applied in this module, Fees Module is completely integrated with Accounting Module, it automatically updates ledgers and accounting books.</p>

 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Fees Collection Module makes the complete process of fees collection too easy and simple.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/productimages/fees management/Dynamic Fees Heads.svg" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Dynamic Fees Heads</h1>
 	     				<p>Fees heads with different amount of fees for different classes can be managed.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/productimages/fees management/012-fine-arts.svg" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Online Fees Payments</h1>
 	     				<p>Parents can pay fees online from the application and website with Credit Card, Debit Card and E-wallets.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/productimages/fees management/Concession Management.svg" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Concession Management</h1>
 	     				<p>Student’s concession of various types can be easily setup in the system.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/productimages/fees management/Fees Scheduling.svg" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Fees Scheduling</h1>
 	     				<p>Fees Scheduling send alerts to the parents for dues, which helps in collection on the time.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Fees Management</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/productimages/fees management/001-winner.svg" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Fees Heads</a></h1>
					<p>This module allow easy to create fees particulars on installment and one time, and can be applied on both new student and existing students.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/productimages/fees management/002-mobile-app.svg" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Facilities fees</a></h1>
					<p>Academic Eye also allow school to manage fees for facilities like Transportation, Hostel, Library, so that school can collect entire fees in one module.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/productimages/fees management/003-check.svg" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Multiple Payment Modes</a> </h1>
					<p>Academic Eye allows school to take fees from multiple payment mode i.e, Online, Cash, Cheque, Swipe Machine, so every transaction can be track easily when needed.</p>
				</div> 				
 			</div>
 			
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/productimages/fees management/004-phone.svg" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Cheques Collection</a></h1>
					<p>Fees Collection module also manage all the cheques received and allows to change status of cheques from uncleared to cleared for proper accounting.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/productimages/fees management/005-phone-1.svg" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Fees Counter</a> </h1>
					<p>Fees Counter window allows schools to actual enter the details of payment, it manages complete information like wallet, discount, pay later, payment modes, imprest a/c, it is designed so simple that anyone can easily understand its working.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/fees management/006-mobile.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Govt RTE Management</a> </h1>
					<p>Schools can easily manage student under RTE scheme, their detailed report and fees received.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/fees management/007-cheque.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Imprest A/C</a> </h1>
					<p>Student Imprest A/C management can be easily done in this fees modules.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/fees management/008-office-worker.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Wallet A/C </a> </h1>
					<p>Surplus payment at the time of fees payment is managed in Wallet A/C of student.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/fees management/009-businessman.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Dynamic Fine System</a> </h1>
					<p>Fees Collection module allows school impose fine on the basis of delay payment on different fees heads, fines are completely dynamic, imposed fine can be cancelled.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/fees management/010-calculator.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Receipts</a> </h1>
					<p>Fees Collection provide various templates of fees receipts, but school can easily get customized fees reports as per their needs.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/productimages/fees management/011-wallet.svg" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Reports</a> </h1>
					<p>Fees reports are most important in school hence we provide complete reports of Paid and Dues fees i.e, Particular wise, Class wise, Facilities wise, Month wise, Date Range wise, Concession, Imprest A/C, Wallet A/C, RTE reports, Cheques wise.</p>
				</div> 				
 			</div>

 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

