-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 12:44 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `v2rteste_academic_school_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_group`
--

CREATE TABLE `acc_group` (
  `acc_group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `acc_sub_head_id` int(10) UNSIGNED DEFAULT NULL,
  `acc_group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_group_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_group_heads`
--

CREATE TABLE `acc_group_heads` (
  `acc_group_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `acc_sub_head_id` int(10) UNSIGNED DEFAULT NULL,
  `acc_group_id` int(10) UNSIGNED DEFAULT NULL,
  `acc_group_head` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_group_head_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_main_heads`
--

CREATE TABLE `acc_main_heads` (
  `acc_main_head_id` int(10) UNSIGNED NOT NULL,
  `acc_main_head` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_main_head_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_open_balance`
--

CREATE TABLE `acc_open_balance` (
  `acc_open_balance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `acc_sub_head_id` int(10) UNSIGNED DEFAULT NULL,
  `acc_group_head_id` int(10) UNSIGNED DEFAULT NULL,
  `balance_amount` double(18,2) DEFAULT NULL,
  `balance_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_sub_heads`
--

CREATE TABLE `acc_sub_heads` (
  `acc_sub_head_id` int(10) UNSIGNED NOT NULL,
  `acc_main_head_id` int(10) UNSIGNED DEFAULT NULL,
  `acc_sub_head` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_sub_head_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ac_entry_map`
--

CREATE TABLE `ac_entry_map` (
  `ac_entry_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `ac_entry_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `ac_entry_map_amt` int(11) DEFAULT NULL,
  `ac_entry_map_amt_paid` int(11) DEFAULT NULL,
  `ac_entry_map_amt_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Due,1=Paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ac_entry_map`
--

INSERT INTO `ac_entry_map` (`ac_entry_map_id`, `admin_id`, `update_by`, `ac_entry_id`, `session_id`, `class_id`, `section_id`, `student_id`, `ac_entry_map_amt`, `ac_entry_map_amt_paid`, `ac_entry_map_amt_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, 8, 1500, 1500, 1, '2019-04-03 16:25:06', '2019-04-03 16:25:06'),
(2, 2, 2, 2, 1, 1, 1, 8, 510, 510, 1, '2019-04-03 16:25:06', '2019-04-03 16:25:06');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0: super admin,1: school admin,2: staff admin,3: parent admin,4: student admin',
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_profile_img` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notification_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=false,1=true',
  `android_current_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_current_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enroll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_device_id` text COLLATE utf8mb4_unicode_ci,
  `device_type` int(11) DEFAULT NULL COMMENT '1=Android,2=IOS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_type`, `admin_name`, `email`, `password`, `admin_profile_img`, `remember_token`, `created_at`, `updated_at`, `notification_status`, `android_current_app_version`, `ios_current_app_version`, `mobile_no`, `enroll_no`, `fcm_device_id`, `device_type`) VALUES
(1, 0, 'Super Admin', 'admin@gmail.com', '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG', NULL, '7Fwfqcmk1fxc2H0wNsWKMyfkBc3MrONiHRTnj6UZxnxfN1WLeRdchBhnTZQ7', '2019-01-23 09:36:01', '2019-01-23 09:36:01', 1, NULL, NULL, NULL, NULL, '1', NULL),
(2, 1, 'School', 'school@gmail.com', '$2y$10$plabX.6CrXgxMxUJsBWfWOHP9p0gxh6S3V2TMsTRhaOo17pV64g4m', NULL, '08rv8DnCyhsKkW9LuI2z7UfqLc6tirvadpo4WNgiU8xDkovIFZ3JijWYaOv8', '2019-01-23 09:36:01', '2019-06-18 16:30:08', 1, NULL, NULL, '1234567890', NULL, 'eUqLEozVYc0:APA91bHGp5PxXBYsyxzsdaAgZsGUHH1ykfjUdQuz33xIXpn3Stg39u5yStrS_Ww9SKn9Vy7erJKFJvB0n4Tt15G_OP78iANO6Ec9GOFLCkrvBFZbLZCSs6fkLpLfXY7MYPlR6Ma3T224', 1),
(3, 2, 'Ravi', 'ravi@gmail.com', '$2y$10$U3c0.hKQfblv6NrbG5Tj8Octpj9c43sj/ERpq5WJYsvX.k9HO3SaC', NULL, 'FhgoZhBrMIBQBJWVGe98ED0q3NuXvqGsoDXEF9hJjZ1dEmICl78HFvFnDEq4', '2019-01-23 11:10:14', '2019-05-21 21:50:18', 1, NULL, NULL, '9999999999', NULL, 'fgrnu0LwrjY:APA91bFC43KdxdFyEAvRDIfsKhYsFkpLDO9Ga9M5Ih3l99dgsMPgm96kbwvqdD15FONM05yPnjwLLO8ZiraeNiX6tYb2y5C6cgw9bmqtDuwixa6yWyVoaiuvYUy3xywt-9gYxugkHecJ', 2),
(5, 3, 'Nath', 'nath1@gmail.com', '$2y$10$gnObYYmxRxeWQRlTxKgOPuifJ3qykjl/CGkJlTIH1fx3M0ZCHRnlm', NULL, 'ZBNIg9NIU8TvWmSsNTKStz9rXoA9vegw6dPR1GaZQzzksrK9EaPmOqPGWsRS', '2019-01-23 11:16:47', '2019-07-04 16:43:58', 1, NULL, NULL, '8888888888', NULL, 'dE3UgozYj5U:APA91bEH2r09LD-2HeRGIQH4uWEhQrskOmLjBVZS5z3X2EOngmzbNjC-jqTKhBHHsSzhiv8XdldaWZT94YwuGcRMdIQ6CWPBY8Q305w44uYaswZnR3XdTG0vXDuHd-DP_1pQ9_V7NhmH', 2),
(6, 4, 'Teena', 'N0001', '$2y$10$3CRirgMbCvAGJ3rLHWXCG.tgNHl4kcQaG0PeGPZK1p/mno6DR0Zti', NULL, 'Y90AhUHvYNeowKvWgxHcd6Wajcdui9CZvrZBfYtRy7VeSWw7rMCDYZV442y0', '2019-01-23 11:16:47', '2019-04-09 00:04:18', 1, NULL, NULL, NULL, 'N0001', 'diNQ-d-tezM:APA91bEWnUt_pVE0owT5OaFk2SteCQun_yVrQm3KmnRIm4cJJ8bC5tCfQZ8Wxujw9GkutDYQdjNahHfVr-ykIlitD2CJdC2DbE3bcRKcc9KGMr6a-8ljUO1HdoYrf9cT46ZD_dNnkpu9', 1),
(8, 4, 'Pankaj', 'N0002', '$2y$10$IGe4r8hnLCxydEFQX87XDuc1htacivyym4Sz0doWEUQ4FxP0LykwG', NULL, 'T3aPoNU2SPBd7LpDIbHldFXK9ji2VdITaAhixPcNFaLqtEF9GshPaXzyTJN0', '2019-01-23 11:25:12', '2019-01-23 11:25:12', 1, NULL, NULL, NULL, 'N0002', '1', NULL),
(9, 3, 'Kamal', 'kamal@gmail.com', '$2y$10$iA/YuUL0mx5r/sGbS/dAl.B8nvgZ4TcedFc8gisOyx.aUZ1YxyG.u', NULL, 'm71TLV1hTYlFF4yQTEoFHTetNZgcK0UOZL8hkDDa4YUKpVW8DFFzzRW01bkD', '2019-01-23 11:28:31', '2019-01-23 11:28:31', 1, NULL, NULL, '7073660684', NULL, '1', NULL),
(10, 4, 'Ajay', 'N003', '$2y$10$m6JjyV7niIgZPYe/s1Fz0eUvIvSJDbOmCuU2JtfdW/F2zwBY5RpMu', NULL, 'qgsSb7R3NitPx97INCb6G82IIRW2hEHaDWT7l9MSMQoxAIXH81XEHNpunvB5', '2019-01-23 11:28:31', '2019-01-23 11:28:31', 1, NULL, NULL, NULL, 'N003', '1', NULL),
(12, 4, 'Neelam', NULL, '$2y$10$xiNytijLehAAsl2eRn4qI.iu829DH5KySzffWpQ2WccGiiMyeQTPu', NULL, '6spYiWKbZ7uTLAWEJPY8pNa6Rs0RIsDHvuRNveE7b4iOUE7B3Gm0tsYVU3yt', '2019-01-25 06:42:42', '2019-01-25 06:42:42', 1, NULL, NULL, NULL, 'N0004', '1', NULL),
(13, 3, 'Shri', NULL, '$2y$10$UAfV8fxeO1Xs.QxPLeEDSe5h9CpCteEtVuXkiuXvDM/NfiAIZiK12', NULL, NULL, '2019-01-25 10:47:36', '2019-01-25 10:47:36', 1, NULL, NULL, '', NULL, 'e_xpNlsa7iM:APA91bGKFpCKvP6yXPL5KCfUOAod5kvA0lLkcKbowx0BxgDX44jL0LZz-XHH59XBuEj3R1fhvCVdtwZYRazPv7Mdwf0E0Y2OMEl5fkJGKjtbZ4pFKX7MCPA9G6IC4-yAuYhWXxZ0JtLJ', NULL),
(14, 4, 'Shri', NULL, '$2y$10$JKKtZcGBdfC64rx3.9gxc.WZitXy6BLv.IXKbXsLofdkca6jU8xeq', NULL, NULL, '2019-01-25 10:47:36', '2019-01-25 10:47:36', 1, NULL, NULL, NULL, 'N0006', 'e_xpNlsa7iM:APA91bGKFpCKvP6yXPL5KCfUOAod5kvA0lLkcKbowx0BxgDX44jL0LZz-XHH59XBuEj3R1fhvCVdtwZYRazPv7Mdwf0E0Y2OMEl5fkJGKjtbZ4pFKX7MCPA9G6IC4-yAuYhWXxZ0JtLJ', NULL),
(15, 3, 'ravi', 'ravi1@gmail.com', '$2y$10$9AU/HhLQtHttqK9Y1MUh3OM0SzhVrKPCOoVn0EKmavIF1vNbREsUy', NULL, NULL, '2019-02-04 05:15:04', '2019-02-04 05:15:04', 1, NULL, NULL, '1234567908', NULL, '1', NULL),
(16, 4, 'Amit', 'rakesh@gmail.com', '$2y$10$F76kPa3p4BD1tWrS5tBSJOTGFdc5myQA/bDhxBFkbVPCHNfVkBVAG', NULL, NULL, '2019-02-04 05:15:06', '2019-02-04 05:15:06', 1, NULL, NULL, NULL, '102A', '1', NULL),
(23, 2, 'Dinesh', 'dinesh@gmail.com', '$2y$10$097L9pw8WvRg5acCaa36duAFqVePRc24Oq5eQtROPRgqhb/KAuDK.', NULL, 'Vo8Qi78ySMujRAsYDxPGbEhpBjjZdMa4EoDXYtHFbXhk3ZGNHGJr4QVvaZga', '2019-02-04 06:41:12', '2019-02-04 06:41:12', 1, NULL, NULL, '1234567890', NULL, '1', NULL),
(24, 4, 'Sibling A', 'siblinga@gmail.com', '$2y$10$wlQ/xhyKGPVzbvjs.lK.GeSIgLqB1sJMxY/3PzIiRHbFNWc7fO2cu', NULL, NULL, '2019-03-05 11:19:20', '2019-03-05 11:19:20', 1, NULL, NULL, NULL, 'N0007', NULL, NULL),
(25, 2, 'ramesh', 'ramesh@gmail.com', '$2y$10$yeUtMomBsGqUVxoFcr585..HKCOH.xlJctR3cjTdN67KkzYjUlka.', NULL, NULL, '2019-04-02 16:23:23', '2019-04-09 20:03:40', 1, NULL, NULL, '1524895632', NULL, 'diNQ-d-tezM:APA91bEWnUt_pVE0owT5OaFk2SteCQun_yVrQm3KmnRIm4cJJ8bC5tCfQZ8Wxujw9GkutDYQdjNahHfVr-ykIlitD2CJdC2DbE3bcRKcc9KGMr6a-8ljUO1HdoYrf9cT46ZD_dNnkpu9', 1),
(26, 2, 'Mohan', 'mohan@gmail.com', '$2y$10$WT21W9MwC3MwTE9DvbTNyufqxbcPEkLAzcGOivbMuHeFwJhuD9oTG', NULL, NULL, '2019-04-02 16:26:13', '2019-04-02 16:26:13', 1, NULL, NULL, '2589631485', NULL, NULL, NULL),
(27, 3, 'ram', 'ram@gmail.com', '$2y$10$gIBRKm0c/tt5ikPEuA4jm.qRi3IrePgpXV4DR/d0N1uXBseiNBbzC', NULL, NULL, '2019-04-02 17:11:06', '2019-04-02 17:11:06', 1, NULL, NULL, '5564545454', NULL, NULL, NULL),
(28, 4, 'khushbu', 'khushbu@gmail.com', '$2y$10$EmVi6EqeI.crBpBUUo6/9ewhtfydp2gOJH2KLAS9dnV527LPpDc4e', NULL, NULL, '2019-04-02 17:11:06', '2019-04-09 18:47:43', 1, NULL, NULL, NULL, '7890', 'diNQ-d-tezM:APA91bEWnUt_pVE0owT5OaFk2SteCQun_yVrQm3KmnRIm4cJJ8bC5tCfQZ8Wxujw9GkutDYQdjNahHfVr-ykIlitD2CJdC2DbE3bcRKcc9KGMr6a-8ljUO1HdoYrf9cT46ZD_dNnkpu9', 1),
(29, 2, 'babu lal', 'babu32lal@gmail.com', '$2y$10$.ZvhXYnvdpvfSC89DQSM/OH1Ge9lUQ8wwwBmzsbjPcK6JuOg8kvzq', NULL, NULL, '2019-04-02 18:01:58', '2019-04-09 18:56:36', 1, NULL, NULL, '7654397654', NULL, 'diNQ-d-tezM:APA91bEWnUt_pVE0owT5OaFk2SteCQun_yVrQm3KmnRIm4cJJ8bC5tCfQZ8Wxujw9GkutDYQdjNahHfVr-ykIlitD2CJdC2DbE3bcRKcc9KGMr6a-8ljUO1HdoYrf9cT46ZD_dNnkpu9', 1),
(30, 2, 'Sourabh Rohila 123', 'A@mailinator.com', '$2y$10$dGI6.PsHnVBfZ0WE7Nmc7ueE1uxm5bH15vfEwuTSzi6hjc6due5A2', NULL, 'j0MK7MWzOytLjVyD4w3Hs6R9xkZMPSrflHgA8rXiykOcw4aOAHopA86HnnCw', '2019-04-03 21:32:51', '2019-04-05 22:11:59', 0, NULL, NULL, '1231231231', NULL, 'fgrnu0LwrjY:APA91bFC43KdxdFyEAvRDIfsKhYsFkpLDO9Ga9M5Ih3l99dgsMPgm96kbwvqdD15FONM05yPnjwLLO8ZiraeNiX6tYb2y5C6cgw9bmqtDuwixa6yWyVoaiuvYUy3xywt-9gYxugkHecJ', 1),
(31, 3, 'Sourabh Rohila', 'sourabh@mailinator.com', '$2y$10$NkAGB.GLG32s/T1Kbmwa3eTHXIT.He9o5AyooxNlgxeKY1YFKubCe', NULL, 'yB8IWZDyBEtuHaO4Zbsrcvb3sa0gyTYwEg5HIky59E7veUy0NibEhRNCXdae', '2019-04-03 23:28:02', '2019-04-05 16:57:54', 1, NULL, NULL, '8209683554', NULL, 'cVCVhUTNs_s:APA91bE62wjUNV9lZwpBCV1-SOJjyGcOaqnvG822DdhZCPCIQtnNnsaWVWM_74kqilY8OYYVMp7_2EZ7NK_pkXWTwe9BuviBRscQc8TottVWaF945Tunb_mRpu5QwPblG8qfwgdKVvxh', 1),
(32, 4, 'Sourabh Rohila', NULL, '$2y$10$lvEeIxcCeacQcSlV7Bw34uIgdYTQfcirZempQuxbYLnMOwEtDelO.', NULL, NULL, '2019-04-03 23:28:02', '2019-04-03 23:28:02', 1, NULL, NULL, NULL, 'N00123', NULL, NULL),
(33, 2, 'Testing Sourabh', 'testing@mailinator.com', '$2y$10$UmsZXHZOTe.RK0Nsh2CmyOerWf8TDZpTk1m9BmwPXj8siLeJP9LDK', NULL, NULL, '2019-04-05 21:54:48', '2020-02-13 17:19:10', 1, NULL, NULL, '9696969696', NULL, 'cVCVhUTNs_s:APA91bE62wjUNV9lZwpBCV1-SOJjyGcOaqnvG822DdhZCPCIQtnNnsaWVWM_74kqilY8OYYVMp7_2EZ7NK_pkXWTwe9BuviBRscQc8TottVWaF945Tunb_mRpu5QwPblG8qfwgdKVvxh', 1),
(34, 3, 'parent testing', 'parent@mailinator.com', '$2y$10$jydHUCSbHR52RJQLAhXqIOyegQO4JWGpMeV/UyV55VBN7JnkQ.fvC', NULL, NULL, '2019-04-05 22:29:56', '2019-04-05 22:29:56', 1, NULL, NULL, '9595959595', NULL, NULL, NULL),
(35, 4, 'Student testing 1', 'student@mailinator.com', '$2y$10$t7xw0IBlIwQI03uzhq70Eukc.dya6w2p4wVbIB6uwWEx8EjzWV.OG', NULL, NULL, '2019-04-05 22:29:56', '2019-04-05 22:29:56', 1, NULL, NULL, NULL, 'Titoo', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enroll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_password_resets`
--

INSERT INTO `admin_password_resets` (`email`, `mobile_no`, `enroll_no`, `token`, `created_at`) VALUES
(NULL, NULL, NULL, '$2y$10$hgQsD0lOdn8P2FuwHE9G1emo7CogDe5HtNSZfmGlZs5dj./F8SaK2', '2019-04-02 16:19:50'),
(NULL, NULL, NULL, '$2y$10$vec.o/Ab3l6JWbfJgbyPBOjI3ZRCKqmIkvzfPb81gRv4hk3PMZC0G', '2019-04-02 16:26:42'),
(NULL, NULL, NULL, '$2y$10$hpSMoR5uKor6DjO/fvTFSuWgZwYkMoYO6KIxEOsuh5a0KybSUASw.', '2019-04-02 16:28:17'),
(NULL, NULL, NULL, '$2y$10$ET/xHjJ3vpqivaLy3F3ABOjPh5FBn3hdcoCJXq9QJApjvrxNKjEA6', '2019-04-02 16:58:07'),
(NULL, '9686779416', NULL, '$2y$10$M2LJOsLTQAwqGL/k94qYCO8g7WQgLKPo.MpRISQmMIOGvGr/CSLv.', '2019-04-02 17:00:58'),
(NULL, '9686779416', NULL, '$2y$10$8li3VduuTaXHtyvjyX4bben3jo6JfZ18YdXBFxSzregySyZgNqamW', '2019-04-02 17:01:51'),
(NULL, '8888888888', NULL, '$2y$10$KhkjfvRPEZdBR6cBY2iuveShmHJDXKe5sQP3Eq4ywhZMcluEmRgKu', '2019-04-04 16:24:20'),
(NULL, '8888888888', NULL, '$2y$10$eINiN0nVUL0qAixEcXoas.FhmYiCuB.3NjQ148QT9fMQAePXSf2sy', '2019-04-04 16:25:06'),
(NULL, '8888888888', NULL, '$2y$10$cRSYZRebBZbZI7iKZbvlq..GEOorUBtVR/Qt4jBX6MTr71Sbz86I6', '2019-04-04 16:25:13'),
(NULL, '9782218265', NULL, '$2y$10$HA04/k81cSR6mAFZ67WmB.W4M6zqCKqRJiqV0EFEnPL0OcpOslkfy', '2019-04-04 17:35:45'),
(NULL, '9686779416', NULL, '$2y$10$fhAtz876JuOjTVUADE5BXutCl9FcUujwTYhsw11ngqyht8.uFI6wS', '2019-04-05 17:18:54'),
(NULL, '7073660684', NULL, '$2y$10$l7mBs9XRY7Eca3M94WaB5eS5LNQltCAaaXGbdhE2EWOXx0L9EK3BG', '2019-04-05 23:13:24'),
(NULL, '7073660684', NULL, '$2y$10$Zd1eO7oEgxYhGYUvAnatvOjY9cKg5Yye9BiwlNNtpNmS0HugaXdrq', '2019-04-05 23:16:34'),
(NULL, '7073660684', NULL, '$2y$10$Zyl6u7nOt58bQPDkvpTFCeZC/j79TTmaZGPFvooNl/seCkEJCuD/G', '2019-04-05 23:52:11'),
(NULL, '7073660684', NULL, '$2y$10$BeCvVFcNkVEICBN1YgwUzOIkKsHmfD6Od20048r8q8axGVx6cdyoa', '2019-04-05 23:52:35'),
(NULL, '7073660684', NULL, '$2y$10$PTHvlyvwBPDLq0JvBDgRJeHa7AJUKweAi07402t62U/Lk1WK.ZCCm', '2019-04-05 23:54:40'),
(NULL, '7073660684', NULL, '$2y$10$NHpQ8fhzvpvbiKO1TPPgLOt23HeaKIMcp40ff3uYcfOfopDHbc6.K', '2019-04-06 00:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `admission_data`
--

CREATE TABLE `admission_data` (
  `admission_data_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `admission_form_id` int(10) UNSIGNED DEFAULT NULL,
  `form_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Admission,1=Enquiry',
  `form_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_enroll_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_roll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_image` text COLLATE utf8mb4_unicode_ci,
  `student_reg_date` date DEFAULT NULL,
  `student_dob` date DEFAULT NULL,
  `student_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boy,1=Girl',
  `student_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=PaidBoy,1=RTE,2=FREE',
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `student_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `student_sibling_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_sibling_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_adhar_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_date` date DEFAULT NULL,
  `student_privious_result` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `student_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_session_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_class_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_section_id` int(10) UNSIGNED DEFAULT NULL,
  `current_session_id` int(10) UNSIGNED DEFAULT NULL,
  `current_class_id` int(10) UNSIGNED DEFAULT NULL,
  `current_section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `student_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_vision_left` text COLLATE utf8mb4_unicode_ci,
  `student_vision_right` text COLLATE utf8mb4_unicode_ci,
  `medical_issues` longtext COLLATE utf8mb4_unicode_ci,
  `student_login_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_tongue` text COLLATE utf8mb4_unicode_ci,
  `student_guardian_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_relation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_data_mode` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Offline,1=Online',
  `admission_data_pay_mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_data_fees` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Paid',
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_data_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Pending,1=Contacted, 2=Admitted ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admission_data`
--

INSERT INTO `admission_data` (`admission_data_id`, `admin_id`, `update_by`, `admission_form_id`, `form_type`, `form_number`, `student_enroll_number`, `student_roll_no`, `student_name`, `student_image`, `student_reg_date`, `student_dob`, `student_email`, `student_gender`, `student_type`, `medium_type`, `student_category`, `title_id`, `caste_id`, `religion_id`, `nationality_id`, `student_sibling_name`, `student_sibling_class_id`, `student_adhar_card_number`, `student_privious_school`, `student_privious_class`, `student_privious_tc_no`, `student_privious_tc_date`, `student_privious_result`, `student_temporary_address`, `student_temporary_city`, `student_temporary_state`, `student_temporary_county`, `student_temporary_pincode`, `student_permanent_address`, `student_permanent_city`, `student_permanent_state`, `student_permanent_county`, `student_permanent_pincode`, `admission_session_id`, `admission_class_id`, `admission_section_id`, `current_session_id`, `current_class_id`, `current_section_id`, `student_unique_id`, `group_id`, `stream_id`, `student_height`, `student_weight`, `student_blood_group`, `student_vision_left`, `student_vision_right`, `medical_issues`, `student_login_name`, `student_login_email`, `student_login_contact_no`, `student_father_name`, `student_father_mobile_number`, `student_father_email`, `student_father_occupation`, `student_father_annual_salary`, `student_mother_name`, `student_mother_mobile_number`, `student_mother_email`, `student_mother_occupation`, `student_mother_annual_salary`, `student_mother_tongue`, `student_guardian_name`, `student_guardian_email`, `student_guardian_mobile_number`, `student_guardian_relation`, `admission_data_mode`, `admission_data_pay_mode`, `admission_data_fees`, `student_id`, `admission_data_status`, `created_at`, `updated_at`) VALUES
(9, 2, 2, 1, 1, 'ENQ1', 'N0006', NULL, 'Babita', 'dinesh91281548917658.jpg', '2019-02-25', '2019-01-25', 'babita@gmail.com', 1, 0, 1, 'OBC', 1, 1, 1, 1, NULL, NULL, NULL, 'School A', 'Class A', 'TC001', '2019-01-25', 'Pass', NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 2, 1, 1, '342001', 1, 1, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, 'No', NULL, NULL, NULL, 'Ramesh', '8890149916', 'ramesh@gmail.com', 'Job', 'Below 5 Lac', 'Mamta', '8579641230', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', 0, NULL, 1, '2019-01-31 06:54:18', '2019-01-31 06:54:18'),
(10, 2, 2, 3, 0, 'recruitment1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Cash', 0, NULL, 1, '2019-04-02 16:48:23', '2019-04-02 16:48:23'),
(11, 2, 2, 2, 0, 'ADMISSION1', '5221', NULL, 'aman', 'avatar-business-man-people-person-3f86f80ad185a286-512x512471051534936792503221554186138.png', NULL, '1996-06-15', 'aman@gmail.com', 0, 0, 1, 'General', 1, 1, 1, 1, NULL, NULL, NULL, 'Cambridge public school', 'Class4', 'edfd43dsd', '2019-03-14', 'pass', '4/A/56', NULL, NULL, NULL, '654444', 'tesssdssd', NULL, NULL, 1, '654444', 1, 1, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, 'no', NULL, NULL, NULL, 'ram lal', '6454545454', 'ramlal@gmail.com', 'govt teacher', 'Below 2.5 Lac', 'dfd', '5455555555', 'dfd@gmail.com', 'doctor', 'Below 2.5 Lac', 'ffdf', 'mohan', 'mohan@gmail.com', '4566545445', 'brother', 0, 'Cash', 2, NULL, 1, '2019-04-02 16:52:18', '2019-04-02 16:52:18');

-- --------------------------------------------------------

--
-- Table structure for table `admission_fields`
--

CREATE TABLE `admission_fields` (
  `admission_field_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `admission_form_id` int(10) UNSIGNED DEFAULT NULL,
  `form_keys` text COLLATE utf8mb4_unicode_ci,
  `form_pay_mode` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admission_fields`
--

INSERT INTO `admission_fields` (`admission_field_id`, `admin_id`, `update_by`, `admission_form_id`, `form_keys`, `form_pay_mode`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'student_enroll_number,student_name,student_type,student_image,student_reg_date,student_dob,student_email,student_gender,admission_class_id,student_privious_school,student_privious_class,student_privious_tc_no,student_privious_tc_date,student_privious_result,student_category,title_id,caste_id,religion_id,nationality_id,stream_id,student_sibling_name,student_sibling_class_id,student_adhar_card_number,group_id,student_father_name,student_father_mobile_number,student_father_email,student_father_occupation,student_father_annual_salary,student_mother_name,student_mother_mobile_number,student_mother_email,student_mother_occupation,student_mother_annual_salary,student_mother_tongue,student_guardian_name,student_guardian_email,student_guardian_mobile_number,student_guardian_relation,student_temporary_address,student_temporary_city,student_temporary_state,student_temporary_country,student_temporary_pincode,student_permanent_address,student_permanent_city,student_permanent_state,student_permanent_county,student_permanent_pincode,student_height,student_weight,student_blood_group,student_vision_left,student_vision_right,medical_issues,document_category_id,student_document_details,student_document_file', 'Cash,Cheque', '2019-01-31 05:02:26', '2019-01-31 05:02:26'),
(2, 2, 2, 2, 'student_enroll_number,student_name,student_image,student_dob,student_email,student_gender,admission_class_id,student_privious_school,student_privious_class,student_privious_tc_no,student_privious_tc_date,student_privious_result,student_category,title_id,caste_id,religion_id,nationality_id,student_father_name,student_father_mobile_number,student_father_email,student_father_occupation,student_father_annual_salary,student_mother_name,student_mother_mobile_number,student_mother_email,student_mother_occupation,student_mother_annual_salary,student_mother_tongue,student_guardian_name,student_guardian_email,student_guardian_mobile_number,student_guardian_relation,student_temporary_address,student_temporary_city,student_temporary_state,student_temporary_country,student_temporary_pincode,student_permanent_address,student_permanent_city,student_permanent_state,student_permanent_county,student_permanent_pincode,student_blood_group,medical_issues', 'Cash,Cheque,Online,Swipe Machine', '2019-04-02 16:41:50', '2019-04-02 16:41:50'),
(3, 2, 2, 5, 'student_enroll_number,student_name,student_type,student_image,student_reg_date,student_dob,student_email,student_gender,admission_class_id,student_privious_school,student_privious_class,student_privious_tc_no,student_privious_tc_date,student_privious_result,student_category,title_id,caste_id,religion_id,nationality_id,stream_id,student_adhar_card_number,group_id,student_father_name,student_father_mobile_number,student_father_email,student_father_occupation,student_father_annual_salary,student_mother_name,student_mother_mobile_number,student_mother_email,student_mother_occupation,student_mother_annual_salary,student_mother_tongue,student_guardian_name,student_guardian_email,student_guardian_mobile_number,student_guardian_relation,student_temporary_address,student_temporary_city,student_temporary_state,student_temporary_country,student_temporary_pincode,student_permanent_address,student_permanent_city,student_permanent_state,student_permanent_county,student_permanent_pincode,student_height,student_weight,student_blood_group,student_vision_left,student_vision_right,medical_issues,document_category_id,student_document_details,student_document_file', 'Cash,Cheque,Online,Swipe Machine', '2019-04-06 17:55:13', '2019-04-06 17:55:13');

-- --------------------------------------------------------

--
-- Table structure for table `admission_forms`
--

CREATE TABLE `admission_forms` (
  `admission_form_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `form_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_ids` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `no_of_intake` int(11) DEFAULT NULL,
  `form_url` text COLLATE utf8mb4_unicode_ci,
  `form_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_instruction` text COLLATE utf8mb4_unicode_ci,
  `form_information` text COLLATE utf8mb4_unicode_ci,
  `form_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Admission,1=Enquiry',
  `brochure_id` int(10) UNSIGNED DEFAULT NULL,
  `form_last_date` date DEFAULT NULL,
  `form_fee_amount` int(10) UNSIGNED DEFAULT NULL,
  `form_pay_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_email_receipt` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `form_success_message` text COLLATE utf8mb4_unicode_ci,
  `form_failed_message` text COLLATE utf8mb4_unicode_ci,
  `form_message_via` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Email,1=Phone,2=Both',
  `form_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admission_forms`
--

INSERT INTO `admission_forms` (`admission_form_id`, `admin_id`, `update_by`, `form_name`, `session_id`, `class_ids`, `medium_type`, `no_of_intake`, `form_url`, `form_prefix`, `form_instruction`, `form_information`, `form_type`, `brochure_id`, `form_last_date`, `form_fee_amount`, `form_pay_mode`, `form_email_receipt`, `form_success_message`, `form_failed_message`, `form_message_via`, `form_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'ENQ', 1, '1', 1, 50, 'http://v2rsolution.co.in/academic-management/enquiry-form/eyJpdiI6IlpxT044bERxXC9mZUd5QVJldG1qZVZBPT0iLCJ2YWx1ZSI6IjhWWUpLM3FaaWJyWXRWVVdcL1RtajdBPT0iLCJtYWMiOiIwNDFjYWJjYjdmY2VhYTIxMmE5NzI1OWE1NmQ2Zjc1NjI0MTIxMGI2NTY4MTQzNjkwNDdhZGE1ODI5ZTY0ODI5In0=', 'ENQ', 'Instruction', 'Information', 1, NULL, '2019-03-25', 100, 'Online', 0, 'Success', 'Failure', 2, 1, '2019-01-31 05:01:14', '2019-04-02 16:46:21'),
(2, 2, 2, 'New Admission Form', 1, '1,3', 1, 20, 'http://v2rsolution.co.in/academic-management/admission-form/eyJpdiI6IkhuSFp5MDR4a0l2dXdSRzNNb2s0anc9PSIsInZhbHVlIjoiTGoxZ0pYeGs3MEZaNzRBR0dOMmRndz09IiwibWFjIjoiNDkwNzhiYTZhODBkNzM5NDBjNmRkYmQ4MTNlNmMwZGQzZGVmNzg1NjM0MDQwZjIwOTBmNTFkZTMzNjljMTg0YyJ9', 'ADMISSION', 'test', 'test', 0, 2, '2019-04-26', 1000, 'Cash,Cheque,Swipe Machine', 1, 'success', 'failed', 1, 1, '2019-04-02 16:40:39', '2019-04-02 16:40:39'),
(3, 2, 2, 'JOB Form', 1, '1,3', 1, 20, 'http://v2rsolution.co.in/academic-management/admission-form/eyJpdiI6ImV1OXg2VTh0Ujc1XC9lbzJOcExkblBnPT0iLCJ2YWx1ZSI6Ino4cFRkdXhtMTNBSWVKZUVJNnRudlE9PSIsIm1hYyI6IjM3NzIxNDMwMjE5Y2ZkMzcxYjQ0NDMzMDNhYTgzOGU1OGQ0MWIyMDNkOWEzZTNhNWFjZGE1YTliY2U2NjhhYjYifQ==', 'recruitment', 'tst', 'testtt', 0, 1, '2019-04-30', 200, 'Cash,Cheque,Online,Swipe Machine', 1, 'testset', 'estsetset', 1, 1, '2019-04-02 16:43:25', '2019-04-02 16:43:25'),
(4, 2, 2, 'Enquiry form', 1, '1,3', 1, 30, 'http://v2rsolution.co.in/academic-management/enquiry-form/eyJpdiI6Ilh3NG9QWGZCMEhSa0tPeVpPcW5NRlE9PSIsInZhbHVlIjoiRDBnMTcrelBzZ3o2bnhSdGJQWWVNQT09IiwibWFjIjoiZDY5YmM2ODFiMDM4NWI1NmJjOWU2MDZiMTNlZmYxYWZkNjY2ZDUzY2ZjNjk3ZmY5MTIyNTlmNWExNGIyOTI0NCJ9', 'ENQUIRY', 'follow all rules', 'carefully fills form', 1, NULL, NULL, 0, NULL, 0, 'successfully saved', 'failed in enquiry try again', 2, 1, '2019-04-02 16:45:56', '2019-04-02 16:45:56'),
(5, 2, 2, 'Admission for 5th class', 5, '6', 6, 2, 'http://v2rsolution.co.in/academic-management/admission-form/eyJpdiI6InhKOEErejFqNWt5ZDlBclpVaEl0eXc9PSIsInZhbHVlIjoiYldrUlQwWjN5YkF0c1FzN0JLSnQrQT09IiwibWFjIjoiY2U0OTA2OWVhZWE4ZjBhMjIzN2JhNjNkY2Y1NWFiOGE4MWZkMTRiZmNiZWNlZGZkNjhiZWJjODliNzNmNzZjYiJ9', 'Prefix here', 'INSTRUCTION Information', 'Information', 0, 4, '2019-04-07', 500, 'Cash,Cheque,Online,Swipe Machine', 1, 'MESSAGE Information', 'Failure Message *', 2, 1, '2019-04-06 17:53:40', '2019-04-06 18:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `api_tokens`
--

CREATE TABLE `api_tokens` (
  `token_id` int(10) UNSIGNED NOT NULL,
  `device_id` text COLLATE utf8mb4_unicode_ci,
  `csrf_token` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `api_tokens`
--

INSERT INTO `api_tokens` (`token_id`, `device_id`, `csrf_token`, `created_at`, `updated_at`) VALUES
(1, '123', '1234', '2019-04-02 15:10:57', '2019-04-02 17:04:03'),
(2, 'f96f87752acd4b39', '$2y$10$853/GwK0ylaZU3dw9d/U5u43.T.QTJ9ej5w0OBBU91oA0Ohlfj3x.', '2019-04-02 17:11:46', '2019-06-18 15:48:56'),
(3, 'C98D22FB-2739-44BD-AE65-4907D6486562', '$2y$10$bhMjmXltNs2M5heuppNpDeGqiBlcLM9MpFVkg8cMT.TS6E/tCAdiu', '2019-04-04 03:03:43', '2019-07-04 16:43:52'),
(4, '66563bcb94abe993', '$2y$10$lMAPgKwcZRjDgVSW9gXnf.sPgjKFgVL4zAY5QAks4NIJuBXfEbSge', '2019-04-04 17:19:32', '2020-02-13 17:18:14'),
(5, '1A8B944B-CB90-4C53-9EE1-194ECC86A0C1', '$2y$10$e2gYppmA240i6gcMwYna4ehwBaUo2pfqZ9cTyyYmSN13NF0XqCuyu', '2019-04-05 16:39:09', '2019-06-27 14:54:40'),
(6, 'b36464a1bd65d423', '$2y$10$zGyLt9Mp9r4wqlAFdT7By.xm9tThBah0qIcgKJT.z0Hv0e0/l84im', '2019-04-05 18:08:00', '2019-05-17 02:31:21'),
(7, '161ba4209c4dd4dc', '$2y$10$cPyn5vdBQ.6wHsCXC7x3x.b5qFpFASg9jVKlJF9V8g7RtcFP9e98K', '2019-06-03 18:36:00', '2019-06-18 16:29:14'),
(8, '05CD290D-AB74-4050-9203-7809EB0EAD37', '$2y$10$bAJk7axOfAALA3i.8pVjAO5fLjvDua7YhNr/1pR9eCnMQg8JkByym', '2019-06-20 21:56:35', '2019-06-20 21:56:35'),
(9, 'ff4b77eb89e3c95e', '$2y$10$WZIHl2wWaM6ITdylOLd/h.PWMwBmEMIYtQOe45PqEH702FHpEUJvy', '2019-11-19 20:24:42', '2020-01-22 00:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `app_setting_id` int(10) UNSIGNED NOT NULL,
  `android_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_mandatory` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `comapny_page_url` text COLLATE utf8mb4_unicode_ci,
  `privacy_policy_url` text COLLATE utf8mb4_unicode_ci,
  `terms_condition_url` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assign_driver_conductor_routes`
--

CREATE TABLE `assign_driver_conductor_routes` (
  `assign_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_id` int(10) UNSIGNED DEFAULT NULL,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `conductor_id` int(10) UNSIGNED DEFAULT NULL,
  `route_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assign_driver_conductor_routes`
--

INSERT INTO `assign_driver_conductor_routes` (`assign_id`, `admin_id`, `update_by`, `vehicle_id`, `driver_id`, `conductor_id`, `route_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, NULL, NULL, 1, '2019-03-11 04:53:26', '2019-03-11 04:53:26'),
(2, 2, 2, 2, 5, 5, 2, '2019-03-11 04:53:26', '2019-04-02 18:07:15'),
(3, 2, 2, 3, NULL, NULL, 1, '2019-04-04 17:33:25', '2019-04-04 17:33:25'),
(4, 2, 2, 4, NULL, NULL, 2, '2019-04-04 17:33:25', '2019-04-04 17:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `bank_id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`bank_id`, `bank_name`, `bank_status`, `created_at`, `updated_at`) VALUES
(1, 'BOB', 1, '2019-03-13 12:43:34', '2019-03-13 12:43:34'),
(2, 'ICICI Bank', 1, '2019-04-02 17:13:44', '2019-04-02 17:13:44'),
(3, 'Bank of Broda', 1, '2019-04-02 17:13:53', '2019-04-02 17:13:53'),
(4, 'SBI', 1, '2019-04-02 17:14:05', '2019-04-02 17:14:05');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_category_id` int(10) UNSIGNED DEFAULT NULL,
  `book_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=General ,1=Barcoded',
  `book_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_isbn_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_id` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_no` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_id` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboardshelf_id` int(10) UNSIGNED DEFAULT NULL,
  `book_price` double(18,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `book_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `admin_id`, `update_by`, `book_category_id`, `book_name`, `book_type`, `book_subtitle`, `book_isbn_no`, `author_name`, `publisher_name`, `edition`, `book_vendor_id`, `book_copies_no`, `book_cupboard_id`, `book_cupboardshelf_id`, `book_price`, `book_remark`, `book_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 4, 'Book-1', 0, 'tttt', '54321111', 'p.k. vyas', 'ttttttttt', 'gggfg', 1, 0, 1, 1, 200.00, 'test', 1, '2019-04-02 18:30:59', '2019-04-02 18:30:59'),
(2, 2, 2, 3, 'Book-2', 0, 'dsdsdsds', 'dsdsd33333', 'p.k. vyas', 'ttttttttt', 'gggfg', 2, 0, 2, 4, 160.00, 'tested', 1, '2019-04-02 18:32:16', '2019-04-02 18:32:16');

-- --------------------------------------------------------

--
-- Table structure for table `book_allowance`
--

CREATE TABLE `book_allowance` (
  `book_allowance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `allow_for_staff` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `allow_for_student` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_allowance`
--

INSERT INTO `book_allowance` (`book_allowance_id`, `admin_id`, `update_by`, `allow_for_staff`, `allow_for_student`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 59, 25, '2019-01-23 09:39:33', '2019-04-05 22:31:09');

-- --------------------------------------------------------

--
-- Table structure for table `book_categories`
--

CREATE TABLE `book_categories` (
  `book_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_categories`
--

INSERT INTO `book_categories` (`book_category_id`, `admin_id`, `update_by`, `book_category_name`, `book_category_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'sport', 1, '2019-04-02 18:24:58', '2019-04-02 18:24:58'),
(2, 2, 2, 'story book', 1, '2019-04-02 18:25:08', '2019-04-02 18:25:08'),
(3, 2, 2, 'history book', 1, '2019-04-02 18:25:21', '2019-04-02 18:25:21'),
(4, 2, 2, 'academic book', 1, '2019-04-02 18:25:59', '2019-04-02 18:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `book_copies_info`
--

CREATE TABLE `book_copies_info` (
  `book_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `book_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exclusive_for_staff` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=No,1=Yes',
  `book_copy_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Available,1=Issued',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_copies_info`
--

INSERT INTO `book_copies_info` (`book_info_id`, `admin_id`, `update_by`, `book_id`, `book_unique_id`, `exclusive_for_staff`, `book_copy_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '309', 1, 0, '2019-04-02 18:30:59', '2019-04-02 18:30:59'),
(2, 2, 2, 1, '310', 0, 1, '2019-04-02 18:30:59', '2019-04-02 18:34:23'),
(3, 2, 2, 1, '311', 0, 1, '2019-04-02 18:30:59', '2019-04-02 18:35:06'),
(4, 2, 2, 1, '312', 0, 1, '2019-04-02 18:30:59', '2019-04-02 18:35:39'),
(5, 2, 2, 1, '313', 0, 1, '2019-04-02 18:30:59', '2019-04-02 18:36:12'),
(6, 2, 2, 1, '314', 0, 1, '2019-04-02 18:30:59', '2019-04-03 15:21:08'),
(7, 2, 2, 1, '315', 0, 1, '2019-04-02 18:30:59', '2019-04-03 15:21:26'),
(8, 2, 2, 1, '316', 0, 1, '2019-04-02 18:30:59', '2019-04-03 15:21:41'),
(9, 2, 2, 2, '400', 0, 1, '2019-04-02 18:32:16', '2019-04-02 18:35:20'),
(10, 2, 2, 2, '401', 0, 1, '2019-04-02 18:32:16', '2019-04-02 18:36:01'),
(11, 2, 2, 2, '402', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16'),
(12, 2, 2, 2, '403', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16'),
(13, 2, 2, 2, '404', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16'),
(14, 2, 2, 2, '405', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16'),
(15, 2, 2, 2, '406', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16'),
(16, 2, 2, 2, '407', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16'),
(17, 2, 2, 2, '408', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16'),
(18, 2, 2, 2, '409', 0, 0, '2019-04-02 18:32:16', '2019-04-02 18:32:16');

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboards`
--

CREATE TABLE `book_cupboards` (
  `book_cupboard_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboard_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_cupboards`
--

INSERT INTO `book_cupboards` (`book_cupboard_id`, `admin_id`, `update_by`, `book_cupboard_name`, `book_cupboard_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'CB-1', 1, '2019-04-02 18:27:22', '2019-04-02 18:27:22'),
(2, 2, 2, 'CB-2', 1, '2019-04-02 18:27:29', '2019-04-02 18:27:29');

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboardshelfs`
--

CREATE TABLE `book_cupboardshelfs` (
  `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboard_id` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboardshelf_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_capacity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_shelf_detail` text COLLATE utf8mb4_unicode_ci,
  `book_cupboardshelf_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_cupboardshelfs`
--

INSERT INTO `book_cupboardshelfs` (`book_cupboardshelf_id`, `admin_id`, `update_by`, `book_cupboard_id`, `book_cupboardshelf_name`, `book_cupboard_capacity`, `book_cupboard_shelf_detail`, `book_cupboardshelf_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'CBS-1', 5, 'teststet', 1, '2019-04-02 18:28:02', '2019-04-02 18:28:02'),
(2, 2, 2, 1, 'CBS-2', 20, 'ytyty', 1, '2019-04-02 18:28:21', '2019-04-02 18:28:21'),
(3, 2, 2, 2, 'CBS-2', 30, 'ujuiuyy', 1, '2019-04-02 18:28:44', '2019-04-02 18:28:44'),
(4, 2, 2, 2, 'CBS-3', 10, 'tested', 1, '2019-04-02 18:29:13', '2019-04-02 18:29:13'),
(5, 2, 2, 2, 'CBS-1', 20, 'tytyty', 1, '2019-04-02 18:29:37', '2019-04-02 18:29:37');

-- --------------------------------------------------------

--
-- Table structure for table `book_vendors`
--

CREATE TABLE `book_vendors` (
  `book_vendor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_vendor_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_vendors`
--

INSERT INTO `book_vendors` (`book_vendor_id`, `admin_id`, `update_by`, `book_vendor_name`, `book_vendor_number`, `book_vendor_email`, `book_vendor_address`, `book_vendor_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'test', '5896471253', 'test@gmail.com', 'dsdsdsdsdsd', 1, '2019-04-02 18:26:46', '2019-04-02 18:26:46'),
(2, 2, 2, 'test 2', '7777777777', 'test002@gmail.com', 'rewewwwew', 1, '2019-04-02 18:27:04', '2019-04-02 18:27:04');

-- --------------------------------------------------------

--
-- Table structure for table `brochures`
--

CREATE TABLE `brochures` (
  `brochure_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `brochure_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brochure_upload_date` date DEFAULT NULL,
  `brochure_file` text COLLATE utf8mb4_unicode_ci,
  `brochure_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brochures`
--

INSERT INTO `brochures` (`brochure_id`, `admin_id`, `update_by`, `session_id`, `brochure_name`, `brochure_upload_date`, `brochure_file`, `brochure_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'brochure', '2019-01-31', 'dinesh172721548909880.jpg', 1, '2019-01-31 04:44:40', '2019-01-31 04:44:40'),
(2, 2, 2, 1, 'brochure -2', '2019-04-02', 'Admin dashboard-06.02.19.833501554185366docx', 1, '2019-04-02 16:39:26', '2019-04-02 16:39:26'),
(3, 2, 2, 3, 'Great man', '2019-04-02', '11632191554209943.png', 1, '2019-04-02 23:29:03', '2019-04-02 23:29:03'),
(4, 2, 2, 5, 'Sourabh Brochure 1', '2019-04-06', 'Tulips795341554534938.jpg', 1, '2019-04-06 17:39:17', '2019-04-06 17:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `caste`
--

CREATE TABLE `caste` (
  `caste_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `caste_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `caste`
--

INSERT INTO `caste` (`caste_id`, `admin_id`, `update_by`, `caste_name`, `caste_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Jain', 1, '2019-01-23 10:45:22', '2019-01-23 10:45:22'),
(2, 2, 2, 'Sikhism', 1, '2019-04-05 21:10:58', '2019-04-05 21:10:58');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `certificate_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `template_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=TC,1=CC,2=COMP',
  `template_for` tinyint(4) DEFAULT NULL COMMENT '0=Participation,1=Winner',
  `template_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_no_start` int(10) UNSIGNED DEFAULT NULL,
  `book_no_end` int(10) UNSIGNED DEFAULT NULL,
  `serial_no_start` int(10) UNSIGNED DEFAULT NULL,
  `serial_no_end` int(10) UNSIGNED DEFAULT NULL,
  `certificate_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`certificate_id`, `admin_id`, `update_by`, `template_name`, `template_type`, `template_for`, `template_link`, `book_no_start`, `book_no_end`, `serial_no_start`, `serial_no_end`, `certificate_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Participation Certificate', 2, 0, 'participation_template1', 0, 0, 0, 0, 0, '2019-01-23 09:57:31', '2019-01-23 09:57:31'),
(2, 1, 1, 'Winner Certificate', 2, 1, 'winner_template1', 0, 0, 0, 0, 0, '2019-01-23 09:57:31', '2019-01-23 09:57:31'),
(3, 1, 1, 'Character Certificate 1', 1, 0, 'character_template1', 0, 0, 0, 0, 0, '2019-01-23 09:57:31', '2019-01-23 09:57:31'),
(4, 1, 1, 'Character Certificate 2', 1, 0, 'character_template2', 0, 0, 0, 0, 1, '2019-01-23 09:57:31', '2019-01-23 09:57:31'),
(5, 1, 2, 'Transfer Certificate 1', 0, 0, 'transfer_template1', 10, 100, 50, 500, 1, '2019-01-23 09:57:31', '2019-04-03 20:51:44'),
(6, 1, 1, 'Transfer Certificate 2', 0, 0, 'transfer_template2', 0, 0, 0, 0, 0, '2019-01-23 09:57:31', '2019-01-23 09:57:31'),
(7, 1, 1, 'Transfer Certificate 3', 0, 0, 'transfer_template3', 0, 0, 0, 0, 0, '2019-01-23 09:57:31', '2019-01-23 09:57:31');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `state_id`, `city_name`, `city_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Jodhpur', 1, '2019-01-23 10:47:40', '2019-01-23 10:47:40'),
(2, 1, 'Jaipur', 1, '2019-01-23 10:47:57', '2019-01-23 10:47:57'),
(3, 2, 'state city 123', 1, '2019-04-02 18:47:18', '2019-04-02 18:47:18'),
(4, 3, 'Sydney', 1, '2019-04-05 21:18:50', '2019-04-05 21:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `board_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `class_stream` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `admin_id`, `session_id`, `update_by`, `class_name`, `class_order`, `board_id`, `medium_type`, `class_stream`, `class_status`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, 2, 'Class I', 0, 1, 1, 0, 1, '2019-01-23 10:35:00', '2019-04-03 17:17:24'),
(2, 2, NULL, 2, 'class X', 0, 2, 2, 1, 1, '2019-04-02 16:18:04', '2019-04-02 16:18:04'),
(3, 2, NULL, 2, 'class II', 0, 1, 1, 0, 1, '2019-04-02 16:18:27', '2019-04-02 16:18:27'),
(4, 2, NULL, 2, 'Non medical', 20, 6, 4, 1, 1, '2019-04-02 20:43:33', '2019-04-03 17:18:24'),
(5, 2, NULL, 2, 'Cup of class', 10, 7, 5, 0, 1, '2019-04-03 18:01:10', '2019-04-03 18:01:10'),
(6, 2, NULL, 2, 'Fifth', 12, 8, 6, 1, 1, '2019-04-05 21:20:32', '2019-04-05 21:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `class_subject_staff_map`
--

CREATE TABLE `class_subject_staff_map` (
  `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_ids` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_subject_staff_map`
--

INSERT INTO `class_subject_staff_map` (`class_subject_staff_map_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `subject_id`, `staff_ids`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 2, 1, '2019-02-07 05:38:54', '2019-02-07 05:38:54'),
(2, 2, 2, 1, 1, 2, 4, 1, '2019-02-07 05:38:54', '2019-04-02 16:32:53'),
(3, 2, 2, 1, 1, 3, 3, 1, '2019-02-07 05:38:54', '2019-04-02 16:32:53'),
(4, 2, 2, 1, 1, 4, 1, 1, '2019-02-07 05:38:55', '2019-02-07 05:38:55'),
(5, 2, 2, 5, 5, 6, 6, 1, '2019-04-03 18:46:28', '2019-04-04 15:28:16'),
(6, 2, 2, 5, 5, 7, 4, 1, '2019-04-03 18:46:28', '2019-04-03 18:46:45'),
(7, 2, 2, 5, 5, 8, 2, 1, '2019-04-03 18:46:28', '2019-04-03 18:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `communication_message`
--

CREATE TABLE `communication_message` (
  `comm_message_id` int(10) UNSIGNED NOT NULL,
  `student_parent_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `sender_type` tinyint(4) NOT NULL COMMENT '1=Staff,2=Parent',
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `communication_message`
--

INSERT INTO `communication_message` (`comm_message_id`, `student_parent_id`, `staff_id`, `sender_id`, `sender_type`, `message`, `created_at`, `updated_at`) VALUES
(1, 6, 6, 6, 1, 'Hiiii', '2019-04-05 19:54:59', '2019-04-05 19:54:59'),
(2, 5, 1, 1, 1, 'Hi', '2019-04-17 22:24:49', '2019-04-17 22:24:49'),
(3, 5, 1, 1, 1, 'hi', '2019-04-17 22:24:57', '2019-04-17 22:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `competitions`
--

CREATE TABLE `competitions` (
  `competition_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `competition_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `competition_description` text COLLATE utf8mb4_unicode_ci,
  `competition_date` date DEFAULT NULL,
  `competition_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=School,1=Class',
  `competition_class_ids` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `competition_issue_participation_certificate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `competition_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `competitions`
--

INSERT INTO `competitions` (`competition_id`, `admin_id`, `update_by`, `session_id`, `competition_name`, `competition_description`, `competition_date`, `competition_level`, `competition_class_ids`, `medium_type`, `competition_issue_participation_certificate`, `competition_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, NULL, 'test', 'ddddddd', '2020-01-31', 0, NULL, 1, 1, 1, '2019-01-29 10:10:39', '2019-01-31 06:16:23'),
(2, 2, 2, NULL, 'Cultural Competition', 'all preparation arranged by school and this competition will enjoy all the students', '2019-04-26', 0, NULL, 1, 1, 1, '2019-04-02 16:35:32', '2019-04-02 16:35:32');

-- --------------------------------------------------------

--
-- Table structure for table `competition_map`
--

CREATE TABLE `competition_map` (
  `student_competition_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `competition_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `position_rank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `issue_certificate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `competition_map`
--

INSERT INTO `competition_map` (`student_competition_map_id`, `admin_id`, `update_by`, `competition_id`, `student_id`, `position_rank`, `issue_certificate`, `created_at`, `updated_at`) VALUES
(7, 2, 2, 1, 7, '1', 0, '2019-02-12 11:21:40', '2019-02-12 11:22:17'),
(8, 2, 2, 1, 6, '2', 0, '2019-02-12 11:21:41', '2019-02-12 11:22:18'),
(9, 2, 2, 2, 8, '2', 0, '2019-04-02 16:35:56', '2019-04-02 16:37:21'),
(10, 2, 2, 2, 7, '5', 0, '2019-04-02 16:35:57', '2019-04-02 16:37:21'),
(11, 2, 2, 2, 6, '3', 0, '2019-04-02 16:35:57', '2019-04-02 16:37:21'),
(12, 2, 2, 2, 5, '4', 0, '2019-04-02 16:35:57', '2019-04-02 16:37:21'),
(13, 2, 2, 2, 3, '1', 0, '2019-04-02 16:35:57', '2019-04-02 16:37:21'),
(14, 2, 2, 2, 1, '6', 0, '2019-04-02 16:35:57', '2019-04-02 16:37:21'),
(15, 2, 2, 2, 2, '', 0, '2019-04-02 16:36:23', '2019-04-02 16:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `concession_map`
--

CREATE TABLE `concession_map` (
  `concession_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `reason_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` text COLLATE utf8mb4_unicode_ci,
  `concession_amt` int(10) UNSIGNED DEFAULT NULL,
  `concession_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unpaid,1=Paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `concession_map`
--

INSERT INTO `concession_map` (`concession_map_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `head_id`, `reason_id`, `head_type`, `concession_amt`, `concession_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 6, 2, 1, 'ONETIME', 500, 1, '2019-03-12 06:01:46', '2019-03-12 11:45:45'),
(2, 2, 2, 1, 1, 6, 3, 3, 'ONETIME', 500, 0, '2019-03-12 06:01:46', '2019-04-02 17:17:37'),
(3, 2, 2, 1, 1, 2, 2, 2, 'ONETIME', 200, 0, '2019-04-02 17:18:23', '2019-04-02 17:18:23'),
(4, 2, 2, 1, 1, 2, 3, 3, 'ONETIME', 1000, 0, '2019-04-02 17:18:23', '2019-04-02 17:18:23'),
(5, 2, 2, 1, 1, 2, 2, 2, 'ONETIME', 200, 0, '2019-04-02 17:18:36', '2019-04-02 17:18:36'),
(6, 2, 2, 1, 1, 2, 3, 3, 'ONETIME', 1000, 0, '2019-04-02 17:18:36', '2019-04-02 17:18:36'),
(7, 2, 2, 1, 1, 2, 3, 1, 'RECURRING', 500, 0, '2019-04-02 17:18:36', '2019-04-02 17:18:36');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `country_status`, `created_at`, `updated_at`) VALUES
(1, 'India', 1, '2019-01-23 10:47:08', '2019-01-23 10:47:08'),
(2, '123', 1, '2019-04-02 18:46:22', '2019-04-02 18:46:22'),
(3, 'Australia', 1, '2019-04-05 21:17:52', '2019-04-05 21:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `co_scholastic_types`
--

CREATE TABLE `co_scholastic_types` (
  `co_scholastic_type_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `update_by` int(10) UNSIGNED NOT NULL,
  `co_scholastic_type_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `co_scholastic_type_description` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `co_scholastic_types`
--

INSERT INTO `co_scholastic_types` (`co_scholastic_type_id`, `admin_id`, `update_by`, `co_scholastic_type_name`, `co_scholastic_type_description`, `co_scholastic_type_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'D', 'D', 1, '2019-02-26 14:12:04', '2019-02-26 14:12:04'),
(2, 2, 2, 'co-scholastic-1', 'allowed...', 1, '2019-04-02 16:28:38', '2019-04-02 16:28:38'),
(3, 2, 2, 'Sourabh Co scholastic', 'Description box here', 1, '2019-04-03 18:03:34', '2019-04-03 18:06:57');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `designation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_description` text COLLATE utf8mb4_unicode_ci,
  `teaching_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Non Teaching,1=Teaching',
  `designation_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `admin_id`, `update_by`, `designation_name`, `designation_description`, `teaching_status`, `designation_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Class Teacher', NULL, 1, 1, '2019-01-23 10:45:00', '2019-01-23 10:45:00'),
(2, 2, 2, 'Subject Teacher', NULL, 1, 1, '2019-01-23 10:45:11', '2019-01-23 10:45:11'),
(3, 2, 2, '123', NULL, 1, 1, '2019-04-02 18:45:04', '2019-04-02 18:45:04'),
(4, 2, 2, '12', NULL, 1, 1, '2019-04-02 18:45:17', '2019-04-02 18:45:17'),
(5, 2, 2, '1', NULL, 1, 1, '2019-04-02 18:45:40', '2019-04-02 18:45:40'),
(6, 2, 2, 'test', NULL, 1, 1, '2019-04-02 22:50:45', '2019-04-02 22:58:35'),
(7, 2, 2, 'Sourabh Monitor', NULL, 1, 1, '2019-04-05 21:08:44', '2019-04-05 21:08:59');

-- --------------------------------------------------------

--
-- Table structure for table `document_category`
--

CREATE TABLE `document_category` (
  `document_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `document_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=staff,2=both',
  `document_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_category`
--

INSERT INTO `document_category` (`document_category_id`, `admin_id`, `update_by`, `document_category_name`, `document_category_for`, `document_category_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Common Document', 2, 1, '2019-01-23 10:46:37', '2019-04-05 21:07:56'),
(2, 2, 2, 'Sourabh required documents', 1, 1, '2019-04-05 21:01:47', '2019-04-05 21:02:17');

-- --------------------------------------------------------

--
-- Table structure for table `esi_setting`
--

CREATE TABLE `esi_setting` (
  `esi_setting_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `employee` double(8,2) DEFAULT NULL,
  `employer` double(8,2) DEFAULT NULL,
  `esi_cut_off` double(18,2) DEFAULT NULL,
  `round_off` tinyint(4) DEFAULT NULL COMMENT '0=Nearest Rupee,1=Highest Rupee',
  `esi_setting_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `esi_setting`
--

INSERT INTO `esi_setting` (`esi_setting_id`, `admin_id`, `update_by`, `session_id`, `employee`, `employer`, `esi_cut_off`, `round_off`, `esi_setting_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 12.00, 10.00, 2.00, 1, 1, '2019-04-02 18:45:06', '2019-04-02 18:45:06'),
(2, 2, 2, 1, 25.00, 10.00, 5.00, 0, 1, '2019-04-02 18:45:30', '2019-04-02 18:45:30');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `term_exam_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`exam_id`, `admin_id`, `update_by`, `session_id`, `term_exam_id`, `exam_name`, `medium_type`, `exam_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, NULL, 1, 'Mid Term 1', 1, 1, '2019-01-25 09:57:26', '2019-01-25 09:57:26'),
(2, 2, 2, NULL, 1, 'Exam New', 1, 1, '2019-02-27 10:59:08', '2019-02-27 10:59:08'),
(3, 2, 2, NULL, 2, 'first test', 1, 1, '2019-04-02 16:56:05', '2019-04-02 16:56:05'),
(4, 2, 2, NULL, 3, 'S Exam', 1, 1, '2019-04-06 18:22:24', '2019-04-06 18:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `exam_map`
--

CREATE TABLE `exam_map` (
  `exam_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `marks_criteria_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_map`
--

INSERT INTO `exam_map` (`exam_map_id`, `admin_id`, `update_by`, `exam_id`, `session_id`, `class_id`, `section_id`, `subject_id`, `marks_criteria_id`, `grade_scheme_id`, `created_at`, `updated_at`) VALUES
(5, 2, 2, 1, 1, 1, 1, 1, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:21:53'),
(6, 2, 2, 1, 1, 1, 1, 2, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:21:53'),
(7, 2, 2, 1, 1, 1, 1, 3, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:21:53'),
(8, 2, 2, 1, 1, 1, 1, 4, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:21:53'),
(9, 2, 2, 1, 1, 1, 2, 1, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:22:15'),
(10, 2, 2, 1, 1, 1, 2, 2, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:22:15'),
(11, 2, 2, 1, 1, 1, 2, 3, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:22:15'),
(12, 2, 2, 1, 1, 1, 2, 4, 1, 1, '2019-01-28 07:49:14', '2019-02-06 10:22:15'),
(22, 2, 2, 2, 1, 1, 1, 1, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:04:58'),
(23, 2, 2, 2, 1, 1, 1, 2, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:04:58'),
(24, 2, 2, 2, 1, 1, 1, 3, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:04:58'),
(25, 2, 2, 2, 1, 1, 1, 4, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:04:58'),
(26, 2, 2, 2, 1, 1, 1, 5, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:04:58'),
(27, 2, 2, 2, 1, 1, 2, 1, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:05:12'),
(28, 2, 2, 2, 1, 1, 2, 2, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:05:12'),
(29, 2, 2, 2, 1, 1, 2, 3, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:05:12'),
(30, 2, 2, 2, 1, 1, 2, 4, 1, 1, '2019-02-27 11:03:07', '2019-02-27 11:05:12'),
(32, 2, 2, 3, 1, 1, 2, 2, 2, NULL, '2019-04-02 16:59:15', '2019-04-02 17:01:26'),
(33, 2, 2, 3, 1, 1, 2, 3, 2, NULL, '2019-04-02 16:59:15', '2019-04-02 17:01:26'),
(34, 2, 2, 3, 1, 1, 2, 4, 2, NULL, '2019-04-02 16:59:15', '2019-04-02 17:01:26'),
(35, 2, 2, 3, 1, 1, 1, 1, 1, 2, '2019-04-02 16:59:15', '2019-04-02 17:01:55'),
(36, 2, 2, 3, 1, 1, 1, 2, 1, 2, '2019-04-02 16:59:15', '2019-04-02 17:01:55'),
(37, 2, 2, 3, 1, 1, 1, 3, 1, 2, '2019-04-02 16:59:15', '2019-04-02 17:01:55'),
(38, 2, 2, 3, 1, 1, 1, 4, 1, 2, '2019-04-02 16:59:15', '2019-04-02 17:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `exam_marks`
--

CREATE TABLE `exam_marks` (
  `exam_mark_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_schedule_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `marks` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attend_flag` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Absent,1=Present',
  `marks_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Not submit,1=Marks submit'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_marks`
--

INSERT INTO `exam_marks` (`exam_mark_id`, `admin_id`, `update_by`, `session_id`, `exam_schedule_id`, `exam_id`, `class_id`, `section_id`, `student_id`, `subject_id`, `marks`, `created_at`, `updated_at`, `attend_flag`, `marks_status`) VALUES
(1, 2, 2, 1, NULL, 1, 1, 2, 2, 1, 91, '2019-02-20 09:53:07', '2019-02-20 09:53:07', 1, 1),
(2, 2, 2, 1, NULL, 1, 1, 2, 2, 2, 98, '2019-02-20 09:53:07', '2019-02-20 09:53:07', 1, 1),
(3, 2, 2, 1, NULL, 1, 1, 2, 2, 3, 90, '2019-02-20 09:53:07', '2019-02-20 09:53:07', 1, 1),
(4, 2, 2, 1, NULL, 1, 1, 2, 2, 4, 88, '2019-02-20 09:53:07', '2019-02-20 09:53:07', 1, 1),
(5, 2, 2, 1, NULL, 1, 1, 1, 7, 1, 80, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(6, 2, 2, 1, NULL, 1, 1, 1, 7, 2, 65, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(7, 2, 2, 1, NULL, 1, 1, 1, 7, 3, 75, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(8, 2, 2, 1, NULL, 1, 1, 1, 7, 4, 78, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(9, 2, 2, 1, NULL, 1, 1, 1, 6, 1, 75, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(10, 2, 2, 1, NULL, 1, 1, 1, 6, 2, 55, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(11, 2, 2, 1, NULL, 1, 1, 1, 6, 3, 60, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(12, 2, 2, 1, NULL, 1, 1, 1, 6, 4, 69, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(13, 2, 2, 1, NULL, 1, 1, 1, 5, 1, 58, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(14, 2, 2, 1, NULL, 1, 1, 1, 5, 2, 90, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(15, 2, 2, 1, NULL, 1, 1, 1, 5, 3, 85, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(16, 2, 2, 1, NULL, 1, 1, 1, 5, 4, 76, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(17, 2, 2, 1, NULL, 1, 1, 1, 3, 1, 48, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(18, 2, 2, 1, NULL, 1, 1, 1, 3, 2, 55, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(19, 2, 2, 1, NULL, 1, 1, 1, 3, 3, 55, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(20, 2, 2, 1, NULL, 1, 1, 1, 3, 4, 15, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(21, 2, 2, 1, NULL, 1, 1, 1, 1, 1, 91, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(22, 2, 2, 1, NULL, 1, 1, 1, 1, 2, 98, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(23, 2, 2, 1, NULL, 1, 1, 1, 1, 3, 90, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(24, 2, 2, 1, NULL, 1, 1, 1, 1, 4, 88, '2019-02-20 09:54:26', '2019-02-20 09:54:26', 1, 1),
(25, 2, 2, 1, NULL, 2, 1, 1, 6, 1, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(26, 2, 2, 1, NULL, 2, 1, 1, 6, 2, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(27, 2, 2, 1, NULL, 2, 1, 1, 6, 3, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(28, 2, 2, 1, NULL, 2, 1, 1, 6, 4, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(29, 2, 2, 1, NULL, 2, 1, 1, 6, 5, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(30, 2, 2, 1, NULL, 2, 1, 1, 5, 1, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(31, 2, 2, 1, NULL, 2, 1, 1, 5, 2, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(32, 2, 2, 1, NULL, 2, 1, 1, 5, 3, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(33, 2, 2, 1, NULL, 2, 1, 1, 5, 4, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(34, 2, 2, 1, NULL, 2, 1, 1, 5, 5, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(35, 2, 2, 1, NULL, 2, 1, 1, 3, 1, 65, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(36, 2, 2, 1, NULL, 2, 1, 1, 3, 2, 60, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(37, 2, 2, 1, NULL, 2, 1, 1, 3, 3, 75, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(38, 2, 2, 1, NULL, 2, 1, 1, 3, 4, 84, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(39, 2, 2, 1, NULL, 2, 1, 1, 3, 5, 55, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(40, 2, 2, 1, NULL, 2, 1, 1, 1, 1, 88, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(41, 2, 2, 1, NULL, 2, 1, 1, 1, 2, 87, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(42, 2, 2, 1, NULL, 2, 1, 1, 1, 3, 82, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(43, 2, 2, 1, NULL, 2, 1, 1, 1, 4, 80, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(44, 2, 2, 1, NULL, 2, 1, 1, 1, 5, 88, '2019-02-27 11:21:01', '2019-02-27 11:21:01', 1, 1),
(45, 2, 2, 1, NULL, 2, 1, 2, 2, 1, 88, '2019-02-27 11:21:14', '2019-02-27 11:27:36', 1, 1),
(46, 2, 2, 1, NULL, 2, 1, 2, 2, 2, 88, '2019-02-27 11:21:14', '2019-02-27 11:27:36', 1, 1),
(47, 2, 2, 1, NULL, 2, 1, 2, 2, 3, 85, '2019-02-27 11:21:14', '2019-02-27 11:27:36', 1, 1),
(48, 2, 2, 1, NULL, 2, 1, 2, 2, 4, 88, '2019-02-27 11:21:14', '2019-02-27 11:27:36', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam_schedules`
--

CREATE TABLE `exam_schedules` (
  `exam_schedule_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `schedule_name` text COLLATE utf8mb4_unicode_ci,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_schedules`
--

INSERT INTO `exam_schedules` (`exam_schedule_id`, `admin_id`, `update_by`, `schedule_name`, `session_id`, `exam_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Schedule', 1, 1, '2019-01-25 10:10:06', '2019-01-25 10:10:06'),
(2, 2, 2, 'Schedule 1', 1, 2, '2019-02-27 11:05:39', '2019-02-27 11:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `facility_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `facility_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_fees_applied` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=not apply,1=apply',
  `facility_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fees_st_map`
--

CREATE TABLE `fees_st_map` (
  `fees_st_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` text COLLATE utf8mb4_unicode_ci,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_ids` text COLLATE utf8mb4_unicode_ci COMMENT 'student ids are stored',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fees_st_map`
--

INSERT INTO `fees_st_map` (`fees_st_map_id`, `admin_id`, `update_by`, `session_id`, `head_id`, `head_type`, `class_id`, `section_id`, `student_ids`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 'ONETIME', 1, 1, '7,6', '2019-02-14 06:19:18', '2019-02-14 06:19:18'),
(2, 2, 2, 1, 1, 'ONETIME', 1, 1, '7,6', '2019-02-14 06:19:21', '2019-02-14 06:19:21'),
(3, 2, 2, 1, 1, 'ONETIME', 1, 1, '7,6', '2019-02-14 06:19:22', '2019-02-14 06:19:22'),
(4, 2, 2, 1, 1, 'ONETIME', 1, 1, '7,6', '2019-02-14 06:19:26', '2019-02-14 06:19:26'),
(5, 2, 2, 1, 3, 'ONETIME', 1, 1, '8,6,5,3,1', '2019-03-08 12:23:28', '2019-03-08 12:23:28'),
(6, 2, 2, 1, 2, 'ONETIME', 1, 1, '5', '2019-03-14 05:45:20', '2019-03-14 05:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `fee_receipt`
--

CREATE TABLE `fee_receipt` (
  `receipt_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `receipt_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total receipt amount',
  `wallet_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Wallet amount',
  `total_concession_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Concession amount',
  `total_fine_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Fine amount',
  `total_calculate_fine_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Calculate Fine amount',
  `net_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total net amount',
  `pay_later_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Pay later amount',
  `imprest_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Imprest amount',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_date` date DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'If cheque mode',
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If online mode',
  `transaction_date` date DEFAULT NULL COMMENT 'If online mode',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Cancel,1=Approve',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_receipt`
--

INSERT INTO `fee_receipt` (`receipt_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `receipt_number`, `receipt_date`, `total_amount`, `wallet_amount`, `total_concession_amount`, `total_fine_amount`, `total_calculate_fine_amount`, `net_amount`, `pay_later_amount`, `imprest_amount`, `bank_id`, `pay_mode`, `cheque_date`, `cheque_number`, `cheque_amt`, `transaction_id`, `transaction_date`, `description`, `receipt_status`, `created_at`, `updated_at`) VALUES
(30, 2, 2, 1, 1, 6, '30', '2019-03-25', 6000.00, NULL, NULL, NULL, NULL, 5000.00, NULL, 1000.00, NULL, 'offline', NULL, NULL, NULL, NULL, NULL, 'D', 1, '2019-03-14 05:36:04', '2019-03-14 05:36:04'),
(31, 2, 2, 1, 1, 8, '31', '2019-04-18', 5000.00, NULL, 0.00, NULL, NULL, 11000.00, 4000.00, NULL, NULL, 'offline', NULL, NULL, NULL, NULL, NULL, 'test', 1, '2019-04-02 17:20:40', '2019-04-02 17:20:40'),
(32, 2, 2, 1, 1, 6, '32', '2019-04-27', 10000.00, NULL, 500.00, NULL, NULL, 16000.00, 5500.00, NULL, NULL, 'cheque', '2019-04-25', '85746', 10000.00, NULL, NULL, 'test', 1, '2019-04-02 17:23:38', '2019-04-02 17:23:38'),
(33, 2, 2, 1, 1, 8, '33', '2019-04-25', 8000.00, NULL, NULL, NULL, NULL, 8010.00, 10.00, NULL, NULL, 'cheque', '2019-04-25', '45454', 10000.00, NULL, NULL, 'tttttttt', 1, '2019-04-03 16:25:06', '2019-04-03 16:25:06'),
(34, 2, 2, 1, 1, 8, '34', '2019-04-25', 10000.00, NULL, NULL, NULL, NULL, 11000.00, 1000.00, NULL, NULL, 'online', NULL, NULL, NULL, '785963254', '2019-04-18', 'etstest', 1, '2019-04-04 19:53:38', '2019-04-04 19:53:38');

-- --------------------------------------------------------

--
-- Table structure for table `fee_receipt_details`
--

CREATE TABLE `fee_receipt_details` (
  `fee_receipt_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `receipt_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `head_month_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `head_inst_id` int(10) UNSIGNED DEFAULT NULL,
  `inst_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total installment amount',
  `inst_paid_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total paid installment amount',
  `inst_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Less,1=complete',
  `r_concession_map_id` int(10) UNSIGNED DEFAULT NULL,
  `r_concession_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Concession amount',
  `r_fine_id` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_detail_id` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_detail_days` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Fine amount',
  `cancel_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `map_student_staff_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_receipt_details`
--

INSERT INTO `fee_receipt_details` (`fee_receipt_detail_id`, `admin_id`, `update_by`, `receipt_id`, `head_id`, `head_type`, `head_month_year`, `head_inst_id`, `inst_amt`, `inst_paid_amt`, `inst_status`, `r_concession_map_id`, `r_concession_amt`, `r_fine_id`, `r_fine_detail_id`, `r_fine_detail_days`, `r_fine_amt`, `cancel_status`, `created_at`, `updated_at`, `map_student_staff_id`) VALUES
(23, 2, 2, 30, 1, 'TRANSPORT', '2019-01', NULL, 5000.00, 5000.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-03-14 05:36:04', '2019-03-14 05:36:04', 1),
(24, 2, 2, 31, 2, 'ONETIME', NULL, NULL, 1000.00, 0.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-02 17:20:40', '2019-04-02 17:20:40', NULL),
(25, 2, 2, 31, 1, 'RECURRING', NULL, 1, 10000.00, 5000.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-02 17:20:40', '2019-04-02 17:20:40', NULL),
(26, 2, 2, 32, 2, 'ONETIME', NULL, NULL, 1000.00, 0.00, 0, 1, 500.00, NULL, NULL, NULL, NULL, 0, '2019-04-02 17:23:38', '2019-04-02 17:23:38', NULL),
(27, 2, 2, 32, 1, 'RECURRING', NULL, 1, 10000.00, 6000.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-02 17:23:38', '2019-04-02 17:23:38', NULL),
(28, 2, 2, 32, 1, 'TRANSPORT', '2018-12', NULL, 5000.00, 5000.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-02 17:23:38', '2019-04-02 17:23:38', 1),
(29, 2, 2, 33, 2, 'ONETIME', NULL, NULL, 1000.00, 990.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-03 16:25:06', '2019-04-03 16:25:06', NULL),
(30, 2, 2, 33, 1, 'RECURRING', NULL, 1, 5000.00, 5000.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-03 16:25:06', '2019-04-03 16:25:06', NULL),
(31, 2, 2, 33, 1, 'IMPREST', NULL, NULL, 1500.00, 1500.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-03 16:25:06', '2019-04-03 16:25:06', NULL),
(32, 2, 2, 33, 2, 'IMPREST', NULL, NULL, 510.00, 510.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-03 16:25:06', '2019-04-03 16:25:06', NULL),
(33, 2, 2, 34, 2, 'RECURRING', NULL, 2, 10000.00, 9000.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-04 19:53:38', '2019-04-04 19:53:38', NULL),
(34, 2, 2, 34, 3, 'RECURRING', NULL, 3, 1000.00, 1000.00, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-04-04 19:53:38', '2019-04-04 19:53:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fine`
--

CREATE TABLE `fine` (
  `fine_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `fine_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fine_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=One Time Fees,1=Recurring Fees',
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `fine_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fine`
--

INSERT INTO `fine` (`fine_id`, `admin_id`, `update_by`, `fine_name`, `fine_for`, `head_id`, `fine_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'late fee submission', 0, 2, 1, '2019-04-02 17:16:16', '2019-04-02 17:16:16'),
(2, 2, 2, 'test', 1, 1, 1, '2019-04-02 17:16:54', '2019-10-18 22:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `fine_details`
--

CREATE TABLE `fine_details` (
  `fine_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `fine_id` int(10) UNSIGNED DEFAULT NULL,
  `fine_detail_days` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fine_detail_amt` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fine_details`
--

INSERT INTO `fine_details` (`fine_detail_id`, `admin_id`, `update_by`, `fine_id`, `fine_detail_days`, `fine_detail_amt`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '5', 30, '2019-04-02 17:16:16', '2019-04-02 17:16:16'),
(2, 2, 2, 2, '10', 100, '2019-04-02 17:16:54', '2019-04-02 17:16:54');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `grade_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `grade_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_min` int(10) UNSIGNED DEFAULT NULL,
  `grade_max` int(10) UNSIGNED DEFAULT NULL,
  `grade_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_point` double DEFAULT NULL,
  `grade_remark` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`grade_id`, `admin_id`, `update_by`, `grade_scheme_id`, `grade_min`, `grade_max`, `grade_name`, `grade_point`, `grade_remark`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 80, 100, 'A', 100, 'A Grade', '2019-01-25 09:58:35', '2019-01-25 09:58:35'),
(2, 2, 2, 1, 60, 80, 'B', 80, 'B Grade', '2019-01-25 09:58:35', '2019-01-25 09:58:35'),
(3, 2, 2, 2, 100, 500, 'percentage', 100, 'in percentage', '2019-04-02 16:57:10', '2019-04-02 16:57:10'),
(6, 2, 2, 3, 40, 100, 'Old ones', 10, 'This is for old student !!', '2019-04-06 18:27:16', '2019-04-06 18:27:16'),
(7, 2, 2, 3, 33, 100, 'new student grades', 2, 'This is for new student !!', '2019-04-06 18:28:08', '2019-04-06 18:28:08');

-- --------------------------------------------------------

--
-- Table structure for table `grade_schemes`
--

CREATE TABLE `grade_schemes` (
  `grade_scheme_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `scheme_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Percent,1=Marks',
  `scheme_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade_schemes`
--

INSERT INTO `grade_schemes` (`grade_scheme_id`, `admin_id`, `update_by`, `scheme_name`, `grade_type`, `scheme_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Scheme 1', 0, 1, '2019-01-25 09:58:35', '2019-01-25 09:58:35'),
(2, 2, 2, 'Scheme -2', 1, 1, '2019-04-02 16:57:10', '2019-04-02 16:57:10'),
(3, 2, 2, 'S scheme', 1, 1, '2019-04-06 18:25:53', '2019-04-06 18:25:53');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `admin_id`, `update_by`, `group_name`, `group_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'SUPO Group for support', 1, '2019-04-05 21:13:13', '2019-04-05 21:13:13');

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `holiday_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `holiday_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holiday_start_date` date DEFAULT NULL,
  `holiday_end_date` date DEFAULT NULL,
  `holiday_for` tinyint(4) DEFAULT NULL COMMENT '0=Both,1=Students,2=Staff',
  `holiday_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`holiday_id`, `admin_id`, `update_by`, `session_id`, `holiday_name`, `holiday_start_date`, `holiday_end_date`, `holiday_for`, `holiday_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Republic Day', '2019-01-26', '2019-01-26', 0, 1, '2019-01-23 10:02:56', '2019-01-23 10:02:56'),
(2, 2, 2, 1, 's Holiday', '2019-04-10', '2019-04-20', 0, 1, '2019-04-05 20:58:20', '2019-04-05 20:58:20');

-- --------------------------------------------------------

--
-- Table structure for table `homework_conversations`
--

CREATE TABLE `homework_conversations` (
  `h_conversation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `homework_group_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `h_conversation_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `h_conversation_attechment` text COLLATE utf8mb4_unicode_ci,
  `h_session_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homework_conversations`
--

INSERT INTO `homework_conversations` (`h_conversation_id`, `admin_id`, `update_by`, `homework_group_id`, `staff_id`, `subject_id`, `h_conversation_text`, `created_at`, `updated_at`, `h_conversation_attechment`, `h_session_id`) VALUES
(1, 3, 3, 1, 1, 3, 'Test Attachement', '2019-04-01 07:54:42', '2019-04-01 07:54:42', NULL, NULL),
(2, 3, 3, 1, 1, 3, 'Test', '2019-04-01 07:57:28', '2019-04-01 07:57:28', NULL, NULL),
(3, 3, 3, 1, 1, 3, 'fdsf', '2019-04-01 08:23:36', '2019-04-01 08:23:36', NULL, NULL),
(5, 3, 3, 1, 1, 3, 'test new', '2019-04-01 08:32:26', '2019-04-01 08:32:26', '511131554107546.jpg', 1),
(6, 3, 3, 1, 1, 3, 'test', '2019-04-01 09:49:34', '2019-04-01 09:49:35', '6148861554112175docx', 1),
(7, 3, 3, 1, 1, 3, 'Test', '2019-04-01 09:53:50', '2019-04-01 09:53:50', '7943711554112430.docx', 1),
(8, 23, 23, 1, 2, 2, 'fff', '2019-04-01 10:05:15', '2019-04-01 10:05:15', '8890611554113115.xlsx', 1),
(9, 3, 3, 3, 1, 1, 'hello', '2019-04-02 17:59:59', '2019-04-02 17:59:59', NULL, NULL),
(10, 30, 30, 5, 6, 6, 'Great work this is home work', '2019-04-04 19:48:02', '2019-04-04 19:48:02', NULL, NULL),
(11, 1, 1, 1, 1, 1, 'test', '2019-04-04 22:06:25', '2019-04-04 22:06:25', NULL, NULL),
(12, 3, 3, 1, 1, 4, 'test', '2019-04-04 22:07:23', '2019-04-04 22:07:23', NULL, NULL),
(13, 3, 3, 1, 1, 4, 'test', '2019-04-04 22:10:43', '2019-04-04 22:10:43', NULL, NULL),
(14, 3, 3, 1, 1, 4, 'testing attachment', '2019-04-04 22:33:08', '2019-04-04 22:33:08', NULL, NULL),
(15, 3, 3, 1, 1, 4, 'test done done', '2019-04-04 22:43:26', '2019-04-04 22:43:26', NULL, NULL),
(16, 3, 3, 1, 1, 4, 'the same to e', '2019-04-04 22:44:23', '2019-04-04 22:44:23', NULL, NULL),
(17, 3, 3, 1, 1, 4, 'the', '2019-04-04 22:46:24', '2019-04-04 22:46:24', NULL, NULL),
(18, 3, 3, 1, 1, 4, 'testing', '2019-04-04 23:09:48', '2019-04-04 23:09:48', NULL, NULL),
(19, 3, 3, 1, 1, 4, 'testing 5 april', '2019-04-04 23:16:30', '2019-04-04 23:16:30', NULL, NULL),
(20, 3, 3, 1, 1, 4, 'the', '2019-04-04 23:22:35', '2019-04-04 23:22:35', '20957821554382354.png', NULL),
(21, 3, 3, 3, 1, 2, 'testing', '2019-04-04 23:38:14', '2019-04-04 23:38:14', '21168751554383294.png', NULL),
(22, 30, 30, 5, 6, 6, 'Hii', '2019-04-05 20:30:53', '2019-04-05 20:30:53', NULL, NULL),
(23, 3, 3, 3, 1, 1, 'done', '2019-04-05 22:22:34', '2019-04-05 22:22:34', 'Screenshot_2019-04-05-17-22-03-949_com.academiceye.debug1554465151648.png', NULL),
(24, 3, 3, 3, 1, 4, 'testing', '2019-04-08 23:09:53', '2019-04-08 23:09:53', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `homework_groups`
--

CREATE TABLE `homework_groups` (
  `homework_group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `homework_group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homework_groups`
--

INSERT INTO `homework_groups` (`homework_group_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `homework_group_name`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 'Class I - A Group', '2019-01-23 10:35:16', '2019-01-23 10:35:16'),
(2, 2, 2, 1, 2, 'Class I - B Group', '2019-01-23 10:35:31', '2019-01-23 10:35:31'),
(3, 2, 2, 2, 3, 'class X - Section A Group', '2019-04-02 16:19:17', '2019-04-02 16:19:17'),
(4, 2, 2, 4, 4, 'Non medical - ECE Group', '2019-04-02 20:59:19', '2019-04-02 20:59:19'),
(5, 2, 2, 5, 5, 'Cup of class - Test section for tea 1 Group', '2019-04-03 18:32:11', '2019-04-03 18:32:11'),
(6, 2, 2, 6, 6, 'Fifth - Sec-A Group', '2019-04-05 21:22:00', '2019-04-05 21:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `hostels`
--

CREATE TABLE `hostels` (
  `hostel_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `hostel_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostel_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boys,1=Girls',
  `hostel_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `h_fees_head_id` text COLLATE utf8mb4_unicode_ci COMMENT 'Hostel fees head ids are comma seperate stored'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostels`
--

INSERT INTO `hostels` (`hostel_id`, `admin_id`, `update_by`, `hostel_name`, `hostel_type`, `hostel_status`, `created_at`, `updated_at`, `h_fees_head_id`) VALUES
(1, 2, 2, 'Hostel 1', 0, 1, '2019-02-28 05:34:41', '2019-02-28 05:34:41', '1,2'),
(2, 2, 2, 'Hostel-3', 1, 1, '2019-04-02 18:07:57', '2019-04-02 18:07:57', '1,2'),
(3, 2, 2, 'Hostel-2', 2, 1, '2019-04-02 18:08:05', '2019-04-02 18:08:05', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_blocks`
--

CREATE TABLE `hostel_blocks` (
  `h_block_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `hostel_id` int(10) UNSIGNED DEFAULT NULL,
  `h_block_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_block_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_blocks`
--

INSERT INTO `hostel_blocks` (`h_block_id`, `admin_id`, `update_by`, `hostel_id`, `h_block_name`, `h_block_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Block 1', 1, '2019-02-28 05:35:05', '2019-02-28 05:35:05'),
(2, 2, 2, 1, 'block-1', 1, '2019-04-02 18:09:05', '2019-04-02 18:09:05'),
(3, 2, 2, 1, 'block-2', 1, '2019-04-02 18:09:28', '2019-04-02 18:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_fees`
--

CREATE TABLE `hostel_fees` (
  `hostel_fee_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `h_fee_session_id` int(10) UNSIGNED DEFAULT NULL,
  `h_fee_class_id` int(10) UNSIGNED DEFAULT NULL,
  `h_fee_student_id` int(10) UNSIGNED DEFAULT NULL,
  `h_fee_student_map_id` int(10) UNSIGNED DEFAULT NULL,
  `receipt_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receipt_date` date NOT NULL,
  `net_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pay_later_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pay_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `cheque_date` date DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'If cheque mode',
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If online mode',
  `transaction_date` date DEFAULT NULL COMMENT 'If online mode',
  `fee_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Paid, 2:Reverted,3:Refunded',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `concession_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fine_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cancel_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_fees`
--

INSERT INTO `hostel_fees` (`hostel_fee_id`, `admin_id`, `update_by`, `h_fee_session_id`, `h_fee_class_id`, `h_fee_student_id`, `h_fee_student_map_id`, `receipt_number`, `receipt_date`, `net_amount`, `total_paid_amount`, `pay_later_amount`, `pay_mode`, `bank_id`, `cheque_date`, `cheque_number`, `cheque_amt`, `transaction_id`, `transaction_date`, `fee_status`, `created_at`, `updated_at`, `concession_amount`, `fine_amount`, `cancel_status`, `description`) VALUES
(7, 2, 2, 1, 1, 6, 3, '7', '1993-03-25', 13450.00, 13000.00, 450.00, 'offline', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-03-15 13:10:39', '2019-03-15 13:10:39', 0.00, 0.00, 0, NULL),
(8, 2, 2, 1, 1, 6, 3, '8', '2019-03-25', 350.00, 350.00, 0.00, 'offline', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-03-23 07:22:00', '2019-03-23 07:22:00', 0.00, 0.00, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hostel_fees_head`
--

CREATE TABLE `hostel_fees_head` (
  `h_fees_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `h_fees_head_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_fees_head_price` double(18,2) UNSIGNED DEFAULT NULL,
  `h_fees_head_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_fees_head`
--

INSERT INTO `hostel_fees_head` (`h_fees_head_id`, `admin_id`, `update_by`, `h_fees_head_name`, `h_fees_head_price`, `h_fees_head_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Head 1', 3000.00, 1, '2019-02-28 05:34:17', '2019-02-28 05:34:17'),
(2, 2, 2, 'Head 2', 3500.00, 1, '2019-02-28 05:34:27', '2019-02-28 05:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_fee_details`
--

CREATE TABLE `hostel_fee_details` (
  `hostel_fee_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `hostel_fee_id` int(10) UNSIGNED DEFAULT NULL,
  `month_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paid_fee_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amount_status` tinyint(4) NOT NULL COMMENT '1:full amount, 2:Less amount',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `month_date` date DEFAULT NULL,
  `cancel_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hostel_floors`
--

CREATE TABLE `hostel_floors` (
  `h_floor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `h_block_id` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_floor_rooms` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_floors`
--

INSERT INTO `hostel_floors` (`h_floor_id`, `admin_id`, `update_by`, `h_block_id`, `h_floor_no`, `h_floor_rooms`, `h_floor_description`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '1', 1, '1', '2019-02-28 05:35:05', '2019-02-28 05:35:05'),
(2, 2, 2, 1, '2', 2, '22', '2019-02-28 05:35:05', '2019-02-28 05:35:05'),
(3, 2, 2, 2, 'floor-1', 20, 'well furnitured', '2019-04-02 18:09:05', '2019-04-02 18:09:05'),
(4, 2, 2, 3, 'floor-2', 15, 'tested', '2019-04-02 18:09:28', '2019-04-02 18:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_rooms`
--

CREATE TABLE `hostel_rooms` (
  `h_room_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `hostel_id` int(10) UNSIGNED DEFAULT NULL,
  `h_block_id` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_id` int(10) UNSIGNED DEFAULT NULL,
  `h_room_cate_id` int(10) UNSIGNED DEFAULT NULL,
  `h_room_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_room_alias` text COLLATE utf8mb4_unicode_ci,
  `h_room_capacity` int(10) UNSIGNED DEFAULT NULL,
  `h_room_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_rooms`
--

INSERT INTO `hostel_rooms` (`h_room_id`, `admin_id`, `update_by`, `hostel_id`, `h_block_id`, `h_floor_id`, `h_room_cate_id`, `h_room_no`, `h_room_alias`, `h_room_capacity`, `h_room_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, '1', '1', 4, 1, '2019-02-28 05:35:58', '2019-02-28 05:35:58'),
(2, 2, 2, 1, 2, 3, 2, '5', 'tested', 3, 1, '2019-04-02 18:11:00', '2019-04-02 18:11:00'),
(3, 2, 2, 1, 3, 4, 3, '2', 'tested', 5, 1, '2019-04-02 18:11:33', '2019-04-02 18:11:33'),
(4, 2, 2, 1, 2, 3, 4, '10', 'test', 10, 1, '2019-04-02 18:12:01', '2019-04-02 18:12:01');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_room_cate`
--

CREATE TABLE `hostel_room_cate` (
  `h_room_cate_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `h_room_cate_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_room_cate_fees` int(10) UNSIGNED DEFAULT NULL,
  `h_room_cate_des` text COLLATE utf8mb4_unicode_ci,
  `h_room_cate_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_room_cate`
--

INSERT INTO `hostel_room_cate` (`h_room_cate_id`, `admin_id`, `update_by`, `h_room_cate_name`, `h_room_cate_fees`, `h_room_cate_des`, `h_room_cate_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Category', 200, 'Desc', 1, '2019-02-28 05:35:36', '2019-02-28 05:35:36'),
(2, 2, 2, 'AC Room', 1000, 'ac room', 1, '2019-04-02 18:10:04', '2019-04-02 18:10:04'),
(3, 2, 2, 'Cooler room', 500, 'test', 1, '2019-04-02 18:10:19', '2019-04-02 18:10:19'),
(4, 2, 2, 'simple room', 200, 'testedd', 1, '2019-04-02 18:10:32', '2019-04-02 18:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_student_map`
--

CREATE TABLE `hostel_student_map` (
  `h_student_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `hostel_id` int(10) UNSIGNED DEFAULT NULL,
  `h_block_id` int(10) UNSIGNED DEFAULT NULL,
  `h_floor_id` int(10) UNSIGNED DEFAULT NULL,
  `h_room_cate_id` int(10) UNSIGNED DEFAULT NULL,
  `h_room_id` int(10) UNSIGNED DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `fees_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unpaid,1=paid',
  `leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Stay,1=leave',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `h_fees_head_ids` text COLLATE utf8mb4_unicode_ci COMMENT 'Hostel fees head ids are comma seperate stored'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hostel_student_map`
--

INSERT INTO `hostel_student_map` (`h_student_map_id`, `admin_id`, `update_by`, `student_id`, `hostel_id`, `h_block_id`, `h_floor_id`, `h_room_cate_id`, `h_room_id`, `join_date`, `leave_date`, `fees_status`, `leave_status`, `created_at`, `updated_at`, `h_fees_head_ids`) VALUES
(1, 2, 2, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2019-04-03 22:19:50', '2019-04-03 22:19:50', NULL),
(2, 2, 2, 7, 1, 3, 4, 3, 3, '2019-04-03', '2019-04-24', 0, 0, '2019-04-03 22:23:29', '2019-04-03 22:26:39', '1,2'),
(3, 2, 2, 6, 1, 1, 1, 1, 1, '2019-04-05', '2019-04-30', 0, 0, '2019-04-03 22:23:41', '2019-04-03 22:27:36', '1,2'),
(4, 2, 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2019-04-03 22:24:13', '2019-04-03 22:24:13', NULL),
(5, 2, 2, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2019-04-03 22:24:48', '2019-04-03 22:24:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac`
--

CREATE TABLE `imprest_ac` (
  `imprest_ac_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `imprest_ac_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imprest_ac_amt` double DEFAULT NULL,
  `imprest_ac_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imprest_ac`
--

INSERT INTO `imprest_ac` (`imprest_ac_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `imprest_ac_name`, `imprest_ac_amt`, `imprest_ac_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 'Teena', 0, 1, '2019-01-23 11:16:47', '2019-01-23 11:16:47'),
(2, 2, 2, 1, 1, 2, 'Pankaj', 0, 1, '2019-01-23 11:25:12', '2019-01-23 11:25:12'),
(3, 2, 2, 1, 1, 3, 'Ajay', 0, 1, '2019-01-23 11:28:31', '2019-01-23 11:28:31'),
(4, 2, 2, 1, 1, 5, 'Neelam', 3000, 1, '2019-01-25 06:42:42', '2019-03-13 13:34:27'),
(5, 2, 2, 1, 1, 6, 'Shri', 7000, 1, '2019-01-25 10:47:36', '2019-03-14 05:36:04'),
(6, NULL, 2, NULL, 1, 7, 'Amit', 0, 1, '2019-02-04 05:15:06', '2019-02-04 05:15:06'),
(7, 2, 2, 1, 1, 8, 'Sibling A', 2010, 1, '2019-03-05 11:19:22', '2019-04-03 16:25:06'),
(8, 2, 2, 1, 2, 9, 'khushbu', 0, 1, '2019-04-02 17:11:06', '2019-04-02 17:11:06'),
(9, 2, 2, 1, 5, 10, 'Sourabh Rohila', 0, 1, '2019-04-03 23:28:02', '2019-04-03 23:28:02'),
(10, 2, 2, 5, 6, 11, 'Student testing 1', 0, 1, '2019-04-05 22:29:56', '2019-04-05 22:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac_confi`
--

CREATE TABLE `imprest_ac_confi` (
  `imprest_ac_confi_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `imprest_ac_confi_amt` double UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imprest_ac_confi`
--

INSERT INTO `imprest_ac_confi` (`imprest_ac_confi_id`, `admin_id`, `update_by`, `imprest_ac_confi_amt`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, '2019-01-23 09:36:14', '2019-01-23 09:36:14');

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac_entries`
--

CREATE TABLE `imprest_ac_entries` (
  `ac_entry_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `ac_entry_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ac_entry_date` date DEFAULT NULL,
  `ac_entry_amt` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imprest_ac_entries`
--

INSERT INTO `imprest_ac_entries` (`ac_entry_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `ac_entry_name`, `ac_entry_date`, `ac_entry_amt`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 'test', '2019-02-07', 1500, '2019-02-07 06:36:40', '2019-02-07 06:36:40'),
(2, 2, 2, 1, 1, 1, 'test2', '2019-04-02', 510, '2019-04-02 17:28:31', '2019-04-02 17:28:31'),
(3, 2, 2, 1, 2, 3, 'test 3', '2019-04-02', 100, '2019-04-02 17:28:57', '2019-04-02 17:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_config`
--

CREATE TABLE `invoice_config` (
  `invoice_config_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `invoice_prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` tinyint(4) DEFAULT '0',
  `month` tinyint(4) DEFAULT '0',
  `day` tinyint(4) DEFAULT '0',
  `invoice_type` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_invoice` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_config`
--

INSERT INTO `invoice_config` (`invoice_config_id`, `admin_id`, `update_by`, `invoice_prefix`, `year`, `month`, `day`, `invoice_type`, `created_at`, `updated_at`, `total_invoice`) VALUES
(1, 2, 2, 'PI', 1, 1, 1, 1, '2019-01-29 08:08:16', '2019-01-29 08:08:16', 2),
(2, 2, 2, 'PR', 1, 1, 1, 2, '2019-01-29 08:08:16', '2019-01-29 08:08:16', 0),
(3, 2, 2, 'SI', 1, 1, 1, 3, '2019-01-29 08:08:16', '2019-01-29 08:08:16', 2),
(4, 2, 2, 'SR', 1, 1, 1, 4, '2019-01-29 08:08:16', '2019-01-29 08:08:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inv_category`
--

CREATE TABLE `inv_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `category_level` int(11) DEFAULT '0',
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inv_category`
--

INSERT INTO `inv_category` (`category_id`, `admin_id`, `update_by`, `category_level`, `category_name`, `category_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 0, 'Pen', 1, '2019-04-02 18:56:20', '2019-04-02 18:56:20'),
(2, 2, 2, 0, 'Register', 1, '2019-04-02 18:56:29', '2019-04-02 18:56:29'),
(3, 2, 2, 1, 'Black Pen', 1, '2019-04-02 18:56:35', '2019-04-02 18:56:35'),
(4, 2, 2, 1, 'Blue Pen', 1, '2019-04-02 18:56:43', '2019-04-02 18:56:43'),
(5, 2, 2, 2, 'short Register', 1, '2019-04-02 18:56:51', '2019-04-02 18:56:51'),
(6, 2, 2, 2, 'Long Register', 1, '2019-04-02 18:56:58', '2019-04-02 18:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `inv_items`
--

CREATE TABLE `inv_items` (
  `item_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sub_category_id` int(10) UNSIGNED DEFAULT NULL,
  `unit_id` int(10) UNSIGNED DEFAULT NULL,
  `item_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `available_quantity` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inv_items`
--

INSERT INTO `inv_items` (`item_id`, `admin_id`, `update_by`, `category_id`, `sub_category_id`, `unit_id`, `item_name`, `item_status`, `created_at`, `updated_at`, `available_quantity`) VALUES
(1, 2, 2, 1, 3, 3, 'jell pen', 1, '2019-04-02 18:57:39', '2019-04-02 19:48:52', 28),
(2, 2, 2, 2, 6, 3, 'choice', 1, '2019-04-02 18:58:04', '2019-04-02 19:50:34', 88),
(3, 2, 2, 1, 4, 3, 'cello', 1, '2019-04-02 18:58:25', '2019-04-02 19:00:19', 40);

-- --------------------------------------------------------

--
-- Table structure for table `inv_unit`
--

CREATE TABLE `inv_unit` (
  `unit_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `unit_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inv_unit`
--

INSERT INTO `inv_unit` (`unit_id`, `admin_id`, `update_by`, `unit_name`, `unit_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Mtr', 1, '2019-04-02 18:57:12', '2019-04-02 18:57:12'),
(2, 2, 2, 'Cm', 1, '2019-04-02 18:57:17', '2019-04-02 18:57:17'),
(3, 2, 2, 'set', 1, '2019-04-02 18:57:21', '2019-04-02 18:57:21');

-- --------------------------------------------------------

--
-- Table structure for table `inv_vendor`
--

CREATE TABLE `inv_vendor` (
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `vendor_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_gstin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inv_vendor`
--

INSERT INTO `inv_vendor` (`vendor_id`, `admin_id`, `update_by`, `vendor_name`, `vendor_gstin`, `vendor_contact_no`, `vendor_email`, `vendor_address`, `vendor_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'VENDOR 1', '5', '9876567677', 'dfss@gmail.com', 'tesssdssd', 1, '2019-04-02 18:55:04', '2019-04-02 18:55:04'),
(2, 2, 2, 'test', '444', '6666666666', 'test@gmail.com', 'dsdsdsdsdsd', 1, '2019-04-02 18:55:30', '2019-04-02 18:55:30'),
(3, 2, 2, 'test', '444', '9999999999', 'tes77t@gmail.com', 'dsdsdsdsdsd', 1, '2019-04-02 18:55:56', '2019-04-02 18:55:56');

-- --------------------------------------------------------

--
-- Table structure for table `issued_books`
--

CREATE TABLE `issued_books` (
  `issued_book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_id` int(10) UNSIGNED DEFAULT NULL,
  `issued_from_date` date DEFAULT NULL,
  `issued_to_date` date DEFAULT NULL,
  `issued_book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=issued,2=returned',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issued_books`
--

INSERT INTO `issued_books` (`issued_book_id`, `admin_id`, `update_by`, `book_id`, `member_id`, `member_type`, `book_copies_id`, `issued_from_date`, `issued_to_date`, `issued_book_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 2, '2019-04-02', '2019-04-18', 1, '2019-04-02 18:34:23', '2019-04-02 18:34:23'),
(2, 2, 2, 1, 4, 1, 3, '2019-04-03', '2019-04-19', 1, '2019-04-02 18:35:06', '2019-04-02 18:35:06'),
(3, 2, 2, 2, 4, 1, 9, '2019-04-03', '2019-04-25', 1, '2019-04-02 18:35:20', '2019-04-02 18:35:20'),
(4, 2, 2, 1, 4, 1, 4, '2019-04-02', '2019-04-25', 1, '2019-04-02 18:35:39', '2019-04-02 18:35:39'),
(5, 2, 2, 2, 7, 1, 10, '2019-04-02', '2019-04-30', 1, '2019-04-02 18:36:01', '2019-04-02 18:36:01'),
(6, 2, 2, 1, 7, 1, 5, '2019-04-19', '2019-04-30', 1, '2019-04-02 18:36:12', '2019-04-02 18:36:12'),
(7, 2, 2, 1, 4, 1, 6, '2019-04-18', '2019-04-25', 1, '2019-04-03 15:21:08', '2019-04-03 15:21:08'),
(8, 2, 2, 1, 4, 1, 7, '2019-04-23', '2019-04-26', 1, '2019-04-03 15:21:26', '2019-04-03 15:21:26'),
(9, 2, 2, 1, 4, 1, 8, '2019-06-12', '2019-07-25', 1, '2019-04-03 15:21:41', '2019-04-03 15:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `issued_books_history`
--

CREATE TABLE `issued_books_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_id` int(10) UNSIGNED DEFAULT NULL,
  `issued_from_date` date DEFAULT NULL,
  `issued_to_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `job_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_medium_type` int(10) UNSIGNED DEFAULT NULL,
  `job_type` int(10) UNSIGNED NOT NULL COMMENT '0=Temporary,1=Permament',
  `job_durations` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_no_of_vacancy` int(10) UNSIGNED DEFAULT NULL,
  `job_recruitment` text COLLATE utf8mb4_unicode_ci,
  `job_description` text COLLATE utf8mb4_unicode_ci,
  `job_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`job_id`, `admin_id`, `update_by`, `job_name`, `job_medium_type`, `job_type`, `job_durations`, `job_no_of_vacancy`, `job_recruitment`, `job_description`, `job_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Python', 1, 1, NULL, 2, NULL, 'development in python', 1, '2019-04-02 19:52:11', '2019-04-02 19:52:11'),
(2, 2, 2, 'Laravel', 1, 0, '2 year', 5, NULL, 'web development', 1, '2019-04-02 19:53:02', '2019-04-02 19:53:02');

-- --------------------------------------------------------

--
-- Table structure for table `job_applied_cadidates`
--

CREATE TABLE `job_applied_cadidates` (
  `applied_cadidate_id` int(10) UNSIGNED NOT NULL,
  `job_id` int(10) UNSIGNED DEFAULT NULL,
  `job_form_id` int(10) UNSIGNED DEFAULT NULL,
  `form_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_profile_img` text COLLATE utf8mb4_unicode_ci,
  `staff_resume` text COLLATE utf8mb4_unicode_ci,
  `staff_aadhar` text COLLATE utf8mb4_unicode_ci,
  `staff_UAN` text COLLATE utf8mb4_unicode_ci,
  `staff_ESIN` text COLLATE utf8mb4_unicode_ci,
  `staff_PAN` text COLLATE utf8mb4_unicode_ci,
  `designation_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_dob` date DEFAULT NULL,
  `staff_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Male,1=Female',
  `staff_father_name_husband_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_father_husband_mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_marital_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unmarried,1=Married',
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_qualification` text COLLATE utf8mb4_unicode_ci COMMENT 'Qualifications are store with comma',
  `staff_specialization` text COLLATE utf8mb4_unicode_ci COMMENT 'Specializations are store with comma',
  `staff_reference` text COLLATE utf8mb4_unicode_ci,
  `staff_experience` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `staff_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `form_message_via` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Email,1=Phone,2=Both',
  `status_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=Schedule,2=Approved,3=Disapproved',
  `staff_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_applied_cadidates`
--

INSERT INTO `job_applied_cadidates` (`applied_cadidate_id`, `job_id`, `job_form_id`, `form_number`, `staff_name`, `staff_profile_img`, `staff_resume`, `staff_aadhar`, `staff_UAN`, `staff_ESIN`, `staff_PAN`, `designation_id`, `staff_email`, `staff_mobile_number`, `staff_dob`, `staff_gender`, `staff_father_name_husband_name`, `staff_father_husband_mobile_no`, `staff_mother_name`, `staff_blood_group`, `staff_marital_status`, `title_id`, `caste_id`, `nationality_id`, `religion_id`, `staff_qualification`, `staff_specialization`, `staff_reference`, `staff_experience`, `staff_temporary_address`, `staff_temporary_city`, `staff_temporary_state`, `staff_temporary_county`, `staff_temporary_pincode`, `staff_permanent_address`, `staff_permanent_city`, `staff_permanent_state`, `staff_permanent_county`, `staff_permanent_pincode`, `datetime`, `message`, `form_message_via`, `status_type`, `staff_status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'recruit-python - 2', 'raju', NULL, 'AcademicEyeAdminPanel371261554197369.pdf', NULL, NULL, NULL, NULL, NULL, 'raju@gmail.com', '4567894332', '1991-05-20', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'test', 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, 'tset', 2, 1, 1, '306401', '0000-00-00 00:00:00', NULL, 0, 0, 1, '2019-04-02 19:59:29', '2019-04-02 19:59:29'),
(2, 1, 2, 'recruit-python - 2', 'khushbu', NULL, 'AcademicEyeAdminPanel783251554197493.pdf', NULL, NULL, NULL, NULL, NULL, 'khushbu@gmail.com', '7777777777', '1996-02-20', 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'tset', 'tset', 'test', NULL, NULL, NULL, NULL, NULL, NULL, 'test', 2, 1, 1, '666666', '2019-04-02 10:00:00', 'Dear Khushbu Vaishnav, you applied for recruit-pyhton-2 in Academic Eye, your interview is scheduled on 10-04-2019, we request you to be on time with all the Education and work experience related documents.', 2, 1, 1, '2019-04-02 20:01:33', '2019-04-02 20:09:12'),
(3, 2, 1, 'Recruitment - 1', 'ridhima', NULL, 'AcademicEyeAdminPanel718321554197588.pdf', NULL, NULL, NULL, NULL, NULL, 'ridhima@gmail.com', '5289637455', '1993-02-12', 1, NULL, NULL, NULL, 'A+', 0, NULL, NULL, 1, NULL, 'test', 'tset', 'tset', NULL, NULL, NULL, NULL, NULL, NULL, 'tset', 1, 1, 1, '342001', '2019-04-04 11:00:00', 'Dear Ridhima, you applied for Laravel in Academic Eye, your interview is scheduled on 12-04-2019, we request you to be on time with all the Education and work experience related documents.', 0, 1, 1, '2019-04-02 20:03:08', '2019-04-02 20:07:33');

-- --------------------------------------------------------

--
-- Table structure for table `job_applied_cadidate_status`
--

CREATE TABLE `job_applied_cadidate_status` (
  `applied_cadidate_status_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `applied_cadidate_id` int(10) UNSIGNED DEFAULT NULL,
  `job_id` int(10) UNSIGNED DEFAULT NULL,
  `job_form_id` int(10) UNSIGNED DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `form_message_via` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Email,1=Phone,2=Both',
  `status_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=Schedule,2=Approved,3=Disapproved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_forms`
--

CREATE TABLE `job_forms` (
  `job_form_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `form_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `form_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_instruction` text COLLATE utf8mb4_unicode_ci,
  `form_information` text COLLATE utf8mb4_unicode_ci,
  `form_success_message` text COLLATE utf8mb4_unicode_ci,
  `form_failed_message` text COLLATE utf8mb4_unicode_ci,
  `form_message_via` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Email,1=Phone,2=Both',
  `form_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_forms`
--

INSERT INTO `job_forms` (`job_form_id`, `admin_id`, `update_by`, `form_name`, `job_id`, `session_id`, `form_prefix`, `form_instruction`, `form_information`, `form_success_message`, `form_failed_message`, `form_message_via`, `form_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'JOB Form', 2, 1, 'Recruitment', 'test', 'test', NULL, NULL, 0, 1, '2019-04-02 19:53:53', '2019-04-02 19:53:53'),
(2, 2, 2, 'New JOB Form', 1, 1, 'recruit-python', 'test', 'test', NULL, NULL, 0, 1, '2019-04-02 19:55:34', '2019-04-02 19:55:34');

-- --------------------------------------------------------

--
-- Table structure for table `job_form_fields`
--

CREATE TABLE `job_form_fields` (
  `job_form_field_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `job_form_id` int(10) UNSIGNED DEFAULT NULL,
  `form_keys` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_form_fields`
--

INSERT INTO `job_form_fields` (`job_form_field_id`, `admin_id`, `update_by`, `job_form_id`, `form_keys`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'staff_name,staff_profile_img,staff_email,staff_mobile_number,staff_dob,staff_gender,staff_blood_group,staff_marital_status,nationality_id,staff_qualification,staff_specialization,staff_reference,staff_experience,staff_permanent_address,staff_permanent_county,staff_permanent_state,staff_permanent_city,staff_permanent_pincode', '2019-04-02 19:54:44', '2019-04-02 19:54:44'),
(2, 2, 2, 2, 'staff_name,staff_profile_img,staff_email,staff_mobile_number,staff_dob,staff_gender,staff_marital_status,staff_qualification,staff_specialization,staff_reference,staff_experience,staff_permanent_address,staff_permanent_county,staff_permanent_state,staff_permanent_city,staff_permanent_pincode', '2019-04-02 19:56:16', '2019-04-02 19:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `library_fine`
--

CREATE TABLE `library_fine` (
  `library_fine_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=student,2=staff',
  `fine_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `fine_date` date DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `fine_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=due,2=paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `library_fine`
--

INSERT INTO `library_fine` (`library_fine_id`, `admin_id`, `update_by`, `member_id`, `member_type`, `fine_amount`, `fine_date`, `remark`, `fine_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 55.00, '2019-04-02', 'urgent', 1, '2019-04-02 18:37:18', '2019-04-02 18:37:18');

-- --------------------------------------------------------

--
-- Table structure for table `library_members`
--

CREATE TABLE `library_members` (
  `library_member_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `library_member_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=student,2=staff',
  `member_member_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `library_members`
--

INSERT INTO `library_members` (`library_member_id`, `admin_id`, `update_by`, `member_id`, `library_member_type`, `member_member_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, '2019-04-02 18:32:57', '2019-04-02 18:32:57'),
(2, 2, 2, 2, 1, 1, '2019-04-02 18:32:57', '2019-04-02 18:32:57'),
(3, 2, 2, 8, 1, 1, '2019-04-02 18:32:57', '2019-04-02 18:32:57'),
(4, 2, 2, 3, 1, 1, '2019-04-02 18:32:57', '2019-04-02 18:32:57'),
(5, 2, 2, 5, 1, 1, '2019-04-02 18:32:57', '2019-04-02 18:32:57'),
(6, 2, 2, 6, 1, 1, '2019-04-02 18:32:57', '2019-04-02 18:32:57'),
(7, 2, 2, 9, 1, 1, '2019-04-02 18:32:57', '2019-04-02 18:32:57'),
(8, 2, 2, 1, 2, 1, '2019-04-02 18:33:40', '2019-04-02 18:33:40'),
(9, 2, 2, 3, 2, 1, '2019-04-02 18:33:40', '2019-04-02 18:33:40'),
(10, 2, 2, 4, 2, 1, '2019-04-02 18:33:40', '2019-04-02 18:33:40'),
(11, 2, 2, 5, 2, 1, '2019-04-02 18:33:40', '2019-04-02 18:33:40');

-- --------------------------------------------------------

--
-- Table structure for table `map_student_staff_vehicle`
--

CREATE TABLE `map_student_staff_vehicle` (
  `map_student_staff_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `update_by` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `route_id` int(10) UNSIGNED NOT NULL,
  `route_location_id` int(10) UNSIGNED NOT NULL,
  `student_staff_id` int(10) UNSIGNED DEFAULT NULL,
  `student_staff_type` int(10) UNSIGNED DEFAULT NULL COMMENT '1 = Student ,2 = Staff',
  `student_staff_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `map_student_staff_vehicle`
--

INSERT INTO `map_student_staff_vehicle` (`map_student_staff_id`, `admin_id`, `update_by`, `vehicle_id`, `route_id`, `route_location_id`, `student_staff_id`, `student_staff_type`, `student_staff_status`, `created_at`, `updated_at`, `session_id`) VALUES
(1, 2, 2, 1, 1, 2, 6, 1, 1, '2019-01-01 05:02:00', '2019-03-11 05:02:00', 1),
(2, 2, 2, 2, 2, 4, 8, 1, 1, '2019-04-02 17:57:32', '2019-04-02 17:57:32', NULL),
(3, 2, 2, 2, 2, 6, 5, 1, 1, '2019-04-02 17:57:47', '2019-04-02 17:57:47', NULL),
(4, 2, 2, 2, 2, 5, 2, 1, 1, '2019-04-02 17:58:07', '2019-04-02 17:58:07', NULL),
(5, 2, 2, 2, 2, 4, 4, 2, 1, '2019-04-02 17:58:38', '2019-04-02 17:58:38', NULL),
(6, 2, 2, 4, 2, 5, 8, 1, 1, '2019-04-04 17:34:13', '2019-04-04 17:34:13', NULL),
(7, 2, 2, 4, 2, 5, 3, 1, 1, '2019-04-04 17:34:39', '2019-04-04 17:34:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `marksheet_templates`
--

CREATE TABLE `marksheet_templates` (
  `marksheet_template_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `template_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Exam Wise,1=Term Wise,2=Consolidated/Annual Marksheet',
  `template_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marksheet_templates`
--

INSERT INTO `marksheet_templates` (`marksheet_template_id`, `admin_id`, `update_by`, `template_name`, `template_type`, `template_link`, `template_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Exam Wise T1', 0, 'examwise-template1', 1, '2019-02-26 05:26:20', '2019-02-26 05:26:20'),
(2, 1, 1, 'Term Wise T1', 1, 'termwise-template1', 1, '2019-02-26 05:26:20', '2019-02-26 05:26:20'),
(3, 1, 1, 'Annual Marksheet T1', 2, 'annual-template1', 1, '2019-02-26 05:26:20', '2019-02-26 10:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `marks_criteria`
--

CREATE TABLE `marks_criteria` (
  `marks_criteria_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `criteria_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_marks` double UNSIGNED DEFAULT NULL,
  `passing_marks` double UNSIGNED DEFAULT NULL,
  `criteria_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marks_criteria`
--

INSERT INTO `marks_criteria` (`marks_criteria_id`, `admin_id`, `update_by`, `criteria_name`, `max_marks`, `passing_marks`, `criteria_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Criteria 1', 100, 34, 1, '2019-01-25 09:59:03', '2019-01-25 09:59:03'),
(2, 2, 2, 'criteria 2', 500, 200, 1, '2019-04-02 16:57:53', '2019-04-02 16:58:05'),
(3, 2, 2, 's Criteria marks', 100, 32, 1, '2019-04-06 18:29:04', '2019-04-06 18:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `master_modules`
--

CREATE TABLE `master_modules` (
  `master_module_id` int(10) UNSIGNED NOT NULL,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mandatory` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_modules`
--

INSERT INTO `master_modules` (`master_module_id`, `module_name`, `created_at`, `updated_at`, `mandatory`) VALUES
(1, 'Academics', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(2, 'Admission', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(3, 'Student Info System', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(4, 'Staff Info System', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(5, 'Examination and Report Card', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(6, 'Fees Collection', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(7, 'Communication', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(8, 'Visitor Management', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(9, 'Online Content', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(10, 'Notice Board', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(11, 'System Utilities', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 1),
(12, 'Task Manager', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(13, 'Transport', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(14, 'Hostel', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(15, 'Library', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(16, 'Payroll', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(17, 'Inventory', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(18, 'Recruitment', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(19, 'Substitute Management', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0),
(20, 'Accounts', '2019-03-25 12:55:22', '2019-03-25 12:55:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `medium_type`
--

CREATE TABLE `medium_type` (
  `medium_type_id` int(10) UNSIGNED NOT NULL,
  `medium_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `medium_type_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medium_type`
--

INSERT INTO `medium_type` (`medium_type_id`, `medium_type`, `medium_type_status`, `created_at`, `updated_at`) VALUES
(1, 'English', 1, '2019-01-23 10:34:31', '2019-01-23 10:34:31'),
(2, 'Hindi', 1, '2019-04-02 16:09:19', '2019-04-02 16:09:19'),
(4, 'Arabic', 1, '2019-04-02 20:40:16', '2019-04-02 20:40:16'),
(5, 'Cup of Tea medium', 1, '2019-04-03 17:58:22', '2019-04-03 17:58:49'),
(6, 'PUNJABI', 1, '2019-04-05 21:19:59', '2019-04-05 21:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_09_04_081951_create_admins_table', 1),
(2, '2018_09_04_082724_create_country_table', 1),
(3, '2018_09_04_082725_create_medium_type_table', 1),
(4, '2018_09_04_085450_create_state_table', 1),
(5, '2018_09_04_085724_create_city_table', 1),
(6, '2018_09_04_090000_create_school_boards_table', 1),
(7, '2018_09_04_090037_create_school_table', 1),
(8, '2018_09_04_090038_create_imprest_ac_confi_table', 1),
(9, '2018_09_04_091709_create_sessions_table', 1),
(10, '2018_09_04_092243_create_holidays_table', 1),
(11, '2018_09_04_092628_create_shifts_table', 1),
(12, '2018_09_04_092824_create_facilities_table', 1),
(13, '2018_09_04_093153_create_document_category_table', 1),
(14, '2018_09_04_093346_create_designation_table', 1),
(15, '2018_09_04_093413_create_titles_table', 1),
(16, '2018_09_04_093453_create_caste_table', 1),
(17, '2018_09_04_093537_create_religions_table', 1),
(18, '2018_09_04_093957_create_nationality_table', 1),
(19, '2018_09_04_094116_create_groups_table', 1),
(20, '2018_09_04_094117_create_streams_table', 1),
(21, '2018_09_04_094221_create_classes_table', 1),
(22, '2018_09_04_094401_create_sections_table', 1),
(23, '2018_09_04_094530_create_co_scholastic_types_table', 1),
(24, '2018_09_04_100509_create_subjects_table', 1),
(25, '2018_09_04_100807_create_virtual_classes_table', 1),
(26, '2018_09_04_100943_create_competitions_table', 1),
(27, '2018_09_04_101149_create_room_no_table', 1),
(28, '2018_09_04_101335_create_student_parents_table', 1),
(29, '2018_09_06_065837_create_term_exams_table', 1),
(30, '2018_09_06_065937_create_exams_table', 1),
(31, '2018_09_06_083000_create_student_table', 1),
(32, '2018_09_06_100023_create_student_academic_info_table', 1),
(33, '2018_09_06_100515_create_student_academic_history_info_table', 1),
(34, '2018_09_06_101111_create_student_health_info_table', 1),
(35, '2018_09_06_101112_create_imprest_ac_table', 1),
(36, '2018_09_06_101113_create_wallet_ac_table', 1),
(37, '2018_09_06_101240_create_virtual_class_map_table', 1),
(38, '2018_09_06_101310_create_competition_map_table', 1),
(39, '2018_09_06_104053_create_online_notes_table', 1),
(40, '2018_09_06_104234_create_staff_roles_table', 1),
(41, '2018_09_06_104354_create_staff_table', 1),
(42, '2018_09_06_132553_create_time_tables_table', 1),
(43, '2018_09_07_043215_create_question_papers_table', 1),
(44, '2018_09_07_043425_create_staff_documents_table', 1),
(45, '2018_09_07_044319_create_student_documents_table', 1),
(46, '2018_09_08_082046_create_staff_class_allocations_table', 1),
(47, '2018_09_08_082829_create_subject_class_map_table', 1),
(48, '2018_09_08_083054_create_class_subject_staff_map_table', 1),
(49, '2018_09_08_083343_create_staff_leaves_table', 1),
(50, '2018_09_08_091546_create_student_leaves_table', 1),
(51, '2018_09_10_061556_create_brochures_table', 1),
(52, '2018_09_30_094828_create_class_subject_staff_maping_table', 1),
(53, '2018_10_01_043537_create_one_time_heads_table', 1),
(54, '2018_10_01_044251_create_recurring_heads_table', 1),
(55, '2018_10_01_044530_create_recurring_inst_table', 1),
(56, '2018_10_02_005202_create_book_category_table', 1),
(57, '2018_10_02_005800_create_book_allowance_table', 1),
(58, '2018_10_02_005938_create_book_vendor_table', 1),
(59, '2018_10_02_010120_create_book_cupboard_table', 1),
(60, '2018_10_02_010258_create_book_cupboardshelf_table', 1),
(61, '2018_10_02_044518_create_banks_table', 1),
(62, '2018_10_02_044836_create_student_cheques_table', 1),
(63, '2018_10_02_045309_create_rte_cheques_table', 1),
(64, '2018_10_02_165720_create_books_table', 1),
(65, '2018_10_02_170222_create_book_copies_info_table', 1),
(66, '2018_10_02_170530_create_library_members_table', 1),
(67, '2018_10_02_170949_create_issued_books_table', 1),
(68, '2018_10_03_045935_create_rte_heads_table', 1),
(69, '2018_10_03_140228_create_visitors_table', 1),
(70, '2018_10_06_075741_create_grade_schemes_table', 1),
(71, '2018_10_06_080011_create_grades_table', 1),
(72, '2018_10_08_072229_create_fine_table', 1),
(73, '2018_10_08_072737_create_fine_details_table', 1),
(74, '2018_10_09_055058_create_rte_head_map_table', 1),
(75, '2018_10_09_125120_create_marks_criteria_table', 1),
(76, '2018_10_09_141430_create_subject_section_map_table', 1),
(77, '2018_10_11_123244_create_api_tokens_table', 1),
(78, '2018_10_12_091353_create_reasons_table', 1),
(79, '2018_10_12_091354_create_concession_map_table', 1),
(80, '2018_10_13_054337_create_fees_st_map_table', 1),
(81, '2018_10_18_073910_create_fee_receipt_table', 1),
(82, '2018_10_18_074452_create_fee_receipt_details_table', 1),
(83, '2018_10_21_160700_create_homework_groups_table', 1),
(84, '2018_10_22_063731_create_exam_map_table', 1),
(85, '2018_10_23_111947_create_staff_attendance_table', 1),
(86, '2018_10_23_112802_create_student_attendance_table', 1),
(87, '2018_10_27_095406_create_student_subjects_table', 1),
(88, '2018_10_29_120146_create_admission_forms_table', 1),
(89, '2018_10_29_120354_create_admission_fields_table', 1),
(90, '2018_10_29_120645_create_admission_data_table', 1),
(91, '2018_10_29_170551_create_issued_books_history_table', 1),
(92, '2018_10_29_171144_create_library_fine_table', 1),
(93, '2018_10_30_131719_create_st_parent_groups_table', 1),
(94, '2018_10_30_131914_create_st_parent_group_map_table', 1),
(95, '2018_10_31_093217_create_remarks_table', 1),
(96, '2018_11_12_093152_create_hostels_table', 1),
(97, '2018_11_13_092000_create_hostel_blocks_table', 1),
(98, '2018_11_13_092231_create_hostel_floors_table', 1),
(99, '2018_11_13_092838_create_hostel_room_cate_table', 1),
(100, '2018_11_13_093306_create_hostel_rooms_table', 1),
(101, '2018_11_16_055339_create_homework_conversations_table', 1),
(102, '2018_11_20_074124_create_hostel_student_map_table', 1),
(103, '2018_11_22_075313_create_imprest_ac_entries_table', 1),
(104, '2018_11_22_104714_create_ac_entry_map_table', 1),
(105, '2018_11_22_113151_create_notice_table', 1),
(106, '2018_11_27_070619_create_time_table_map_table', 1),
(107, '2018_11_28_071442_add_notification_status_to_admins', 1),
(108, '2018_12_05_072332_create_certificates_table', 1),
(109, '2018_12_06_064816_create_exam_schedules_table', 1),
(110, '2018_12_06_065948_create_schedule_map_table', 1),
(111, '2018_12_06_070715_create_schedule_room_map_table', 1),
(112, '2018_12_06_071222_create_exam_marks_table', 1),
(113, '2018_12_13_052715_add_fields_to_admins_and_staff', 1),
(114, '2018_12_13_062324_create_app_settings_table', 1),
(115, '2018_12_24_103214_create_api_tokens_table', 1),
(116, '2019_01_05_112116_create_staff_attend_details_table', 1),
(117, '2019_01_05_112804_create_student_attend_details_table', 1),
(118, '2019_01_07_112528_create_inv_vendor_table', 1),
(119, '2019_01_07_114053_create_inv_category_table', 1),
(120, '2019_01_07_114116_create_inv_unit_table', 1),
(121, '2019_01_07_114117_create_inv_items_table', 1),
(122, '2019_01_08_092613_create_tasks_table', 1),
(123, '2019_01_08_100512_create_user_details_table', 1),
(124, '2019_01_08_100525_create_task_response_table', 1),
(125, '2019_01_11_102337_add_fields_to_admins_table', 1),
(126, '2019_01_12_110735_create_admin_password_resets_table', 1),
(127, '2019_01_18_113327_create_hostel_fees_head_table', 1),
(128, '2019_01_18_121139_add_fields_to_hostels_table', 1),
(129, '2019_01_18_184326_add_fields_to_hostel_student_map_table', 1),
(130, '2019_01_19_062316_create_student_tc_table', 1),
(131, '2019_01_24_160320_add_morefields_to_admins', 2),
(134, '2019_01_25_120000_create_student_trigger', 3),
(135, '2019_01_25_121317_create_staff_trigger', 4),
(136, '2019_01_25_175121_create_update_schedule_map_table', 5),
(137, '2019_01_24_113303_create_invoice_config_table', 6),
(138, '2019_01_25_124918_create_purchases_table', 6),
(139, '2019_01_25_125542_create_communication_table', 6),
(140, '2019_01_25_145547_create_purchase_items_table', 6),
(141, '2019_01_26_102657_add_fields_to_inv_items_table', 6),
(142, '2019_01_25_145222_create_vehicles_table', 7),
(143, '2019_01_25_145425_create_vehicle_documents_table', 7),
(144, '2019_01_25_145626_create_routes_table', 7),
(145, '2019_01_25_145627_create_assign_driver_conductor_routes_table', 7),
(150, '2019_01_25_145655_create_route_locations_table', 8),
(151, '2019_01_25_151432_create_fees_heads_table', 8),
(152, '2019_01_31_121614_create_invoice_config_trigger', 8),
(153, '2019_01_31_131805_add_field_to_invoice_config_table', 8),
(154, '2019_02_01_134534_create_notifications_table', 8),
(155, '2019_02_03_144416_create_pay_dept_table', 8),
(156, '2019_02_03_145934_create_pay_dept_map_table', 8),
(165, '2019_02_05_145455_create_pay_grade_table', 9),
(166, '2019_02_05_162151_remove_field_from_pay_dept_table', 9),
(167, '2019_02_05_162955_create_payroll_mapping_table', 9),
(168, '2019_02_06_112853_add_field_school_table', 9),
(169, '2019_02_07_170200_create_add_field_marks_table', 10),
(170, '2019_02_06_170236_create_pay_config_table', 11),
(171, '2019_02_07_153355_create_pf_setting_table', 11),
(172, '2019_02_07_172937_table_map_student_staff_vehicle', 11),
(173, '2019_02_08_121150_create_tds_setting_table', 11),
(174, '2019_02_08_170653_create_esi_setting_table', 11),
(175, '2019_02_08_184901_create_pt_setting_table', 11),
(176, '2019_02_09_102129_table_vehicle_attendence', 11),
(177, '2019_02_09_102357_table_vehicle_attend_details', 11),
(178, '2019_02_09_134844_add_field_to_pf_setting_table', 11),
(179, '2019_02_11_162506_table_job', 11),
(180, '2019_02_12_113511_table_job_form', 12),
(181, '2019_02_12_114729_table_job_form_fields', 12),
(182, '2019_02_15_112950_create_pay_arrear_table', 12),
(183, '2019_02_15_131200_create_pay_arrear_map_table', 12),
(184, '2019_02_15_131922_create_pay_bonus_table', 12),
(185, '2019_02_15_132320_create_pay_bonus_map_table', 12),
(186, '2019_02_15_132321_create_vehicle_documents_table', 12),
(187, '2019_02_15_152901_create_job_applied_candidates_table', 12),
(188, '2019_02_15_152902_table_job_applied_candidates_status', 12),
(189, '2019_02_15_152903_add_field_job_applied_candidates', 12),
(190, '2019_02_15_153257_remove_field_from_job', 12),
(191, '2019_02_15_172348_add_field_staff_resume', 12),
(192, '2019_02_16_114131_add_field_applied_attendence', 12),
(193, '2019_02_19_115148_create_pay_salary_head_table', 12),
(194, '2019_02_19_160004_add_table_plan_schedule', 13),
(195, '2019_02_19_184811_add_field_plan_schedule', 14),
(202, '2019_02_20_174822_create_student_marksheets', 15),
(203, '2019_02_21_114008_create_st_exam_result_table', 15),
(204, '2019_02_20_160816_add_field_task_manager', 16),
(205, '2019_02_20_173004_add_field_task_manager_response', 16),
(206, '2019_02_21_093759_add_field_task_manager_response', 17),
(207, '2019_02_21_101526_table_task_manager', 17),
(209, '2019_02_21_114009_create_student_tc_table', 18),
(210, '2019_02_19_161427_create_pay_loan_table', 19),
(211, '2019_02_20_164839_create_pay_advance_table', 19),
(212, '2019_02_20_183051_create_salary_structure_table', 19),
(213, '2019_02_20_183120_create_salary_structure_map_table', 19),
(214, '2019_02_20_183137_create_sal_struct_map_staff_table', 19),
(217, '2019_02_22_150424_create_c_certs_table', 20),
(218, '2019_02_22_184942_create_add_c_c_status_field_s_tudent_table', 20),
(220, '2019_02_25_122025_create_marksheet_templates_table', 21),
(221, '2019_02_26_153316_add_field_purchase_items', 22),
(226, '2019_02_21_093758_create_task_response_table', 23),
(227, '2019_02_27_164509_add_table_sales', 23),
(228, '2019_02_27_164541_add_table_sales_return', 23),
(229, '2019_02_28_175613_create_transport_fees_table', 23),
(230, '2019_03_01_111909_create_transport_fee_details_table', 24),
(231, '2019_03_01_182121_add_field_table_sales', 24),
(232, '2019_03_07_161358_create_hostel_fees_table', 24),
(233, '2019_03_07_162111_create_hostel_fee_details_table', 24),
(234, '2019_03_11_110643_drop_add_field_route', 25),
(235, '2019_03_11_125828_add_field_map_student_staff_vehicle', 25),
(236, '2019_03_11_134348_create_inv_config_sales_trigger', 25),
(237, '2019_03_12_122549_create_add_fields_fee_details_table', 26),
(238, '2019_03_12_111138_add_field_schedule_map', 27),
(242, '2019_03_12_115555_add_field_exam_result', 28),
(244, '2019_03_13_154911_create_add_hostel_fee_fields_table', 29),
(245, '2019_03_13_175948_create_st_cheques_update_table', 29),
(247, '2019_03_15_165132_create_add_status_stcheques_table', 30),
(248, '2019_03_22_115548_create_pay_leave_scheme_table', 31),
(249, '2019_03_22_122623_create_pay_leave_scheme_map_table', 31),
(250, '2019_03_25_181352_create_master_module_table', 32),
(251, '2019_03_25_182717_create_module_permissions_table', 33),
(252, '2019_03_22_115549_create_hostel_fees_table', 34),
(253, '2019_03_22_115551_create_add_status_stcheques_table', 35),
(256, '2019_03_22_122622_create_pay_leave_scheme_map_table', 36),
(257, '2019_03_22_122623_create_hostel_fee_details_table', 36),
(258, '2019_03_22_122624_create_add_hostel_fee_fields_table', 36),
(259, '2019_03_26_184601_create_staff_leave_history_info_table', 37),
(260, '2019_03_27_101416_add_fields_staff_leaves_table', 37),
(261, '2019_03_27_175521_add_fields_staff_leave_history_table', 37),
(262, '2019_03_28_161630_add_field_staff_leave_table', 37),
(263, '2019_03_30_120308_create_add_field_permission_table', 38),
(264, '2019_03_30_124834_add_fields_leave_history_info_table', 39),
(266, '2019_04_01_130008_create_add_homework_fields_table', 40),
(267, '2019_04_01_151135_create_system_config_table', 41),
(268, '2019_04_01_165310_create_add_staffleave_session_table', 41),
(269, '2019_04_01_182430_create_acc_group_head_table', 42);

-- --------------------------------------------------------

--
-- Table structure for table `module_permissions`
--

CREATE TABLE `module_permissions` (
  `module_permission_id` int(10) UNSIGNED NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_permissions`
--

INSERT INTO `module_permissions` (`module_permission_id`, `permissions`, `created_at`, `updated_at`) VALUES
(1, '12,13,14,15,16,17,18,19,20', '2019-03-25 13:01:25', '2019-04-01 06:09:01');

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `nationality_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `nationality_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`nationality_id`, `admin_id`, `update_by`, `nationality_name`, `nationality_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Indian', 1, '2019-01-23 10:45:47', '2019-01-23 10:45:57'),
(2, 2, 2, 'AUSTRALIA', 1, '2019-04-05 21:12:49', '2019-04-05 21:12:49');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `notice_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_text` text COLLATE utf8mb4_unicode_ci,
  `class_ids` text COLLATE utf8mb4_unicode_ci,
  `staff_ids` text COLLATE utf8mb4_unicode_ci,
  `for_class` tinyint(4) DEFAULT NULL COMMENT '0=All,1=Selected',
  `for_staff` tinyint(4) DEFAULT NULL COMMENT '0=All,1=Selected',
  `notice_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `admin_id`, `update_by`, `session_id`, `notice_name`, `notice_text`, `class_ids`, `staff_ids`, `for_class`, `for_staff`, `notice_status`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 1, 'B', 'B', NULL, NULL, 0, NULL, 0, '2019-02-12 11:10:29', '2019-04-06 18:41:58'),
(3, 2, 2, 1, 'C', 'C', '1', NULL, 1, NULL, 0, '2019-02-12 11:10:48', '2019-04-06 18:41:53'),
(4, 2, 2, 1, 'D', 'D', NULL, '2,1', NULL, 1, 0, '2019-02-12 11:11:39', '2019-04-06 18:41:28'),
(7, 2, 2, 1, 'Sourabh is coming in your school !!', 'Everyone is required !!', '5,6', '6', 1, 1, 1, '2019-04-06 18:43:04', '2019-04-06 18:44:09');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `notification_via` tinyint(4) DEFAULT NULL COMMENT '0=Normal,1=Topic,2=All Students,3=All Staff',
  `topic_type` tinyint(4) DEFAULT NULL COMMENT '1=Student,1=Parent',
  `notification_type` text COLLATE utf8mb4_unicode_ci,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `module_id` int(10) UNSIGNED DEFAULT NULL,
  `student_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `school_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `notification_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `admin_id`, `update_by`, `notification_via`, `topic_type`, `notification_type`, `session_id`, `class_id`, `section_id`, `module_id`, `student_admin_id`, `parent_admin_id`, `staff_admin_id`, `school_admin_id`, `notification_text`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 3, NULL, 'notice_issue', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:09:14', '2019-02-12 11:09:14'),
(2, 2, 2, 2, 1, 'notice_issue', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:09:15', '2019-02-12 11:09:15'),
(3, 2, 2, 2, 2, 'notice_issue', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:09:16', '2019-02-12 11:09:16'),
(4, 2, 2, 2, 1, 'notice_issue', 1, NULL, NULL, 2, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:10:32', '2019-02-12 11:10:32'),
(5, 2, 2, 2, 2, 'notice_issue', 1, NULL, NULL, 2, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:10:33', '2019-02-12 11:10:33'),
(6, 2, 2, 1, 1, 'notice_issue', 1, 1, 1, 3, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:10:48', '2019-02-12 11:10:48'),
(7, 2, 2, 1, 2, 'notice_issue', 1, 1, 1, 3, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:10:49', '2019-02-12 11:10:49'),
(8, 2, 2, 1, 1, 'notice_issue', 1, 1, 2, 3, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:10:49', '2019-02-12 11:10:49'),
(9, 2, 2, 1, 2, 'notice_issue', 1, 1, 2, 3, NULL, NULL, NULL, NULL, 'New Notice Issued', '2019-02-12 11:10:49', '2019-02-12 11:10:49'),
(10, 2, 2, 0, NULL, 'notice_issue', 1, NULL, NULL, 6, NULL, NULL, 3, NULL, NULL, '2019-02-12 11:16:45', '2019-02-12 11:16:45'),
(11, 2, 2, 0, NULL, 'notice_issue', 1, NULL, NULL, 6, NULL, NULL, 23, NULL, NULL, '2019-02-12 11:16:45', '2019-02-12 11:16:45'),
(12, 2, 2, 1, 1, 'notes_add', 1, 1, 2, NULL, NULL, NULL, NULL, NULL, 'New online content is published, click here to check.', '2019-02-12 11:20:39', '2019-02-12 11:20:39'),
(13, 2, 2, 1, 2, 'notes_add', 1, 1, 2, NULL, NULL, NULL, NULL, NULL, 'New online content is published, click here to check.', '2019-02-12 11:20:40', '2019-02-12 11:20:40'),
(14, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 1, 16, 15, NULL, NULL, 'Amit you have got a participation certificate of test', '2019-02-12 11:21:40', '2019-02-12 11:21:40'),
(15, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 1, 14, 13, NULL, NULL, 'Shri you have got a participation certificate of test', '2019-02-12 11:21:41', '2019-02-12 11:21:41'),
(16, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 1, 16, 15, NULL, NULL, NULL, '2019-02-12 11:22:17', '2019-02-12 11:22:17'),
(17, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 1, 14, 13, NULL, NULL, NULL, '2019-02-12 11:22:18', '2019-02-12 11:22:18'),
(18, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 1, NULL, 5, NULL, NULL, 'Teena is Present in school on 12 February 2019.', '2019-02-12 11:24:59', '2019-02-12 11:24:59'),
(19, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 2, NULL, 9, NULL, NULL, 'Ajay is Absent in school on 12 February 2019.', '2019-02-12 11:25:00', '2019-02-12 11:25:00'),
(20, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 3, NULL, 9, NULL, NULL, 'Neelam is Absent in school on 12 February 2019.', '2019-02-12 11:25:00', '2019-02-12 11:25:00'),
(21, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 4, NULL, 13, NULL, NULL, 'Shri is Absent in school on 12 February 2019.', '2019-02-12 11:25:01', '2019-02-12 11:25:01'),
(22, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 5, NULL, 5, NULL, NULL, 'Pankaj is Present in school on 12 February 2019.', '2019-02-12 11:25:01', '2019-02-12 11:25:01'),
(23, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 6, NULL, 15, NULL, NULL, 'Amit is Present in school on 12 February 2019.', '2019-02-12 11:25:02', '2019-02-12 11:25:02'),
(24, 2, 2, 1, 1, 'exam_schedule_create', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-12 11:26:29', '2019-02-12 11:26:29'),
(25, 2, 2, 1, 2, 'exam_schedule_create', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-12 11:26:30', '2019-02-12 11:26:30'),
(26, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 1, NULL, NULL, 23, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-12 11:26:31', '2019-02-12 11:26:31'),
(27, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 1, NULL, NULL, 23, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-12 11:26:31', '2019-02-12 11:26:31'),
(28, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 1, NULL, NULL, 3, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-12 11:26:31', '2019-02-12 11:26:31'),
(29, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 1, NULL, NULL, 3, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-12 11:26:31', '2019-02-12 11:26:31'),
(30, 2, 2, 1, 1, 'exam_schedule_create', 1, 1, 2, 1, NULL, NULL, NULL, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-14 08:08:27', '2019-02-14 08:08:27'),
(31, 2, 2, 1, 2, 'exam_schedule_create', 1, 1, 2, 1, NULL, NULL, NULL, NULL, 'Mid Term 1 Scheduled is published.', '2019-02-14 08:08:28', '2019-02-14 08:08:28'),
(32, 2, 2, 1, 1, 'exam_schedule_create', 1, 1, 1, 2, NULL, NULL, NULL, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:36', '2019-02-27 11:13:36'),
(33, 2, 2, 1, 2, 'exam_schedule_create', 1, 1, 1, 2, NULL, NULL, NULL, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:37', '2019-02-27 11:13:37'),
(34, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 2, NULL, NULL, 23, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:37', '2019-02-27 11:13:37'),
(35, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 2, NULL, NULL, 23, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:37', '2019-02-27 11:13:37'),
(36, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 2, NULL, NULL, 3, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:37', '2019-02-27 11:13:37'),
(37, 2, 2, 0, NULL, 'exam_schedule_create', 1, NULL, NULL, 2, NULL, NULL, 3, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:37', '2019-02-27 11:13:37'),
(38, 2, 2, 1, 1, 'exam_schedule_create', 1, 1, 2, 2, NULL, NULL, NULL, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:40', '2019-02-27 11:13:40'),
(39, 2, 2, 1, 2, 'exam_schedule_create', 1, 1, 2, 2, NULL, NULL, NULL, NULL, 'Exam New Scheduled is published.', '2019-02-27 11:13:41', '2019-02-27 11:13:41'),
(40, 3, 3, 1, 1, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 07:54:43', '2019-04-01 07:54:43'),
(41, 3, 3, 1, 2, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 07:54:44', '2019-04-01 07:54:44'),
(42, 3, 3, 1, 1, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 07:57:29', '2019-04-01 07:57:29'),
(43, 3, 3, 1, 2, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 07:57:29', '2019-04-01 07:57:29'),
(44, 3, 3, 1, 1, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 08:23:38', '2019-04-01 08:23:38'),
(45, 3, 3, 1, 2, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 08:23:38', '2019-04-01 08:23:38'),
(46, 3, 3, 1, 1, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 08:32:26', '2019-04-01 08:32:26'),
(47, 3, 3, 1, 2, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 08:32:26', '2019-04-01 08:32:26'),
(48, 3, 3, 1, 1, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 09:49:36', '2019-04-01 09:49:36'),
(49, 3, 3, 1, 2, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 09:49:36', '2019-04-01 09:49:36'),
(50, 3, 3, 1, 1, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 09:53:51', '2019-04-01 09:53:51'),
(51, 3, 3, 1, 2, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 09:53:51', '2019-04-01 09:53:51'),
(52, 23, 23, 1, 1, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 10:05:16', '2019-04-01 10:05:16'),
(53, 23, 23, 1, 2, 'student_homework_by_staff', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'Today\'s homework is updated.', '2019-04-01 10:05:16', '2019-04-01 10:05:16'),
(54, NULL, NULL, 0, NULL, 'staff_leave', NULL, NULL, NULL, 1, NULL, NULL, 23, NULL, 'Dinesh applied for leave', '2019-04-01 10:57:10', '2019-04-01 10:57:10'),
(55, NULL, NULL, 0, NULL, 'staff_leave', NULL, NULL, NULL, 2, NULL, NULL, 23, NULL, 'Dinesh applied for leave', '2019-04-01 10:57:40', '2019-04-01 10:57:40'),
(56, NULL, NULL, 0, NULL, 'staff_leave', NULL, NULL, NULL, 3, NULL, NULL, 23, NULL, 'Dinesh applied for leave', '2019-04-01 11:22:17', '2019-04-01 11:22:17'),
(57, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 2, NULL, 5, NULL, NULL, 'Sibling A you have got a participation certificate of Cultural Competition', '2019-04-02 16:35:56', '2019-04-02 16:35:56'),
(58, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 2, NULL, NULL, NULL, NULL, ' you have got a participation certificate of Cultural Competition', '2019-04-02 16:35:57', '2019-04-02 16:35:57'),
(59, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 2, 14, 13, NULL, NULL, 'Shri you have got a participation certificate of Cultural Competition', '2019-04-02 16:35:57', '2019-04-02 16:35:57'),
(60, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 2, 12, 9, NULL, NULL, 'Neelam you have got a participation certificate of Cultural Competition', '2019-04-02 16:35:57', '2019-04-02 16:35:57'),
(61, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 2, 10, 9, NULL, NULL, 'Ajay you have got a participation certificate of Cultural Competition', '2019-04-02 16:35:57', '2019-04-02 16:35:57'),
(62, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 2, 6, 5, NULL, NULL, 'Teena you have got a participation certificate of Cultural Competition', '2019-04-02 16:35:57', '2019-04-02 16:35:57'),
(63, 2, 2, 0, NULL, 'competition_participate', 1, NULL, NULL, 2, 8, 5, NULL, NULL, 'Pankaj you have got a participation certificate of Cultural Competition', '2019-04-02 16:36:23', '2019-04-02 16:36:23'),
(64, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, NULL, 5, NULL, NULL, NULL, '2019-04-02 16:37:21', '2019-04-02 16:37:21'),
(65, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2019-04-02 16:37:21', '2019-04-02 16:37:21'),
(66, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, 14, 13, NULL, NULL, NULL, '2019-04-02 16:37:21', '2019-04-02 16:37:21'),
(67, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, 12, 9, NULL, NULL, NULL, '2019-04-02 16:37:21', '2019-04-02 16:37:21'),
(68, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, 10, 9, NULL, NULL, NULL, '2019-04-02 16:37:21', '2019-04-02 16:37:21'),
(69, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, 6, 5, NULL, NULL, NULL, '2019-04-02 16:37:21', '2019-04-02 16:37:21'),
(70, NULL, NULL, 0, NULL, 'admission_new_applicant', NULL, NULL, NULL, 10, NULL, NULL, 23, NULL, 'New Application for admission in  received', '2019-04-02 16:48:23', '2019-04-02 16:48:23'),
(71, NULL, NULL, 0, NULL, 'admission_new_applicant', NULL, NULL, NULL, 11, NULL, NULL, 23, NULL, 'New Application for admission in Class I received', '2019-04-02 16:52:18', '2019-04-02 16:52:18'),
(72, 2, 2, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 1, NULL, NULL, 26, NULL, NULL, '2019-04-02 17:32:58', '2019-04-02 17:32:58'),
(73, 2, 2, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 2, NULL, NULL, 23, NULL, NULL, '2019-04-02 17:33:29', '2019-04-02 17:33:29'),
(74, 2, 2, 1, 1, 'notes_add', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 'New online content is published, click here to check.', '2019-04-02 17:43:22', '2019-04-02 17:43:22'),
(75, 2, 2, 1, 2, 'notes_add', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 'New online content is published, click here to check.', '2019-04-02 17:43:23', '2019-04-02 17:43:23'),
(76, 2, 2, 0, NULL, 'task_add_student', 1, NULL, NULL, 1, 1, 5, NULL, NULL, 'learning science topic', '2019-04-02 17:48:02', '2019-04-02 17:48:02'),
(77, 2, 2, 0, NULL, 'task_add_student', 1, NULL, NULL, 2, 5, 9, NULL, NULL, 'learning', '2019-04-02 17:49:38', '2019-04-02 17:49:38'),
(78, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, NULL, 5, NULL, NULL, NULL, '2019-04-03 16:08:37', '2019-04-03 16:08:37'),
(79, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, 14, 13, NULL, NULL, NULL, '2019-04-03 16:08:38', '2019-04-03 16:08:38'),
(80, 2, 2, 0, NULL, 'competition_winner', 1, NULL, NULL, 2, 10, 9, NULL, NULL, NULL, '2019-04-03 16:08:38', '2019-04-03 16:08:38'),
(81, NULL, NULL, 0, NULL, 'staff_leave', NULL, NULL, NULL, 4, NULL, NULL, 23, NULL, 'Sourabh Rohila applied for leave', '2019-04-03 22:34:02', '2019-04-03 22:34:02'),
(82, NULL, NULL, 0, NULL, 'staff_leave', NULL, NULL, NULL, 5, NULL, NULL, 23, NULL, 'Sourabh Rohila applied for leave', '2019-04-03 22:39:57', '2019-04-03 22:39:57'),
(83, 2, 2, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 3, NULL, NULL, 30, NULL, NULL, '2019-04-03 22:46:31', '2019-04-03 22:46:31'),
(84, 2, 2, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 3, NULL, NULL, 30, NULL, NULL, '2019-04-03 22:49:01', '2019-04-03 22:49:01'),
(85, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 1 - 001', '2019-04-04 15:36:11', '2019-04-04 15:36:11'),
(86, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:36:24', '2019-04-04 15:36:24'),
(87, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:36:35', '2019-04-04 15:36:35'),
(88, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 2, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:36:57', '2019-04-04 15:36:57'),
(89, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 3, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:37:06', '2019-04-04 15:37:06'),
(90, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 4, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:37:19', '2019-04-04 15:37:19'),
(91, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 5, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:37:28', '2019-04-04 15:37:28'),
(92, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 6, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:37:35', '2019-04-04 15:37:35'),
(93, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 7, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:37:46', '2019-04-04 15:37:46'),
(94, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 8, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:38:10', '2019-04-04 15:38:10'),
(95, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 9, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 5 - 005', '2019-04-04 15:38:19', '2019-04-04 15:38:19'),
(96, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 10, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 1 - 001', '2019-04-04 15:39:55', '2019-04-04 15:39:55'),
(97, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 1 - 001', '2019-04-04 16:45:03', '2019-04-04 16:45:03'),
(98, 3, 3, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 4, NULL, NULL, 29, NULL, 'testing plan', '2019-04-04 18:16:48', '2019-04-04 18:16:48'),
(99, 30, 30, 0, NULL, 'staff_leave', NULL, NULL, NULL, 6, NULL, NULL, 23, NULL, 'Sourabh Rohila applied for leave', '2019-04-04 18:48:38', '2019-04-04 18:48:38'),
(100, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, 'Sourabh Rohila is Absent in school on 04 April 2019.', '2019-04-04 18:56:09', '2019-04-04 18:56:09'),
(101, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, 'Sourabh Rohila is Present in school on 04 April 2019.', '2019-04-04 18:56:17', '2019-04-04 18:56:17'),
(102, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, 'Sourabh Rohila is Absent in school on 04 April 2019.', '2019-04-04 18:56:26', '2019-04-04 18:56:26'),
(103, 3, 3, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 4, NULL, NULL, 29, NULL, 'testing plan on going on in the best time for a good time for a few weeks back I have to be able and the other hand I promise to the same the day and I am not a good time for you are not under the best time for you to the legend and to the greatest of these are all set', '2019-04-04 18:56:54', '2019-04-04 18:56:54'),
(104, 3, 3, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 4, NULL, NULL, 29, NULL, 'testing plan on going on in the best time for a good time for a few weeks back I have to be able and the other hand I promise to the same the day and I am not a good time for you are not under the best time for you to the legend and to the greatest of these are all set', '2019-04-04 18:57:20', '2019-04-04 18:57:20'),
(105, 3, 3, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 4, NULL, NULL, 29, NULL, 'testing plan on going on in the best time for a good time for a few weeks back I have to be able and the other hand I promise to the same the day and I am not a good time for you are not under the best time for you to the legend and to the greatest of these are all set', '2019-04-04 19:03:18', '2019-04-04 19:03:18'),
(106, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 1, NULL, NULL, 30, NULL, 'Sourabh Rohila applied for leave', '2019-04-04 21:59:10', '2019-04-04 21:59:10'),
(107, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 1, NULL, NULL, 30, NULL, 'Sourabh Rohila applied for leave', '2019-04-04 22:00:11', '2019-04-04 22:00:11'),
(108, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 1, NULL, NULL, 30, NULL, 'Sourabh Rohila applied for leave', '2019-04-04 22:00:43', '2019-04-04 22:00:43'),
(109, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 1, NULL, NULL, 30, NULL, 'Sourabh Rohila applied for leave', '2019-04-04 22:01:07', '2019-04-04 22:01:07'),
(110, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 2, NULL, NULL, 30, NULL, 'Sourabh Rohila applied for leave', '2019-04-04 22:02:53', '2019-04-04 22:02:53'),
(111, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 3, NULL, NULL, 30, NULL, 'Sourabh Rohila applied for leave', '2019-04-04 22:06:32', '2019-04-04 22:06:32'),
(112, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 12, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 1 - 001', '2019-04-04 22:27:54', '2019-04-04 22:27:54'),
(113, NULL, NULL, 0, NULL, 'student_dairy_remark_by_staff', 1, NULL, NULL, 13, NULL, NULL, NULL, NULL, 'Sourabh Rohila received remark in Test Subject 1 - 001', '2019-04-04 22:28:22', '2019-04-04 22:28:22'),
(114, 2, 2, 0, NULL, 'plan_scheduled', 1, NULL, NULL, 5, NULL, NULL, 30, NULL, NULL, '2019-04-04 22:30:56', '2019-04-04 22:30:56'),
(115, 30, 30, 0, NULL, 'staff_leave', NULL, NULL, NULL, 7, NULL, NULL, 23, NULL, 'Sourabh Rohila 123 applied for leave', '2019-04-05 20:08:23', '2019-04-05 20:08:23'),
(116, 30, 30, 0, NULL, 'staff_leave', NULL, NULL, NULL, 8, NULL, NULL, 23, NULL, 'Sourabh Rohila 123 applied for leave', '2019-04-05 20:08:48', '2019-04-05 20:08:48'),
(117, 30, 30, 0, NULL, 'staff_leave', NULL, NULL, NULL, 9, NULL, NULL, 23, NULL, 'Sourabh Rohila 123 applied for leave', '2019-04-05 20:09:19', '2019-04-05 20:09:19'),
(118, 2, 2, 3, 0, 'notice_issue', 1, NULL, NULL, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:43:04', '2019-04-06 18:43:04'),
(119, 2, 2, 2, 1, 'notice_issue', 1, NULL, NULL, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:43:05', '2019-04-06 18:43:05'),
(120, 2, 2, 2, 2, 'notice_issue', 1, NULL, NULL, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:43:05', '2019-04-06 18:43:05'),
(121, 2, 2, 3, 0, 'notice_issue', 1, NULL, NULL, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:43:37', '2019-04-06 18:43:37'),
(122, 2, 2, 1, 1, 'notice_issue', 1, 6, 6, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:43:38', '2019-04-06 18:43:38'),
(123, 2, 2, 1, 2, 'notice_issue', 1, 6, 6, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:43:38', '2019-04-06 18:43:38'),
(124, 2, 2, 0, NULL, 'notice_issue', 1, NULL, NULL, 7, NULL, NULL, 30, NULL, 'Everyone is required !!', '2019-04-06 18:44:09', '2019-04-06 18:44:09'),
(125, 2, 2, 1, 1, 'notice_issue', 1, 5, 5, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:44:09', '2019-04-06 18:44:09'),
(126, 2, 2, 1, 2, 'notice_issue', 1, 5, 5, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:44:09', '2019-04-06 18:44:09'),
(127, 2, 2, 1, 1, 'notice_issue', 1, 6, 6, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:44:10', '2019-04-06 18:44:10'),
(128, 2, 2, 1, 2, 'notice_issue', 1, 6, 6, 7, NULL, NULL, NULL, NULL, 'Everyone is required !!', '2019-04-06 18:44:10', '2019-04-06 18:44:10'),
(129, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, 'khushbu is Present in school on 17 March 2019.', '2019-04-08 22:20:28', '2019-04-08 22:20:28'),
(130, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'khushbu is Absent in school on 07 April 2019.', '2019-04-08 22:56:49', '2019-04-08 22:56:49'),
(131, NULL, NULL, 0, NULL, 'student_attendance', NULL, NULL, NULL, 12, NULL, NULL, NULL, NULL, 'khushbu is Absent in school on 29 March 2019.', '2019-04-09 16:52:07', '2019-04-09 16:52:07'),
(132, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 13, NULL, NULL, 23, NULL, 'Teena applied for leave', '2019-04-09 17:44:02', '2019-04-09 17:44:02'),
(133, NULL, NULL, 0, NULL, 'student_leave_by_parent', NULL, NULL, NULL, 14, NULL, NULL, 25, NULL, 'Pankaj applied for leave', '2019-04-09 18:59:02', '2019-04-09 18:59:02'),
(134, 25, 25, 0, NULL, 'staff_leave', NULL, NULL, NULL, 10, NULL, NULL, 23, NULL, 'ramesh applied for leave', '2019-04-09 20:06:05', '2019-04-09 20:06:05'),
(135, 25, 25, 0, NULL, 'staff_leave', NULL, NULL, NULL, 11, NULL, NULL, 23, NULL, 'ramesh applied for leave', '2019-04-09 20:06:34', '2019-04-09 20:06:34'),
(136, 25, 25, 0, NULL, 'staff_leave', NULL, NULL, NULL, 12, NULL, NULL, 23, NULL, 'ramesh applied for leave', '2019-04-09 20:09:51', '2019-04-09 20:09:51');

-- --------------------------------------------------------

--
-- Table structure for table `one_time_heads`
--

CREATE TABLE `one_time_heads` (
  `ot_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `ot_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ot_alias` text COLLATE utf8mb4_unicode_ci,
  `ot_classes` text COLLATE utf8mb4_unicode_ci COMMENT 'Classes ids are comma seperated store here',
  `ot_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `ot_date` date DEFAULT NULL,
  `ot_refundable` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `for_students` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=New,1=Old,2=Both',
  `deduct_imprest_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `receipt_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `ot_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `one_time_heads`
--

INSERT INTO `one_time_heads` (`ot_head_id`, `admin_id`, `update_by`, `ot_particular_name`, `ot_alias`, `ot_classes`, `ot_amount`, `ot_date`, `ot_refundable`, `for_students`, `deduct_imprest_status`, `receipt_status`, `ot_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Picnic', 'Picnic', '1', 100.00, '2019-02-28', 0, 0, 1, 0, 1, '2019-02-14 06:14:11', '2019-02-14 06:14:11'),
(2, 2, 2, 'Admission Fee', 'Admission Fee', '1', 1000.00, '2019-03-06', 0, 0, 0, 0, 1, '2019-03-05 11:23:33', '2019-03-05 11:23:33'),
(3, 2, 2, 'Test Fees', 'Test Fees', '1', 5000.00, '2019-03-10', 0, 2, 0, 0, 1, '2019-03-08 12:22:11', '2019-03-08 12:22:36');

-- --------------------------------------------------------

--
-- Table structure for table `online_notes`
--

CREATE TABLE `online_notes` (
  `online_note_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `online_note_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online_note_topic` text COLLATE utf8mb4_unicode_ci,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_url` text COLLATE utf8mb4_unicode_ci,
  `online_note_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `online_notes`
--

INSERT INTO `online_notes` (`online_note_id`, `admin_id`, `update_by`, `online_note_name`, `class_id`, `section_id`, `subject_id`, `online_note_unit`, `online_note_topic`, `staff_id`, `online_note_url`, `online_note_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'test', 1, 1, 1, 'test', 'dfgd', 2, NULL, 1, '2019-02-01 06:56:33', '2019-02-01 06:56:33'),
(9, 2, 2, 'gfgf', 1, 1, 2, 'dg', 're', 2, NULL, 1, '2019-02-01 06:58:41', '2019-02-01 06:58:41'),
(10, 2, 2, 'tesr', 1, 2, 2, 'yee', 'eee', 2, NULL, 1, '2019-02-12 11:20:38', '2019-02-12 11:20:38'),
(11, 2, 2, 'science notes', 1, 1, 4, 'chapter 5', 'cover all topic of chapter 5', 2, 'Admin dashboard-06.02.19.68361554189202docx', 1, '2019-04-02 17:43:22', '2019-04-02 17:43:22');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_mapping`
--

CREATE TABLE `payroll_mapping` (
  `payroll_mapping_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_dept_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_grade_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payroll_mapping`
--

INSERT INTO `payroll_mapping` (`payroll_mapping_id`, `admin_id`, `update_by`, `session_id`, `pay_dept_id`, `pay_grade_id`, `staff_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 2, 2, 1, '2019-04-02 18:40:10', '2019-04-02 18:41:03'),
(2, 2, 2, 1, 2, 1, 2, '2019-04-02 18:40:10', '2019-04-02 18:41:20'),
(3, 2, 2, 1, 1, 2, 3, '2019-04-02 18:40:23', '2019-04-02 18:41:03'),
(4, 2, 2, 1, 1, NULL, 4, '2019-04-02 18:40:23', '2019-04-02 18:40:23'),
(5, 2, 2, 1, NULL, 2, 5, '2019-04-02 18:41:03', '2019-04-02 18:41:03');

-- --------------------------------------------------------

--
-- Table structure for table `pay_advance`
--

CREATE TABLE `pay_advance` (
  `pay_advance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `advance_amount` double(18,2) DEFAULT NULL,
  `suggested_amount` double(18,2) DEFAULT NULL,
  `paid_amount` double(18,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_advance`
--

INSERT INTO `pay_advance` (`pay_advance_id`, `admin_id`, `update_by`, `session_id`, `staff_id`, `advance_amount`, `suggested_amount`, `paid_amount`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 4, 5000.00, 1000.00, NULL, '2019-04-02 18:53:52', '2019-04-02 18:53:52'),
(2, 2, 2, 1, 1, 10000.00, 2000.00, NULL, '2019-04-02 18:54:11', '2019-04-02 18:54:11');

-- --------------------------------------------------------

--
-- Table structure for table `pay_arrear`
--

CREATE TABLE `pay_arrear` (
  `pay_arrear_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `arrear_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrear_from_date` date DEFAULT NULL,
  `arrear_to_date` date DEFAULT NULL,
  `arrear_amount` double(18,2) DEFAULT NULL,
  `arrear_total_amt` double(18,2) DEFAULT NULL,
  `round_off` tinyint(4) DEFAULT NULL COMMENT '0=Nearest Rupee,1=Highest Rupee',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_arrear`
--

INSERT INTO `pay_arrear` (`pay_arrear_id`, `admin_id`, `update_by`, `arrear_name`, `arrear_from_date`, `arrear_to_date`, `arrear_amount`, `arrear_total_amt`, `round_off`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'arrear1', '2019-04-04', '2019-04-24', 10000.00, 120000.00, 1, '2019-04-02 18:50:43', '2019-04-02 18:50:43'),
(2, 2, 2, 'arrear2', '2019-04-17', '2019-05-09', 6000.00, 72000.00, 0, '2019-04-02 18:51:06', '2019-04-02 18:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `pay_arrear_map`
--

CREATE TABLE `pay_arrear_map` (
  `pay_arrear_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_arrear_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_arrear_map`
--

INSERT INTO `pay_arrear_map` (`pay_arrear_map_id`, `admin_id`, `update_by`, `session_id`, `pay_arrear_id`, `staff_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, '2019-04-02 18:50:43', '2019-04-02 18:50:43'),
(2, 2, 2, 1, 1, 2, '2019-04-02 18:50:43', '2019-04-02 18:50:43'),
(3, 2, 2, 1, 1, 3, '2019-04-02 18:50:43', '2019-04-02 18:50:43'),
(4, 2, 2, 1, 1, 4, '2019-04-02 18:50:43', '2019-04-02 18:50:43'),
(5, 2, 2, 1, 1, 5, '2019-04-02 18:50:43', '2019-04-02 18:50:43'),
(6, 2, 2, 1, 2, 1, '2019-04-02 18:51:06', '2019-04-02 18:51:06'),
(7, 2, 2, 1, 2, 2, '2019-04-02 18:51:06', '2019-04-02 18:51:06'),
(8, 2, 2, 1, 2, 3, '2019-04-02 18:51:06', '2019-04-02 18:51:06'),
(9, 2, 2, 1, 2, 5, '2019-04-02 18:51:06', '2019-04-02 18:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `pay_bonus`
--

CREATE TABLE `pay_bonus` (
  `pay_bonus_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `bonus_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bonus_amount` double(18,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_bonus`
--

INSERT INTO `pay_bonus` (`pay_bonus_id`, `admin_id`, `update_by`, `bonus_name`, `bonus_amount`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'bonus1', 2000.00, '2019-04-02 18:51:27', '2019-04-02 18:51:27'),
(2, 2, 2, 'bonus2', 5000.00, '2019-04-02 18:51:37', '2019-04-02 18:51:37'),
(3, 2, 2, 'bonus3', 10000.00, '2019-04-02 18:51:54', '2019-04-02 18:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `pay_bonus_map`
--

CREATE TABLE `pay_bonus_map` (
  `pay_bonus_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_bonus_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_bonus_map`
--

INSERT INTO `pay_bonus_map` (`pay_bonus_map_id`, `admin_id`, `update_by`, `session_id`, `pay_bonus_id`, `staff_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, '2019-04-02 18:51:27', '2019-04-02 18:51:27'),
(2, 2, 2, 1, 1, 2, '2019-04-02 18:51:27', '2019-04-02 18:51:27'),
(3, 2, 2, 1, 1, 3, '2019-04-02 18:51:27', '2019-04-02 18:51:27'),
(4, 2, 2, 1, 1, 4, '2019-04-02 18:51:27', '2019-04-02 18:51:27'),
(5, 2, 2, 1, 1, 5, '2019-04-02 18:51:27', '2019-04-02 18:51:27'),
(6, 2, 2, 1, 2, 1, '2019-04-02 18:51:37', '2019-04-02 18:51:37'),
(7, 2, 2, 1, 2, 2, '2019-04-02 18:51:37', '2019-04-02 18:51:37'),
(8, 2, 2, 1, 2, 3, '2019-04-02 18:51:37', '2019-04-02 18:51:37'),
(9, 2, 2, 1, 2, 4, '2019-04-02 18:51:37', '2019-04-02 18:51:37'),
(10, 2, 2, 1, 2, 5, '2019-04-02 18:51:37', '2019-04-02 18:51:37'),
(11, 2, 2, 1, 3, 1, '2019-04-02 18:51:54', '2019-04-02 18:51:54'),
(12, 2, 2, 1, 3, 2, '2019-04-02 18:51:54', '2019-04-02 18:51:54'),
(13, 2, 2, 1, 3, 3, '2019-04-02 18:51:54', '2019-04-02 18:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `pay_config`
--

CREATE TABLE `pay_config` (
  `pay_config_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `pay_config_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_config_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_config`
--

INSERT INTO `pay_config` (`pay_config_id`, `admin_id`, `update_by`, `pay_config_name`, `pay_config_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Provident Fund', 1, '2019-02-13 07:11:10', '2019-04-02 18:37:47'),
(2, 1, 1, 'Employee State Insurance', 1, '2019-02-13 07:11:10', '2019-04-02 18:37:47'),
(3, 1, 1, 'Tax Deducted at Source', 1, '2019-02-13 07:11:10', '2019-04-02 18:37:47'),
(4, 1, 1, 'Professional Tax', 1, '2019-02-13 07:11:10', '2019-04-02 18:37:47');

-- --------------------------------------------------------

--
-- Table structure for table `pay_dept`
--

CREATE TABLE `pay_dept` (
  `pay_dept_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `dept_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dept_description` text COLLATE utf8mb4_unicode_ci,
  `dept_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_dept`
--

INSERT INTO `pay_dept` (`pay_dept_id`, `admin_id`, `update_by`, `dept_name`, `dept_description`, `dept_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'dept-1', 'test', 1, '2019-04-02 18:39:52', '2019-04-02 18:39:52'),
(2, 2, 2, 'dept-2', 'tested', 1, '2019-04-02 18:40:00', '2019-04-02 18:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `pay_dept_map`
--

CREATE TABLE `pay_dept_map` (
  `pay_dept_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_dept_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_grade`
--

CREATE TABLE `pay_grade` (
  `pay_grade_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `grade_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_description` text COLLATE utf8mb4_unicode_ci,
  `grade_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_grade`
--

INSERT INTO `pay_grade` (`pay_grade_id`, `admin_id`, `update_by`, `grade_name`, `grade_description`, `grade_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Grade-1', 'test', 1, '2019-04-02 18:40:48', '2019-04-02 18:40:48'),
(2, 2, 2, 'Grade-2', 'tested', 1, '2019-04-02 18:40:54', '2019-04-02 18:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `pay_leave_scheme`
--

CREATE TABLE `pay_leave_scheme` (
  `pay_leave_scheme_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `lscheme_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lscheme_period` int(11) NOT NULL DEFAULT '1' COMMENT '1=Monthly,2=Yearly',
  `carry_forward_limit` int(11) DEFAULT NULL,
  `no_of_leaves` int(11) DEFAULT NULL,
  `special_instruction` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_leave_scheme`
--

INSERT INTO `pay_leave_scheme` (`pay_leave_scheme_id`, `admin_id`, `update_by`, `lscheme_name`, `lscheme_period`, `carry_forward_limit`, `no_of_leaves`, `special_instruction`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Every Month Leave', 1, 2, 2, 'monthly common leave', '2019-04-02 18:42:37', '2019-04-02 18:42:37'),
(2, 2, 2, 'Yearly Leaves', 2, NULL, 20, 'year wise urgent leaves', '2019-04-02 18:43:25', '2019-04-02 18:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `pay_leave_scheme_map`
--

CREATE TABLE `pay_leave_scheme_map` (
  `pay_leave_scheme_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_leave_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_leave_scheme_map`
--

INSERT INTO `pay_leave_scheme_map` (`pay_leave_scheme_map_id`, `admin_id`, `update_by`, `session_id`, `pay_leave_scheme_id`, `staff_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, '2019-04-02 18:42:37', '2019-04-02 18:42:37'),
(2, 2, 2, 1, 1, 2, '2019-04-02 18:42:37', '2019-04-02 18:42:37'),
(3, 2, 2, 1, 1, 3, '2019-04-02 18:42:37', '2019-04-02 18:42:37'),
(4, 2, 2, 1, 1, 4, '2019-04-02 18:42:37', '2019-04-02 18:42:37'),
(5, 2, 2, 1, 1, 5, '2019-04-02 18:42:37', '2019-04-02 18:42:37'),
(6, 2, 2, 1, 2, 1, '2019-04-02 18:43:25', '2019-04-02 18:43:25'),
(7, 2, 2, 1, 2, 2, '2019-04-02 18:43:25', '2019-04-02 18:43:25'),
(8, 2, 2, 1, 2, 3, '2019-04-02 18:43:25', '2019-04-02 18:43:25'),
(9, 2, 2, 1, 2, 4, '2019-04-02 18:43:25', '2019-04-02 18:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `pay_loan`
--

CREATE TABLE `pay_loan` (
  `pay_loan_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `loan_principal` double(18,2) DEFAULT NULL,
  `loan_from_date` date DEFAULT NULL,
  `loan_to_date` date DEFAULT NULL,
  `loan_inrterest_rate` double(8,2) DEFAULT NULL,
  `loan_deduct_amount` double(18,2) DEFAULT NULL,
  `loan_total_amount` double(18,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_loan`
--

INSERT INTO `pay_loan` (`pay_loan_id`, `admin_id`, `update_by`, `session_id`, `staff_id`, `loan_principal`, `loan_from_date`, `loan_to_date`, `loan_inrterest_rate`, `loan_deduct_amount`, `loan_total_amount`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 500000.00, '2019-04-02', '2020-04-02', 5.00, 25000.00, 825000.00, '2019-04-02 18:52:33', '2019-04-02 18:52:33'),
(2, 2, 2, 1, 2, 250000.00, '2019-04-01', '2020-04-01', 6.00, 15000.00, 445000.00, '2019-04-02 18:53:11', '2019-04-02 18:53:11');

-- --------------------------------------------------------

--
-- Table structure for table `pay_salary_head`
--

CREATE TABLE `pay_salary_head` (
  `pay_salary_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `sal_head_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `head_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deduction,1=Allowance',
  `pay_salary_head_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_salary_head`
--

INSERT INTO `pay_salary_head` (`pay_salary_head_id`, `admin_id`, `update_by`, `sal_head_name`, `head_type`, `pay_salary_head_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Basic Pay', 1, 1, '2019-02-19 10:16:33', '2019-02-19 10:16:33'),
(2, 2, 2, 'TA', 1, 1, '2019-04-02 18:48:40', '2019-04-02 18:48:40'),
(3, 2, 2, 'DA', 1, 1, '2019-04-02 18:48:44', '2019-04-02 18:48:44'),
(4, 2, 2, 'HRA', 1, 1, '2019-04-02 18:48:49', '2019-04-02 18:48:49');

-- --------------------------------------------------------

--
-- Table structure for table `pf_setting`
--

CREATE TABLE `pf_setting` (
  `pf_setting_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `pf_month` tinyint(4) DEFAULT NULL,
  `pf_year` int(11) DEFAULT NULL,
  `epf_a` double(8,2) DEFAULT NULL,
  `pension_fund_b` double(8,2) DEFAULT NULL,
  `epf_a_b` double(8,2) DEFAULT NULL,
  `pf_cuff_off` double(18,2) DEFAULT NULL,
  `acc_no_02` double(8,2) DEFAULT NULL,
  `acc_no_21` double(8,2) DEFAULT NULL,
  `acc_no_22` double(8,2) DEFAULT NULL,
  `round_off` tinyint(4) DEFAULT NULL COMMENT '0=Nearest Rupee,1=Highest Rupee',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pf_setting_status` int(11) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pf_setting`
--

INSERT INTO `pf_setting` (`pf_setting_id`, `admin_id`, `update_by`, `session_id`, `pf_month`, `pf_year`, `epf_a`, `pension_fund_b`, `epf_a_b`, `pf_cuff_off`, `acc_no_02`, `acc_no_21`, `acc_no_22`, `round_off`, `created_at`, `updated_at`, `pf_setting_status`) VALUES
(1, 2, 2, 1, 1, 1, 12.00, 5.00, 7.00, 2.00, 2.00, 2.00, 1.00, 1, '2019-04-02 18:44:03', '2019-04-02 18:44:03', 1),
(2, 2, 2, 1, 2, 2, 20.00, 10.00, 10.00, 10.00, 2.00, 5.00, 4.00, 0, '2019-04-02 18:44:25', '2019-04-02 18:44:25', 1),
(3, 2, 2, 1, 3, 2, 25.00, 5.00, 20.00, 5.00, 2.00, 3.00, 6.00, 0, '2019-04-02 18:44:44', '2019-04-02 18:44:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `plan_schedule`
--

CREATE TABLE `plan_schedule` (
  `plan_schedule_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `current_session_id` int(10) UNSIGNED DEFAULT NULL,
  `plan_description` text COLLATE utf8mb4_unicode_ci,
  `plan_date` date DEFAULT NULL,
  `plan_time` time DEFAULT NULL,
  `plan_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plan_schedule`
--

INSERT INTO `plan_schedule` (`plan_schedule_id`, `admin_id`, `update_by`, `staff_id`, `current_session_id`, `plan_description`, `plan_date`, `plan_time`, `plan_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 4, 1, 'lecture', '2019-04-18', '10:00:00', 1, '2019-04-02 17:32:58', '2019-04-02 17:32:58'),
(2, 2, 2, 2, 1, 'going outside', '2019-04-25', '11:00:00', 1, '2019-04-02 17:33:29', '2019-04-02 17:33:29'),
(3, 2, 2, 6, 1, '2213213', '1994-12-02', '10:00:00', 0, '2019-04-03 22:46:31', '2019-04-05 20:01:21'),
(5, 2, 2, 6, 1, 'Call me once !!', '2019-04-05', '13:00:00', 1, '2019-04-04 22:30:56', '2019-04-04 22:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `pt_setting`
--

CREATE TABLE `pt_setting` (
  `pt_setting_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `salary_min` double(18,2) DEFAULT NULL,
  `salary_max` double(18,2) DEFAULT NULL,
  `monthly_amount` text COLLATE utf8mb4_unicode_ci,
  `pt_setting_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pt_setting`
--

INSERT INTO `pt_setting` (`pt_setting_id`, `admin_id`, `update_by`, `session_id`, `salary_min`, `salary_max`, `monthly_amount`, `pt_setting_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 10000.00, 20000.00, '{\"jan\":\"500\",\"feb\":\"500\",\"mar\":\"500\",\"apr\":\"500\",\"may\":\"500\",\"jun\":\"500\",\"jul\":\"500\",\"aug\":\"500\",\"sep\":\"500\",\"oct\":\"500\",\"nov\":\"1000\",\"dec\":\"500\"}', 1, '2019-04-02 18:47:56', '2019-04-02 18:47:56'),
(2, 2, 2, 1, 20001.00, 30000.00, '{\"jan\":\"2000\",\"feb\":\"2000\",\"mar\":\"2000\",\"apr\":\"2000\",\"may\":\"2000\",\"jun\":\"2000\",\"jul\":\"2000\",\"aug\":\"2000\",\"sep\":\"2000\",\"oct\":\"2000\",\"nov\":\"500\",\"dec\":\"2000\"}', 1, '2019-04-02 18:48:23', '2019-04-02 18:48:23');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchase_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `system_invoice_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_invoice_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `vendor_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `subcategory_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_mode` tinyint(4) DEFAULT NULL COMMENT '0=Cash,1=Credit,2=Cheque',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `cheque_no` int(11) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `cheque_amount` bigint(20) DEFAULT NULL,
  `total_amount` double(18,2) DEFAULT NULL,
  `tax` double(18,2) DEFAULT NULL,
  `discount` double(18,2) DEFAULT NULL,
  `gross_amount` double(18,2) DEFAULT NULL,
  `net_amount` double(18,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`purchase_id`, `admin_id`, `update_by`, `system_invoice_no`, `ref_invoice_no`, `date`, `vendor_id`, `category_id`, `subcategory_id`, `payment_mode`, `bank_id`, `cheque_no`, `cheque_date`, `cheque_amount`, `total_amount`, `tax`, `discount`, `gross_amount`, `net_amount`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'PI2019040211', 'PI-02', '2019-04-10', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1650.00, 10.00, 5.00, 1724.25, 1725.00, '2019-04-02 19:00:19', '2019-04-02 19:00:19'),
(2, 2, 2, 'PI2019040212', 'PI-03', '2019-04-18', 3, NULL, NULL, 2, 3, 45863, '2019-04-11', 15000, 5000.00, 20.00, 15.00, 5100.00, 5100.00, '2019-04-02 19:01:07', '2019-04-02 19:01:07');

--
-- Triggers `purchases`
--
DELIMITER $$
CREATE TRIGGER `update_purchase_counter` AFTER INSERT ON `purchases` FOR EACH ROW BEGIN
            UPDATE `invoice_config` 
            SET `total_invoice` = total_invoice+1 
            WHERE `invoice_type` = 1;
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_items`
--

CREATE TABLE `purchase_items` (
  `purchase_items_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `purchase_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `unit_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sub_category_id` int(10) UNSIGNED DEFAULT NULL,
  `purchase_type` int(10) UNSIGNED DEFAULT NULL COMMENT '1=Purchase,2=purchase Return',
  `rate` double(8,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amount` double(18,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_items`
--

INSERT INTO `purchase_items` (`purchase_items_id`, `admin_id`, `update_by`, `purchase_id`, `item_id`, `unit_id`, `category_id`, `sub_category_id`, `purchase_type`, `rate`, `quantity`, `amount`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 3, 1, 3, 1, 5.00, 50, 250.00, '2019-04-02 19:00:19', '2019-04-02 19:00:19'),
(2, 2, 2, 1, 3, 3, 1, 4, 1, 10.00, 40, 400.00, '2019-04-02 19:00:19', '2019-04-02 19:00:19'),
(3, 2, 2, 1, 2, 3, 2, 6, 1, 20.00, 50, 1000.00, '2019-04-02 19:00:19', '2019-04-02 19:00:19'),
(4, 2, 2, 2, 2, 3, 2, 6, 1, 50.00, 100, 5000.00, '2019-04-02 19:01:07', '2019-04-02 19:01:07'),
(5, 2, 2, 2, 2, 3, 2, 6, 2, 50.00, 50, 2500.00, '2019-04-02 19:47:01', '2019-04-02 19:47:01'),
(6, 2, 2, 1, 1, 3, 1, 3, 2, 5.00, 20, 100.00, '2019-04-02 19:48:52', '2019-04-02 19:48:52'),
(7, 2, 2, 1, 2, 3, 2, 6, 2, 20.00, 2, 40.00, '2019-04-02 19:48:52', '2019-04-02 19:48:52');

-- --------------------------------------------------------

--
-- Table structure for table `question_papers`
--

CREATE TABLE `question_papers` (
  `question_paper_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `question_paper_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `question_paper_file` text COLLATE utf8mb4_unicode_ci,
  `question_paper_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_papers`
--

INSERT INTO `question_papers` (`question_paper_id`, `admin_id`, `update_by`, `question_paper_name`, `class_id`, `section_id`, `subject_id`, `exam_id`, `staff_id`, `question_paper_file`, `question_paper_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'english question paper', 2, 3, 2, 3, NULL, 'Admin dashboard-06.02.19.282211554189285docx', 1, '2019-04-02 17:44:45', '2019-04-02 17:45:13'),
(2, 2, 2, 'hindi question paper', 2, 3, 1, 3, NULL, 'Admin dashboard-06.02.19.232541554189350docx', 1, '2019-04-02 17:45:50', '2019-04-02 17:45:50'),
(3, 2, 2, 'test', 1, 1, 1, 4, NULL, 'studentReceipt78761554791958.pdf', 1, '2019-04-09 17:09:18', '2019-04-09 17:09:18'),
(4, 2, 2, 'testing questions', 2, 3, 1, 1, NULL, 'microlog178641554792608.txt', 1, '2019-04-09 17:20:08', '2019-04-09 17:20:08'),
(5, 2, 2, 'Website Question', 1, 1, 1, 4, NULL, 'Student App testing_28.2.19.452631554792773docx', 1, '2019-04-09 17:22:53', '2019-04-09 17:22:53');

-- --------------------------------------------------------

--
-- Table structure for table `reasons`
--

CREATE TABLE `reasons` (
  `reason_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reason_text` text COLLATE utf8mb4_unicode_ci,
  `reason_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reasons`
--

INSERT INTO `reasons` (`reason_id`, `admin_id`, `update_by`, `reason_text`, `reason_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'By Management', 1, '2019-03-12 06:01:30', '2019-03-12 06:01:30'),
(2, 2, 2, 'poor famliy', 1, '2019-04-02 17:14:38', '2019-04-02 17:14:38'),
(3, 2, 2, 'student intelligence', 1, '2019-04-02 17:15:10', '2019-04-02 17:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `recurring_heads`
--

CREATE TABLE `recurring_heads` (
  `rc_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rc_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_alias` text COLLATE utf8mb4_unicode_ci,
  `rc_classes` text COLLATE utf8mb4_unicode_ci COMMENT 'Classes ids are comma seperated store here',
  `rc_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rc_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recurring_heads`
--

INSERT INTO `recurring_heads` (`rc_head_id`, `admin_id`, `update_by`, `rc_particular_name`, `rc_alias`, `rc_classes`, `rc_amount`, `rc_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Tuition Fee', NULL, '1', 20000.00, 1, '2019-03-07 07:30:10', '2019-03-07 07:30:10'),
(2, 2, 2, 'Tuition Fee 1', NULL, '1', 1000.00, 1, '2019-03-08 12:24:15', '2019-03-08 12:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `recurring_inst`
--

CREATE TABLE `recurring_inst` (
  `rc_inst_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rc_head_id` int(10) UNSIGNED DEFAULT NULL,
  `rc_inst_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_inst_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rc_inst_date` date DEFAULT NULL,
  `rc_inst_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recurring_inst`
--

INSERT INTO `recurring_inst` (`rc_inst_id`, `admin_id`, `update_by`, `rc_head_id`, `rc_inst_particular_name`, `rc_inst_amount`, `rc_inst_date`, `rc_inst_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '1st', 10000.00, '2019-03-08', 1, '2019-03-07 07:30:10', '2019-03-07 07:30:10'),
(2, 2, 2, 1, '2nd', 10000.00, '2019-03-10', 1, '2019-03-07 07:30:10', '2019-03-07 07:30:10'),
(3, 2, 2, 2, '1st', 1000.00, '2019-03-25', 1, '2019-03-08 12:24:15', '2019-03-08 12:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `religion_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `religion_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`religion_id`, `admin_id`, `update_by`, `religion_name`, `religion_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Hindu', 1, '2019-01-23 10:45:35', '2019-01-23 10:45:35'),
(2, 2, 2, 'SIKH', 1, '2019-04-05 21:12:09', '2019-04-05 21:12:09');

-- --------------------------------------------------------

--
-- Table structure for table `remarks`
--

CREATE TABLE `remarks` (
  `remark_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `remark_text` text COLLATE utf8mb4_unicode_ci,
  `remark_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `remarks`
--

INSERT INTO `remarks` (`remark_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `student_id`, `staff_id`, `subject_id`, `remark_text`, `remark_date`, `created_at`, `updated_at`) VALUES
(1, 30, 30, 1, 5, 5, 10, 6, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique enim nec ornare bibendum. Aenean ac tempus metus. Suspendisse potenti. In id ipsum non diam placerat hendrerit eget id sapien. Ut condimentum, metus scelerisque blandit pellentesque, ex velit pretium eros, vitae feugiat mi est et turpis. Suspendisse fringilla tincidunt libero id congue. Maecenas convallis non massa a ullamcorper. In sit amet mollis erat. Nunc metus massa, laoreet sit amet eleifend porta, aliquam a felis. Sed aliquam enim velit, id auctor arcu gravida ut. In hac habitasse platea dictumst. Quisque congue id enim ut rutrum. Cras eu eleifend magna, at porttitor ligula.', '2019-04-04', '2019-04-04 15:36:11', '2019-04-04 15:36:35'),
(2, 30, 30, 1, 5, 5, 10, 6, 10, 'dsdsad', '2019-04-04', '2019-04-04 15:36:57', '2019-04-04 15:36:57'),
(3, 30, 30, 1, 5, 5, 10, 6, 10, 'sadsadsadsad', '2019-04-04', '2019-04-04 15:37:06', '2019-04-04 15:37:06'),
(4, 30, 30, 1, 5, 5, 10, 6, 10, 'sasadsadsad', '2019-04-04', '2019-04-04 15:37:19', '2019-04-04 15:37:19'),
(5, 30, 30, 1, 5, 5, 10, 6, 10, 'sadsadsad', '2019-04-04', '2019-04-04 15:37:28', '2019-04-04 15:37:28'),
(6, 30, 30, 1, 5, 5, 10, 6, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique enim nec ornare bibendum. Aenean ac tempus metus. Suspendisse potenti. In id ipsum non diam placerat hendrerit eget id sapien. Ut condimentum, metus scelerisque blandit pellentesque, ex velit pretium eros, vitae feugiat mi est et turpis. Suspendisse fringilla tincidunt libero id congue. Maecenas convallis non massa a ullamcorper. In sit amet mollis erat. Nunc metus massa, laoreet sit amet eleifend porta, aliquam a felis. Sed aliquam enim velit, id auctor arcu gravida ut. In hac habitasse platea dictumst. Quisque congue id enim ut rutrum. Cras eu eleifend magna, at porttitor ligula.', '2019-04-04', '2019-04-04 15:37:35', '2019-04-04 15:37:35'),
(7, 30, 30, 1, 5, 5, 10, 6, 10, 'sadasdsad', '2019-04-04', '2019-04-04 15:37:46', '2019-04-04 15:37:46'),
(8, 30, 30, 1, 5, 5, 10, 6, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique enim nec ornare bibendum. Aenean ac tempus metus. Suspendisse potenti. In id ipsum non diam placerat hendrerit eget id sapien. Ut condimentum, metus scelerisque blandit pellentesque, ex velit pretium eros, vitae feugiat mi est et turpis. Suspendisse fringilla tincidunt libero id congue. Maecenas convallis non massa a ullamcorper. In sit amet mollis erat. Nunc metus massa, laoreet sit amet eleifend porta, aliquam a felis. Sed aliquam enim velit, id auctor arcu gravida ut. In hac habitasse platea dictumst. Quisque congue id enim ut rutrum. Cras eu eleifend magna, at porttitor ligula.', '2019-04-04', '2019-04-04 15:38:10', '2019-04-04 15:38:10'),
(9, 30, 30, 1, 5, 5, 10, 6, 10, 'sdsad', '2019-04-04', '2019-04-04 15:38:19', '2019-04-04 15:38:19'),
(10, 30, 30, 1, 5, 5, 10, 6, 6, 'http://v2rsolution.co.in/academic-management/admin-panel', '2019-04-04', '2019-04-04 15:39:55', '2019-04-04 15:39:55'),
(11, 30, 30, 1, 5, 5, 10, 6, 6, 'Test', '2019-04-04', '2019-04-04 16:45:03', '2019-04-04 16:45:03'),
(12, 30, 30, 1, 5, 5, 10, 6, 6, 'dssadsad', '2019-04-04', '2019-04-04 22:27:54', '2019-04-04 22:27:54'),
(13, 30, 30, 1, 5, 5, 10, 6, 6, 'Sourabh remark', '2019-04-05', '2019-04-04 22:28:22', '2019-04-05 18:09:23');

-- --------------------------------------------------------

--
-- Table structure for table `room_no`
--

CREATE TABLE `room_no` (
  `room_no_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `room_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_no_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_no`
--

INSERT INTO `room_no` (`room_no_id`, `admin_id`, `update_by`, `room_no`, `room_no_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Room-1', 1, '2019-04-02 16:02:28', '2019-04-02 16:02:28'),
(2, 2, 2, 'Room-2', 1, '2019-04-02 16:02:39', '2019-04-02 16:02:39'),
(3, 2, 2, 'ROOM', 1, '2019-04-05 21:13:36', '2019-04-05 21:13:36');

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `route_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `route_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1=Pickup,2=Drop',
  `route_description` text COLLATE utf8mb4_unicode_ci,
  `route_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`route_id`, `admin_id`, `update_by`, `route_name`, `route_location`, `route_description`, `route_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Paota', NULL, 'Paota Route', 1, '2019-03-11 04:51:49', '2019-03-11 04:51:49'),
(2, 2, 2, 'ratanada route', '1,2', 'tested', 1, '2019-04-02 17:54:03', '2019-04-02 17:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `route_locations`
--

CREATE TABLE `route_locations` (
  `route_location_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `route_id` int(10) UNSIGNED DEFAULT NULL,
  `location_sequence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_logitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_pickup_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_drop_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_km_distance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `route_locations`
--

INSERT INTO `route_locations` (`route_location_id`, `admin_id`, `update_by`, `route_id`, `location_sequence`, `location_name`, `location_latitude`, `location_logitude`, `location_pickup_time`, `location_drop_time`, `location_km_distance`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '1', 'Paota', '26.2974632', '73.03745320000007', '07:05', '12:35', '11.4 km', '2019-03-11 04:51:50', '2019-03-11 04:51:50'),
(2, 2, 2, 1, '2', 'KN College', '26.2878246', '73.03550070000006', '07:06', '12:37', '10.2 km', '2019-03-11 04:51:50', '2019-03-11 04:51:50'),
(3, 2, 2, 1, '3', 'circuit house', '26.279656', '73.03741990000003', '07:08', '12:39', '10.4 km', '2019-03-11 04:51:50', '2019-03-11 04:51:50'),
(4, 2, 2, 2, '1', 'kudi', '26.2133428', '73.0235834', '10:00', '10:10', NULL, '2019-04-02 17:54:03', '2019-04-02 17:54:03'),
(5, 2, 2, 2, '2', 'basani', '26.2274684', '73.01912749999997', '10:10', '10:20', NULL, '2019-04-02 17:54:04', '2019-04-02 17:54:04'),
(6, 2, 2, 2, '3', 'bhagat ki kothi', '26.256344552715234', '73.01709962216796', '10:20', '10:30', NULL, '2019-04-02 17:54:04', '2019-04-02 17:54:04'),
(7, 2, 2, 2, '4', 'ratanada', '26.273123890549922', '73.02808595029296', '10:30', '10:45', NULL, '2019-04-02 17:54:49', '2019-04-02 17:54:49');

-- --------------------------------------------------------

--
-- Table structure for table `rte_cheques`
--

CREATE TABLE `rte_cheques` (
  `rte_cheque_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `rte_cheque_no` text COLLATE utf8mb4_unicode_ci,
  `rte_cheque_date` date DEFAULT NULL,
  `rte_cheque_amt` text COLLATE utf8mb4_unicode_ci,
  `rte_cheque_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Cancel,2=Cleared,3=Bounce',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rte_cheques`
--

INSERT INTO `rte_cheques` (`rte_cheque_id`, `admin_id`, `update_by`, `bank_id`, `rte_cheque_no`, `rte_cheque_date`, `rte_cheque_amt`, `rte_cheque_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 3, '63214', '2019-04-02', '15000', 2, '2019-04-02 17:25:52', '2019-04-02 17:26:54'),
(2, 2, 2, 4, '56879', '2019-04-26', '50000', 0, '2019-04-02 17:26:17', '2019-04-02 17:26:17');

-- --------------------------------------------------------

--
-- Table structure for table `rte_heads`
--

CREATE TABLE `rte_heads` (
  `rte_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rte_head` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rte_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rte_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rte_heads`
--

INSERT INTO `rte_heads` (`rte_head_id`, `admin_id`, `update_by`, `rte_head`, `rte_amount`, `rte_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'RTE head 1', 5000.00, 1, '2019-04-02 17:24:40', '2019-04-02 17:24:40'),
(2, 2, 2, 'RTE head 2', 10000.00, 1, '2019-04-02 17:24:50', '2019-04-02 17:24:50');

-- --------------------------------------------------------

--
-- Table structure for table `rte_head_map`
--

CREATE TABLE `rte_head_map` (
  `rte_head_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rte_head_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_structure`
--

CREATE TABLE `salary_structure` (
  `salary_structure_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `sal_structure_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `round_off` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Nearest Rupee,1=Highest Rupee',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salary_structure`
--

INSERT INTO `salary_structure` (`salary_structure_id`, `admin_id`, `update_by`, `sal_structure_name`, `round_off`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'sal1', 1, '2019-04-02 18:49:22', '2019-04-02 18:49:22'),
(2, 2, 2, 'sal2', 0, '2019-04-02 18:49:45', '2019-04-02 18:49:45'),
(3, 2, 2, 'sal3', 1, '2019-04-02 18:50:02', '2019-04-02 18:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `salary_structure_map`
--

CREATE TABLE `salary_structure_map` (
  `salary_structure_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `salary_structure_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_salary_head_id` int(10) UNSIGNED DEFAULT NULL,
  `salary_head_pf` double(8,2) DEFAULT NULL,
  `salary_head_esi` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salary_structure_map`
--

INSERT INTO `salary_structure_map` (`salary_structure_map_id`, `admin_id`, `update_by`, `salary_structure_id`, `pay_salary_head_id`, `salary_head_pf`, `salary_head_esi`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 2, 5.00, NULL, '2019-04-02 18:49:22', '2019-04-02 18:49:22'),
(2, 2, 2, 1, 3, NULL, 2.00, '2019-04-02 18:49:23', '2019-04-02 18:49:23'),
(3, 2, 2, 2, 2, 1.00, NULL, '2019-04-02 18:49:45', '2019-04-02 18:49:45'),
(4, 2, 2, 2, 3, 2.00, NULL, '2019-04-02 18:49:45', '2019-04-02 18:49:45'),
(5, 2, 2, 2, 4, 3.00, NULL, '2019-04-02 18:49:45', '2019-04-02 18:49:45'),
(6, 2, 2, 3, 2, 1.00, 2.00, '2019-04-02 18:50:02', '2019-04-02 18:50:02'),
(7, 2, 2, 3, 3, 2.00, 3.00, '2019-04-02 18:50:02', '2019-04-02 18:50:02'),
(8, 2, 2, 3, 4, 2.00, 5.00, '2019-04-02 18:50:02', '2019-04-02 18:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `sale_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `system_invoice_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_invoice_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `sale_type` tinyint(4) DEFAULT NULL COMMENT '1=Student,2=Staff',
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_mode` tinyint(4) DEFAULT NULL COMMENT '0=Cash,1=Credit,2=Cheque',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `cheque_no` int(11) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `cheque_amount` bigint(20) DEFAULT NULL,
  `total_amount` double(18,2) DEFAULT NULL,
  `tax` double(18,2) DEFAULT NULL,
  `discount` double(18,2) DEFAULT NULL,
  `gross_amount` double(18,2) DEFAULT NULL,
  `net_amount` double(18,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sale_id`, `admin_id`, `update_by`, `system_invoice_no`, `ref_invoice_no`, `date`, `sale_type`, `class_id`, `section_id`, `student_id`, `role_id`, `staff_id`, `payment_mode`, `bank_id`, `cheque_no`, `cheque_date`, `cheque_amount`, `total_amount`, `tax`, `discount`, `gross_amount`, `net_amount`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'SI2019040231', 'SI-01', '2019-04-03', 1, 1, 1, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 170.00, 5.00, 5.00, 169.57, 170.00, '2019-04-02 19:40:02', '2019-04-02 19:40:02'),
(2, 2, 2, 'SI2019040232', 'SI-02', '2019-04-11', 1, 1, 1, 5, NULL, NULL, 0, NULL, NULL, NULL, NULL, 500.00, 10.00, 5.00, 522.50, 523.00, '2019-04-02 19:45:49', '2019-04-02 19:45:49');

--
-- Triggers `sales`
--
DELIMITER $$
CREATE TRIGGER `update_sales_counter` AFTER INSERT ON `sales` FOR EACH ROW BEGIN
            UPDATE `invoice_config` 
            SET `total_invoice` = total_invoice+1 
            WHERE `invoice_type` = 3;
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE `sale_items` (
  `sale_items_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `sale_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `unit_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sub_category_id` int(10) UNSIGNED DEFAULT NULL,
  `sales_type` int(10) UNSIGNED DEFAULT NULL COMMENT '1=Sale,2=sales Return',
  `rate` double(8,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amount` double(18,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sale_items`
--

INSERT INTO `sale_items` (`sale_items_id`, `admin_id`, `update_by`, `sale_id`, `item_id`, `unit_id`, `category_id`, `sub_category_id`, `sales_type`, `rate`, `quantity`, `amount`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 3, 1, 3, 1, 10.00, 2, 20.00, '2019-04-02 19:40:02', '2019-04-02 19:40:02'),
(2, 2, 2, 1, 2, 3, 2, 6, 1, 30.00, 5, 150.00, '2019-04-02 19:40:02', '2019-04-02 19:40:02'),
(3, 2, 2, 2, 2, 3, 2, 6, 1, 50.00, 10, 500.00, '2019-04-02 19:45:49', '2019-04-02 19:45:49'),
(4, 2, 2, 2, 2, 3, 2, 6, 2, 50.00, 5, 250.00, '2019-04-02 19:50:34', '2019-04-02 19:50:34');

-- --------------------------------------------------------

--
-- Table structure for table `sal_struct_map_staff`
--

CREATE TABLE `sal_struct_map_staff` (
  `sal_struct_map_staff_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `basic_pay_amt` double(18,2) DEFAULT NULL,
  `salary_structure_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_map`
--

CREATE TABLE `schedule_map` (
  `schedule_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_schedule_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_ids` text COLLATE utf8mb4_unicode_ci,
  `exam_date` date DEFAULT NULL,
  `exam_time_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exam_time_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cronjob_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Not Sent,1=Sent',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `publish_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule_map`
--

INSERT INTO `schedule_map` (`schedule_map_id`, `admin_id`, `update_by`, `session_id`, `exam_schedule_id`, `exam_id`, `class_id`, `section_id`, `subject_id`, `staff_ids`, `exam_date`, `exam_time_from`, `exam_time_to`, `cronjob_status`, `created_at`, `updated_at`, `publish_status`) VALUES
(1, 2, 2, 1, 1, 1, 1, 1, 1, '2', '2019-02-01', '10:00', '13:00', 0, '2019-02-04 07:20:06', '2019-02-04 07:20:06', 1),
(2, 2, 2, 1, 1, 1, 1, 1, 2, '2', '2019-02-02', '10:00', '13:00', 0, '2019-02-06 10:26:09', '2019-02-06 10:26:09', 1),
(3, 2, 2, 1, 1, 1, 1, 1, 3, '1', '2019-02-04', '10:00', '13:00', 0, '2019-02-06 10:26:10', '2019-02-06 10:26:10', 1),
(4, 2, 2, 1, 1, 1, 1, 1, 4, '2', '2019-02-05', '10:00', '13:00', 0, '2019-02-06 10:26:10', '2019-02-06 10:26:10', 1),
(5, 2, 2, 1, 1, 1, 1, 2, 1, '2', '2019-02-09', '10:00', '13:00', 0, '2019-02-14 08:08:16', '2019-02-14 08:08:16', 1),
(6, 2, 2, 1, 1, 1, 1, 2, 2, '2', '2019-02-10', '10:00', '13:00', 0, '2019-02-14 08:08:16', '2019-02-14 08:08:16', 1),
(7, 2, 2, 1, 1, 1, 1, 2, 3, '2', '2019-02-11', '10:00', '13:00', 0, '2019-02-14 08:08:16', '2019-02-14 08:08:16', 1),
(8, 2, 2, 1, 1, 1, 1, 2, 4, '2', '2019-02-12', '10:00', '13:00', 0, '2019-02-14 08:08:16', '2019-02-14 08:08:16', 1),
(9, 2, 2, 1, 2, 2, 1, 1, 1, '2', '2019-02-15', '10:00', '11:00', 0, '2019-02-27 11:07:13', '2019-04-02 17:03:57', 1),
(10, 2, 2, 1, 2, 2, 1, 1, 2, '4', '2019-02-16', '11:00', '12:00', 0, '2019-02-27 11:07:13', '2019-04-02 17:03:57', 1),
(11, 2, 2, 1, 2, 2, 1, 1, 3, '3', '2019-02-17', '12:00', '13:00', 0, '2019-02-27 11:07:13', '2019-04-02 17:03:57', 1),
(12, 2, 2, 1, 2, 2, 1, 1, 4, '1', '2019-02-18', '13:00', '14:00', 0, '2019-02-27 11:07:13', '2019-04-02 17:03:57', 1),
(13, 2, 2, 1, 2, 2, 1, 1, 5, '2', '2019-02-19', '10:00', '13:00', 0, '2019-02-27 11:07:14', '2019-02-27 11:07:14', 1),
(14, 2, 2, 1, 2, 2, 1, 2, 1, '1', '2019-02-17', '10:00', '13:00', 0, '2019-02-27 11:09:33', '2019-02-27 11:09:33', 1),
(15, 2, 2, 1, 2, 2, 1, 2, 2, '2', '2019-02-18', '10:00', '13:00', 0, '2019-02-27 11:09:33', '2019-02-27 11:09:33', 1),
(16, 2, 2, 1, 2, 2, 1, 2, 3, '1', '2019-02-19', '10:00', '13:00', 0, '2019-02-27 11:09:33', '2019-02-27 11:09:33', 1),
(17, 2, 2, 1, 2, 2, 1, 2, 4, '2', '2019-02-20', '10:00', '13:00', 0, '2019-02-27 11:09:33', '2019-02-27 11:09:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `schedule_room_map`
--

CREATE TABLE `schedule_room_map` (
  `schedule_room_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_schedule_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `room_no_id` int(10) UNSIGNED DEFAULT NULL,
  `roll_no_from` text COLLATE utf8mb4_unicode_ci,
  `roll_no_to` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule_room_map`
--

INSERT INTO `schedule_room_map` (`schedule_room_map_id`, `admin_id`, `update_by`, `session_id`, `exam_schedule_id`, `exam_id`, `class_id`, `section_id`, `room_no_id`, `roll_no_from`, `roll_no_to`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 2, 2, 1, 1, 1, '1', '10', '2019-04-04 21:24:13', '2019-04-04 21:24:13'),
(2, 2, 2, 1, 2, 2, 1, 1, 2, '11', '20', '2019-04-04 21:27:02', '2019-04-04 21:27:02');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `school_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_registration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_sno_numbers` text COLLATE utf8mb4_unicode_ci COMMENT 'SNO numbers are stored with comma',
  `school_description` text COLLATE utf8mb4_unicode_ci,
  `school_address` text COLLATE utf8mb4_unicode_ci,
  `school_district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `state_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `school_pincode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_medium` text COLLATE utf8mb4_unicode_ci COMMENT 'Medium names are stored with comma',
  `school_board_of_exams` text COLLATE utf8mb4_unicode_ci COMMENT 'Board names are stored with comma',
  `school_class_from` text COLLATE utf8mb4_unicode_ci,
  `school_class_to` text COLLATE utf8mb4_unicode_ci,
  `school_total_students` int(10) UNSIGNED DEFAULT NULL,
  `school_total_staff` int(10) UNSIGNED DEFAULT NULL,
  `school_fee_range_from` double(18,2) UNSIGNED DEFAULT NULL,
  `school_fee_range_to` double(18,2) UNSIGNED DEFAULT NULL,
  `school_facilities` text COLLATE utf8mb4_unicode_ci COMMENT 'Facilities are stored with comma',
  `school_url` text COLLATE utf8mb4_unicode_ci,
  `school_logo` text COLLATE utf8mb4_unicode_ci,
  `school_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_mobile_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_telephone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_fax_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_img1` text COLLATE utf8mb4_unicode_ci,
  `school_img2` text COLLATE utf8mb4_unicode_ci,
  `school_img3` text COLLATE utf8mb4_unicode_ci,
  `school_img4` text COLLATE utf8mb4_unicode_ci,
  `school_img5` text COLLATE utf8mb4_unicode_ci,
  `school_img6` text COLLATE utf8mb4_unicode_ci,
  `imprest_ac_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `school_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `school_latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `admin_id`, `update_by`, `school_name`, `school_registration_no`, `school_sno_numbers`, `school_description`, `school_address`, `school_district`, `city_id`, `state_id`, `country_id`, `school_pincode`, `school_medium`, `school_board_of_exams`, `school_class_from`, `school_class_to`, `school_total_students`, `school_total_staff`, `school_fee_range_from`, `school_fee_range_to`, `school_facilities`, `school_url`, `school_logo`, `school_email`, `school_mobile_number`, `school_telephone`, `school_fax_number`, `school_img1`, `school_img2`, `school_img3`, `school_img4`, `school_img5`, `school_img6`, `imprest_ac_status`, `school_status`, `created_at`, `updated_at`, `school_latitude`, `school_longitude`) VALUES
(1, 2, 2, '123', '12345', NULL, ':D', 'ADDRESS Information', NULL, 2, 1, 1, '124563', '1,2', '1,2,3,4,5', '1', '12', 9, 7, 1.00, 100.00, NULL, NULL, 'cartoon_birds_blue_flying_animation_clipart947901554190385.gif', '123@aaa', '1234567890', NULL, NULL, 'cartoon_birds_blue_flying_animation_clipart993051554190907.gif', 'cartoon_birds_blue_flying_animation_clipart71421554190936.gif', 'software-tester823491554190907.jpg', NULL, NULL, NULL, 1, 1, '2019-01-23 09:36:11', '2019-04-02 18:36:53', 'Latitude', 'Longitude');

-- --------------------------------------------------------

--
-- Table structure for table `school_boards`
--

CREATE TABLE `school_boards` (
  `board_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `board_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `board_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `school_boards`
--

INSERT INTO `school_boards` (`board_id`, `admin_id`, `update_by`, `board_name`, `board_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'CBSE Board', 1, '2019-01-23 09:36:07', '2019-01-23 09:36:07'),
(2, 1, 1, 'RBSE Board', 1, '2019-01-23 09:36:07', '2019-01-23 09:36:07'),
(3, 1, 1, 'IB Board', 1, '2019-01-23 09:36:07', '2019-01-23 09:36:07'),
(4, 1, 1, 'IGCSE Board', 1, '2019-01-23 09:36:07', '2019-01-23 09:36:07'),
(5, 1, 1, 'State Board', 1, '2019-01-23 09:36:07', '2019-01-23 09:36:07'),
(6, 2, 2, 'PUP patiala', 1, '2019-04-02 20:41:51', '2019-04-02 20:41:51'),
(7, 2, 2, 'Cup of Board', 1, '2019-04-03 17:59:48', '2019-04-03 18:00:01'),
(8, 2, 2, 'University Patiala', 1, '2019-04-05 21:16:49', '2019-04-05 21:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `section_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `shift_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `section_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_intake` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'capacity/seats',
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `section_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `section_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`section_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `shift_id`, `stream_id`, `section_name`, `section_intake`, `medium_type`, `section_order`, `section_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, NULL, 1, 1, NULL, 'A', 30, 1, 0, 1, '2019-01-23 10:35:16', '2019-01-23 10:35:16'),
(2, 2, 2, NULL, 1, 1, NULL, 'B', 30, 1, 0, 1, '2019-01-23 10:35:31', '2019-01-23 10:35:31'),
(3, 2, 2, NULL, 2, 1, 1, 'Section A', 40, 1, 0, 1, '2019-04-02 16:19:17', '2019-04-02 16:19:17'),
(4, 2, 2, NULL, 4, 1, 2, 'ECE', 50, 1, 5, 1, '2019-04-02 20:59:19', '2019-04-02 20:59:19'),
(5, 2, 2, NULL, 5, 3, NULL, 'Test section for tea 1', 2, 1, 12, 1, '2019-04-03 18:32:11', '2019-04-03 18:32:11'),
(6, 2, 2, NULL, 6, 5, 4, 'Sec-A', 50, 1, 13, 1, '2019-04-05 21:22:00', '2019-04-05 21:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_start_date` date DEFAULT NULL,
  `session_end_date` date DEFAULT NULL,
  `session_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `admin_id`, `update_by`, `session_name`, `session_start_date`, `session_end_date`, `session_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '2018-2019', '2018-04-01', '2019-03-31', 1, '2019-01-23 10:02:26', '2019-04-02 18:19:44'),
(3, 2, 2, '2020-2021', '1994-12-02', '1995-12-02', 1, '2019-04-02 18:21:11', '2019-04-02 18:21:56'),
(4, 2, 2, '2015-2016', '2015-04-01', '2016-04-30', 1, '2019-04-03 17:56:56', '2019-04-03 17:56:56'),
(5, 2, 2, '2024-2025', '2019-04-01', '2022-02-08', 1, '2019-04-05 20:56:37', '2019-04-05 20:57:22');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `shift_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `shift_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shift_start_time` time DEFAULT NULL,
  `shift_end_time` time DEFAULT NULL,
  `shift_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`shift_id`, `admin_id`, `update_by`, `shift_name`, `shift_start_time`, `shift_end_time`, `shift_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Shift 1', '07:00:00', '13:00:00', 1, '2019-01-23 10:34:06', '2019-01-23 10:34:06'),
(2, 2, 2, 'Delhi Tour', '10:00:00', '15:00:00', 1, '2019-04-02 18:39:54', '2019-04-02 18:40:31'),
(3, 2, 2, 'test shift for cup class', '09:00:00', '15:30:00', 1, '2019-04-03 18:30:40', '2019-04-03 18:30:40'),
(4, 2, 2, 'S Shift', '05:00:00', '10:00:00', 1, '2019-04-05 20:58:52', '2019-04-05 20:58:52'),
(5, 2, 2, 'S Shift 1', '12:00:00', '17:00:00', 1, '2019-04-05 20:59:15', '2019-04-05 20:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_profile_img` text COLLATE utf8mb4_unicode_ci,
  `staff_aadhar` text COLLATE utf8mb4_unicode_ci,
  `staff_UAN` text COLLATE utf8mb4_unicode_ci,
  `staff_ESIN` text COLLATE utf8mb4_unicode_ci,
  `staff_PAN` text COLLATE utf8mb4_unicode_ci,
  `designation_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_role_id` text COLLATE utf8mb4_unicode_ci COMMENT 'Multiple staff role ids are store',
  `staff_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_dob` date DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `staff_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Male,1=Female',
  `staff_father_name_husband_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_father_husband_mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_marital_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unmarried,1=Married',
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_attendance_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_qualification` text COLLATE utf8mb4_unicode_ci COMMENT 'Qualifications are store with comma',
  `staff_specialization` text COLLATE utf8mb4_unicode_ci COMMENT 'Specializations are store with comma',
  `staff_reference` text COLLATE utf8mb4_unicode_ci,
  `staff_experience` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `staff_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shift_ids` text COLLATE utf8mb4_unicode_ci,
  `teaching_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `admin_id`, `update_by`, `reference_admin_id`, `staff_name`, `staff_profile_img`, `staff_aadhar`, `staff_UAN`, `staff_ESIN`, `staff_PAN`, `designation_id`, `staff_role_id`, `staff_email`, `staff_mobile_number`, `staff_dob`, `medium_type`, `staff_gender`, `staff_father_name_husband_name`, `staff_father_husband_mobile_no`, `staff_mother_name`, `staff_blood_group`, `staff_marital_status`, `title_id`, `caste_id`, `nationality_id`, `religion_id`, `staff_attendance_unique_id`, `staff_qualification`, `staff_specialization`, `staff_reference`, `staff_experience`, `staff_temporary_address`, `staff_temporary_city`, `staff_temporary_state`, `staff_temporary_county`, `staff_temporary_pincode`, `staff_permanent_address`, `staff_permanent_city`, `staff_permanent_state`, `staff_permanent_county`, `staff_permanent_pincode`, `staff_status`, `created_at`, `updated_at`, `shift_ids`, `teaching_status`) VALUES
(1, 2, 2, 3, 'Ravi', 'dinesh420951548241814.jpg', '12345', NULL, NULL, NULL, 1, '1,2,3,10', 'ravi@gmail.com', '9999999999', '1970-01-01', 1, 1, 'harsh', '8888888888', 'mamta', 'O+', 1, 1, 1, 1, 1, 'Staff_1', 'Qualification', 'Specialization', NULL, '[\"Experience 1\",\"Experience 2\"]', 'test temp', 1, 1, 1, '342001', 'Bhaskar Circle', 2, 1, 1, '324002', 1, '2019-01-23 11:10:14', '2019-04-02 17:39:18', NULL, 1),
(2, NULL, 2, 23, 'Dinesh', NULL, '12345', '23454', '43556R544', '3567765', 1, '1,2', 'dinesh@gmail.com', '1234567890', '1994-01-25', 1, 0, 'Ravi', '7777777777', 'Suman', 'A-', 0, 1, 1, 1, 1, NULL, '1. Qualification\n2. Qualification', '1. Specialization\n2. Specialization', '1. Reference\n2. Reference', '1. Experience\n2. Experience', NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 1, 1, 1, '342005', 1, '2019-02-04 06:41:12', '2019-02-04 06:41:12', '1', 1),
(3, 2, 2, 25, 'ramesh', 'avatar-business-man-people-person-3f86f80ad185a286-512x512471051534936792783041554184403.png', '568785443333', '444444444444', 'Bihar', '44444444444444444', 1, '2,3', 'ramesh@gmail.com', '1524895632', '1986-03-20', 1, 0, 'tttttt', '7677777777', 'test', 'B+', 1, 1, 1, 1, 1, 'Staff_3', 'testt', 'testt', 'test', '[\"test\"]', 'tesssdssd', 2, 1, 1, '654444', 'tesssdssd', 2, 1, 1, '654444', 1, '2019-04-02 16:23:23', '2019-04-02 16:23:24', '1', 1),
(4, 2, 2, 26, 'Mohan', NULL, '568785443333', '345678765432', 'Assam', '66666666666', 2, '3,5', 'mohan@gmail.com', '2589631485', '1988-08-30', 1, 0, 'test', '7677777777', 'test', 'A+', 0, 1, 1, 1, 1, 'Staff_4', 'ttttttttttt', 'ttttttttt', 'tttttttttttttttt', '[\"tttttttttt\"]', 'ffdfddf', 1, 1, 1, '654444', 'tesssdssd', 1, 1, 1, '654444', 1, '2019-04-02 16:26:13', '2019-04-02 16:26:13', '1', 1),
(5, 2, 2, 29, 'babu lal', NULL, '568785448596', '345678765432', 'Rajasthan', '44444444444444444', 1, '14,15', 'babu32lal@gmail.com', '7654397654', '1976-01-20', 1, 0, 'tttttt', '7677777777', 'test', 'B+', 1, 1, 1, 1, 1, 'Staff_5', 'ttttttttttt', 'ttttttttt', 'tttttttttttttttttttttt', '[\"ttttttttttttttttt\"]', 'dsdsdsdsdsd', 2, 1, 1, '306401', 'dsdsdsdsdsd', 2, 1, 1, '306401', 1, '2019-04-02 18:01:58', '2019-04-02 18:01:58', '1', 0),
(6, 2, 2, 30, 'Sourabh Rohila 123', 'KcMI887281554451083.jpg', '454545454545', NULL, NULL, NULL, 6, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15', 'A@mailinator.com', '1231231231', '2012-12-02', 1, 0, 'Father Name', '1231231231', 'Qwe', 'O+', 1, 1, 1, 1, 1, 'Staff_6', 'Great man', 'I am don !!', 'I don\'t have Any', '[\"123\"]', '1', 3, 2, 1, '1', 'd', 3, 2, 2, '123456', 1, '2019-04-03 21:32:51', '2019-04-05 20:23:48', '3', 1),
(7, 2, 2, 33, 'Testing Sourabh', 'Penguins315981554463488.jpg', '123456789101', '123456789123', '12', '123', 7, '7', 'testing@mailinator.com', '9696969696', '1994-02-18', 1, 0, 'Testing sourabh father', '1234567890', 'Testing sourabh mother', 'A+', 0, 2, 2, 2, 2, 'Staff_7', 'B.tech in ECE', 'specialist in software testing!!', 'I don\'t have any !!', '[\"Three years !!\",\"I don\'t have any !!\"]', 'Udaipur', 4, 3, 3, '3215', 'bathinda', 4, 3, 3, '151005', 1, '2019-04-05 21:54:48', '2019-04-05 21:54:48', '5', 1);

--
-- Triggers `staff`
--
DELIMITER $$
CREATE TRIGGER `update_staff_counter` AFTER INSERT ON `staff` FOR EACH ROW BEGIN
            UPDATE `school` 
            SET `school_total_staff` = (SELECT COUNT(*) FROM `staff` );
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_attendance`
--

CREATE TABLE `staff_attendance` (
  `staff_attendance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `total_present_staff` int(10) UNSIGNED DEFAULT NULL,
  `total_absent_staff` int(10) UNSIGNED DEFAULT NULL,
  `total_halfday_staff` int(10) UNSIGNED DEFAULT NULL,
  `staff_attendance_date` date DEFAULT NULL,
  `staff_attendance_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_attendance`
--

INSERT INTO `staff_attendance` (`staff_attendance_id`, `admin_id`, `update_by`, `session_id`, `total_present_staff`, `total_absent_staff`, `total_halfday_staff`, `staff_attendance_date`, `staff_attendance_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 6, 0, 0, '2019-04-02', 1, '2019-04-02 17:30:55', '2019-04-03 21:49:42'),
(2, 2, 2, 1, 6, 0, 0, '2019-04-25', 1, '2019-04-03 21:53:51', '2019-04-03 21:53:51'),
(3, 2, 2, 1, 5, 1, 0, '2019-04-09', 1, '2019-04-03 22:01:09', '2019-04-03 22:04:43'),
(4, 2, 2, 1, 6, 0, 0, '2019-04-03', 1, '2019-04-03 22:07:05', '2019-04-03 22:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `staff_attend_details`
--

CREATE TABLE `staff_attend_details` (
  `staff_attend_d_id` int(10) UNSIGNED NOT NULL,
  `staff_attendance_id` int(10) UNSIGNED DEFAULT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `in_time` time DEFAULT NULL,
  `out_time` time DEFAULT NULL,
  `staff_attendance_type` tinyint(4) DEFAULT NULL COMMENT '0=Absent,1=Present,2=Half Day',
  `staff_overtime_type` tinyint(4) DEFAULT NULL COMMENT '0=Hours,1=Lectures',
  `staff_overtime_value` double(18,2) DEFAULT NULL,
  `staff_attendance_date` date DEFAULT NULL,
  `staff_attendance_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_attend_details`
--

INSERT INTO `staff_attend_details` (`staff_attend_d_id`, `staff_attendance_id`, `admin_id`, `update_by`, `session_id`, `staff_id`, `in_time`, `out_time`, `staff_attendance_type`, `staff_overtime_type`, `staff_overtime_value`, `staff_attendance_date`, `staff_attendance_status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 1, 1, '10:00:00', '17:00:00', 1, 0, 2.00, '2019-04-02', 1, '2019-04-02 17:30:55', '2019-04-02 17:30:55'),
(2, 1, 2, 2, 1, 2, NULL, NULL, 0, 2, NULL, '2019-04-02', 1, '2019-04-02 17:30:55', '2019-04-02 17:31:19'),
(3, 1, 2, 2, 1, 3, NULL, NULL, 0, 2, NULL, '2019-04-02', 1, '2019-04-02 17:30:55', '2019-04-02 17:31:19'),
(4, 1, 2, 2, 1, 4, '08:00:00', '14:00:00', 1, 2, NULL, '2019-04-02', 1, '2019-04-02 17:30:55', '2019-04-02 17:30:55'),
(5, 3, 2, 2, 1, 1, NULL, NULL, 1, 2, NULL, '2019-04-09', 1, '2019-04-03 21:49:42', '2019-04-03 22:01:09'),
(6, 3, 2, 2, 1, 2, NULL, NULL, 1, 2, NULL, '2019-04-09', 1, '2019-04-03 21:49:42', '2019-04-03 22:01:09'),
(7, 3, 2, 2, 1, 3, NULL, NULL, 1, 2, NULL, '2019-04-09', 1, '2019-04-03 21:49:42', '2019-04-03 22:01:09'),
(8, 3, 2, 2, 1, 4, NULL, NULL, 1, 2, NULL, '2019-04-09', 1, '2019-04-03 21:49:42', '2019-04-03 22:01:09'),
(9, 3, 2, 2, 1, 5, NULL, NULL, 1, 2, NULL, '2019-04-09', 1, '2019-04-03 21:49:42', '2019-04-03 22:01:09'),
(10, 3, 2, 2, 1, 6, '10:00:00', '12:00:00', 0, 1, 500.00, '2019-04-09', 1, '2019-04-03 21:49:42', '2019-04-03 22:04:43'),
(11, 2, 2, 2, 1, 1, NULL, NULL, 1, 2, NULL, '2019-04-25', 1, '2019-04-03 21:53:51', '2019-04-03 21:53:51'),
(12, 2, 2, 2, 1, 2, NULL, NULL, 1, 2, NULL, '2019-04-25', 1, '2019-04-03 21:53:51', '2019-04-03 21:53:51'),
(13, 2, 2, 2, 1, 3, NULL, NULL, 1, 2, NULL, '2019-04-25', 1, '2019-04-03 21:53:51', '2019-04-03 21:53:51'),
(14, 2, 2, 2, 1, 4, NULL, NULL, 1, 2, NULL, '2019-04-25', 1, '2019-04-03 21:53:51', '2019-04-03 21:53:51'),
(15, 2, 2, 2, 1, 5, NULL, NULL, 1, 2, NULL, '2019-04-25', 1, '2019-04-03 21:53:51', '2019-04-03 21:53:51'),
(16, 2, 2, 2, 1, 6, NULL, NULL, 1, 2, NULL, '2019-04-25', 1, '2019-04-03 21:53:51', '2019-04-03 21:53:51'),
(17, 4, 2, 2, 1, 1, NULL, NULL, 1, 2, NULL, '2019-04-03', 1, '2019-04-03 22:07:05', '2019-04-03 22:07:05'),
(18, 4, 2, 2, 1, 2, NULL, NULL, 1, 2, NULL, '2019-04-03', 1, '2019-04-03 22:07:05', '2019-04-03 22:07:05'),
(19, 4, 2, 2, 1, 3, NULL, NULL, 1, 2, NULL, '2019-04-03', 1, '2019-04-03 22:07:05', '2019-04-03 22:07:05'),
(20, 4, 2, 2, 1, 4, NULL, NULL, 1, 2, NULL, '2019-04-03', 1, '2019-04-03 22:07:05', '2019-04-03 22:07:05'),
(21, 4, 2, 2, 1, 5, NULL, NULL, 1, 2, NULL, '2019-04-03', 1, '2019-04-03 22:07:05', '2019-04-03 22:07:05'),
(22, 4, 2, 2, 1, 6, NULL, NULL, 1, 2, NULL, '2019-04-03', 1, '2019-04-03 22:07:05', '2019-04-03 22:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `staff_class_allocations`
--

CREATE TABLE `staff_class_allocations` (
  `staff_class_allocation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_class_allocations`
--

INSERT INTO `staff_class_allocations` (`staff_class_allocation_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `staff_id`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 2, 1, '2019-02-12 11:24:34', '2019-02-12 11:24:34'),
(2, 2, 2, 2, 3, 1, 1, '2019-04-02 16:20:06', '2019-04-02 16:20:06'),
(3, 2, 2, 1, 2, 3, 1, '2019-04-02 16:27:46', '2019-04-02 16:27:46'),
(4, 2, 2, 5, 5, 6, 1, '2019-04-04 16:43:43', '2019-04-04 16:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `staff_documents`
--

CREATE TABLE `staff_documents` (
  `staff_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_document_details` text COLLATE utf8mb4_unicode_ci,
  `staff_document_file` text COLLATE utf8mb4_unicode_ci,
  `staff_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_documents`
--

INSERT INTO `staff_documents` (`staff_document_id`, `admin_id`, `update_by`, `staff_id`, `document_category_id`, `staff_document_details`, `staff_document_file`, `staff_document_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 3, 1, 'testtt', NULL, 0, '2019-04-02 16:23:24', '2019-04-02 16:23:24'),
(2, 2, 2, 5, 1, 'test', NULL, 0, '2019-04-02 18:01:58', '2019-04-02 18:01:58'),
(3, 2, 2, 6, 1, '12121212', '32154091554289371.png', 0, '2019-04-03 21:32:51', '2019-04-03 21:32:51'),
(4, 2, 2, 6, 1, NULL, '31654641554289697.png', 0, '2019-04-03 21:32:51', '2019-04-03 21:38:17'),
(5, 2, 2, 7, 2, 'bug 1', '81664041554463488.png', 0, '2019-04-05 21:54:48', '2019-04-05 21:54:48'),
(6, 2, 2, 7, 1, 'bug  2', '60193231554464748.png', 0, '2019-04-05 21:54:48', '2019-04-05 22:15:48');

-- --------------------------------------------------------

--
-- Table structure for table `staff_leaves`
--

CREATE TABLE `staff_leaves` (
  `staff_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_leave_from_date` date DEFAULT NULL,
  `staff_leave_to_date` date DEFAULT NULL,
  `staff_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pay_leave_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_leaves_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=paid leave,2=loss of pay leave',
  `no_of_loss_pay_leave` int(11) DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_leaves`
--

INSERT INTO `staff_leaves` (`staff_leave_id`, `admin_id`, `update_by`, `staff_id`, `staff_leave_reason`, `staff_leave_type`, `staff_leave_from_date`, `staff_leave_to_date`, `staff_leave_attachment`, `staff_leave_status`, `created_at`, `updated_at`, `pay_leave_scheme_id`, `pay_leaves_status`, `no_of_loss_pay_leave`, `session_id`) VALUES
(1, 23, 23, 6, 'Login as staff >> Go to Profile >> Click on attendance>> Try to scroll for previous months nad check', NULL, '2019-04-10', '2019-04-15', NULL, 1, '2019-04-01 10:57:10', '2019-04-04 22:01:24', NULL, 1, NULL, 1),
(2, 23, 23, 2, 'T', NULL, '2019-04-05', '2019-04-05', NULL, 1, '2019-04-01 10:57:40', '2019-04-01 10:57:40', NULL, 1, NULL, 1),
(3, 23, 23, 2, 'DD', NULL, '2019-04-06', '2019-04-06', NULL, 1, '2019-04-01 11:22:17', '2019-04-01 11:22:17', NULL, 1, NULL, 1),
(5, 30, 30, 6, '111', NULL, '2019-04-04', '2019-04-05', NULL, 0, '2019-04-03 22:39:57', '2019-04-03 23:07:15', NULL, 2, 1, NULL),
(6, 30, 30, 6, 'Sss', NULL, '2019-04-04', '2019-04-16', NULL, 0, '2019-04-04 18:48:38', '2019-04-04 18:48:38', NULL, 1, NULL, NULL),
(7, 30, 30, 6, 'Marriage 3', NULL, '2019-05-05', '2019-05-06', NULL, 0, '2019-04-05 20:08:23', '2019-04-05 20:08:23', NULL, 1, NULL, NULL),
(8, 30, 30, 6, 'Aww', NULL, '2019-05-05', '2019-05-06', NULL, 0, '2019-04-05 20:08:48', '2019-04-05 20:08:48', NULL, 1, NULL, NULL),
(9, 30, 30, 6, '11111', NULL, '2019-05-05', '2019-05-06', NULL, 0, '2019-04-05 20:09:19', '2019-04-05 20:11:55', NULL, 1, NULL, NULL),
(10, 25, 25, 3, 'testing leave', NULL, '2019-04-11', '2019-04-13', NULL, 0, '2019-04-09 20:06:05', '2019-04-09 20:06:05', NULL, 1, NULL, NULL),
(11, 25, 25, 3, 'test', NULL, '2019-04-11', '2019-04-13', NULL, 0, '2019-04-09 20:06:34', '2019-04-09 20:06:34', NULL, 1, NULL, NULL),
(12, 25, 25, 3, 'testing', NULL, '2019-04-11', '2019-04-13', NULL, 0, '2019-04-09 20:09:51', '2019-04-09 20:09:51', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_leave_history_info`
--

CREATE TABLE `staff_leave_history_info` (
  `staff_leave_history_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_leave_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `lscheme_period_type` int(11) DEFAULT NULL COMMENT '1=Monthly,2=Yearly',
  `total_no_of_leaves` int(11) DEFAULT NULL,
  `remaining_leaves` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `staff_leave_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_leave_history_info`
--

INSERT INTO `staff_leave_history_info` (`staff_leave_history_info_id`, `admin_id`, `update_by`, `session_id`, `staff_id`, `pay_leave_scheme_id`, `lscheme_period_type`, `total_no_of_leaves`, `remaining_leaves`, `created_at`, `updated_at`, `staff_leave_id`) VALUES
(1, 2, 2, 1, 6, NULL, NULL, NULL, 0, '2019-04-03 22:43:58', '2019-04-03 22:43:58', 5);

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles`
--

CREATE TABLE `staff_roles` (
  `staff_role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_role_name` text COLLATE utf8mb4_unicode_ci,
  `staff_role_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_roles`
--

INSERT INTO `staff_roles` (`staff_role_id`, `admin_id`, `update_by`, `staff_role_name`, `staff_role_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'School Principal', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(2, 1, 1, 'Class Teacher', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(3, 1, 1, 'Subject Teacher', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(4, 1, 1, 'Marks Manager', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(5, 1, 1, 'Exam Manager', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(6, 1, 1, 'Accountant', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(7, 1, 1, 'Hostel Warden', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(8, 1, 1, 'HR Manager', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(9, 1, 1, 'Finance Head', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(10, 1, 1, 'Admission Officer / Public Relation Officer', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(11, 1, 1, 'Guard for Visitor Management', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(12, 1, 1, 'Librarian', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(13, 1, 1, 'Website Manager', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(14, 1, 1, 'Driver', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22'),
(15, 1, 1, 'Conductor', 1, '2019-01-23 09:38:22', '2019-01-23 09:38:22');

-- --------------------------------------------------------

--
-- Table structure for table `staff_substitute`
--

CREATE TABLE `staff_substitute` (
  `substitute_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `ref_leave_id` int(10) UNSIGNED DEFAULT NULL,
  `leave_staff_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `lecture_no` int(10) UNSIGNED DEFAULT NULL,
  `day` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `as_class_teacher` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `pay_extra` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=NA,2=Per Day, 3=Per Lecture',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `state_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `country_id`, `state_name`, `state_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rajasthan', 1, '2019-01-23 10:47:25', '2019-01-23 10:47:25'),
(2, 2, 'state 123', 1, '2019-04-02 18:46:41', '2019-04-02 18:46:41'),
(3, 3, 'Vancuver', 1, '2019-04-05 21:18:21', '2019-04-05 21:18:21');

-- --------------------------------------------------------

--
-- Table structure for table `streams`
--

CREATE TABLE `streams` (
  `stream_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `stream_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stream_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `streams`
--

INSERT INTO `streams` (`stream_id`, `admin_id`, `update_by`, `medium_type`, `stream_name`, `stream_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'stream-1', 1, '2019-04-02 16:06:10', '2019-04-02 16:06:10'),
(2, 2, 2, 1, 'IT', 1, '2019-04-02 20:42:13', '2019-04-02 20:42:13'),
(3, 2, 2, 1, 'Test Stream for Tea 1', 1, '2019-04-03 18:33:02', '2019-04-03 18:33:02'),
(4, 2, 2, 1, 'Electronics', 1, '2019-04-05 21:16:22', '2019-04-05 21:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `student_parent_id` int(10) UNSIGNED DEFAULT NULL,
  `student_enroll_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_roll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_image` text COLLATE utf8mb4_unicode_ci,
  `student_reg_date` date DEFAULT NULL,
  `student_dob` date DEFAULT NULL,
  `student_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boy,1=Girl',
  `student_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=PaidBoy,1=RTE,2=FREE',
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `student_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `student_sibling_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_sibling_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_adhar_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_date` date DEFAULT NULL,
  `student_privious_result` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `student_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rte_apply_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `student_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `student_cc_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `admin_id`, `update_by`, `reference_admin_id`, `student_parent_id`, `student_enroll_number`, `student_roll_no`, `student_name`, `student_image`, `student_reg_date`, `student_dob`, `student_email`, `student_gender`, `student_type`, `medium_type`, `student_category`, `caste_id`, `title_id`, `religion_id`, `nationality_id`, `student_sibling_name`, `student_sibling_class_id`, `student_adhar_card_number`, `student_privious_school`, `student_privious_class`, `student_privious_tc_no`, `student_privious_tc_date`, `student_privious_result`, `student_temporary_address`, `student_temporary_city`, `student_temporary_state`, `student_temporary_county`, `student_temporary_pincode`, `student_permanent_address`, `student_permanent_city`, `student_permanent_state`, `student_permanent_county`, `student_permanent_pincode`, `rte_apply_status`, `student_status`, `created_at`, `updated_at`, `student_cc_status`) VALUES
(1, 2, 2, 6, 1, 'N0001', '2', 'Teena', NULL, '2019-01-23', '2010-01-25', NULL, 1, 0, 1, 'General', 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 1, 1, 1, '342001', 0, 1, '2019-01-23 11:16:47', '2019-01-23 11:28:51', 0),
(2, 2, 2, 8, 1, 'N0002', '1', 'Pankaj', NULL, '2019-01-25', '2010-01-25', NULL, 0, 0, 1, 'General', 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 1, 1, 1, '342006', 0, 1, '2019-01-23 11:25:12', '2019-01-23 11:28:55', 0),
(3, 2, 2, 10, 2, 'N003', '1', 'Ajay', NULL, '2018-12-28', '2010-08-25', NULL, 0, 0, 1, 'General', 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 1, 1, 1, '342006', 0, 1, '2019-01-23 11:28:31', '2019-01-23 11:28:51', 0),
(5, 2, 2, 12, 2, 'N0004', NULL, 'Neelam', NULL, '2015-01-26', '2013-01-26', NULL, 1, 0, 1, 'General', 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jodhpur', 1, 1, 1, '342006', 0, 1, '2019-01-25 06:42:42', '2019-01-25 06:42:42', 0),
(6, 2, 2, 14, 3, 'N0006', NULL, 'Shri', NULL, '2019-01-20', '2018-01-20', NULL, 0, 0, 1, 'OBC', 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', 2, 1, 1, '342001', 0, 1, '2019-01-25 10:47:36', '2019-02-21 08:08:16', 0),
(7, 2, 2, 16, 4, '102A', NULL, 'Amit', NULL, '2018-07-01', '2001-09-23', 'rakesh@gmail.com', 0, 0, 1, 'OBC', 1, 1, 1, 1, 'aman', 1, '12345666', 'Apex senior secondary school', '0', '2345', '2018-04-06', 'pass', NULL, NULL, NULL, NULL, NULL, 'Jodhpur Rajasthan', 1, 1, 1, '342005', 0, 0, '2019-02-04 05:15:06', '2019-02-23 05:29:17', 1),
(8, 2, 2, 24, 1, 'N0007', NULL, 'Sibling A', 'dinesh826921551784762.jpg', '2019-03-05', '1993-03-05', 'siblinga@gmail.com', 0, 0, 1, 'SC', 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Address', 2, 1, 1, '342006', 0, 1, '2019-03-05 11:19:22', '2019-03-05 11:19:22', 0),
(9, 2, 2, 28, 5, '7890', NULL, 'khushbu', '1862551554187266.png', '2019-04-26', '1996-10-22', 'khushbu@gmail.com', 0, 1, 2, 'OBC', 1, 1, 1, 1, NULL, NULL, '666666666666', 'Apex Public School', 'class 10', '435666666', '2019-02-12', 'pass', NULL, NULL, NULL, NULL, NULL, '4-r-34', 1, 1, 1, '666666', 0, 1, '2019-04-02 17:11:06', '2019-04-02 17:12:39', 0),
(10, 2, 2, 32, 6, 'N00123', NULL, 'Sourabh Rohila', '31935741554296282.png', '2019-04-17', '2019-04-01', NULL, 0, 0, 5, 'SC', 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123', 2, 1, 1, '123456', 0, 1, '2019-04-03 23:28:02', '2019-04-03 23:28:02', 0),
(11, 2, 2, 35, 7, 'Titoo', '1', 'Student testing 1', 'Koala343851554465596.jpg', '2019-04-05', '2019-04-02', 'student@mailinator.com', 0, 0, 6, 'OBC', 2, 2, 2, 2, NULL, NULL, '332321321321', 'Previous school', '4th', 'I dont have TC', '2019-04-03', 'FAIL !!', 'Temporary address', 4, 3, 3, '123', 'PERMANENT ADDRESS', 2, 1, 1, '456', 0, 1, '2019-04-05 22:29:56', '2019-04-06 18:52:50', 0);

--
-- Triggers `students`
--
DELIMITER $$
CREATE TRIGGER `update_student_counter` AFTER INSERT ON `students` FOR EACH ROW BEGIN
            UPDATE `school` 
            SET `school_total_students` = (SELECT COUNT(*) FROM `students` where student_status = 1);
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_history_info`
--

CREATE TABLE `student_academic_history_info` (
  `student_academic_history_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_academic_info_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `percentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `activity_winner` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_info`
--

CREATE TABLE `student_academic_info` (
  `student_academic_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_session_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_class_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_section_id` int(10) UNSIGNED DEFAULT NULL,
  `current_session_id` int(10) UNSIGNED DEFAULT NULL,
  `current_class_id` int(10) UNSIGNED DEFAULT NULL,
  `current_section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_academic_info`
--

INSERT INTO `student_academic_info` (`student_academic_info_id`, `admin_id`, `update_by`, `student_id`, `admission_session_id`, `admission_class_id`, `admission_section_id`, `current_session_id`, `current_class_id`, `current_section_id`, `student_unique_id`, `group_id`, `stream_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 'N0001_1', NULL, NULL, '2019-01-23 11:16:47', '2019-01-23 11:16:47'),
(2, 2, 2, 2, 1, 1, 2, 1, 1, 2, 'N0002_2', NULL, NULL, '2019-01-23 11:25:12', '2019-01-23 11:25:12'),
(3, 2, 2, 3, 1, 1, 1, 1, 1, 1, 'N003_3', NULL, NULL, '2019-01-23 11:28:31', '2019-01-23 11:28:31'),
(4, 2, 2, 5, 1, 1, 1, 1, 1, 1, 'N0004_5', NULL, NULL, '2019-01-25 06:42:42', '2019-01-25 06:42:42'),
(5, 2, 2, 6, 1, 1, 1, 1, 1, 1, 'N0006_6', NULL, NULL, '2019-01-25 10:47:36', '2019-01-25 10:47:36'),
(6, NULL, 2, 7, 1, 1, 1, NULL, 1, 1, '102A_7', NULL, NULL, '2019-02-04 05:15:06', '2019-02-04 05:15:06'),
(7, 2, 2, 8, 1, 1, 1, 1, 1, 1, 'N0007_8', NULL, NULL, '2019-03-05 11:19:22', '2019-03-05 11:19:22'),
(8, 2, 2, 9, 1, 2, 3, 1, 2, 3, '7890_9', NULL, NULL, '2019-04-02 17:11:06', '2019-04-02 17:11:06'),
(9, 2, 2, 10, 4, 5, 5, 1, 5, 5, 'N00123_10', NULL, NULL, '2019-04-03 23:28:02', '2019-04-03 23:28:02'),
(10, 2, 2, 11, 5, 6, 6, 5, 6, 6, 'Titoo_11', 1, NULL, '2019-04-05 22:29:56', '2019-04-05 22:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance`
--

CREATE TABLE `student_attendance` (
  `student_attendance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `total_present_student` int(10) UNSIGNED DEFAULT NULL,
  `total_absent_student` int(10) UNSIGNED DEFAULT NULL,
  `student_attendance_date` date DEFAULT NULL,
  `student_attendance_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_attendance`
--

INSERT INTO `student_attendance` (`student_attendance_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `total_present_student`, `total_absent_student`, `student_attendance_date`, `student_attendance_status`, `created_at`, `updated_at`) VALUES
(1, 23, 23, 1, 1, 1, 3, 3, '2019-02-12', 1, '2019-02-12 11:24:59', '2019-02-12 11:25:02'),
(2, 30, 30, 1, NULL, NULL, 0, 0, '2019-04-03', 1, '2019-04-04 15:16:47', '2019-04-04 15:16:47'),
(3, 30, 30, 1, 5, 5, 0, 3, '2019-04-04', 1, '2019-04-04 18:56:08', '2019-04-04 18:56:40'),
(4, 3, 3, 1, 2, 3, 0, 0, '2019-03-17', 1, '2019-04-08 22:20:28', '2019-04-08 22:20:28'),
(5, 3, 3, 1, 2, 3, 0, 1, '2019-04-07', 1, '2019-04-08 22:56:48', '2019-04-08 22:56:49'),
(6, 3, 3, 1, 2, 3, 0, 1, '2019-03-29', 1, '2019-04-09 16:52:06', '2019-04-09 16:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `student_attend_details`
--

CREATE TABLE `student_attend_details` (
  `student_attend_d_id` int(10) UNSIGNED NOT NULL,
  `student_attendance_id` int(10) UNSIGNED DEFAULT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_attendance_type` tinyint(4) DEFAULT NULL COMMENT '0=Absent,1=Present',
  `student_attendance_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_attend_details`
--

INSERT INTO `student_attend_details` (`student_attend_d_id`, `student_attendance_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `student_id`, `student_attendance_type`, `student_attendance_date`, `created_at`, `updated_at`) VALUES
(1, 1, 23, 23, 1, 1, 1, 1, 1, '2019-02-12', '2019-02-12 11:24:59', '2019-02-12 11:24:59'),
(2, 1, 23, 23, 1, 1, 1, 3, 0, '2019-02-12', '2019-02-12 11:25:00', '2019-02-12 11:25:00'),
(3, 1, 23, 23, 1, 1, 1, 5, 0, '2019-02-12', '2019-02-12 11:25:00', '2019-02-12 11:25:00'),
(4, 1, 23, 23, 1, 1, 1, 6, 0, '2019-02-12', '2019-02-12 11:25:01', '2019-02-12 11:25:01'),
(5, 1, 23, 23, 1, 1, 1, 2, 1, '2019-02-12', '2019-02-12 11:25:01', '2019-02-12 11:25:01'),
(6, 1, 23, 23, 1, 1, 1, 7, 1, '2019-02-12', '2019-02-12 11:25:02', '2019-02-12 11:25:02'),
(7, 3, 30, 30, 1, 5, 5, 10, 0, '2019-04-04', '2019-04-04 18:56:09', '2019-04-04 18:56:09'),
(8, 3, 30, 30, 1, 5, 5, 10, 0, '2019-04-04', '2019-04-04 18:56:17', '2019-04-04 18:56:40'),
(9, 3, 30, 30, 1, 5, 5, 10, 0, '2019-04-04', '2019-04-04 18:56:26', '2019-04-04 18:56:26'),
(10, 4, 3, 3, 1, 2, 3, 9, 3, '2019-03-17', '2019-04-08 22:20:28', '2019-04-08 22:20:28'),
(11, 5, 3, 3, 1, 2, 3, 9, 0, '2019-04-07', '2019-04-08 22:56:48', '2019-04-08 22:56:48'),
(12, 6, 3, 3, 1, 2, 3, 9, 0, '2019-03-29', '2019-04-09 16:52:07', '2019-04-09 16:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `student_cc`
--

CREATE TABLE `student_cc` (
  `student_cc_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `cc_date` date DEFAULT NULL,
  `cc_reason` text COLLATE utf8mb4_unicode_ci,
  `cc_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Cancel,1=Approved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_cc`
--

INSERT INTO `student_cc` (`student_cc_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `student_id`, `cc_date`, `cc_reason`, `cc_status`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 1, 1, 1, 7, '2019-02-26', 'Test', 1, '2019-02-23 05:29:17', '2019-02-23 05:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `student_cheques`
--

CREATE TABLE `student_cheques` (
  `student_cheque_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `student_cheque_no` text COLLATE utf8mb4_unicode_ci,
  `student_cheque_date` date DEFAULT NULL,
  `student_cheque_amt` text COLLATE utf8mb4_unicode_ci,
  `student_cheque_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Cancel,2=Cleared,3=Bounce',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cheques_session_id` int(10) UNSIGNED DEFAULT NULL,
  `cheques_class_id` int(10) UNSIGNED DEFAULT NULL,
  `cheques_receipt_id` int(11) DEFAULT NULL,
  `cheques_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Academic Fee,1=Hostel Fee'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_cheques`
--

INSERT INTO `student_cheques` (`student_cheque_id`, `admin_id`, `update_by`, `student_id`, `bank_id`, `student_cheque_no`, `student_cheque_date`, `student_cheque_amt`, `student_cheque_status`, `created_at`, `updated_at`, `cheques_session_id`, `cheques_class_id`, `cheques_receipt_id`, `cheques_for`) VALUES
(1, 2, 2, 6, 2, '85746', '2019-04-25', '10000', 0, '2019-04-02 17:23:38', '2019-04-02 17:23:38', 1, 1, 32, 0),
(2, 2, 2, 8, 3, '45454', '2019-04-25', '10000', 0, '2019-04-03 16:25:06', '2019-04-03 16:25:06', 1, 1, 33, 0);

-- --------------------------------------------------------

--
-- Table structure for table `student_documents`
--

CREATE TABLE `student_documents` (
  `student_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `student_document_details` text COLLATE utf8mb4_unicode_ci,
  `student_document_file` text COLLATE utf8mb4_unicode_ci,
  `student_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_documents`
--

INSERT INTO `student_documents` (`student_document_id`, `admin_id`, `update_by`, `student_id`, `document_category_id`, `student_document_details`, `student_document_file`, `student_document_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 9, 1, 'details are attached', NULL, 0, '2019-04-02 17:11:06', '2019-04-02 17:11:06'),
(2, 2, 2, 11, 1, '123', 'Jellyfish148011554465596.jpg', 0, '2019-04-05 22:29:56', '2019-04-05 22:29:56'),
(3, 2, 2, 11, 1, '11', NULL, 0, '2019-04-05 22:29:56', '2019-04-05 22:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `student_health_info`
--

CREATE TABLE `student_health_info` (
  `student_health_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_vision_left` text COLLATE utf8mb4_unicode_ci,
  `student_vision_right` text COLLATE utf8mb4_unicode_ci,
  `medical_issues` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_health_info`
--

INSERT INTO `student_health_info` (`student_health_info_id`, `admin_id`, `update_by`, `student_id`, `student_height`, `student_weight`, `student_blood_group`, `student_vision_left`, `student_vision_right`, `medical_issues`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, 'No', '2019-01-23 11:16:47', '2019-01-23 11:16:47'),
(2, 2, 2, 2, NULL, NULL, NULL, NULL, NULL, 'No', '2019-01-23 11:25:12', '2019-01-23 11:25:12'),
(3, 2, 2, 3, NULL, NULL, NULL, NULL, NULL, 'No', '2019-01-23 11:28:31', '2019-01-23 11:28:31'),
(4, 2, 2, 5, NULL, NULL, NULL, NULL, NULL, 'No', '2019-01-25 06:42:42', '2019-01-25 06:42:42'),
(5, 2, 2, 6, NULL, NULL, NULL, NULL, NULL, 'No', '2019-01-25 10:47:36', '2019-01-25 10:47:36'),
(6, NULL, 2, 7, '3.4', '37', 'B+', 'Left', 'Right', 'NO', '2019-02-04 05:15:06', '2019-02-04 05:15:06'),
(7, 2, 2, 8, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-05 11:19:22', '2019-03-05 11:19:22'),
(8, 2, 2, 9, '5', '48', 'A+', 'eaasas', 'fdfdf', 'no', '2019-04-02 17:11:06', '2019-04-02 17:11:06'),
(9, 2, 2, 10, '1', '2', '3', '4', '5', 'NO,dfd,fdsfdf,erere,fdfsfsd,werwer,rwerwer', '2019-04-03 23:28:02', '2019-04-04 21:49:35'),
(10, 2, 2, 11, '5.2', '70', 'A+', '6', '6', 'NA,NO', '2019-04-05 22:29:56', '2019-04-05 22:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `student_leaves`
--

CREATE TABLE `student_leaves` (
  `student_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `student_leave_from_date` date DEFAULT NULL,
  `student_leave_to_date` date DEFAULT NULL,
  `student_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `student_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_leaves`
--

INSERT INTO `student_leaves` (`student_leave_id`, `admin_id`, `update_by`, `student_id`, `student_leave_reason`, `student_leave_from_date`, `student_leave_to_date`, `student_leave_attachment`, `student_leave_status`, `created_at`, `updated_at`) VALUES
(1, 31, 31, 10, 'Login as staff >> Go to Profile >> Click on attendance>> Try to scroll for previous months nad check', '2019-04-10', '2019-04-15', '', 2, '2019-04-04 21:59:10', '2019-04-04 22:14:27'),
(2, 31, 31, 10, 'qwe', '2019-04-16', '2019-04-20', '', 2, '2019-04-04 22:02:53', '2019-04-04 22:14:25'),
(3, 31, 31, 10, 'new pending', '2019-04-22', '2019-04-25', '', 0, '2019-04-04 22:06:32', '2019-04-05 20:42:21'),
(13, 5, 5, 1, 'test', '2019-04-09', '2019-04-11', NULL, 0, '2019-04-09 17:44:02', '2019-04-09 17:44:02'),
(14, 5, 5, 2, 'test', '2019-04-10', '2019-04-13', '', 1, '2019-04-09 18:59:02', '2019-04-09 19:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `student_marksheet`
--

CREATE TABLE `student_marksheet` (
  `student_marksheet_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `total_marks` double(18,2) DEFAULT NULL,
  `obtain_marks` double(18,2) DEFAULT NULL,
  `percentage` double(18,2) DEFAULT NULL,
  `class_rank` int(10) UNSIGNED DEFAULT NULL,
  `section_rank` int(10) UNSIGNED DEFAULT NULL,
  `pass_fail` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Fail,1=Pass',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_marksheet`
--

INSERT INTO `student_marksheet` (`student_marksheet_id`, `admin_id`, `update_by`, `session_id`, `exam_id`, `class_id`, `section_id`, `student_id`, `total_marks`, `obtain_marks`, `percentage`, `class_rank`, `section_rank`, `pass_fail`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, 3, 300.00, 118.00, 39.33, 4, 4, 0, '2019-02-21 06:46:37', '2019-02-26 14:16:50'),
(2, 2, 2, 1, 1, 1, 1, 5, 300.00, 224.00, 74.67, 2, 2, 1, '2019-02-21 06:46:37', '2019-02-26 14:16:50'),
(3, 2, 2, 1, 1, 1, 2, 2, 300.00, 277.00, 92.33, 1, 1, 1, '2019-02-21 06:46:37', '2019-02-26 14:16:50'),
(4, 2, 2, 1, 1, 1, 1, 6, 300.00, 199.00, 66.33, 3, 3, 1, '2019-02-21 06:46:38', '2019-02-26 14:16:50'),
(5, 2, 2, 1, 1, 1, 1, 1, 300.00, 277.00, 92.33, 1, 1, 1, '2019-02-21 06:46:38', '2019-02-26 14:16:50'),
(6, 2, 2, 1, 2, 1, 1, 3, 400.00, 264.00, 66.00, 4, 3, 1, '2019-02-27 11:34:15', '2019-04-04 19:59:50'),
(7, 2, 2, 1, 2, 1, 1, 5, 400.00, 300.00, 75.00, 3, 2, 1, '2019-02-27 11:34:15', '2019-04-04 19:59:50'),
(8, 2, 2, 1, 2, 1, 2, 2, 300.00, 264.00, 88.00, 1, 1, 1, '2019-02-27 11:34:15', '2019-04-04 19:59:50'),
(9, 2, 2, 1, 2, 1, 1, 6, 400.00, 300.00, 75.00, 3, 2, 1, '2019-02-27 11:34:15', '2019-04-04 19:59:50'),
(10, 2, 2, 1, 2, 1, 1, 1, 400.00, 343.00, 85.75, 2, 1, 1, '2019-02-27 11:34:15', '2019-04-04 19:59:50'),
(11, 2, 2, 1, 2, 1, 1, 8, 0.00, 0.00, 0.00, 5, 4, 1, '2019-03-23 09:54:47', '2019-04-04 19:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `student_parents`
--

CREATE TABLE `student_parents` (
  `student_parent_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `student_login_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_tongue` text COLLATE utf8mb4_unicode_ci,
  `student_guardian_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_relation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_parent_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_parents`
--

INSERT INTO `student_parents` (`student_parent_id`, `admin_id`, `update_by`, `reference_admin_id`, `student_login_name`, `student_login_email`, `student_login_contact_no`, `student_father_name`, `student_father_mobile_number`, `student_father_email`, `student_father_occupation`, `student_father_annual_salary`, `student_mother_name`, `student_mother_mobile_number`, `student_mother_email`, `student_mother_occupation`, `student_mother_annual_salary`, `student_mother_tongue`, `student_guardian_name`, `student_guardian_email`, `student_guardian_mobile_number`, `student_guardian_relation`, `student_parent_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 5, 'Nath', 'nath1@gmail.com', '8888888888', 'Nath', '8888888888', 'nath@gmail.com', 'Job', 'Below 5 Lac', 'Neeta', NULL, NULL, NULL, 'Below 1 Lac', NULL, NULL, NULL, NULL, NULL, 1, '2019-01-23 11:16:47', '2019-03-05 11:19:22'),
(2, 2, 2, 9, 'Kamal', 'kamal@gmail.com', '7777777777', 'Kamal', '7777777777', 'kamal@gmail.com', 'Job', NULL, 'Manisha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-01-23 11:28:31', '2019-01-23 11:28:31'),
(3, 2, 2, 13, 'Shri', NULL, '5555555555', 'Shri', '5555555555', NULL, 'A', NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-01-25 10:47:36', '2019-01-25 10:47:36'),
(4, NULL, 2, 15, 'ravi', 'ravi@gmail.com', '1234567908', 'ravi', '1234567908', 'ravi@gmail.com', 'teacher', 'Below 1 Lac', 'durga', '5678975432', 'durga@gmail.com', 'Police inspector', 'Below 1 Lac', 'Hindi', 'rakesh', 'rakesh@gmail.com', '5643212332', 'brother', 1, '2019-02-04 05:15:06', '2019-02-04 05:15:06'),
(5, 2, 2, 27, 'ram', 'ram@gmail.com', '5564545454', 'ram', '6454545454', 'ram@gmail.com', 'doctor', 'Below 7.5 Lac', 'asha', '5455555555', 'asha@gmail.com', 'house wife', 'Below 1 Lac', 'tsttt', NULL, NULL, NULL, NULL, 1, '2019-04-02 17:11:06', '2019-04-02 17:11:06'),
(6, 2, 2, 31, 'Sourabh Rohila', 'sourabh@mailinator.com', '8209683554', 'Sourabh Rohila', '8209683554', 'sourabh@mailinator.com', '123', 'Below 1 Lac', '123', NULL, NULL, NULL, 'Below 1 Lac', NULL, NULL, NULL, NULL, NULL, 1, '2019-04-03 23:28:02', '2019-04-03 23:28:02'),
(7, 2, 2, 34, 'parent testing', 'parent@mailinator.com', '9595959595', 'parent testing', '9595959595', 'parent@mailinator.com', 'Agriculture !!', 'Below 7.5 Lac', 'Mother testing', '9494949494', 'mother@mailinator.com', 'Home maker', 'Below 5 Lac', 'Punjabi', 'Guardian testing', 'guardian@mailinator.com', '9393939393', 'brother', 1, '2019-04-05 22:29:56', '2019-04-05 22:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `student_tc`
--

CREATE TABLE `student_tc` (
  `student_tc_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `tc_date` date DEFAULT NULL,
  `tc_remark` text COLLATE utf8mb4_unicode_ci,
  `tc_reason` text COLLATE utf8mb4_unicode_ci,
  `tc_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Cancel,1=Approved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_tc`
--

INSERT INTO `student_tc` (`student_tc_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `student_id`, `tc_date`, `tc_remark`, `tc_reason`, `tc_status`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 1, 1, 1, 7, '2019-02-25', 'test', 'test', 1, '2019-02-22 04:31:24', '2019-02-22 04:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `st_exam_result`
--

CREATE TABLE `st_exam_result` (
  `st_exam_result_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `result_date` date DEFAULT NULL,
  `result_time` time DEFAULT NULL,
  `cronjob_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Not Sent,1=Sent',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `st_exam_result`
--

INSERT INTO `st_exam_result` (`st_exam_result_id`, `admin_id`, `update_by`, `session_id`, `exam_id`, `class_id`, `result_date`, `result_time`, `cronjob_status`, `created_at`, `updated_at`) VALUES
(4, 2, 2, 1, 1, 1, '2019-02-28', '17:00:00', 0, '2019-02-27 05:01:03', '2019-02-27 05:01:03'),
(5, 2, 2, 1, 2, 1, '2019-04-27', '01:00:00', 0, '2019-02-27 11:40:31', '2019-04-02 17:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `st_exclude_sub`
--

CREATE TABLE `st_exclude_sub` (
  `st_exclude_sub_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `st_exclude_sub`
--

INSERT INTO `st_exclude_sub` (`st_exclude_sub_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `subject_id`, `student_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, 8, '2019-04-02 16:31:43', '2019-04-02 16:31:43'),
(2, 2, 2, 1, 1, 1, 2, 1, '2019-04-02 16:32:19', '2019-04-02 16:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `st_parent_groups`
--

CREATE TABLE `st_parent_groups` (
  `st_parent_group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=parent',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `st_parent_group_map`
--

CREATE TABLE `st_parent_group_map` (
  `st_parent_group_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `st_parent_group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_member_id` int(11) DEFAULT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=parent',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `subject_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_is_coscholastic` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `class_ids` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_id` int(10) UNSIGNED DEFAULT NULL,
  `co_scholastic_subject_des` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `subject_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `admin_id`, `update_by`, `subject_name`, `subject_code`, `subject_is_coscholastic`, `class_ids`, `co_scholastic_type_id`, `co_scholastic_subject_des`, `medium_type`, `subject_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Hindi', 'H', 0, '', NULL, NULL, 1, 1, '2019-01-23 10:35:55', '2019-01-23 10:35:55'),
(2, 2, 2, 'English', 'E', 0, '', NULL, NULL, 1, 1, '2019-01-23 10:36:04', '2019-01-23 10:36:04'),
(3, 2, 2, 'Math', 'M', 1, '1', 1, 'D', 1, 1, '2019-01-23 10:36:19', '2019-02-26 14:12:17'),
(4, 2, 2, 'Science', 'S', 0, '', NULL, NULL, 1, 1, '2019-01-23 10:36:39', '2019-01-23 10:36:39'),
(5, 2, 2, 'Env', 'ENV', 0, '', NULL, NULL, 1, 1, '2019-02-07 13:01:16', '2019-02-07 13:01:16'),
(6, 2, 2, 'Test Subject 1', '001', 0, '', NULL, NULL, 1, 1, '2019-04-03 18:02:14', '2019-04-03 18:02:14'),
(7, 2, 2, 'Test Subject 2', '002', 1, '5', 3, '123', 1, 1, '2019-04-03 18:14:42', '2019-04-03 18:15:17'),
(8, 2, 2, 'Test Subject 3', '003', 0, '', NULL, NULL, 1, 1, '2019-04-03 18:15:49', '2019-04-03 18:15:49'),
(9, 2, 2, 'Test Subject 4', '004', 0, '', NULL, NULL, 1, 1, '2019-04-03 18:16:08', '2019-04-03 18:16:08'),
(10, 2, 2, 'Test Subject 5', '005', 0, '', NULL, NULL, 1, 1, '2019-04-03 18:16:21', '2019-04-03 18:16:21'),
(11, 2, 2, 'Test Subject 6', '006', 0, '', NULL, NULL, 1, 1, '2019-04-03 18:16:41', '2019-04-03 18:16:41');

-- --------------------------------------------------------

--
-- Table structure for table `subject_class_map`
--

CREATE TABLE `subject_class_map` (
  `subject_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_class_map`
--

INSERT INTO `subject_class_map` (`subject_class_map_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `subject_id`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, NULL, 1, 1, '2019-01-23 10:36:51', '2019-01-23 10:36:51'),
(2, 2, 2, 1, 1, NULL, 2, 1, '2019-01-23 10:36:51', '2019-01-23 10:36:51'),
(3, 2, 2, 1, 1, NULL, 3, 1, '2019-01-23 10:36:51', '2019-01-23 10:36:51'),
(4, 2, 2, 1, 1, NULL, 4, 1, '2019-01-23 10:36:51', '2019-01-23 10:36:51'),
(5, 2, 2, 1, 1, NULL, 5, 1, '2019-02-07 13:01:32', '2019-02-07 13:01:32'),
(6, 2, 2, 1, 2, NULL, 1, 1, '2019-04-02 16:29:43', '2019-04-02 16:29:43'),
(7, 2, 2, 1, 2, NULL, 2, 1, '2019-04-02 16:29:43', '2019-04-02 16:29:43'),
(8, 2, 2, 1, 2, NULL, 3, 1, '2019-04-02 16:29:43', '2019-04-02 16:29:43'),
(9, 2, 2, 1, 3, NULL, 1, 1, '2019-04-02 16:30:03', '2019-04-02 16:30:03'),
(10, 2, 2, 1, 3, NULL, 2, 1, '2019-04-02 16:30:03', '2019-04-02 16:30:03'),
(14, 2, 2, 1, 4, NULL, 1, 1, '2019-04-02 20:48:58', '2019-04-02 20:48:58'),
(16, 2, 2, 1, 5, NULL, 6, 1, '2019-04-03 18:23:42', '2019-04-03 18:23:42'),
(17, 2, 2, 1, 5, NULL, 7, 1, '2019-04-03 18:23:42', '2019-04-03 18:23:42'),
(18, 2, 2, 1, 5, NULL, 8, 1, '2019-04-03 18:23:42', '2019-04-03 18:23:42'),
(19, 2, 2, 1, 6, NULL, 6, 1, '2019-04-06 18:49:38', '2019-04-06 18:49:38'),
(20, 2, 2, 1, 6, NULL, 10, 1, '2019-04-06 18:56:10', '2019-04-06 18:56:10'),
(21, 2, 2, 1, 6, NULL, 11, 1, '2019-04-06 18:56:10', '2019-04-06 18:56:10');

-- --------------------------------------------------------

--
-- Table structure for table `subject_section_map`
--

CREATE TABLE `subject_section_map` (
  `subject_section_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_section_map`
--

INSERT INTO `subject_section_map` (`subject_section_map_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `section_id`, `subject_id`, `created_at`, `updated_at`) VALUES
(5, 2, 2, 1, 1, 1, 1, '2019-01-25 10:00:06', '2019-01-25 10:00:06'),
(6, 2, 2, 1, 1, 1, 2, '2019-01-25 10:00:06', '2019-01-25 10:00:06'),
(7, 2, 2, 1, 1, 1, 3, '2019-01-25 10:00:06', '2019-01-25 10:00:06'),
(8, 2, 2, 1, 1, 1, 4, '2019-01-25 10:00:06', '2019-01-25 10:00:06'),
(9, 2, 2, 1, 1, 2, 1, '2019-01-28 07:47:25', '2019-01-28 07:47:25'),
(10, 2, 2, 1, 1, 2, 2, '2019-01-28 07:47:25', '2019-01-28 07:47:25'),
(11, 2, 2, 1, 1, 2, 3, '2019-01-28 07:47:25', '2019-01-28 07:47:25'),
(12, 2, 2, 1, 1, 2, 4, '2019-01-28 07:47:25', '2019-01-28 07:47:25'),
(14, 2, 2, 1, 5, 5, 6, '2019-04-03 18:35:30', '2019-04-03 18:35:30'),
(15, 2, 2, 1, 5, 5, 7, '2019-04-03 18:35:30', '2019-04-03 18:35:30'),
(16, 2, 2, 1, 5, 5, 8, '2019-04-03 18:35:30', '2019-04-03 18:35:30'),
(17, 2, 2, 1, 6, 6, 6, '2019-04-06 18:50:51', '2019-04-06 18:50:51'),
(18, 2, 2, 1, 6, 6, 10, '2019-04-06 18:56:34', '2019-04-06 18:56:34'),
(19, 2, 2, 1, 6, 6, 11, '2019-04-06 18:56:34', '2019-04-06 18:56:34');

-- --------------------------------------------------------

--
-- Table structure for table `system_config`
--

CREATE TABLE `system_config` (
  `system_config_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_gateway_url` text COLLATE utf8mb4_unicode_ci,
  `sms_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_sender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_priority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_stype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_config`
--

INSERT INTO `system_config` (`system_config_id`, `admin_id`, `update_by`, `email`, `password`, `sms_gateway_url`, `sms_username`, `sms_password`, `sms_sender`, `sms_priority`, `sms_stype`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'v2rteam@gmail.com', 'v2r@987@7q12t!', 'http://bulksms.wscubetech.com/api/sendmsg.php', 'DEMACD', 'demacd@123!', 'DEMACD', 'ndnd', 'normal', '2019-04-01 11:27:52', '2019-04-01 11:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `task_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `task_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `task_priority` tinyint(4) DEFAULT NULL COMMENT '0=Low,1=Medium,2=High',
  `task_type` int(10) UNSIGNED DEFAULT NULL COMMENT '1=Student,2=Staff',
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `task_date` date DEFAULT NULL,
  `task_description` text COLLATE utf8mb4_unicode_ci,
  `task_file` text COLLATE utf8mb4_unicode_ci,
  `task_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Open,1=On Hold,2=Resolved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `admin_id`, `update_by`, `task_name`, `task_priority`, `task_type`, `class_id`, `section_id`, `student_id`, `staff_id`, `task_date`, `task_description`, `task_file`, `task_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'student task', 1, 1, 1, 1, 1, NULL, '2019-04-04', 'learning science topic', 'Admindashboard-06.02.19.584741554189482.docx', 0, '2019-04-02 17:48:02', '2019-04-02 17:49:05'),
(2, 2, 2, 'science', 1, 1, 1, 1, 5, NULL, '2019-04-05', 'learning', NULL, 1, '2019-04-02 17:49:38', '2019-04-02 17:49:38');

-- --------------------------------------------------------

--
-- Table structure for table `task_response`
--

CREATE TABLE `task_response` (
  `task_response_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `task_type` int(10) UNSIGNED DEFAULT NULL COMMENT '0=Admin,1=Student,2=Staff',
  `staff_student_id` int(10) UNSIGNED DEFAULT NULL,
  `response_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tds_setting`
--

CREATE TABLE `tds_setting` (
  `tds_setting_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `salary_min` double(18,2) DEFAULT NULL,
  `salary_max` double(18,2) DEFAULT NULL,
  `salary_deduction` double(8,2) DEFAULT NULL,
  `tds_setting_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tds_setting`
--

INSERT INTO `tds_setting` (`tds_setting_id`, `admin_id`, `update_by`, `session_id`, `salary_min`, `salary_max`, `salary_deduction`, `tds_setting_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 100.00, 500.00, 5.00, 1, '2019-04-02 18:45:57', '2019-04-02 18:45:57'),
(2, 2, 2, 1, 501.00, 1000.00, 2.00, 1, '2019-04-02 18:46:43', '2019-04-02 18:46:43'),
(3, 2, 2, 1, 5000.00, 10000.00, 5.00, 1, '2019-04-02 18:46:57', '2019-04-02 18:46:57'),
(4, 2, 2, 1, 10001.00, 20000.00, 20.00, 1, '2019-04-02 18:47:24', '2019-04-02 18:47:24');

-- --------------------------------------------------------

--
-- Table structure for table `term_exams`
--

CREATE TABLE `term_exams` (
  `term_exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `term_exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_exam_caption` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `term_exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `term_exams`
--

INSERT INTO `term_exams` (`term_exam_id`, `admin_id`, `update_by`, `term_exam_name`, `term_exam_caption`, `medium_type`, `term_exam_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Term', 'Term', 1, 1, '2019-01-25 09:57:02', '2019-01-25 09:57:02'),
(2, 2, 2, 'term-1', 'testtt', 1, 1, '2019-04-02 16:55:43', '2019-04-02 16:55:43'),
(3, 2, 2, 'Sessional terms', NULL, 1, 1, '2019-04-06 18:19:38', '2019-04-06 18:19:49');

-- --------------------------------------------------------

--
-- Table structure for table `time_tables`
--

CREATE TABLE `time_tables` (
  `time_table_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `time_table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `lecture_no` int(10) UNSIGNED DEFAULT NULL,
  `time_table_week_days` text COLLATE utf8mb4_unicode_ci,
  `time_table_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_tables`
--

INSERT INTO `time_tables` (`time_table_id`, `admin_id`, `update_by`, `session_id`, `time_table_name`, `class_id`, `section_id`, `lecture_no`, `time_table_week_days`, `time_table_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'exam time table', 1, 1, 8, '1,2', 1, '2019-04-02 16:38:09', '2019-04-03 20:34:59'),
(2, 2, 2, 1, 'mid term', 1, 2, 1, '1,3,5', 1, '2019-04-02 16:38:36', '2019-04-02 16:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `time_table_map`
--

CREATE TABLE `time_table_map` (
  `time_table_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `time_table_id` int(10) UNSIGNED DEFAULT NULL,
  `lecture_no` int(10) UNSIGNED DEFAULT NULL,
  `day` int(10) UNSIGNED DEFAULT NULL,
  `lunch_break` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `start_time` text COLLATE utf8mb4_unicode_ci,
  `end_time` text COLLATE utf8mb4_unicode_ci,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `expire_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Expire,1=Running',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_table_map`
--

INSERT INTO `time_table_map` (`time_table_map_id`, `admin_id`, `update_by`, `session_id`, `time_table_id`, `lecture_no`, `day`, `lunch_break`, `start_time`, `end_time`, `subject_id`, `staff_id`, `expire_status`, `created_at`, `updated_at`) VALUES
(8, 2, 2, 1, 1, 1, 4, 1, '10:00', '11:00', NULL, NULL, 1, '2019-04-03 20:24:05', '2019-04-03 20:24:05'),
(9, 2, 2, 1, 1, 1, 3, 1, '10:00', '11:00', NULL, NULL, 1, '2019-04-03 20:24:40', '2019-04-03 20:24:40'),
(10, 2, 2, 1, 1, 2, 4, 1, '10:00', '11:00', NULL, NULL, 1, '2019-04-03 20:24:56', '2019-04-03 20:24:56'),
(11, 2, 2, 1, 1, 1, 2, 0, '10:00', '12:00', 1, 1, 1, '2019-04-03 20:36:47', '2019-04-03 20:36:47'),
(12, 2, 2, 1, 1, 2, 2, 0, '13:00', '14:00', 2, 1, 1, '2019-04-03 20:37:28', '2019-04-03 20:37:28');

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `title_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `title_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`title_id`, `admin_id`, `update_by`, `title_name`, `title_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Mr', 1, '2019-01-23 10:46:15', '2019-01-23 10:46:15'),
(2, 2, 2, 'Minister', 1, '2019-04-05 21:10:32', '2019-04-05 21:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `transport_fees`
--

CREATE TABLE `transport_fees` (
  `transport_fee_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `map_student_staff_id` int(10) UNSIGNED DEFAULT NULL,
  `receipt_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receipt_date` date NOT NULL,
  `total_paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fee_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Paid, 2:Reverted,3:Refunded',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transport_fees_head`
--

CREATE TABLE `transport_fees_head` (
  `fees_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `fees_head_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fees_head_km_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fees_head_km_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fees_head_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'per month',
  `fees_head_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transport_fees_head`
--

INSERT INTO `transport_fees_head` (`fees_head_id`, `admin_id`, `update_by`, `fees_head_name`, `fees_head_km_from`, `fees_head_km_to`, `fees_head_amount`, `fees_head_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Range 1-15', '1', '15', '5000', 1, '2019-03-01 05:56:44', '2019-03-01 05:56:44'),
(2, 2, 2, 'Range 15-20', '15', '20', '10000', 1, '2019-03-01 05:57:04', '2019-03-01 05:57:27'),
(3, 2, 2, 'Range 20-25', '20', '25', '15000', 1, '2019-03-01 05:57:51', '2019-03-01 05:57:51');

-- --------------------------------------------------------

--
-- Table structure for table `transport_fee_details`
--

CREATE TABLE `transport_fee_details` (
  `transport_fee_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `transport_fee_id` int(10) UNSIGNED DEFAULT NULL,
  `month_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paid_fee_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amount_status` tinyint(4) NOT NULL COMMENT '1:full amount, 2:Less amount',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `user_details_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `task_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `user_type` tinyint(4) DEFAULT NULL COMMENT '1=student,2=staff',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_registration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_ownership` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Owner,1=Contract',
  `vehicle_capacity` int(11) DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`vehicle_id`, `admin_id`, `update_by`, `vehicle_name`, `vehicle_registration_no`, `vehicle_ownership`, `vehicle_capacity`, `contact_person`, `contact_number`, `vehicle_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Vehicle 1', 'V00001', 0, 1, NULL, NULL, 1, '2019-02-19 11:13:24', '2019-03-01 06:19:22'),
(2, 2, 2, 'Vehicle 2', 'V00002', 0, 40, NULL, NULL, 1, '2019-03-01 06:20:03', '2019-03-01 06:20:03'),
(3, 2, 2, 'Vehicle-3', 'VH-90543', 0, 30, 'babu lal', '8900067554', 1, '2019-04-03 15:17:04', '2019-04-04 17:29:56'),
(4, 2, 2, 'Vehicle-3', 'VH-656789', 0, 30, 'shyaam', '8900067554', 0, '2019-04-04 17:29:04', '2019-04-04 18:06:18');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_attendence`
--

CREATE TABLE `vehicle_attendence` (
  `vehicle_attendence_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_id` int(10) UNSIGNED DEFAULT NULL,
  `total_present_staff` int(10) UNSIGNED DEFAULT NULL,
  `total_present_student` int(10) UNSIGNED DEFAULT NULL,
  `total_strength` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_attendence_date` date DEFAULT NULL,
  `vehicle_attendence_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_attendence`
--

INSERT INTO `vehicle_attendence` (`vehicle_attendence_id`, `admin_id`, `update_by`, `session_id`, `vehicle_id`, `total_present_staff`, `total_present_student`, `total_strength`, `vehicle_attendence_date`, `vehicle_attendence_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 0, 1, 1, '2019-03-11', 1, '2019-03-11 05:10:45', '2019-03-11 05:10:45'),
(2, 2, 2, 1, 2, 1, 2, 4, '2019-04-02', 1, '2019-04-02 17:59:24', '2019-04-02 17:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_attend_details`
--

CREATE TABLE `vehicle_attend_details` (
  `vehicle_attend_d_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_id` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_attendence_id` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_attendence_type` tinyint(4) DEFAULT NULL COMMENT '0=Absent,1=Present',
  `student_staff_id` int(10) UNSIGNED DEFAULT NULL,
  `student_staff_type` tinyint(4) DEFAULT NULL COMMENT '1=Student,2=Staff',
  `attendence_pick_up` int(10) UNSIGNED DEFAULT NULL,
  `attendence_drop` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_attendence_date` date DEFAULT NULL,
  `vehicle_attendence_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Unmark,1=Mark',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_attend_details`
--

INSERT INTO `vehicle_attend_details` (`vehicle_attend_d_id`, `admin_id`, `update_by`, `session_id`, `vehicle_id`, `vehicle_attendence_id`, `vehicle_attendence_type`, `student_staff_id`, `student_staff_type`, `attendence_pick_up`, `attendence_drop`, `vehicle_attendence_date`, `vehicle_attendence_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, 6, 1, 1, 1, '2019-03-11', 1, '2019-03-11 05:10:45', '2019-03-11 05:10:45'),
(2, 2, 2, 1, 2, 2, 0, 8, 1, NULL, NULL, '2019-04-02', 1, '2019-04-02 17:59:24', '2019-04-02 17:59:24'),
(3, 2, 2, 1, 2, 2, 1, 5, 1, NULL, NULL, '2019-04-02', 1, '2019-04-02 17:59:24', '2019-04-02 17:59:24'),
(4, 2, 2, 1, 2, 2, 1, 2, 1, NULL, NULL, '2019-04-02', 1, '2019-04-02 17:59:24', '2019-04-02 17:59:24'),
(5, 2, 2, 1, 2, 2, 1, 4, 2, NULL, NULL, '2019-04-02', 1, '2019-04-02 17:59:24', '2019-04-02 17:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_documents`
--

CREATE TABLE `vehicle_documents` (
  `vehicle_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_id` int(10) UNSIGNED DEFAULT NULL,
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `vehicle_document_details` text COLLATE utf8mb4_unicode_ci,
  `vehicle_document_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_documents`
--

INSERT INTO `vehicle_documents` (`vehicle_document_id`, `admin_id`, `update_by`, `vehicle_id`, `document_category_id`, `vehicle_document_details`, `vehicle_document_file`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 'RC', 'Screenshot (72)_LI709441550574806.jpg', '2019-02-19 11:13:26', '2019-03-01 06:19:22'),
(2, 2, 2, 2, 1, 'RC', 'Salary Structure678221551421203.png', '2019-03-01 06:20:03', '2019-03-01 06:20:03'),
(3, 2, 2, 3, 1, 'testeed', 'country848911554266824.svg', '2019-04-03 15:17:04', '2019-04-03 15:17:04'),
(4, 2, 2, 4, 1, 'tettttt', 'fee-collection74581554361144.png', '2019-04-04 17:29:04', '2019-04-04 17:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `virtual_classes`
--

CREATE TABLE `virtual_classes` (
  `virtual_class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `virtual_class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `virtual_class_description` text COLLATE utf8mb4_unicode_ci,
  `virtual_class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `virtual_classes`
--

INSERT INTO `virtual_classes` (`virtual_class_id`, `admin_id`, `update_by`, `session_id`, `virtual_class_name`, `virtual_class_description`, `virtual_class_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, NULL, 'Test', 'test', 1, '2019-04-03 16:04:08', '2019-04-03 16:04:08'),
(2, 2, 2, NULL, '1', '1', 1, '2019-04-03 16:56:12', '2019-04-03 16:56:12'),
(3, 2, 2, NULL, 'sourabh test virtual class', 'test desc', 1, '2019-04-03 17:08:47', '2019-04-03 17:08:47');

-- --------------------------------------------------------

--
-- Table structure for table `virtual_class_map`
--

CREATE TABLE `virtual_class_map` (
  `virtual_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `virtual_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `virtual_class_map`
--

INSERT INTO `virtual_class_map` (`virtual_class_map_id`, `admin_id`, `update_by`, `virtual_class_id`, `student_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, '2019-04-03 16:04:39', '2019-04-03 16:04:39'),
(4, 2, 2, 3, 8, '2019-04-03 17:12:23', '2019-04-03 17:12:23');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `visitor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purpose` text COLLATE utf8mb4_unicode_ci,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `check_in_time` time DEFAULT NULL,
  `check_out_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`visitor_id`, `admin_id`, `update_by`, `name`, `purpose`, `contact`, `email`, `visit_date`, `check_in_time`, `check_out_time`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'rajat', 'visit school for child admission', '6578906533', 'rajat@gmail.com', '2019-04-02', '10:00:00', '12:00:00', '2019-04-02 17:35:45', '2019-04-02 17:35:45'),
(2, 2, 2, 'rohit', 'visit school', '6578906533', 'rohit@gmail.com', '2019-04-02', '11:00:00', '14:00:00', '2019-04-02 17:39:14', '2019-04-02 17:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_ac`
--

CREATE TABLE `wallet_ac` (
  `wallet_ac_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `wallet_ac_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet_ac_amt` double DEFAULT NULL,
  `wallet_ac_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_ac`
--

INSERT INTO `wallet_ac` (`wallet_ac_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `wallet_ac_name`, `wallet_ac_amt`, `wallet_ac_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 'Teena', 0, 1, '2019-01-23 11:16:47', '2019-01-23 11:16:47'),
(2, 2, 2, 1, 1, 2, 'Pankaj', 0, 1, '2019-01-23 11:25:12', '2019-01-23 11:25:12'),
(3, 2, 2, 1, 1, 3, 'Ajay', 0, 1, '2019-01-23 11:28:31', '2019-01-23 11:28:31'),
(4, 2, 2, 1, 1, 5, 'Neelam', 0, 1, '2019-01-25 06:42:42', '2019-01-25 06:42:42'),
(5, 2, 2, 1, 1, 6, 'Shri', 0, 1, '2019-01-25 10:47:36', '2019-01-25 10:47:36'),
(6, NULL, 2, NULL, 1, 7, 'Amit', 0, 1, '2019-02-04 05:15:06', '2019-02-04 05:15:06'),
(7, 2, 2, 1, 1, 8, 'Sibling A', 0, 1, '2019-03-05 11:19:22', '2019-03-05 11:19:22'),
(8, 2, 2, 1, 2, 9, 'khushbu', 0, 1, '2019-04-02 17:11:06', '2019-04-02 17:11:06'),
(9, 2, 2, 1, 5, 10, 'Sourabh Rohila', 0, 1, '2019-04-03 23:28:02', '2019-04-03 23:28:02'),
(10, 2, 2, 5, 6, 11, 'Student testing 1', 0, 1, '2019-04-05 22:29:56', '2019-04-05 22:29:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_group`
--
ALTER TABLE `acc_group`
  ADD PRIMARY KEY (`acc_group_id`),
  ADD KEY `acc_group_admin_id_foreign` (`admin_id`),
  ADD KEY `acc_group_update_by_foreign` (`update_by`),
  ADD KEY `acc_group_acc_sub_head_id_foreign` (`acc_sub_head_id`);

--
-- Indexes for table `acc_group_heads`
--
ALTER TABLE `acc_group_heads`
  ADD PRIMARY KEY (`acc_group_head_id`),
  ADD KEY `acc_group_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `acc_group_heads_update_by_foreign` (`update_by`),
  ADD KEY `acc_group_heads_acc_sub_head_id_foreign` (`acc_sub_head_id`),
  ADD KEY `acc_group_heads_acc_group_id_foreign` (`acc_group_id`);

--
-- Indexes for table `acc_main_heads`
--
ALTER TABLE `acc_main_heads`
  ADD PRIMARY KEY (`acc_main_head_id`);

--
-- Indexes for table `acc_open_balance`
--
ALTER TABLE `acc_open_balance`
  ADD PRIMARY KEY (`acc_open_balance_id`),
  ADD KEY `acc_open_balance_admin_id_foreign` (`admin_id`),
  ADD KEY `acc_open_balance_update_by_foreign` (`update_by`),
  ADD KEY `acc_open_balance_acc_sub_head_id_foreign` (`acc_sub_head_id`),
  ADD KEY `acc_open_balance_acc_group_head_id_foreign` (`acc_group_head_id`);

--
-- Indexes for table `acc_sub_heads`
--
ALTER TABLE `acc_sub_heads`
  ADD PRIMARY KEY (`acc_sub_head_id`),
  ADD KEY `acc_sub_heads_acc_main_head_id_foreign` (`acc_main_head_id`);

--
-- Indexes for table `ac_entry_map`
--
ALTER TABLE `ac_entry_map`
  ADD PRIMARY KEY (`ac_entry_map_id`),
  ADD KEY `ac_entry_map_admin_id_foreign` (`admin_id`),
  ADD KEY `ac_entry_map_update_by_foreign` (`update_by`),
  ADD KEY `ac_entry_map_session_id_foreign` (`session_id`),
  ADD KEY `ac_entry_map_class_id_foreign` (`class_id`),
  ADD KEY `ac_entry_map_section_id_foreign` (`section_id`),
  ADD KEY `ac_entry_map_student_id_foreign` (`student_id`),
  ADD KEY `ac_entry_map_ac_entry_id_foreign` (`ac_entry_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admission_data`
--
ALTER TABLE `admission_data`
  ADD PRIMARY KEY (`admission_data_id`),
  ADD KEY `admission_data_admin_id_foreign` (`admin_id`),
  ADD KEY `admission_data_update_by_foreign` (`update_by`),
  ADD KEY `admission_data_admission_form_id_foreign` (`admission_form_id`),
  ADD KEY `admission_data_student_sibling_class_id_foreign` (`student_sibling_class_id`),
  ADD KEY `admission_data_title_id_foreign` (`title_id`),
  ADD KEY `admission_data_caste_id_foreign` (`caste_id`),
  ADD KEY `admission_data_religion_id_foreign` (`religion_id`),
  ADD KEY `admission_data_nationality_id_foreign` (`nationality_id`),
  ADD KEY `admission_data_student_permanent_county_foreign` (`student_permanent_county`),
  ADD KEY `admission_data_student_permanent_state_foreign` (`student_permanent_state`),
  ADD KEY `admission_data_student_permanent_city_foreign` (`student_permanent_city`),
  ADD KEY `admission_data_student_temporary_county_foreign` (`student_temporary_county`),
  ADD KEY `admission_data_student_temporary_state_foreign` (`student_temporary_state`),
  ADD KEY `admission_data_student_temporary_city_foreign` (`student_temporary_city`),
  ADD KEY `admission_data_admission_class_id_foreign` (`admission_class_id`),
  ADD KEY `admission_data_admission_section_id_foreign` (`admission_section_id`),
  ADD KEY `admission_data_admission_session_id_foreign` (`admission_session_id`),
  ADD KEY `admission_data_current_session_id_foreign` (`current_session_id`),
  ADD KEY `admission_data_current_class_id_foreign` (`current_class_id`),
  ADD KEY `admission_data_current_section_id_foreign` (`current_section_id`),
  ADD KEY `admission_data_group_id_foreign` (`group_id`),
  ADD KEY `admission_data_stream_id_foreign` (`stream_id`),
  ADD KEY `admission_data_student_id_foreign` (`student_id`);

--
-- Indexes for table `admission_fields`
--
ALTER TABLE `admission_fields`
  ADD PRIMARY KEY (`admission_field_id`),
  ADD KEY `admission_fields_admin_id_foreign` (`admin_id`),
  ADD KEY `admission_fields_update_by_foreign` (`update_by`),
  ADD KEY `admission_fields_admission_form_id_foreign` (`admission_form_id`);

--
-- Indexes for table `admission_forms`
--
ALTER TABLE `admission_forms`
  ADD PRIMARY KEY (`admission_form_id`),
  ADD KEY `admission_forms_admin_id_foreign` (`admin_id`),
  ADD KEY `admission_forms_update_by_foreign` (`update_by`),
  ADD KEY `admission_forms_session_id_foreign` (`session_id`),
  ADD KEY `admission_forms_brochure_id_foreign` (`brochure_id`);

--
-- Indexes for table `api_tokens`
--
ALTER TABLE `api_tokens`
  ADD PRIMARY KEY (`token_id`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`app_setting_id`);

--
-- Indexes for table `assign_driver_conductor_routes`
--
ALTER TABLE `assign_driver_conductor_routes`
  ADD PRIMARY KEY (`assign_id`),
  ADD KEY `assign_driver_conductor_routes_admin_id_foreign` (`admin_id`),
  ADD KEY `assign_driver_conductor_routes_update_by_foreign` (`update_by`),
  ADD KEY `assign_driver_conductor_routes_vehicle_id_foreign` (`vehicle_id`),
  ADD KEY `assign_driver_conductor_routes_driver_id_foreign` (`driver_id`),
  ADD KEY `assign_driver_conductor_routes_conductor_id_foreign` (`conductor_id`),
  ADD KEY `assign_driver_conductor_routes_route_id_foreign` (`route_id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `books_admin_id_foreign` (`admin_id`),
  ADD KEY `books_update_by_foreign` (`update_by`),
  ADD KEY `books_book_category_id_foreign` (`book_category_id`),
  ADD KEY `books_book_vendor_id_foreign` (`book_vendor_id`),
  ADD KEY `books_book_cupboard_id_foreign` (`book_cupboard_id`),
  ADD KEY `books_book_cupboardshelf_id_foreign` (`book_cupboardshelf_id`);

--
-- Indexes for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD PRIMARY KEY (`book_allowance_id`),
  ADD KEY `book_allowance_admin_id_foreign` (`admin_id`),
  ADD KEY `book_allowance_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD PRIMARY KEY (`book_category_id`),
  ADD KEY `book_categories_admin_id_foreign` (`admin_id`),
  ADD KEY `book_categories_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD PRIMARY KEY (`book_info_id`),
  ADD KEY `book_copies_info_admin_id_foreign` (`admin_id`),
  ADD KEY `book_copies_info_update_by_foreign` (`update_by`),
  ADD KEY `book_copies_info_book_id_foreign` (`book_id`);

--
-- Indexes for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD PRIMARY KEY (`book_cupboard_id`),
  ADD KEY `book_cupboards_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboards_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD PRIMARY KEY (`book_cupboardshelf_id`),
  ADD KEY `book_cupboardshelfs_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboardshelfs_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD PRIMARY KEY (`book_vendor_id`),
  ADD KEY `book_vendors_admin_id_foreign` (`admin_id`),
  ADD KEY `book_vendors_update_by_foreign` (`update_by`);

--
-- Indexes for table `brochures`
--
ALTER TABLE `brochures`
  ADD PRIMARY KEY (`brochure_id`),
  ADD KEY `brochures_admin_id_foreign` (`admin_id`),
  ADD KEY `brochures_update_by_foreign` (`update_by`),
  ADD KEY `brochures_session_id_foreign` (`session_id`);

--
-- Indexes for table `caste`
--
ALTER TABLE `caste`
  ADD PRIMARY KEY (`caste_id`),
  ADD KEY `caste_admin_id_foreign` (`admin_id`),
  ADD KEY `caste_update_by_foreign` (`update_by`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`certificate_id`),
  ADD KEY `certificates_admin_id_foreign` (`admin_id`),
  ADD KEY `certificates_update_by_foreign` (`update_by`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `city_state_id_foreign` (`state_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `classes_admin_id_foreign` (`admin_id`),
  ADD KEY `classes_update_by_foreign` (`update_by`),
  ADD KEY `classes_board_id_foreign` (`board_id`),
  ADD KEY `classes_session_id_foreign` (`session_id`);

--
-- Indexes for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  ADD PRIMARY KEY (`class_subject_staff_map_id`),
  ADD KEY `class_subject_staff_map_admin_id_foreign` (`admin_id`),
  ADD KEY `class_subject_staff_map_update_by_foreign` (`update_by`),
  ADD KEY `class_subject_staff_map_class_id_foreign` (`class_id`),
  ADD KEY `class_subject_staff_map_section_id_foreign` (`section_id`),
  ADD KEY `class_subject_staff_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `communication_message`
--
ALTER TABLE `communication_message`
  ADD PRIMARY KEY (`comm_message_id`),
  ADD KEY `communication_message_staff_id_foreign` (`staff_id`),
  ADD KEY `communication_message_student_parent_id_foreign` (`student_parent_id`);

--
-- Indexes for table `competitions`
--
ALTER TABLE `competitions`
  ADD PRIMARY KEY (`competition_id`),
  ADD KEY `competitions_admin_id_foreign` (`admin_id`),
  ADD KEY `competitions_update_by_foreign` (`update_by`),
  ADD KEY `competitions_session_id_foreign` (`session_id`);

--
-- Indexes for table `competition_map`
--
ALTER TABLE `competition_map`
  ADD PRIMARY KEY (`student_competition_map_id`),
  ADD KEY `competition_map_admin_id_foreign` (`admin_id`),
  ADD KEY `competition_map_update_by_foreign` (`update_by`),
  ADD KEY `competition_map_competition_id_foreign` (`competition_id`),
  ADD KEY `competition_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `concession_map`
--
ALTER TABLE `concession_map`
  ADD PRIMARY KEY (`concession_map_id`),
  ADD KEY `concession_map_admin_id_foreign` (`admin_id`),
  ADD KEY `concession_map_update_by_foreign` (`update_by`),
  ADD KEY `concession_map_class_id_foreign` (`class_id`),
  ADD KEY `concession_map_session_id_foreign` (`session_id`),
  ADD KEY `concession_map_student_id_foreign` (`student_id`),
  ADD KEY `concession_map_reason_id_foreign` (`reason_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD PRIMARY KEY (`co_scholastic_type_id`),
  ADD KEY `co_scholastic_types_admin_id_foreign` (`admin_id`),
  ADD KEY `co_scholastic_types_update_by_foreign` (`update_by`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`),
  ADD KEY `designations_admin_id_foreign` (`admin_id`),
  ADD KEY `designations_update_by_foreign` (`update_by`);

--
-- Indexes for table `document_category`
--
ALTER TABLE `document_category`
  ADD PRIMARY KEY (`document_category_id`),
  ADD KEY `document_category_admin_id_foreign` (`admin_id`),
  ADD KEY `document_category_update_by_foreign` (`update_by`);

--
-- Indexes for table `esi_setting`
--
ALTER TABLE `esi_setting`
  ADD PRIMARY KEY (`esi_setting_id`),
  ADD KEY `esi_setting_admin_id_foreign` (`admin_id`),
  ADD KEY `esi_setting_update_by_foreign` (`update_by`),
  ADD KEY `esi_setting_session_id_foreign` (`session_id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`exam_id`),
  ADD KEY `exams_admin_id_foreign` (`admin_id`),
  ADD KEY `exams_update_by_foreign` (`update_by`),
  ADD KEY `exams_term_exam_id_foreign` (`term_exam_id`),
  ADD KEY `exams_session_id_foreign` (`session_id`);

--
-- Indexes for table `exam_map`
--
ALTER TABLE `exam_map`
  ADD PRIMARY KEY (`exam_map_id`),
  ADD KEY `exam_map_admin_id_foreign` (`admin_id`),
  ADD KEY `exam_map_update_by_foreign` (`update_by`),
  ADD KEY `exam_map_session_id_foreign` (`session_id`),
  ADD KEY `exam_map_class_id_foreign` (`class_id`),
  ADD KEY `exam_map_section_id_foreign` (`section_id`),
  ADD KEY `exam_map_subject_id_foreign` (`subject_id`),
  ADD KEY `exam_map_marks_criteria_id_foreign` (`marks_criteria_id`),
  ADD KEY `exam_map_grade_scheme_id_foreign` (`grade_scheme_id`);

--
-- Indexes for table `exam_marks`
--
ALTER TABLE `exam_marks`
  ADD PRIMARY KEY (`exam_mark_id`),
  ADD KEY `exam_marks_admin_id_foreign` (`admin_id`),
  ADD KEY `exam_marks_update_by_foreign` (`update_by`),
  ADD KEY `exam_marks_session_id_foreign` (`session_id`),
  ADD KEY `exam_marks_exam_schedule_id_foreign` (`exam_schedule_id`),
  ADD KEY `exam_marks_exam_id_foreign` (`exam_id`),
  ADD KEY `exam_marks_class_id_foreign` (`class_id`),
  ADD KEY `exam_marks_section_id_foreign` (`section_id`),
  ADD KEY `exam_marks_student_id_foreign` (`student_id`),
  ADD KEY `exam_marks_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `exam_schedules`
--
ALTER TABLE `exam_schedules`
  ADD PRIMARY KEY (`exam_schedule_id`),
  ADD KEY `exam_schedules_admin_id_foreign` (`admin_id`),
  ADD KEY `exam_schedules_update_by_foreign` (`update_by`),
  ADD KEY `exam_schedules_session_id_foreign` (`session_id`),
  ADD KEY `exam_schedules_exam_id_foreign` (`exam_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`facility_id`),
  ADD KEY `facilities_admin_id_foreign` (`admin_id`),
  ADD KEY `facilities_update_by_foreign` (`update_by`);

--
-- Indexes for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  ADD PRIMARY KEY (`fees_st_map_id`),
  ADD KEY `fees_st_map_admin_id_foreign` (`admin_id`),
  ADD KEY `fees_st_map_update_by_foreign` (`update_by`),
  ADD KEY `fees_st_map_class_id_foreign` (`class_id`),
  ADD KEY `fees_st_map_session_id_foreign` (`session_id`),
  ADD KEY `fees_st_map_section_id_foreign` (`section_id`);

--
-- Indexes for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  ADD PRIMARY KEY (`receipt_id`),
  ADD KEY `fee_receipt_admin_id_foreign` (`admin_id`),
  ADD KEY `fee_receipt_update_by_foreign` (`update_by`),
  ADD KEY `fee_receipt_session_id_foreign` (`session_id`),
  ADD KEY `fee_receipt_class_id_foreign` (`class_id`),
  ADD KEY `fee_receipt_student_id_foreign` (`student_id`),
  ADD KEY `fee_receipt_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  ADD PRIMARY KEY (`fee_receipt_detail_id`),
  ADD KEY `fee_receipt_details_admin_id_foreign` (`admin_id`),
  ADD KEY `fee_receipt_details_update_by_foreign` (`update_by`),
  ADD KEY `fee_receipt_details_receipt_id_foreign` (`receipt_id`),
  ADD KEY `fee_receipt_details_head_inst_id_foreign` (`head_inst_id`),
  ADD KEY `fee_receipt_details_r_concession_map_id_foreign` (`r_concession_map_id`),
  ADD KEY `fee_receipt_details_r_fine_id_foreign` (`r_fine_id`),
  ADD KEY `fee_receipt_details_r_fine_detail_id_foreign` (`r_fine_detail_id`),
  ADD KEY `fee_receipt_details_map_student_staff_id_foreign` (`map_student_staff_id`);

--
-- Indexes for table `fine`
--
ALTER TABLE `fine`
  ADD PRIMARY KEY (`fine_id`),
  ADD KEY `fine_admin_id_foreign` (`admin_id`),
  ADD KEY `fine_update_by_foreign` (`update_by`);

--
-- Indexes for table `fine_details`
--
ALTER TABLE `fine_details`
  ADD PRIMARY KEY (`fine_detail_id`),
  ADD KEY `fine_details_admin_id_foreign` (`admin_id`),
  ADD KEY `fine_details_update_by_foreign` (`update_by`),
  ADD KEY `fine_details_fine_id_foreign` (`fine_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`),
  ADD KEY `grades_admin_id_foreign` (`admin_id`),
  ADD KEY `grades_update_by_foreign` (`update_by`),
  ADD KEY `grades_grade_scheme_id_foreign` (`grade_scheme_id`);

--
-- Indexes for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  ADD PRIMARY KEY (`grade_scheme_id`),
  ADD KEY `grade_schemes_admin_id_foreign` (`admin_id`),
  ADD KEY `grade_schemes_update_by_foreign` (`update_by`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `groups_admin_id_foreign` (`admin_id`),
  ADD KEY `groups_update_by_foreign` (`update_by`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`holiday_id`),
  ADD KEY `holidays_admin_id_foreign` (`admin_id`),
  ADD KEY `holidays_update_by_foreign` (`update_by`),
  ADD KEY `holidays_session_id_foreign` (`session_id`);

--
-- Indexes for table `homework_conversations`
--
ALTER TABLE `homework_conversations`
  ADD PRIMARY KEY (`h_conversation_id`),
  ADD KEY `homework_conversations_admin_id_foreign` (`admin_id`),
  ADD KEY `homework_conversations_update_by_foreign` (`update_by`),
  ADD KEY `homework_conversations_homework_group_id_foreign` (`homework_group_id`),
  ADD KEY `homework_conversations_staff_id_foreign` (`staff_id`),
  ADD KEY `homework_conversations_subject_id_foreign` (`subject_id`),
  ADD KEY `homework_conversations_h_session_id_foreign` (`h_session_id`);

--
-- Indexes for table `homework_groups`
--
ALTER TABLE `homework_groups`
  ADD PRIMARY KEY (`homework_group_id`),
  ADD KEY `homework_groups_admin_id_foreign` (`admin_id`),
  ADD KEY `homework_groups_update_by_foreign` (`update_by`),
  ADD KEY `homework_groups_class_id_foreign` (`class_id`),
  ADD KEY `homework_groups_section_id_foreign` (`section_id`);

--
-- Indexes for table `hostels`
--
ALTER TABLE `hostels`
  ADD PRIMARY KEY (`hostel_id`),
  ADD KEY `hostels_admin_id_foreign` (`admin_id`),
  ADD KEY `hostels_update_by_foreign` (`update_by`);

--
-- Indexes for table `hostel_blocks`
--
ALTER TABLE `hostel_blocks`
  ADD PRIMARY KEY (`h_block_id`),
  ADD KEY `hostel_blocks_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_blocks_update_by_foreign` (`update_by`),
  ADD KEY `hostel_blocks_hostel_id_foreign` (`hostel_id`);

--
-- Indexes for table `hostel_fees`
--
ALTER TABLE `hostel_fees`
  ADD PRIMARY KEY (`hostel_fee_id`),
  ADD KEY `hostel_fees_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_fees_update_by_foreign` (`update_by`),
  ADD KEY `hostel_fees_h_fee_session_id_foreign` (`h_fee_session_id`),
  ADD KEY `hostel_fees_h_fee_class_id_foreign` (`h_fee_class_id`),
  ADD KEY `hostel_fees_h_fee_student_id_foreign` (`h_fee_student_id`),
  ADD KEY `hostel_fees_h_fee_student_map_id_foreign` (`h_fee_student_map_id`),
  ADD KEY `hostel_fees_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `hostel_fees_head`
--
ALTER TABLE `hostel_fees_head`
  ADD PRIMARY KEY (`h_fees_head_id`),
  ADD KEY `hostel_fees_head_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_fees_head_update_by_foreign` (`update_by`);

--
-- Indexes for table `hostel_fee_details`
--
ALTER TABLE `hostel_fee_details`
  ADD PRIMARY KEY (`hostel_fee_detail_id`),
  ADD KEY `hostel_fee_details_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_fee_details_update_by_foreign` (`update_by`),
  ADD KEY `hostel_fee_details_hostel_fee_id_foreign` (`hostel_fee_id`);

--
-- Indexes for table `hostel_floors`
--
ALTER TABLE `hostel_floors`
  ADD PRIMARY KEY (`h_floor_id`),
  ADD KEY `hostel_floors_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_floors_update_by_foreign` (`update_by`),
  ADD KEY `hostel_floors_h_block_id_foreign` (`h_block_id`);

--
-- Indexes for table `hostel_rooms`
--
ALTER TABLE `hostel_rooms`
  ADD PRIMARY KEY (`h_room_id`),
  ADD KEY `hostel_rooms_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_rooms_update_by_foreign` (`update_by`),
  ADD KEY `hostel_rooms_hostel_id_foreign` (`hostel_id`),
  ADD KEY `hostel_rooms_h_block_id_foreign` (`h_block_id`),
  ADD KEY `hostel_rooms_h_floor_id_foreign` (`h_floor_id`),
  ADD KEY `hostel_rooms_h_room_cate_id_foreign` (`h_room_cate_id`);

--
-- Indexes for table `hostel_room_cate`
--
ALTER TABLE `hostel_room_cate`
  ADD PRIMARY KEY (`h_room_cate_id`),
  ADD KEY `hostel_room_cate_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_room_cate_update_by_foreign` (`update_by`);

--
-- Indexes for table `hostel_student_map`
--
ALTER TABLE `hostel_student_map`
  ADD PRIMARY KEY (`h_student_map_id`),
  ADD KEY `hostel_student_map_admin_id_foreign` (`admin_id`),
  ADD KEY `hostel_student_map_h_block_id_foreign` (`h_block_id`),
  ADD KEY `hostel_student_map_h_floor_id_foreign` (`h_floor_id`),
  ADD KEY `hostel_student_map_h_room_cate_id_foreign` (`h_room_cate_id`),
  ADD KEY `hostel_student_map_h_room_id_foreign` (`h_room_id`),
  ADD KEY `hostel_student_map_hostel_id_foreign` (`hostel_id`),
  ADD KEY `hostel_student_map_student_id_foreign` (`student_id`),
  ADD KEY `hostel_student_map_update_by_foreign` (`update_by`);

--
-- Indexes for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  ADD PRIMARY KEY (`imprest_ac_id`),
  ADD KEY `imprest_ac_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_update_by_foreign` (`update_by`),
  ADD KEY `imprest_ac_session_id_foreign` (`session_id`),
  ADD KEY `imprest_ac_class_id_foreign` (`class_id`),
  ADD KEY `imprest_ac_student_id_foreign` (`student_id`);

--
-- Indexes for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  ADD PRIMARY KEY (`imprest_ac_confi_id`),
  ADD KEY `imprest_ac_confi_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_confi_update_by_foreign` (`update_by`);

--
-- Indexes for table `imprest_ac_entries`
--
ALTER TABLE `imprest_ac_entries`
  ADD PRIMARY KEY (`ac_entry_id`),
  ADD KEY `imprest_ac_entries_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_entries_update_by_foreign` (`update_by`),
  ADD KEY `imprest_ac_entries_session_id_foreign` (`session_id`),
  ADD KEY `imprest_ac_entries_class_id_foreign` (`class_id`),
  ADD KEY `imprest_ac_entries_section_id_foreign` (`section_id`);

--
-- Indexes for table `invoice_config`
--
ALTER TABLE `invoice_config`
  ADD PRIMARY KEY (`invoice_config_id`),
  ADD KEY `invoice_config_admin_id_foreign` (`admin_id`),
  ADD KEY `invoice_config_update_by_foreign` (`update_by`);

--
-- Indexes for table `inv_category`
--
ALTER TABLE `inv_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `inv_category_admin_id_foreign` (`admin_id`),
  ADD KEY `inv_category_update_by_foreign` (`update_by`);

--
-- Indexes for table `inv_items`
--
ALTER TABLE `inv_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `inv_items_category_id_foreign` (`category_id`),
  ADD KEY `inv_items_sub_category_id_foreign` (`sub_category_id`),
  ADD KEY `inv_items_unit_id_foreign` (`unit_id`),
  ADD KEY `inv_items_admin_id_foreign` (`admin_id`),
  ADD KEY `inv_items_update_by_foreign` (`update_by`);

--
-- Indexes for table `inv_unit`
--
ALTER TABLE `inv_unit`
  ADD PRIMARY KEY (`unit_id`),
  ADD KEY `inv_unit_admin_id_foreign` (`admin_id`),
  ADD KEY `inv_unit_update_by_foreign` (`update_by`);

--
-- Indexes for table `inv_vendor`
--
ALTER TABLE `inv_vendor`
  ADD PRIMARY KEY (`vendor_id`),
  ADD KEY `inv_vendor_admin_id_foreign` (`admin_id`),
  ADD KEY `inv_vendor_update_by_foreign` (`update_by`);

--
-- Indexes for table `issued_books`
--
ALTER TABLE `issued_books`
  ADD PRIMARY KEY (`issued_book_id`),
  ADD KEY `issued_books_admin_id_foreign` (`admin_id`),
  ADD KEY `issued_books_update_by_foreign` (`update_by`),
  ADD KEY `issued_books_book_id_foreign` (`book_id`),
  ADD KEY `issued_books_book_copies_id_foreign` (`book_copies_id`);

--
-- Indexes for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  ADD PRIMARY KEY (`history_id`),
  ADD KEY `issued_books_history_admin_id_foreign` (`admin_id`),
  ADD KEY `issued_books_history_update_by_foreign` (`update_by`),
  ADD KEY `issued_books_history_book_id_foreign` (`book_id`),
  ADD KEY `issued_books_history_book_copies_id_foreign` (`book_copies_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`job_id`),
  ADD KEY `job_admin_id_foreign` (`admin_id`),
  ADD KEY `job_update_by_foreign` (`update_by`),
  ADD KEY `job_job_medium_type_foreign` (`job_medium_type`);

--
-- Indexes for table `job_applied_cadidates`
--
ALTER TABLE `job_applied_cadidates`
  ADD PRIMARY KEY (`applied_cadidate_id`),
  ADD KEY `job_applied_cadidates_job_id_foreign` (`job_id`),
  ADD KEY `job_applied_cadidates_job_form_id_foreign` (`job_form_id`),
  ADD KEY `job_applied_cadidates_designation_id_foreign` (`designation_id`),
  ADD KEY `job_applied_cadidates_title_id_foreign` (`title_id`),
  ADD KEY `job_applied_cadidates_caste_id_foreign` (`caste_id`),
  ADD KEY `job_applied_cadidates_religion_id_foreign` (`religion_id`),
  ADD KEY `job_applied_cadidates_nationality_id_foreign` (`nationality_id`),
  ADD KEY `job_applied_cadidates_staff_temporary_county_foreign` (`staff_temporary_county`),
  ADD KEY `job_applied_cadidates_staff_temporary_state_foreign` (`staff_temporary_state`),
  ADD KEY `job_applied_cadidates_staff_temporary_city_foreign` (`staff_temporary_city`),
  ADD KEY `job_applied_cadidates_staff_permanent_county_foreign` (`staff_permanent_county`),
  ADD KEY `job_applied_cadidates_staff_permanent_state_foreign` (`staff_permanent_state`),
  ADD KEY `job_applied_cadidates_staff_permanent_city_foreign` (`staff_permanent_city`);

--
-- Indexes for table `job_applied_cadidate_status`
--
ALTER TABLE `job_applied_cadidate_status`
  ADD PRIMARY KEY (`applied_cadidate_status_id`),
  ADD KEY `job_applied_cadidate_status_admin_id_foreign` (`admin_id`),
  ADD KEY `job_applied_cadidate_status_update_by_foreign` (`update_by`),
  ADD KEY `job_applied_cadidate_status_applied_cadidate_id_foreign` (`applied_cadidate_id`),
  ADD KEY `job_applied_cadidate_status_job_id_foreign` (`job_id`),
  ADD KEY `job_applied_cadidate_status_job_form_id_foreign` (`job_form_id`);

--
-- Indexes for table `job_forms`
--
ALTER TABLE `job_forms`
  ADD PRIMARY KEY (`job_form_id`),
  ADD KEY `job_forms_admin_id_foreign` (`admin_id`),
  ADD KEY `job_forms_update_by_foreign` (`update_by`),
  ADD KEY `job_forms_session_id_foreign` (`session_id`),
  ADD KEY `job_forms_job_id_foreign` (`job_id`);

--
-- Indexes for table `job_form_fields`
--
ALTER TABLE `job_form_fields`
  ADD PRIMARY KEY (`job_form_field_id`),
  ADD KEY `job_form_fields_admin_id_foreign` (`admin_id`),
  ADD KEY `job_form_fields_update_by_foreign` (`update_by`),
  ADD KEY `job_form_fields_job_form_id_foreign` (`job_form_id`);

--
-- Indexes for table `library_fine`
--
ALTER TABLE `library_fine`
  ADD PRIMARY KEY (`library_fine_id`),
  ADD KEY `library_fine_admin_id_foreign` (`admin_id`),
  ADD KEY `library_fine_update_by_foreign` (`update_by`),
  ADD KEY `library_fine_member_id_foreign` (`member_id`);

--
-- Indexes for table `library_members`
--
ALTER TABLE `library_members`
  ADD PRIMARY KEY (`library_member_id`),
  ADD KEY `library_members_admin_id_foreign` (`admin_id`),
  ADD KEY `library_members_update_by_foreign` (`update_by`);

--
-- Indexes for table `map_student_staff_vehicle`
--
ALTER TABLE `map_student_staff_vehicle`
  ADD PRIMARY KEY (`map_student_staff_id`),
  ADD KEY `map_student_staff_vehicle_admin_id_foreign` (`admin_id`),
  ADD KEY `map_student_staff_vehicle_update_by_foreign` (`update_by`),
  ADD KEY `map_student_staff_vehicle_vehicle_id_foreign` (`vehicle_id`),
  ADD KEY `map_student_staff_vehicle_route_id_foreign` (`route_id`),
  ADD KEY `map_student_staff_vehicle_route_location_id_foreign` (`route_location_id`),
  ADD KEY `map_student_staff_vehicle_session_id_foreign` (`session_id`);

--
-- Indexes for table `marksheet_templates`
--
ALTER TABLE `marksheet_templates`
  ADD PRIMARY KEY (`marksheet_template_id`),
  ADD KEY `marksheet_templates_admin_id_foreign` (`admin_id`),
  ADD KEY `marksheet_templates_update_by_foreign` (`update_by`);

--
-- Indexes for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  ADD PRIMARY KEY (`marks_criteria_id`),
  ADD KEY `marks_criteria_admin_id_foreign` (`admin_id`),
  ADD KEY `marks_criteria_update_by_foreign` (`update_by`);

--
-- Indexes for table `master_modules`
--
ALTER TABLE `master_modules`
  ADD PRIMARY KEY (`master_module_id`);

--
-- Indexes for table `medium_type`
--
ALTER TABLE `medium_type`
  ADD PRIMARY KEY (`medium_type_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_permissions`
--
ALTER TABLE `module_permissions`
  ADD PRIMARY KEY (`module_permission_id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`nationality_id`),
  ADD KEY `nationality_admin_id_foreign` (`admin_id`),
  ADD KEY `nationality_update_by_foreign` (`update_by`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`),
  ADD KEY `notice_admin_id_foreign` (`admin_id`),
  ADD KEY `notice_update_by_foreign` (`update_by`),
  ADD KEY `notice_session_id_foreign` (`session_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `notifications_admin_id_foreign` (`admin_id`),
  ADD KEY `notifications_update_by_foreign` (`update_by`),
  ADD KEY `notifications_session_id_foreign` (`session_id`),
  ADD KEY `notifications_section_id_foreign` (`section_id`),
  ADD KEY `notifications_student_admin_id_foreign` (`student_admin_id`),
  ADD KEY `notifications_parent_admin_id_foreign` (`parent_admin_id`),
  ADD KEY `notifications_staff_admin_id_foreign` (`staff_admin_id`),
  ADD KEY `notifications_school_admin_id_foreign` (`school_admin_id`),
  ADD KEY `notifications_class_id_foreign` (`class_id`);

--
-- Indexes for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  ADD PRIMARY KEY (`ot_head_id`),
  ADD KEY `one_time_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `one_time_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `online_notes`
--
ALTER TABLE `online_notes`
  ADD PRIMARY KEY (`online_note_id`),
  ADD KEY `online_notes_admin_id_foreign` (`admin_id`),
  ADD KEY `online_notes_update_by_foreign` (`update_by`),
  ADD KEY `online_notes_class_id_foreign` (`class_id`),
  ADD KEY `online_notes_section_id_foreign` (`section_id`),
  ADD KEY `online_notes_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `payroll_mapping`
--
ALTER TABLE `payroll_mapping`
  ADD PRIMARY KEY (`payroll_mapping_id`),
  ADD KEY `payroll_mapping_admin_id_foreign` (`admin_id`),
  ADD KEY `payroll_mapping_update_by_foreign` (`update_by`),
  ADD KEY `payroll_mapping_session_id_foreign` (`session_id`),
  ADD KEY `payroll_mapping_pay_dept_id_foreign` (`pay_dept_id`),
  ADD KEY `payroll_mapping_pay_grade_id_foreign` (`pay_grade_id`),
  ADD KEY `payroll_mapping_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `pay_advance`
--
ALTER TABLE `pay_advance`
  ADD PRIMARY KEY (`pay_advance_id`),
  ADD KEY `pay_advance_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_advance_update_by_foreign` (`update_by`),
  ADD KEY `pay_advance_session_id_foreign` (`session_id`),
  ADD KEY `pay_advance_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `pay_arrear`
--
ALTER TABLE `pay_arrear`
  ADD PRIMARY KEY (`pay_arrear_id`),
  ADD KEY `pay_arrear_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_arrear_update_by_foreign` (`update_by`);

--
-- Indexes for table `pay_arrear_map`
--
ALTER TABLE `pay_arrear_map`
  ADD PRIMARY KEY (`pay_arrear_map_id`),
  ADD KEY `pay_arrear_map_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_arrear_map_update_by_foreign` (`update_by`),
  ADD KEY `pay_arrear_map_session_id_foreign` (`session_id`),
  ADD KEY `pay_arrear_map_pay_arrear_id_foreign` (`pay_arrear_id`),
  ADD KEY `pay_arrear_map_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `pay_bonus`
--
ALTER TABLE `pay_bonus`
  ADD PRIMARY KEY (`pay_bonus_id`),
  ADD KEY `pay_bonus_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_bonus_update_by_foreign` (`update_by`);

--
-- Indexes for table `pay_bonus_map`
--
ALTER TABLE `pay_bonus_map`
  ADD PRIMARY KEY (`pay_bonus_map_id`),
  ADD KEY `pay_bonus_map_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_bonus_map_update_by_foreign` (`update_by`),
  ADD KEY `pay_bonus_map_session_id_foreign` (`session_id`),
  ADD KEY `pay_bonus_map_pay_bonus_id_foreign` (`pay_bonus_id`),
  ADD KEY `pay_bonus_map_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `pay_config`
--
ALTER TABLE `pay_config`
  ADD PRIMARY KEY (`pay_config_id`),
  ADD KEY `pay_config_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_config_update_by_foreign` (`update_by`);

--
-- Indexes for table `pay_dept`
--
ALTER TABLE `pay_dept`
  ADD PRIMARY KEY (`pay_dept_id`),
  ADD KEY `pay_dept_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_dept_update_by_foreign` (`update_by`);

--
-- Indexes for table `pay_dept_map`
--
ALTER TABLE `pay_dept_map`
  ADD PRIMARY KEY (`pay_dept_map_id`),
  ADD KEY `pay_dept_map_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_dept_map_update_by_foreign` (`update_by`),
  ADD KEY `pay_dept_map_session_id_foreign` (`session_id`),
  ADD KEY `pay_dept_map_pay_dept_id_foreign` (`pay_dept_id`),
  ADD KEY `pay_dept_map_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `pay_grade`
--
ALTER TABLE `pay_grade`
  ADD PRIMARY KEY (`pay_grade_id`),
  ADD KEY `pay_grade_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_grade_update_by_foreign` (`update_by`);

--
-- Indexes for table `pay_leave_scheme`
--
ALTER TABLE `pay_leave_scheme`
  ADD PRIMARY KEY (`pay_leave_scheme_id`),
  ADD KEY `pay_leave_scheme_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_leave_scheme_update_by_foreign` (`update_by`);

--
-- Indexes for table `pay_leave_scheme_map`
--
ALTER TABLE `pay_leave_scheme_map`
  ADD PRIMARY KEY (`pay_leave_scheme_map_id`),
  ADD KEY `pay_leave_scheme_map_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_leave_scheme_map_update_by_foreign` (`update_by`),
  ADD KEY `pay_leave_scheme_map_session_id_foreign` (`session_id`),
  ADD KEY `pay_leave_scheme_map_pay_leave_scheme_id_foreign` (`pay_leave_scheme_id`),
  ADD KEY `pay_leave_scheme_map_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `pay_loan`
--
ALTER TABLE `pay_loan`
  ADD PRIMARY KEY (`pay_loan_id`),
  ADD KEY `pay_loan_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_loan_update_by_foreign` (`update_by`),
  ADD KEY `pay_loan_session_id_foreign` (`session_id`),
  ADD KEY `pay_loan_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `pay_salary_head`
--
ALTER TABLE `pay_salary_head`
  ADD PRIMARY KEY (`pay_salary_head_id`),
  ADD KEY `pay_salary_head_admin_id_foreign` (`admin_id`),
  ADD KEY `pay_salary_head_update_by_foreign` (`update_by`);

--
-- Indexes for table `pf_setting`
--
ALTER TABLE `pf_setting`
  ADD PRIMARY KEY (`pf_setting_id`),
  ADD KEY `pf_setting_admin_id_foreign` (`admin_id`),
  ADD KEY `pf_setting_update_by_foreign` (`update_by`),
  ADD KEY `pf_setting_session_id_foreign` (`session_id`);

--
-- Indexes for table `plan_schedule`
--
ALTER TABLE `plan_schedule`
  ADD PRIMARY KEY (`plan_schedule_id`),
  ADD KEY `plan_schedule_admin_id_foreign` (`admin_id`),
  ADD KEY `plan_schedule_update_by_foreign` (`update_by`),
  ADD KEY `plan_schedule_staff_id_foreign` (`staff_id`),
  ADD KEY `plan_schedule_current_session_id_foreign` (`current_session_id`);

--
-- Indexes for table `pt_setting`
--
ALTER TABLE `pt_setting`
  ADD PRIMARY KEY (`pt_setting_id`),
  ADD KEY `pt_setting_admin_id_foreign` (`admin_id`),
  ADD KEY `pt_setting_update_by_foreign` (`update_by`),
  ADD KEY `pt_setting_session_id_foreign` (`session_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `purchases_admin_id_foreign` (`admin_id`),
  ADD KEY `purchases_update_by_foreign` (`update_by`),
  ADD KEY `purchases_vendor_id_foreign` (`vendor_id`),
  ADD KEY `purchases_category_id_foreign` (`category_id`),
  ADD KEY `purchases_subcategory_id_foreign` (`subcategory_id`),
  ADD KEY `purchases_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `purchase_items`
--
ALTER TABLE `purchase_items`
  ADD PRIMARY KEY (`purchase_items_id`),
  ADD KEY `purchase_items_admin_id_foreign` (`admin_id`),
  ADD KEY `purchase_items_update_by_foreign` (`update_by`),
  ADD KEY `purchase_items_purchase_id_foreign` (`purchase_id`),
  ADD KEY `purchase_items_item_id_foreign` (`item_id`),
  ADD KEY `purchase_items_unit_id_foreign` (`unit_id`),
  ADD KEY `purchase_items_category_id_foreign` (`category_id`),
  ADD KEY `purchase_items_sub_category_id_foreign` (`sub_category_id`);

--
-- Indexes for table `question_papers`
--
ALTER TABLE `question_papers`
  ADD PRIMARY KEY (`question_paper_id`),
  ADD KEY `question_papers_admin_id_foreign` (`admin_id`),
  ADD KEY `question_papers_update_by_foreign` (`update_by`),
  ADD KEY `question_papers_class_id_foreign` (`class_id`),
  ADD KEY `question_papers_section_id_foreign` (`section_id`),
  ADD KEY `question_papers_subject_id_foreign` (`subject_id`),
  ADD KEY `question_papers_exam_id_foreign` (`exam_id`),
  ADD KEY `question_papers_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `reasons`
--
ALTER TABLE `reasons`
  ADD PRIMARY KEY (`reason_id`),
  ADD KEY `reasons_admin_id_foreign` (`admin_id`),
  ADD KEY `reasons_update_by_foreign` (`update_by`);

--
-- Indexes for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  ADD PRIMARY KEY (`rc_head_id`),
  ADD KEY `recurring_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `recurring_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  ADD PRIMARY KEY (`rc_inst_id`),
  ADD KEY `recurring_inst_admin_id_foreign` (`admin_id`),
  ADD KEY `recurring_inst_update_by_foreign` (`update_by`),
  ADD KEY `recurring_inst_rc_head_id_foreign` (`rc_head_id`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`religion_id`),
  ADD KEY `religions_admin_id_foreign` (`admin_id`),
  ADD KEY `religions_update_by_foreign` (`update_by`);

--
-- Indexes for table `remarks`
--
ALTER TABLE `remarks`
  ADD PRIMARY KEY (`remark_id`),
  ADD KEY `remarks_admin_id_foreign` (`admin_id`),
  ADD KEY `remarks_update_by_foreign` (`update_by`),
  ADD KEY `remarks_session_id_foreign` (`session_id`),
  ADD KEY `remarks_class_id_foreign` (`class_id`),
  ADD KEY `remarks_section_id_foreign` (`section_id`),
  ADD KEY `remarks_student_id_foreign` (`student_id`),
  ADD KEY `remarks_staff_id_foreign` (`staff_id`),
  ADD KEY `remarks_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `room_no`
--
ALTER TABLE `room_no`
  ADD PRIMARY KEY (`room_no_id`),
  ADD KEY `room_no_admin_id_foreign` (`admin_id`),
  ADD KEY `room_no_update_by_foreign` (`update_by`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`route_id`),
  ADD KEY `routes_admin_id_foreign` (`admin_id`),
  ADD KEY `routes_update_by_foreign` (`update_by`);

--
-- Indexes for table `route_locations`
--
ALTER TABLE `route_locations`
  ADD PRIMARY KEY (`route_location_id`),
  ADD KEY `route_locations_admin_id_foreign` (`admin_id`),
  ADD KEY `route_locations_update_by_foreign` (`update_by`),
  ADD KEY `route_locations_route_id_foreign` (`route_id`);

--
-- Indexes for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  ADD PRIMARY KEY (`rte_cheque_id`),
  ADD KEY `rte_cheques_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_cheques_update_by_foreign` (`update_by`),
  ADD KEY `rte_cheques_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `rte_heads`
--
ALTER TABLE `rte_heads`
  ADD PRIMARY KEY (`rte_head_id`),
  ADD KEY `rte_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  ADD PRIMARY KEY (`rte_head_map_id`),
  ADD KEY `rte_head_map_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_head_map_update_by_foreign` (`update_by`),
  ADD KEY `rte_head_map_rte_head_id_foreign` (`rte_head_id`),
  ADD KEY `rte_head_map_session_id_foreign` (`session_id`),
  ADD KEY `rte_head_map_class_id_foreign` (`class_id`),
  ADD KEY `rte_head_map_section_id_foreign` (`section_id`),
  ADD KEY `rte_head_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `salary_structure`
--
ALTER TABLE `salary_structure`
  ADD PRIMARY KEY (`salary_structure_id`),
  ADD KEY `salary_structure_admin_id_foreign` (`admin_id`),
  ADD KEY `salary_structure_update_by_foreign` (`update_by`);

--
-- Indexes for table `salary_structure_map`
--
ALTER TABLE `salary_structure_map`
  ADD PRIMARY KEY (`salary_structure_map_id`),
  ADD KEY `salary_structure_map_admin_id_foreign` (`admin_id`),
  ADD KEY `salary_structure_map_update_by_foreign` (`update_by`),
  ADD KEY `salary_structure_map_salary_structure_id_foreign` (`salary_structure_id`),
  ADD KEY `salary_structure_map_pay_salary_head_id_foreign` (`pay_salary_head_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD KEY `sales_admin_id_foreign` (`admin_id`),
  ADD KEY `sales_update_by_foreign` (`update_by`),
  ADD KEY `sales_class_id_foreign` (`class_id`),
  ADD KEY `sales_student_id_foreign` (`student_id`),
  ADD KEY `sales_role_id_foreign` (`role_id`),
  ADD KEY `sales_staff_id_foreign` (`staff_id`),
  ADD KEY `sales_bank_id_foreign` (`bank_id`),
  ADD KEY `sales_section_id_foreign` (`section_id`);

--
-- Indexes for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD PRIMARY KEY (`sale_items_id`),
  ADD KEY `sale_items_admin_id_foreign` (`admin_id`),
  ADD KEY `sale_items_category_id_foreign` (`category_id`),
  ADD KEY `sale_items_sub_category_id_foreign` (`sub_category_id`),
  ADD KEY `sale_items_update_by_foreign` (`update_by`),
  ADD KEY `sale_items_sale_id_foreign` (`sale_id`),
  ADD KEY `sale_items_item_id_foreign` (`item_id`),
  ADD KEY `sale_items_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `sal_struct_map_staff`
--
ALTER TABLE `sal_struct_map_staff`
  ADD PRIMARY KEY (`sal_struct_map_staff_id`),
  ADD KEY `sal_struct_map_staff_admin_id_foreign` (`admin_id`),
  ADD KEY `sal_struct_map_staff_update_by_foreign` (`update_by`),
  ADD KEY `sal_struct_map_staff_session_id_foreign` (`session_id`),
  ADD KEY `sal_struct_map_staff_staff_id_foreign` (`staff_id`),
  ADD KEY `sal_struct_map_staff_salary_structure_id_foreign` (`salary_structure_id`);

--
-- Indexes for table `schedule_map`
--
ALTER TABLE `schedule_map`
  ADD PRIMARY KEY (`schedule_map_id`),
  ADD KEY `schedule_map_admin_id_foreign` (`admin_id`),
  ADD KEY `schedule_map_update_by_foreign` (`update_by`),
  ADD KEY `schedule_map_session_id_foreign` (`session_id`),
  ADD KEY `schedule_map_exam_schedule_id_foreign` (`exam_schedule_id`),
  ADD KEY `schedule_map_exam_id_foreign` (`exam_id`),
  ADD KEY `schedule_map_class_id_foreign` (`class_id`),
  ADD KEY `schedule_map_section_id_foreign` (`section_id`),
  ADD KEY `schedule_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `schedule_room_map`
--
ALTER TABLE `schedule_room_map`
  ADD PRIMARY KEY (`schedule_room_map_id`),
  ADD KEY `schedule_room_map_admin_id_foreign` (`admin_id`),
  ADD KEY `schedule_room_map_update_by_foreign` (`update_by`),
  ADD KEY `schedule_room_map_session_id_foreign` (`session_id`),
  ADD KEY `schedule_room_map_exam_schedule_id_foreign` (`exam_schedule_id`),
  ADD KEY `schedule_room_map_exam_id_foreign` (`exam_id`),
  ADD KEY `schedule_room_map_class_id_foreign` (`class_id`),
  ADD KEY `schedule_room_map_section_id_foreign` (`section_id`),
  ADD KEY `schedule_room_map_room_no_id_foreign` (`room_no_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`),
  ADD KEY `school_admin_id_foreign` (`admin_id`),
  ADD KEY `school_update_by_foreign` (`update_by`),
  ADD KEY `school_country_id_foreign` (`country_id`),
  ADD KEY `school_state_id_foreign` (`state_id`),
  ADD KEY `school_city_id_foreign` (`city_id`);

--
-- Indexes for table `school_boards`
--
ALTER TABLE `school_boards`
  ADD PRIMARY KEY (`board_id`),
  ADD KEY `school_boards_admin_id_foreign` (`admin_id`),
  ADD KEY `school_boards_update_by_foreign` (`update_by`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `sections_admin_id_foreign` (`admin_id`),
  ADD KEY `sections_update_by_foreign` (`update_by`),
  ADD KEY `sections_class_id_foreign` (`class_id`),
  ADD KEY `sections_shift_id_foreign` (`shift_id`),
  ADD KEY `sections_stream_id_foreign` (`stream_id`),
  ADD KEY `sections_session_id_foreign` (`session_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `sessions_admin_id_foreign` (`admin_id`),
  ADD KEY `sessions_update_by_foreign` (`update_by`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`shift_id`),
  ADD KEY `shifts_admin_id_foreign` (`admin_id`),
  ADD KEY `shifts_update_by_foreign` (`update_by`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD KEY `staff_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_update_by_foreign` (`update_by`),
  ADD KEY `staff_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `staff_designation_id_foreign` (`designation_id`),
  ADD KEY `staff_title_id_foreign` (`title_id`),
  ADD KEY `staff_caste_id_foreign` (`caste_id`),
  ADD KEY `staff_religion_id_foreign` (`religion_id`),
  ADD KEY `staff_nationality_id_foreign` (`nationality_id`),
  ADD KEY `staff_staff_temporary_county_foreign` (`staff_temporary_county`),
  ADD KEY `staff_staff_temporary_state_foreign` (`staff_temporary_state`),
  ADD KEY `staff_staff_temporary_city_foreign` (`staff_temporary_city`),
  ADD KEY `staff_staff_permanent_county_foreign` (`staff_permanent_county`),
  ADD KEY `staff_staff_permanent_state_foreign` (`staff_permanent_state`),
  ADD KEY `staff_staff_permanent_city_foreign` (`staff_permanent_city`);

--
-- Indexes for table `staff_attendance`
--
ALTER TABLE `staff_attendance`
  ADD PRIMARY KEY (`staff_attendance_id`),
  ADD KEY `staff_attendance_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_attendance_update_by_foreign` (`update_by`),
  ADD KEY `staff_attendance_session_id_foreign` (`session_id`);

--
-- Indexes for table `staff_attend_details`
--
ALTER TABLE `staff_attend_details`
  ADD PRIMARY KEY (`staff_attend_d_id`),
  ADD KEY `staff_attend_details_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_attend_details_update_by_foreign` (`update_by`),
  ADD KEY `staff_attend_details_session_id_foreign` (`session_id`),
  ADD KEY `staff_attend_details_staff_attendance_id_foreign` (`staff_attendance_id`),
  ADD KEY `staff_attend_details_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  ADD PRIMARY KEY (`staff_class_allocation_id`),
  ADD KEY `staff_class_allocations_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_class_allocations_update_by_foreign` (`update_by`),
  ADD KEY `staff_class_allocations_class_id_foreign` (`class_id`),
  ADD KEY `staff_class_allocations_section_id_foreign` (`section_id`),
  ADD KEY `staff_class_allocations_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_documents`
--
ALTER TABLE `staff_documents`
  ADD PRIMARY KEY (`staff_document_id`),
  ADD KEY `staff_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_documents_update_by_foreign` (`update_by`),
  ADD KEY `staff_documents_staff_id_foreign` (`staff_id`),
  ADD KEY `staff_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD PRIMARY KEY (`staff_leave_id`),
  ADD KEY `staff_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_leaves_update_by_foreign` (`update_by`),
  ADD KEY `staff_leaves_staff_id_foreign` (`staff_id`),
  ADD KEY `staff_leaves_pay_leave_scheme_id_foreign` (`pay_leave_scheme_id`),
  ADD KEY `staff_leaves_session_id_foreign` (`session_id`);

--
-- Indexes for table `staff_leave_history_info`
--
ALTER TABLE `staff_leave_history_info`
  ADD PRIMARY KEY (`staff_leave_history_info_id`),
  ADD KEY `staff_leave_history_info_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_leave_history_info_update_by_foreign` (`update_by`),
  ADD KEY `staff_leave_history_info_staff_id_foreign` (`staff_id`),
  ADD KEY `staff_leave_history_info_pay_leave_scheme_id_foreign` (`pay_leave_scheme_id`),
  ADD KEY `staff_leave_history_info_staff_leave_id_foreign` (`staff_leave_id`),
  ADD KEY `staff_leave_history_info_session_id_foreign` (`session_id`);

--
-- Indexes for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD PRIMARY KEY (`staff_role_id`),
  ADD KEY `staff_roles_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_roles_update_by_foreign` (`update_by`);

--
-- Indexes for table `staff_substitute`
--
ALTER TABLE `staff_substitute`
  ADD PRIMARY KEY (`substitute_id`),
  ADD KEY `staff_substitute_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_substitute_update_by_foreign` (`update_by`),
  ADD KEY `staff_substitute_session_id_foreign` (`session_id`),
  ADD KEY `staff_substitute_leave_staff_id_foreign` (`leave_staff_id`),
  ADD KEY `staff_substitute_ref_leave_id_foreign` (`ref_leave_id`),
  ADD KEY `staff_substitute_class_id_foreign` (`class_id`),
  ADD KEY `staff_substitute_section_id_foreign` (`section_id`),
  ADD KEY `staff_substitute_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`),
  ADD KEY `state_country_id_foreign` (`country_id`);

--
-- Indexes for table `streams`
--
ALTER TABLE `streams`
  ADD PRIMARY KEY (`stream_id`),
  ADD KEY `streams_admin_id_foreign` (`admin_id`),
  ADD KEY `streams_update_by_foreign` (`update_by`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `students_admin_id_foreign` (`admin_id`),
  ADD KEY `students_update_by_foreign` (`update_by`),
  ADD KEY `students_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `students_student_parent_id_foreign` (`student_parent_id`),
  ADD KEY `students_student_sibling_class_id_foreign` (`student_sibling_class_id`),
  ADD KEY `students_caste_id_foreign` (`caste_id`),
  ADD KEY `students_title_id_foreign` (`title_id`),
  ADD KEY `students_religion_id_foreign` (`religion_id`),
  ADD KEY `students_nationality_id_foreign` (`nationality_id`),
  ADD KEY `students_student_permanent_county_foreign` (`student_permanent_county`),
  ADD KEY `students_student_permanent_state_foreign` (`student_permanent_state`),
  ADD KEY `students_student_permanent_city_foreign` (`student_permanent_city`),
  ADD KEY `students_student_temporary_county_foreign` (`student_temporary_county`),
  ADD KEY `students_student_temporary_state_foreign` (`student_temporary_state`),
  ADD KEY `students_student_temporary_city_foreign` (`student_temporary_city`);

--
-- Indexes for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  ADD PRIMARY KEY (`student_academic_history_info_id`),
  ADD KEY `student_academic_history_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_history_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_history_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_history_info_student_academic_info_id_foreign` (`student_academic_info_id`),
  ADD KEY `student_academic_history_info_session_id_foreign` (`session_id`),
  ADD KEY `student_academic_history_info_class_id_foreign` (`class_id`),
  ADD KEY `student_academic_history_info_stream_id_foreign` (`stream_id`),
  ADD KEY `student_academic_history_info_section_id_foreign` (`section_id`);

--
-- Indexes for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  ADD PRIMARY KEY (`student_academic_info_id`),
  ADD KEY `student_academic_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_info_admission_class_id_foreign` (`admission_class_id`),
  ADD KEY `student_academic_info_admission_section_id_foreign` (`admission_section_id`),
  ADD KEY `student_academic_info_admission_session_id_foreign` (`admission_session_id`),
  ADD KEY `student_academic_info_current_session_id_foreign` (`current_session_id`),
  ADD KEY `student_academic_info_current_class_id_foreign` (`current_class_id`),
  ADD KEY `student_academic_info_current_section_id_foreign` (`current_section_id`),
  ADD KEY `student_academic_info_group_id_foreign` (`group_id`),
  ADD KEY `student_academic_info_stream_id_foreign` (`stream_id`);

--
-- Indexes for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD PRIMARY KEY (`student_attendance_id`),
  ADD KEY `student_attendance_admin_id_foreign` (`admin_id`),
  ADD KEY `student_attendance_update_by_foreign` (`update_by`),
  ADD KEY `student_attendance_session_id_foreign` (`session_id`),
  ADD KEY `student_attendance_class_id_foreign` (`class_id`),
  ADD KEY `student_attendance_section_id_foreign` (`section_id`);

--
-- Indexes for table `student_attend_details`
--
ALTER TABLE `student_attend_details`
  ADD PRIMARY KEY (`student_attend_d_id`),
  ADD KEY `student_attend_details_admin_id_foreign` (`admin_id`),
  ADD KEY `student_attend_details_update_by_foreign` (`update_by`),
  ADD KEY `student_attend_details_session_id_foreign` (`session_id`),
  ADD KEY `student_attend_details_class_id_foreign` (`class_id`),
  ADD KEY `student_attend_details_section_id_foreign` (`section_id`),
  ADD KEY `student_attend_details_student_attendance_id_foreign` (`student_attendance_id`),
  ADD KEY `student_attend_details_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_cc`
--
ALTER TABLE `student_cc`
  ADD PRIMARY KEY (`student_cc_id`),
  ADD KEY `student_cc_admin_id_foreign` (`admin_id`),
  ADD KEY `student_cc_update_by_foreign` (`update_by`),
  ADD KEY `student_cc_session_id_foreign` (`session_id`),
  ADD KEY `student_cc_class_id_foreign` (`class_id`),
  ADD KEY `student_cc_section_id_foreign` (`section_id`),
  ADD KEY `student_cc_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_cheques`
--
ALTER TABLE `student_cheques`
  ADD PRIMARY KEY (`student_cheque_id`),
  ADD KEY `student_cheques_admin_id_foreign` (`admin_id`),
  ADD KEY `student_cheques_update_by_foreign` (`update_by`),
  ADD KEY `student_cheques_student_id_foreign` (`student_id`),
  ADD KEY `student_cheques_bank_id_foreign` (`bank_id`),
  ADD KEY `student_cheques_cheques_session_id_foreign` (`cheques_session_id`),
  ADD KEY `student_cheques_cheques_class_id_foreign` (`cheques_class_id`);

--
-- Indexes for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD PRIMARY KEY (`student_document_id`),
  ADD KEY `student_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_documents_update_by_foreign` (`update_by`),
  ADD KEY `student_documents_student_id_foreign` (`student_id`),
  ADD KEY `student_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `student_health_info`
--
ALTER TABLE `student_health_info`
  ADD PRIMARY KEY (`student_health_info_id`),
  ADD KEY `student_health_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_health_info_update_by_foreign` (`update_by`),
  ADD KEY `student_health_info_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD PRIMARY KEY (`student_leave_id`),
  ADD KEY `student_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `student_leaves_update_by_foreign` (`update_by`),
  ADD KEY `student_leaves_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_marksheet`
--
ALTER TABLE `student_marksheet`
  ADD PRIMARY KEY (`student_marksheet_id`),
  ADD KEY `student_marksheet_admin_id_foreign` (`admin_id`),
  ADD KEY `student_marksheet_update_by_foreign` (`update_by`),
  ADD KEY `student_marksheet_session_id_foreign` (`session_id`),
  ADD KEY `student_marksheet_exam_id_foreign` (`exam_id`),
  ADD KEY `student_marksheet_class_id_foreign` (`class_id`),
  ADD KEY `student_marksheet_section_id_foreign` (`section_id`),
  ADD KEY `student_marksheet_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_parents`
--
ALTER TABLE `student_parents`
  ADD PRIMARY KEY (`student_parent_id`),
  ADD KEY `student_parents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_parents_update_by_foreign` (`update_by`),
  ADD KEY `student_parents_reference_admin_id_foreign` (`reference_admin_id`);

--
-- Indexes for table `student_tc`
--
ALTER TABLE `student_tc`
  ADD PRIMARY KEY (`student_tc_id`),
  ADD KEY `student_tc_admin_id_foreign` (`admin_id`),
  ADD KEY `student_tc_update_by_foreign` (`update_by`),
  ADD KEY `student_tc_session_id_foreign` (`session_id`),
  ADD KEY `student_tc_class_id_foreign` (`class_id`),
  ADD KEY `student_tc_section_id_foreign` (`section_id`),
  ADD KEY `student_tc_student_id_foreign` (`student_id`);

--
-- Indexes for table `st_exam_result`
--
ALTER TABLE `st_exam_result`
  ADD PRIMARY KEY (`st_exam_result_id`),
  ADD KEY `st_exam_result_admin_id_foreign` (`admin_id`),
  ADD KEY `st_exam_result_update_by_foreign` (`update_by`),
  ADD KEY `st_exam_result_session_id_foreign` (`session_id`),
  ADD KEY `st_exam_result_exam_id_foreign` (`exam_id`),
  ADD KEY `st_exam_result_class_id_foreign` (`class_id`);

--
-- Indexes for table `st_exclude_sub`
--
ALTER TABLE `st_exclude_sub`
  ADD PRIMARY KEY (`st_exclude_sub_id`),
  ADD KEY `st_exclude_sub_admin_id_foreign` (`admin_id`),
  ADD KEY `st_exclude_sub_update_by_foreign` (`update_by`),
  ADD KEY `st_exclude_sub_session_id_foreign` (`session_id`),
  ADD KEY `st_exclude_sub_class_id_foreign` (`class_id`),
  ADD KEY `st_exclude_sub_section_id_foreign` (`section_id`),
  ADD KEY `st_exclude_sub_subject_id_foreign` (`subject_id`),
  ADD KEY `st_exclude_sub_student_id_foreign` (`student_id`);

--
-- Indexes for table `st_parent_groups`
--
ALTER TABLE `st_parent_groups`
  ADD PRIMARY KEY (`st_parent_group_id`),
  ADD KEY `st_parent_groups_admin_id_foreign` (`admin_id`),
  ADD KEY `st_parent_groups_update_by_foreign` (`update_by`);

--
-- Indexes for table `st_parent_group_map`
--
ALTER TABLE `st_parent_group_map`
  ADD PRIMARY KEY (`st_parent_group_map_id`),
  ADD KEY `st_parent_group_map_admin_id_foreign` (`admin_id`),
  ADD KEY `st_parent_group_map_update_by_foreign` (`update_by`),
  ADD KEY `st_parent_group_map_st_parent_group_id_foreign` (`st_parent_group_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`),
  ADD KEY `subjects_admin_id_foreign` (`admin_id`),
  ADD KEY `subjects_update_by_foreign` (`update_by`),
  ADD KEY `subjects_co_scholastic_type_id_foreign` (`co_scholastic_type_id`);

--
-- Indexes for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  ADD PRIMARY KEY (`subject_class_map_id`),
  ADD KEY `subject_class_map_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_class_map_update_by_foreign` (`update_by`),
  ADD KEY `subject_class_map_session_id_foreign` (`session_id`),
  ADD KEY `subject_class_map_class_id_foreign` (`class_id`),
  ADD KEY `subject_class_map_section_id_foreign` (`section_id`),
  ADD KEY `subject_class_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  ADD PRIMARY KEY (`subject_section_map_id`),
  ADD KEY `subject_section_map_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_section_map_update_by_foreign` (`update_by`),
  ADD KEY `subject_section_map_session_id_foreign` (`session_id`),
  ADD KEY `subject_section_map_class_id_foreign` (`class_id`),
  ADD KEY `subject_section_map_section_id_foreign` (`section_id`),
  ADD KEY `subject_section_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `system_config`
--
ALTER TABLE `system_config`
  ADD PRIMARY KEY (`system_config_id`),
  ADD KEY `system_config_admin_id_foreign` (`admin_id`),
  ADD KEY `system_config_update_by_foreign` (`update_by`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`),
  ADD KEY `tasks_admin_id_foreign` (`admin_id`),
  ADD KEY `tasks_update_by_foreign` (`update_by`),
  ADD KEY `tasks_class_id_foreign` (`class_id`),
  ADD KEY `tasks_section_id_foreign` (`section_id`),
  ADD KEY `tasks_student_id_foreign` (`student_id`),
  ADD KEY `tasks_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `task_response`
--
ALTER TABLE `task_response`
  ADD PRIMARY KEY (`task_response_id`),
  ADD KEY `task_response_admin_id_foreign` (`admin_id`),
  ADD KEY `task_response_update_by_foreign` (`update_by`),
  ADD KEY `task_response_task_id_foreign` (`task_id`);

--
-- Indexes for table `tds_setting`
--
ALTER TABLE `tds_setting`
  ADD PRIMARY KEY (`tds_setting_id`),
  ADD KEY `tds_setting_admin_id_foreign` (`admin_id`),
  ADD KEY `tds_setting_update_by_foreign` (`update_by`),
  ADD KEY `tds_setting_session_id_foreign` (`session_id`);

--
-- Indexes for table `term_exams`
--
ALTER TABLE `term_exams`
  ADD PRIMARY KEY (`term_exam_id`),
  ADD KEY `term_exams_admin_id_foreign` (`admin_id`),
  ADD KEY `term_exams_update_by_foreign` (`update_by`);

--
-- Indexes for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD PRIMARY KEY (`time_table_id`),
  ADD KEY `time_tables_admin_id_foreign` (`admin_id`),
  ADD KEY `time_tables_update_by_foreign` (`update_by`),
  ADD KEY `time_tables_session_id_foreign` (`session_id`),
  ADD KEY `time_tables_class_id_foreign` (`class_id`),
  ADD KEY `time_tables_section_id_foreign` (`section_id`);

--
-- Indexes for table `time_table_map`
--
ALTER TABLE `time_table_map`
  ADD PRIMARY KEY (`time_table_map_id`),
  ADD KEY `time_table_map_admin_id_foreign` (`admin_id`),
  ADD KEY `time_table_map_update_by_foreign` (`update_by`),
  ADD KEY `time_table_map_session_id_foreign` (`session_id`),
  ADD KEY `time_table_map_time_table_id_foreign` (`time_table_id`),
  ADD KEY `time_table_map_subject_id_foreign` (`subject_id`),
  ADD KEY `time_table_map_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`),
  ADD KEY `titles_admin_id_foreign` (`admin_id`),
  ADD KEY `titles_update_by_foreign` (`update_by`);

--
-- Indexes for table `transport_fees`
--
ALTER TABLE `transport_fees`
  ADD PRIMARY KEY (`transport_fee_id`),
  ADD KEY `transport_fees_admin_id_foreign` (`admin_id`),
  ADD KEY `transport_fees_update_by_foreign` (`update_by`),
  ADD KEY `transport_fees_session_id_foreign` (`session_id`),
  ADD KEY `transport_fees_class_id_foreign` (`class_id`),
  ADD KEY `transport_fees_student_id_foreign` (`student_id`),
  ADD KEY `transport_fees_map_student_staff_id_foreign` (`map_student_staff_id`);

--
-- Indexes for table `transport_fees_head`
--
ALTER TABLE `transport_fees_head`
  ADD PRIMARY KEY (`fees_head_id`),
  ADD KEY `transport_fees_head_admin_id_foreign` (`admin_id`),
  ADD KEY `transport_fees_head_update_by_foreign` (`update_by`);

--
-- Indexes for table `transport_fee_details`
--
ALTER TABLE `transport_fee_details`
  ADD PRIMARY KEY (`transport_fee_detail_id`),
  ADD KEY `transport_fee_details_admin_id_foreign` (`admin_id`),
  ADD KEY `transport_fee_details_update_by_foreign` (`update_by`),
  ADD KEY `transport_fee_details_transport_fee_id_foreign` (`transport_fee_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_details_id`),
  ADD KEY `user_details_admin_id_foreign` (`admin_id`),
  ADD KEY `user_details_update_by_foreign` (`update_by`),
  ADD KEY `user_details_task_id_foreign` (`task_id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`vehicle_id`),
  ADD KEY `vehicles_admin_id_foreign` (`admin_id`),
  ADD KEY `vehicles_update_by_foreign` (`update_by`);

--
-- Indexes for table `vehicle_attendence`
--
ALTER TABLE `vehicle_attendence`
  ADD PRIMARY KEY (`vehicle_attendence_id`),
  ADD KEY `vehicle_attendence_admin_id_foreign` (`admin_id`),
  ADD KEY `vehicle_attendence_update_by_foreign` (`update_by`),
  ADD KEY `vehicle_attendence_session_id_foreign` (`session_id`),
  ADD KEY `vehicle_attendence_vehicle_id_foreign` (`vehicle_id`);

--
-- Indexes for table `vehicle_attend_details`
--
ALTER TABLE `vehicle_attend_details`
  ADD PRIMARY KEY (`vehicle_attend_d_id`),
  ADD KEY `vehicle_attend_details_admin_id_foreign` (`admin_id`),
  ADD KEY `vehicle_attend_details_update_by_foreign` (`update_by`),
  ADD KEY `vehicle_attend_details_session_id_foreign` (`session_id`),
  ADD KEY `vehicle_attend_details_vehicle_attendence_id_foreign` (`vehicle_attendence_id`),
  ADD KEY `vehicle_attend_details_vehicle_id_foreign` (`vehicle_id`);

--
-- Indexes for table `vehicle_documents`
--
ALTER TABLE `vehicle_documents`
  ADD PRIMARY KEY (`vehicle_document_id`),
  ADD KEY `vehicle_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `vehicle_documents_update_by_foreign` (`update_by`),
  ADD KEY `vehicle_documents_vehicle_id_foreign` (`vehicle_id`),
  ADD KEY `vehicle_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  ADD PRIMARY KEY (`virtual_class_id`),
  ADD KEY `virtual_classes_admin_id_foreign` (`admin_id`),
  ADD KEY `virtual_classes_update_by_foreign` (`update_by`),
  ADD KEY `virtual_classes_session_id_foreign` (`session_id`);

--
-- Indexes for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  ADD PRIMARY KEY (`virtual_class_map_id`),
  ADD KEY `virtual_class_map_admin_id_foreign` (`admin_id`),
  ADD KEY `virtual_class_map_update_by_foreign` (`update_by`),
  ADD KEY `virtual_class_map_virtual_class_id_foreign` (`virtual_class_id`),
  ADD KEY `virtual_class_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`visitor_id`),
  ADD KEY `visitors_admin_id_foreign` (`admin_id`),
  ADD KEY `visitors_update_by_foreign` (`update_by`);

--
-- Indexes for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  ADD PRIMARY KEY (`wallet_ac_id`),
  ADD KEY `wallet_ac_admin_id_foreign` (`admin_id`),
  ADD KEY `wallet_ac_update_by_foreign` (`update_by`),
  ADD KEY `wallet_ac_session_id_foreign` (`session_id`),
  ADD KEY `wallet_ac_class_id_foreign` (`class_id`),
  ADD KEY `wallet_ac_student_id_foreign` (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acc_group`
--
ALTER TABLE `acc_group`
  MODIFY `acc_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acc_group_heads`
--
ALTER TABLE `acc_group_heads`
  MODIFY `acc_group_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acc_main_heads`
--
ALTER TABLE `acc_main_heads`
  MODIFY `acc_main_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acc_open_balance`
--
ALTER TABLE `acc_open_balance`
  MODIFY `acc_open_balance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acc_sub_heads`
--
ALTER TABLE `acc_sub_heads`
  MODIFY `acc_sub_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ac_entry_map`
--
ALTER TABLE `ac_entry_map`
  MODIFY `ac_entry_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `admission_data`
--
ALTER TABLE `admission_data`
  MODIFY `admission_data_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `admission_fields`
--
ALTER TABLE `admission_fields`
  MODIFY `admission_field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admission_forms`
--
ALTER TABLE `admission_forms`
  MODIFY `admission_form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `api_tokens`
--
ALTER TABLE `api_tokens`
  MODIFY `token_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `app_setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assign_driver_conductor_routes`
--
ALTER TABLE `assign_driver_conductor_routes`
  MODIFY `assign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `bank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `book_allowance`
--
ALTER TABLE `book_allowance`
  MODIFY `book_allowance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_categories`
--
ALTER TABLE `book_categories`
  MODIFY `book_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  MODIFY `book_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  MODIFY `book_cupboard_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  MODIFY `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `book_vendors`
--
ALTER TABLE `book_vendors`
  MODIFY `book_vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `brochures`
--
ALTER TABLE `brochures`
  MODIFY `brochure_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `caste`
--
ALTER TABLE `caste`
  MODIFY `caste_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `certificate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  MODIFY `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `communication_message`
--
ALTER TABLE `communication_message`
  MODIFY `comm_message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `competitions`
--
ALTER TABLE `competitions`
  MODIFY `competition_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `competition_map`
--
ALTER TABLE `competition_map`
  MODIFY `student_competition_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `concession_map`
--
ALTER TABLE `concession_map`
  MODIFY `concession_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  MODIFY `co_scholastic_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `document_category`
--
ALTER TABLE `document_category`
  MODIFY `document_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `esi_setting`
--
ALTER TABLE `esi_setting`
  MODIFY `esi_setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `exam_map`
--
ALTER TABLE `exam_map`
  MODIFY `exam_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `exam_marks`
--
ALTER TABLE `exam_marks`
  MODIFY `exam_mark_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `exam_schedules`
--
ALTER TABLE `exam_schedules`
  MODIFY `exam_schedule_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `facility_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  MODIFY `fees_st_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  MODIFY `receipt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  MODIFY `fee_receipt_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `fine`
--
ALTER TABLE `fine`
  MODIFY `fine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fine_details`
--
ALTER TABLE `fine_details`
  MODIFY `fine_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  MODIFY `grade_scheme_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `holiday_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `homework_conversations`
--
ALTER TABLE `homework_conversations`
  MODIFY `h_conversation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `homework_groups`
--
ALTER TABLE `homework_groups`
  MODIFY `homework_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hostels`
--
ALTER TABLE `hostels`
  MODIFY `hostel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hostel_blocks`
--
ALTER TABLE `hostel_blocks`
  MODIFY `h_block_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hostel_fees`
--
ALTER TABLE `hostel_fees`
  MODIFY `hostel_fee_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `hostel_fees_head`
--
ALTER TABLE `hostel_fees_head`
  MODIFY `h_fees_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hostel_fee_details`
--
ALTER TABLE `hostel_fee_details`
  MODIFY `hostel_fee_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hostel_floors`
--
ALTER TABLE `hostel_floors`
  MODIFY `h_floor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hostel_rooms`
--
ALTER TABLE `hostel_rooms`
  MODIFY `h_room_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hostel_room_cate`
--
ALTER TABLE `hostel_room_cate`
  MODIFY `h_room_cate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hostel_student_map`
--
ALTER TABLE `hostel_student_map`
  MODIFY `h_student_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  MODIFY `imprest_ac_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  MODIFY `imprest_ac_confi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `imprest_ac_entries`
--
ALTER TABLE `imprest_ac_entries`
  MODIFY `ac_entry_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_config`
--
ALTER TABLE `invoice_config`
  MODIFY `invoice_config_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `inv_category`
--
ALTER TABLE `inv_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inv_items`
--
ALTER TABLE `inv_items`
  MODIFY `item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inv_unit`
--
ALTER TABLE `inv_unit`
  MODIFY `unit_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inv_vendor`
--
ALTER TABLE `inv_vendor`
  MODIFY `vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `issued_books`
--
ALTER TABLE `issued_books`
  MODIFY `issued_book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `job_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job_applied_cadidates`
--
ALTER TABLE `job_applied_cadidates`
  MODIFY `applied_cadidate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_applied_cadidate_status`
--
ALTER TABLE `job_applied_cadidate_status`
  MODIFY `applied_cadidate_status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_forms`
--
ALTER TABLE `job_forms`
  MODIFY `job_form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job_form_fields`
--
ALTER TABLE `job_form_fields`
  MODIFY `job_form_field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `library_fine`
--
ALTER TABLE `library_fine`
  MODIFY `library_fine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `library_members`
--
ALTER TABLE `library_members`
  MODIFY `library_member_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `map_student_staff_vehicle`
--
ALTER TABLE `map_student_staff_vehicle`
  MODIFY `map_student_staff_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `marksheet_templates`
--
ALTER TABLE `marksheet_templates`
  MODIFY `marksheet_template_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  MODIFY `marks_criteria_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_modules`
--
ALTER TABLE `master_modules`
  MODIFY `master_module_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `medium_type`
--
ALTER TABLE `medium_type`
  MODIFY `medium_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=270;

--
-- AUTO_INCREMENT for table `module_permissions`
--
ALTER TABLE `module_permissions`
  MODIFY `module_permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `nationality_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  MODIFY `ot_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `online_notes`
--
ALTER TABLE `online_notes`
  MODIFY `online_note_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payroll_mapping`
--
ALTER TABLE `payroll_mapping`
  MODIFY `payroll_mapping_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pay_advance`
--
ALTER TABLE `pay_advance`
  MODIFY `pay_advance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pay_arrear`
--
ALTER TABLE `pay_arrear`
  MODIFY `pay_arrear_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pay_arrear_map`
--
ALTER TABLE `pay_arrear_map`
  MODIFY `pay_arrear_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pay_bonus`
--
ALTER TABLE `pay_bonus`
  MODIFY `pay_bonus_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pay_bonus_map`
--
ALTER TABLE `pay_bonus_map`
  MODIFY `pay_bonus_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pay_config`
--
ALTER TABLE `pay_config`
  MODIFY `pay_config_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pay_dept`
--
ALTER TABLE `pay_dept`
  MODIFY `pay_dept_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pay_dept_map`
--
ALTER TABLE `pay_dept_map`
  MODIFY `pay_dept_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pay_grade`
--
ALTER TABLE `pay_grade`
  MODIFY `pay_grade_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pay_leave_scheme`
--
ALTER TABLE `pay_leave_scheme`
  MODIFY `pay_leave_scheme_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pay_leave_scheme_map`
--
ALTER TABLE `pay_leave_scheme_map`
  MODIFY `pay_leave_scheme_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pay_loan`
--
ALTER TABLE `pay_loan`
  MODIFY `pay_loan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pay_salary_head`
--
ALTER TABLE `pay_salary_head`
  MODIFY `pay_salary_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pf_setting`
--
ALTER TABLE `pf_setting`
  MODIFY `pf_setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `plan_schedule`
--
ALTER TABLE `plan_schedule`
  MODIFY `plan_schedule_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pt_setting`
--
ALTER TABLE `pt_setting`
  MODIFY `pt_setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `purchase_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchase_items`
--
ALTER TABLE `purchase_items`
  MODIFY `purchase_items_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `question_papers`
--
ALTER TABLE `question_papers`
  MODIFY `question_paper_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reasons`
--
ALTER TABLE `reasons`
  MODIFY `reason_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  MODIFY `rc_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  MODIFY `rc_inst_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `religion_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `remarks`
--
ALTER TABLE `remarks`
  MODIFY `remark_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `room_no`
--
ALTER TABLE `room_no`
  MODIFY `room_no_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `route_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `route_locations`
--
ALTER TABLE `route_locations`
  MODIFY `route_location_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  MODIFY `rte_cheque_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rte_heads`
--
ALTER TABLE `rte_heads`
  MODIFY `rte_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  MODIFY `rte_head_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_structure`
--
ALTER TABLE `salary_structure`
  MODIFY `salary_structure_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `salary_structure_map`
--
ALTER TABLE `salary_structure_map`
  MODIFY `salary_structure_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `sale_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sale_items`
--
ALTER TABLE `sale_items`
  MODIFY `sale_items_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sal_struct_map_staff`
--
ALTER TABLE `sal_struct_map_staff`
  MODIFY `sal_struct_map_staff_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schedule_map`
--
ALTER TABLE `schedule_map`
  MODIFY `schedule_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `schedule_room_map`
--
ALTER TABLE `schedule_room_map`
  MODIFY `schedule_room_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `school_boards`
--
ALTER TABLE `school_boards`
  MODIFY `board_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `shift_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staff_attendance`
--
ALTER TABLE `staff_attendance`
  MODIFY `staff_attendance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `staff_attend_details`
--
ALTER TABLE `staff_attend_details`
  MODIFY `staff_attend_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  MODIFY `staff_class_allocation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `staff_documents`
--
ALTER TABLE `staff_documents`
  MODIFY `staff_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  MODIFY `staff_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `staff_leave_history_info`
--
ALTER TABLE `staff_leave_history_info`
  MODIFY `staff_leave_history_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff_roles`
--
ALTER TABLE `staff_roles`
  MODIFY `staff_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `staff_substitute`
--
ALTER TABLE `staff_substitute`
  MODIFY `substitute_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `streams`
--
ALTER TABLE `streams`
  MODIFY `stream_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  MODIFY `student_academic_history_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  MODIFY `student_academic_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student_attendance`
--
ALTER TABLE `student_attendance`
  MODIFY `student_attendance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_attend_details`
--
ALTER TABLE `student_attend_details`
  MODIFY `student_attend_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `student_cc`
--
ALTER TABLE `student_cc`
  MODIFY `student_cc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_cheques`
--
ALTER TABLE `student_cheques`
  MODIFY `student_cheque_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_documents`
--
ALTER TABLE `student_documents`
  MODIFY `student_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_health_info`
--
ALTER TABLE `student_health_info`
  MODIFY `student_health_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student_leaves`
--
ALTER TABLE `student_leaves`
  MODIFY `student_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `student_marksheet`
--
ALTER TABLE `student_marksheet`
  MODIFY `student_marksheet_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `student_parents`
--
ALTER TABLE `student_parents`
  MODIFY `student_parent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student_tc`
--
ALTER TABLE `student_tc`
  MODIFY `student_tc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `st_exam_result`
--
ALTER TABLE `st_exam_result`
  MODIFY `st_exam_result_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `st_exclude_sub`
--
ALTER TABLE `st_exclude_sub`
  MODIFY `st_exclude_sub_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `st_parent_groups`
--
ALTER TABLE `st_parent_groups`
  MODIFY `st_parent_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_parent_group_map`
--
ALTER TABLE `st_parent_group_map`
  MODIFY `st_parent_group_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  MODIFY `subject_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  MODIFY `subject_section_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `system_config`
--
ALTER TABLE `system_config`
  MODIFY `system_config_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `task_response`
--
ALTER TABLE `task_response`
  MODIFY `task_response_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tds_setting`
--
ALTER TABLE `tds_setting`
  MODIFY `tds_setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `term_exams`
--
ALTER TABLE `term_exams`
  MODIFY `term_exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `time_tables`
--
ALTER TABLE `time_tables`
  MODIFY `time_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `time_table_map`
--
ALTER TABLE `time_table_map`
  MODIFY `time_table_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transport_fees`
--
ALTER TABLE `transport_fees`
  MODIFY `transport_fee_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transport_fees_head`
--
ALTER TABLE `transport_fees_head`
  MODIFY `fees_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transport_fee_details`
--
ALTER TABLE `transport_fee_details`
  MODIFY `transport_fee_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_details_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `vehicle_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vehicle_attendence`
--
ALTER TABLE `vehicle_attendence`
  MODIFY `vehicle_attendence_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_attend_details`
--
ALTER TABLE `vehicle_attend_details`
  MODIFY `vehicle_attend_d_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vehicle_documents`
--
ALTER TABLE `vehicle_documents`
  MODIFY `vehicle_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  MODIFY `virtual_class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  MODIFY `virtual_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `visitor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  MODIFY `wallet_ac_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `acc_group`
--
ALTER TABLE `acc_group`
  ADD CONSTRAINT `acc_group_acc_sub_head_id_foreign` FOREIGN KEY (`acc_sub_head_id`) REFERENCES `acc_sub_heads` (`acc_sub_head_id`),
  ADD CONSTRAINT `acc_group_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `acc_group_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `acc_group_heads`
--
ALTER TABLE `acc_group_heads`
  ADD CONSTRAINT `acc_group_heads_acc_group_id_foreign` FOREIGN KEY (`acc_group_id`) REFERENCES `acc_group` (`acc_group_id`),
  ADD CONSTRAINT `acc_group_heads_acc_sub_head_id_foreign` FOREIGN KEY (`acc_sub_head_id`) REFERENCES `acc_sub_heads` (`acc_sub_head_id`),
  ADD CONSTRAINT `acc_group_heads_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `acc_group_heads_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `acc_open_balance`
--
ALTER TABLE `acc_open_balance`
  ADD CONSTRAINT `acc_open_balance_acc_group_head_id_foreign` FOREIGN KEY (`acc_group_head_id`) REFERENCES `acc_group_heads` (`acc_group_head_id`),
  ADD CONSTRAINT `acc_open_balance_acc_sub_head_id_foreign` FOREIGN KEY (`acc_sub_head_id`) REFERENCES `acc_sub_heads` (`acc_sub_head_id`),
  ADD CONSTRAINT `acc_open_balance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `acc_open_balance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `acc_sub_heads`
--
ALTER TABLE `acc_sub_heads`
  ADD CONSTRAINT `acc_sub_heads_acc_main_head_id_foreign` FOREIGN KEY (`acc_main_head_id`) REFERENCES `acc_main_heads` (`acc_main_head_id`);

--
-- Constraints for table `ac_entry_map`
--
ALTER TABLE `ac_entry_map`
  ADD CONSTRAINT `ac_entry_map_ac_entry_id_foreign` FOREIGN KEY (`ac_entry_id`) REFERENCES `imprest_ac_entries` (`ac_entry_id`),
  ADD CONSTRAINT `ac_entry_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `ac_entry_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `ac_entry_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `ac_entry_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `ac_entry_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `ac_entry_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `admission_data`
--
ALTER TABLE `admission_data`
  ADD CONSTRAINT `admission_data_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `admission_data_admission_class_id_foreign` FOREIGN KEY (`admission_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `admission_data_admission_form_id_foreign` FOREIGN KEY (`admission_form_id`) REFERENCES `admission_forms` (`admission_form_id`),
  ADD CONSTRAINT `admission_data_admission_section_id_foreign` FOREIGN KEY (`admission_section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `admission_data_admission_session_id_foreign` FOREIGN KEY (`admission_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `admission_data_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`),
  ADD CONSTRAINT `admission_data_current_class_id_foreign` FOREIGN KEY (`current_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `admission_data_current_section_id_foreign` FOREIGN KEY (`current_section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `admission_data_current_session_id_foreign` FOREIGN KEY (`current_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `admission_data_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`),
  ADD CONSTRAINT `admission_data_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`),
  ADD CONSTRAINT `admission_data_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`),
  ADD CONSTRAINT `admission_data_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`),
  ADD CONSTRAINT `admission_data_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `admission_data_student_permanent_city_foreign` FOREIGN KEY (`student_permanent_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `admission_data_student_permanent_county_foreign` FOREIGN KEY (`student_permanent_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `admission_data_student_permanent_state_foreign` FOREIGN KEY (`student_permanent_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `admission_data_student_sibling_class_id_foreign` FOREIGN KEY (`student_sibling_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `admission_data_student_temporary_city_foreign` FOREIGN KEY (`student_temporary_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `admission_data_student_temporary_county_foreign` FOREIGN KEY (`student_temporary_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `admission_data_student_temporary_state_foreign` FOREIGN KEY (`student_temporary_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `admission_data_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`),
  ADD CONSTRAINT `admission_data_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `admission_fields`
--
ALTER TABLE `admission_fields`
  ADD CONSTRAINT `admission_fields_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `admission_fields_admission_form_id_foreign` FOREIGN KEY (`admission_form_id`) REFERENCES `admission_forms` (`admission_form_id`),
  ADD CONSTRAINT `admission_fields_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `admission_forms`
--
ALTER TABLE `admission_forms`
  ADD CONSTRAINT `admission_forms_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `admission_forms_brochure_id_foreign` FOREIGN KEY (`brochure_id`) REFERENCES `brochures` (`brochure_id`),
  ADD CONSTRAINT `admission_forms_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `admission_forms_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `assign_driver_conductor_routes`
--
ALTER TABLE `assign_driver_conductor_routes`
  ADD CONSTRAINT `assign_driver_conductor_routes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `assign_driver_conductor_routes_conductor_id_foreign` FOREIGN KEY (`conductor_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `assign_driver_conductor_routes_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `assign_driver_conductor_routes_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`),
  ADD CONSTRAINT `assign_driver_conductor_routes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `assign_driver_conductor_routes_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`);

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `books_book_category_id_foreign` FOREIGN KEY (`book_category_id`) REFERENCES `book_categories` (`book_category_id`),
  ADD CONSTRAINT `books_book_cupboard_id_foreign` FOREIGN KEY (`book_cupboard_id`) REFERENCES `book_cupboards` (`book_cupboard_id`),
  ADD CONSTRAINT `books_book_cupboardshelf_id_foreign` FOREIGN KEY (`book_cupboardshelf_id`) REFERENCES `book_cupboardshelfs` (`book_cupboardshelf_id`),
  ADD CONSTRAINT `books_book_vendor_id_foreign` FOREIGN KEY (`book_vendor_id`) REFERENCES `book_vendors` (`book_vendor_id`),
  ADD CONSTRAINT `books_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD CONSTRAINT `book_allowance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_allowance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD CONSTRAINT `book_categories_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_categories_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD CONSTRAINT `book_copies_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_copies_info_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
  ADD CONSTRAINT `book_copies_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD CONSTRAINT `book_cupboards_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_cupboards_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD CONSTRAINT `book_cupboardshelfs_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_cupboardshelfs_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD CONSTRAINT `book_vendors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `book_vendors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `brochures`
--
ALTER TABLE `brochures`
  ADD CONSTRAINT `brochures_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `brochures_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `brochures_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `caste`
--
ALTER TABLE `caste`
  ADD CONSTRAINT `caste_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `caste_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `certificates`
--
ALTER TABLE `certificates`
  ADD CONSTRAINT `certificates_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `certificates_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`);

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `classes_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `school_boards` (`board_id`),
  ADD CONSTRAINT `classes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `classes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  ADD CONSTRAINT `class_subject_staff_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `class_subject_staff_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `class_subject_staff_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `class_subject_staff_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `class_subject_staff_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `communication_message`
--
ALTER TABLE `communication_message`
  ADD CONSTRAINT `communication_message_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `communication_message_student_parent_id_foreign` FOREIGN KEY (`student_parent_id`) REFERENCES `student_parents` (`student_parent_id`);

--
-- Constraints for table `competitions`
--
ALTER TABLE `competitions`
  ADD CONSTRAINT `competitions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `competitions_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `competitions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `competition_map`
--
ALTER TABLE `competition_map`
  ADD CONSTRAINT `competition_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `competition_map_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competitions` (`competition_id`),
  ADD CONSTRAINT `competition_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `competition_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `concession_map`
--
ALTER TABLE `concession_map`
  ADD CONSTRAINT `concession_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `concession_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `concession_map_reason_id_foreign` FOREIGN KEY (`reason_id`) REFERENCES `reasons` (`reason_id`),
  ADD CONSTRAINT `concession_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `concession_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `concession_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD CONSTRAINT `co_scholastic_types_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `co_scholastic_types_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `designations`
--
ALTER TABLE `designations`
  ADD CONSTRAINT `designations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `designations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `document_category`
--
ALTER TABLE `document_category`
  ADD CONSTRAINT `document_category_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `document_category_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `esi_setting`
--
ALTER TABLE `esi_setting`
  ADD CONSTRAINT `esi_setting_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `esi_setting_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `esi_setting_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `exams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exams_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `exams_term_exam_id_foreign` FOREIGN KEY (`term_exam_id`) REFERENCES `term_exams` (`term_exam_id`),
  ADD CONSTRAINT `exams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exam_map`
--
ALTER TABLE `exam_map`
  ADD CONSTRAINT `exam_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exam_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `exam_map_grade_scheme_id_foreign` FOREIGN KEY (`grade_scheme_id`) REFERENCES `grade_schemes` (`grade_scheme_id`),
  ADD CONSTRAINT `exam_map_marks_criteria_id_foreign` FOREIGN KEY (`marks_criteria_id`) REFERENCES `marks_criteria` (`marks_criteria_id`),
  ADD CONSTRAINT `exam_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `exam_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `exam_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `exam_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exam_marks`
--
ALTER TABLE `exam_marks`
  ADD CONSTRAINT `exam_marks_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exam_marks_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `exam_marks_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `exam_marks_exam_schedule_id_foreign` FOREIGN KEY (`exam_schedule_id`) REFERENCES `exam_schedules` (`exam_schedule_id`),
  ADD CONSTRAINT `exam_marks_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `exam_marks_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `exam_marks_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `exam_marks_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `exam_marks_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `exam_schedules`
--
ALTER TABLE `exam_schedules`
  ADD CONSTRAINT `exam_schedules_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `exam_schedules_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `exam_schedules_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `exam_schedules_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `facilities_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  ADD CONSTRAINT `fees_st_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fees_st_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `fees_st_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `fees_st_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `fees_st_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  ADD CONSTRAINT `fee_receipt_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fee_receipt_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`),
  ADD CONSTRAINT `fee_receipt_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `fee_receipt_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `fee_receipt_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `fee_receipt_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  ADD CONSTRAINT `fee_receipt_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fee_receipt_details_head_inst_id_foreign` FOREIGN KEY (`head_inst_id`) REFERENCES `recurring_inst` (`rc_inst_id`),
  ADD CONSTRAINT `fee_receipt_details_map_student_staff_id_foreign` FOREIGN KEY (`map_student_staff_id`) REFERENCES `map_student_staff_vehicle` (`map_student_staff_id`),
  ADD CONSTRAINT `fee_receipt_details_r_concession_map_id_foreign` FOREIGN KEY (`r_concession_map_id`) REFERENCES `concession_map` (`concession_map_id`),
  ADD CONSTRAINT `fee_receipt_details_r_fine_detail_id_foreign` FOREIGN KEY (`r_fine_detail_id`) REFERENCES `fine_details` (`fine_detail_id`),
  ADD CONSTRAINT `fee_receipt_details_r_fine_id_foreign` FOREIGN KEY (`r_fine_id`) REFERENCES `fine` (`fine_id`),
  ADD CONSTRAINT `fee_receipt_details_receipt_id_foreign` FOREIGN KEY (`receipt_id`) REFERENCES `fee_receipt` (`receipt_id`),
  ADD CONSTRAINT `fee_receipt_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fine`
--
ALTER TABLE `fine`
  ADD CONSTRAINT `fine_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fine_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `fine_details`
--
ALTER TABLE `fine_details`
  ADD CONSTRAINT `fine_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `fine_details_fine_id_foreign` FOREIGN KEY (`fine_id`) REFERENCES `fine` (`fine_id`),
  ADD CONSTRAINT `fine_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `grades_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `grades_grade_scheme_id_foreign` FOREIGN KEY (`grade_scheme_id`) REFERENCES `grade_schemes` (`grade_scheme_id`),
  ADD CONSTRAINT `grades_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  ADD CONSTRAINT `grade_schemes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `grade_schemes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `holidays`
--
ALTER TABLE `holidays`
  ADD CONSTRAINT `holidays_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `holidays_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `holidays_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `homework_conversations`
--
ALTER TABLE `homework_conversations`
  ADD CONSTRAINT `homework_conversations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `homework_conversations_h_session_id_foreign` FOREIGN KEY (`h_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `homework_conversations_homework_group_id_foreign` FOREIGN KEY (`homework_group_id`) REFERENCES `homework_groups` (`homework_group_id`),
  ADD CONSTRAINT `homework_conversations_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `homework_conversations_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `homework_conversations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `homework_groups`
--
ALTER TABLE `homework_groups`
  ADD CONSTRAINT `homework_groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `homework_groups_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `homework_groups_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `homework_groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostels`
--
ALTER TABLE `hostels`
  ADD CONSTRAINT `hostels_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostels_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_blocks`
--
ALTER TABLE `hostel_blocks`
  ADD CONSTRAINT `hostel_blocks_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_blocks_hostel_id_foreign` FOREIGN KEY (`hostel_id`) REFERENCES `hostels` (`hostel_id`),
  ADD CONSTRAINT `hostel_blocks_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_fees`
--
ALTER TABLE `hostel_fees`
  ADD CONSTRAINT `hostel_fees_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_fees_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`),
  ADD CONSTRAINT `hostel_fees_h_fee_class_id_foreign` FOREIGN KEY (`h_fee_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `hostel_fees_h_fee_session_id_foreign` FOREIGN KEY (`h_fee_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `hostel_fees_h_fee_student_id_foreign` FOREIGN KEY (`h_fee_student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `hostel_fees_h_fee_student_map_id_foreign` FOREIGN KEY (`h_fee_student_map_id`) REFERENCES `hostel_student_map` (`h_student_map_id`),
  ADD CONSTRAINT `hostel_fees_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_fees_head`
--
ALTER TABLE `hostel_fees_head`
  ADD CONSTRAINT `hostel_fees_head_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_fees_head_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_fee_details`
--
ALTER TABLE `hostel_fee_details`
  ADD CONSTRAINT `hostel_fee_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_fee_details_hostel_fee_id_foreign` FOREIGN KEY (`hostel_fee_id`) REFERENCES `hostel_fees` (`hostel_fee_id`),
  ADD CONSTRAINT `hostel_fee_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_floors`
--
ALTER TABLE `hostel_floors`
  ADD CONSTRAINT `hostel_floors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_floors_h_block_id_foreign` FOREIGN KEY (`h_block_id`) REFERENCES `hostel_blocks` (`h_block_id`),
  ADD CONSTRAINT `hostel_floors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_rooms`
--
ALTER TABLE `hostel_rooms`
  ADD CONSTRAINT `hostel_rooms_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_rooms_h_block_id_foreign` FOREIGN KEY (`h_block_id`) REFERENCES `hostel_blocks` (`h_block_id`),
  ADD CONSTRAINT `hostel_rooms_h_floor_id_foreign` FOREIGN KEY (`h_floor_id`) REFERENCES `hostel_floors` (`h_floor_id`),
  ADD CONSTRAINT `hostel_rooms_h_room_cate_id_foreign` FOREIGN KEY (`h_room_cate_id`) REFERENCES `hostel_room_cate` (`h_room_cate_id`),
  ADD CONSTRAINT `hostel_rooms_hostel_id_foreign` FOREIGN KEY (`hostel_id`) REFERENCES `hostels` (`hostel_id`),
  ADD CONSTRAINT `hostel_rooms_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_room_cate`
--
ALTER TABLE `hostel_room_cate`
  ADD CONSTRAINT `hostel_room_cate_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_room_cate_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `hostel_student_map`
--
ALTER TABLE `hostel_student_map`
  ADD CONSTRAINT `hostel_student_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `hostel_student_map_h_block_id_foreign` FOREIGN KEY (`h_block_id`) REFERENCES `hostel_blocks` (`h_block_id`),
  ADD CONSTRAINT `hostel_student_map_h_floor_id_foreign` FOREIGN KEY (`h_floor_id`) REFERENCES `hostel_floors` (`h_floor_id`),
  ADD CONSTRAINT `hostel_student_map_h_room_cate_id_foreign` FOREIGN KEY (`h_room_cate_id`) REFERENCES `hostel_room_cate` (`h_room_cate_id`),
  ADD CONSTRAINT `hostel_student_map_h_room_id_foreign` FOREIGN KEY (`h_room_id`) REFERENCES `hostel_rooms` (`h_room_id`),
  ADD CONSTRAINT `hostel_student_map_hostel_id_foreign` FOREIGN KEY (`hostel_id`) REFERENCES `hostels` (`hostel_id`),
  ADD CONSTRAINT `hostel_student_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `hostel_student_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  ADD CONSTRAINT `imprest_ac_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `imprest_ac_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `imprest_ac_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `imprest_ac_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `imprest_ac_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  ADD CONSTRAINT `imprest_ac_confi_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `imprest_ac_confi_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `imprest_ac_entries`
--
ALTER TABLE `imprest_ac_entries`
  ADD CONSTRAINT `imprest_ac_entries_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `imprest_ac_entries_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `imprest_ac_entries_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `imprest_ac_entries_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `imprest_ac_entries_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `invoice_config`
--
ALTER TABLE `invoice_config`
  ADD CONSTRAINT `invoice_config_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `invoice_config_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `inv_category`
--
ALTER TABLE `inv_category`
  ADD CONSTRAINT `inv_category_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `inv_category_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `inv_items`
--
ALTER TABLE `inv_items`
  ADD CONSTRAINT `inv_items_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inv_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `inv_category` (`category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inv_items_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `inv_category` (`category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inv_items_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `inv_unit` (`unit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inv_items_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `inv_unit`
--
ALTER TABLE `inv_unit`
  ADD CONSTRAINT `inv_unit_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inv_unit_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `inv_vendor`
--
ALTER TABLE `inv_vendor`
  ADD CONSTRAINT `inv_vendor_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inv_vendor_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `issued_books`
--
ALTER TABLE `issued_books`
  ADD CONSTRAINT `issued_books_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `issued_books_book_copies_id_foreign` FOREIGN KEY (`book_copies_id`) REFERENCES `book_copies_info` (`book_info_id`),
  ADD CONSTRAINT `issued_books_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
  ADD CONSTRAINT `issued_books_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  ADD CONSTRAINT `issued_books_history_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `issued_books_history_book_copies_id_foreign` FOREIGN KEY (`book_copies_id`) REFERENCES `book_copies_info` (`book_info_id`),
  ADD CONSTRAINT `issued_books_history_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
  ADD CONSTRAINT `issued_books_history_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `job`
--
ALTER TABLE `job`
  ADD CONSTRAINT `job_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `job_job_medium_type_foreign` FOREIGN KEY (`job_medium_type`) REFERENCES `medium_type` (`medium_type_id`),
  ADD CONSTRAINT `job_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `job_applied_cadidates`
--
ALTER TABLE `job_applied_cadidates`
  ADD CONSTRAINT `job_applied_cadidates_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`),
  ADD CONSTRAINT `job_applied_cadidates_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`designation_id`),
  ADD CONSTRAINT `job_applied_cadidates_job_form_id_foreign` FOREIGN KEY (`job_form_id`) REFERENCES `job_forms` (`job_form_id`),
  ADD CONSTRAINT `job_applied_cadidates_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `job` (`job_id`),
  ADD CONSTRAINT `job_applied_cadidates_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`),
  ADD CONSTRAINT `job_applied_cadidates_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`),
  ADD CONSTRAINT `job_applied_cadidates_staff_permanent_city_foreign` FOREIGN KEY (`staff_permanent_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `job_applied_cadidates_staff_permanent_county_foreign` FOREIGN KEY (`staff_permanent_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `job_applied_cadidates_staff_permanent_state_foreign` FOREIGN KEY (`staff_permanent_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `job_applied_cadidates_staff_temporary_city_foreign` FOREIGN KEY (`staff_temporary_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `job_applied_cadidates_staff_temporary_county_foreign` FOREIGN KEY (`staff_temporary_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `job_applied_cadidates_staff_temporary_state_foreign` FOREIGN KEY (`staff_temporary_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `job_applied_cadidates_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`);

--
-- Constraints for table `job_applied_cadidate_status`
--
ALTER TABLE `job_applied_cadidate_status`
  ADD CONSTRAINT `job_applied_cadidate_status_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `job_applied_cadidate_status_applied_cadidate_id_foreign` FOREIGN KEY (`applied_cadidate_id`) REFERENCES `job_applied_cadidates` (`applied_cadidate_id`),
  ADD CONSTRAINT `job_applied_cadidate_status_job_form_id_foreign` FOREIGN KEY (`job_form_id`) REFERENCES `job_forms` (`job_form_id`),
  ADD CONSTRAINT `job_applied_cadidate_status_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `job` (`job_id`),
  ADD CONSTRAINT `job_applied_cadidate_status_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `job_forms`
--
ALTER TABLE `job_forms`
  ADD CONSTRAINT `job_forms_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `job_forms_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `job` (`job_id`),
  ADD CONSTRAINT `job_forms_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `job_forms_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `job_form_fields`
--
ALTER TABLE `job_form_fields`
  ADD CONSTRAINT `job_form_fields_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `job_form_fields_job_form_id_foreign` FOREIGN KEY (`job_form_id`) REFERENCES `job_forms` (`job_form_id`),
  ADD CONSTRAINT `job_form_fields_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `library_fine`
--
ALTER TABLE `library_fine`
  ADD CONSTRAINT `library_fine_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `library_fine_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `library_members` (`library_member_id`),
  ADD CONSTRAINT `library_fine_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `library_members`
--
ALTER TABLE `library_members`
  ADD CONSTRAINT `library_members_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `library_members_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `map_student_staff_vehicle`
--
ALTER TABLE `map_student_staff_vehicle`
  ADD CONSTRAINT `map_student_staff_vehicle_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `map_student_staff_vehicle_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`),
  ADD CONSTRAINT `map_student_staff_vehicle_route_location_id_foreign` FOREIGN KEY (`route_location_id`) REFERENCES `route_locations` (`route_location_id`),
  ADD CONSTRAINT `map_student_staff_vehicle_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `map_student_staff_vehicle_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `map_student_staff_vehicle_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`);

--
-- Constraints for table `marksheet_templates`
--
ALTER TABLE `marksheet_templates`
  ADD CONSTRAINT `marksheet_templates_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `marksheet_templates_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  ADD CONSTRAINT `marks_criteria_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `marks_criteria_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `nationality`
--
ALTER TABLE `nationality`
  ADD CONSTRAINT `nationality_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `nationality_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `notice`
--
ALTER TABLE `notice`
  ADD CONSTRAINT `notice_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `notice_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `notice_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `notifications_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `notifications_parent_admin_id_foreign` FOREIGN KEY (`parent_admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `notifications_school_admin_id_foreign` FOREIGN KEY (`school_admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `notifications_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `notifications_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `notifications_staff_admin_id_foreign` FOREIGN KEY (`staff_admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `notifications_student_admin_id_foreign` FOREIGN KEY (`student_admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `notifications_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  ADD CONSTRAINT `one_time_heads_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `one_time_heads_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `online_notes`
--
ALTER TABLE `online_notes`
  ADD CONSTRAINT `online_notes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `online_notes_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `online_notes_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `online_notes_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `online_notes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `payroll_mapping`
--
ALTER TABLE `payroll_mapping`
  ADD CONSTRAINT `payroll_mapping_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `payroll_mapping_pay_dept_id_foreign` FOREIGN KEY (`pay_dept_id`) REFERENCES `pay_dept` (`pay_dept_id`),
  ADD CONSTRAINT `payroll_mapping_pay_grade_id_foreign` FOREIGN KEY (`pay_grade_id`) REFERENCES `pay_grade` (`pay_grade_id`),
  ADD CONSTRAINT `payroll_mapping_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `payroll_mapping_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `payroll_mapping_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_advance`
--
ALTER TABLE `pay_advance`
  ADD CONSTRAINT `pay_advance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_advance_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pay_advance_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `pay_advance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_arrear`
--
ALTER TABLE `pay_arrear`
  ADD CONSTRAINT `pay_arrear_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_arrear_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_arrear_map`
--
ALTER TABLE `pay_arrear_map`
  ADD CONSTRAINT `pay_arrear_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_arrear_map_pay_arrear_id_foreign` FOREIGN KEY (`pay_arrear_id`) REFERENCES `pay_arrear` (`pay_arrear_id`),
  ADD CONSTRAINT `pay_arrear_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pay_arrear_map_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `pay_arrear_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_bonus`
--
ALTER TABLE `pay_bonus`
  ADD CONSTRAINT `pay_bonus_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_bonus_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_bonus_map`
--
ALTER TABLE `pay_bonus_map`
  ADD CONSTRAINT `pay_bonus_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_bonus_map_pay_bonus_id_foreign` FOREIGN KEY (`pay_bonus_id`) REFERENCES `pay_bonus` (`pay_bonus_id`),
  ADD CONSTRAINT `pay_bonus_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pay_bonus_map_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `pay_bonus_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_config`
--
ALTER TABLE `pay_config`
  ADD CONSTRAINT `pay_config_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_config_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_dept`
--
ALTER TABLE `pay_dept`
  ADD CONSTRAINT `pay_dept_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_dept_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_dept_map`
--
ALTER TABLE `pay_dept_map`
  ADD CONSTRAINT `pay_dept_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_dept_map_pay_dept_id_foreign` FOREIGN KEY (`pay_dept_id`) REFERENCES `pay_dept` (`pay_dept_id`),
  ADD CONSTRAINT `pay_dept_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pay_dept_map_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `pay_dept_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_grade`
--
ALTER TABLE `pay_grade`
  ADD CONSTRAINT `pay_grade_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_grade_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_leave_scheme`
--
ALTER TABLE `pay_leave_scheme`
  ADD CONSTRAINT `pay_leave_scheme_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_leave_scheme_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_leave_scheme_map`
--
ALTER TABLE `pay_leave_scheme_map`
  ADD CONSTRAINT `pay_leave_scheme_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_leave_scheme_map_pay_leave_scheme_id_foreign` FOREIGN KEY (`pay_leave_scheme_id`) REFERENCES `pay_leave_scheme` (`pay_leave_scheme_id`),
  ADD CONSTRAINT `pay_leave_scheme_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pay_leave_scheme_map_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `pay_leave_scheme_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_loan`
--
ALTER TABLE `pay_loan`
  ADD CONSTRAINT `pay_loan_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_loan_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pay_loan_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `pay_loan_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pay_salary_head`
--
ALTER TABLE `pay_salary_head`
  ADD CONSTRAINT `pay_salary_head_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pay_salary_head_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pf_setting`
--
ALTER TABLE `pf_setting`
  ADD CONSTRAINT `pf_setting_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pf_setting_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pf_setting_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `plan_schedule`
--
ALTER TABLE `plan_schedule`
  ADD CONSTRAINT `plan_schedule_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `plan_schedule_current_session_id_foreign` FOREIGN KEY (`current_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `plan_schedule_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `plan_schedule_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `pt_setting`
--
ALTER TABLE `pt_setting`
  ADD CONSTRAINT `pt_setting_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `pt_setting_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `pt_setting_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `purchases_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`),
  ADD CONSTRAINT `purchases_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `inv_category` (`category_id`),
  ADD CONSTRAINT `purchases_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `inv_category` (`category_id`),
  ADD CONSTRAINT `purchases_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `purchases_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `inv_vendor` (`vendor_id`);

--
-- Constraints for table `purchase_items`
--
ALTER TABLE `purchase_items`
  ADD CONSTRAINT `purchase_items_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `purchase_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `inv_category` (`category_id`),
  ADD CONSTRAINT `purchase_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `inv_items` (`item_id`),
  ADD CONSTRAINT `purchase_items_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`purchase_id`),
  ADD CONSTRAINT `purchase_items_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `inv_category` (`category_id`),
  ADD CONSTRAINT `purchase_items_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `inv_unit` (`unit_id`),
  ADD CONSTRAINT `purchase_items_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `question_papers`
--
ALTER TABLE `question_papers`
  ADD CONSTRAINT `question_papers_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `question_papers_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `question_papers_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `question_papers_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `question_papers_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `question_papers_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `question_papers_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `reasons`
--
ALTER TABLE `reasons`
  ADD CONSTRAINT `reasons_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `reasons_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  ADD CONSTRAINT `recurring_heads_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `recurring_heads_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  ADD CONSTRAINT `recurring_inst_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `recurring_inst_rc_head_id_foreign` FOREIGN KEY (`rc_head_id`) REFERENCES `recurring_heads` (`rc_head_id`),
  ADD CONSTRAINT `recurring_inst_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `religions`
--
ALTER TABLE `religions`
  ADD CONSTRAINT `religions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `religions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `remarks`
--
ALTER TABLE `remarks`
  ADD CONSTRAINT `remarks_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `remarks_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `remarks_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `remarks_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `remarks_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `remarks_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `remarks_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `remarks_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `room_no`
--
ALTER TABLE `room_no`
  ADD CONSTRAINT `room_no_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `room_no_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `routes`
--
ALTER TABLE `routes`
  ADD CONSTRAINT `routes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `routes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `route_locations`
--
ALTER TABLE `route_locations`
  ADD CONSTRAINT `route_locations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `route_locations_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`),
  ADD CONSTRAINT `route_locations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  ADD CONSTRAINT `rte_cheques_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `rte_cheques_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`),
  ADD CONSTRAINT `rte_cheques_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `rte_heads`
--
ALTER TABLE `rte_heads`
  ADD CONSTRAINT `rte_heads_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `rte_heads_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  ADD CONSTRAINT `rte_head_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `rte_head_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `rte_head_map_rte_head_id_foreign` FOREIGN KEY (`rte_head_id`) REFERENCES `rte_heads` (`rte_head_id`),
  ADD CONSTRAINT `rte_head_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `rte_head_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `rte_head_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `rte_head_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `salary_structure`
--
ALTER TABLE `salary_structure`
  ADD CONSTRAINT `salary_structure_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `salary_structure_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `salary_structure_map`
--
ALTER TABLE `salary_structure_map`
  ADD CONSTRAINT `salary_structure_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `salary_structure_map_pay_salary_head_id_foreign` FOREIGN KEY (`pay_salary_head_id`) REFERENCES `pay_salary_head` (`pay_salary_head_id`),
  ADD CONSTRAINT `salary_structure_map_salary_structure_id_foreign` FOREIGN KEY (`salary_structure_id`) REFERENCES `salary_structure` (`salary_structure_id`),
  ADD CONSTRAINT `salary_structure_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `sales_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`),
  ADD CONSTRAINT `sales_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `sales_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `staff_roles` (`staff_role_id`),
  ADD CONSTRAINT `sales_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `sales_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `sales_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `sales_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD CONSTRAINT `sale_items_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `sale_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `inv_category` (`category_id`),
  ADD CONSTRAINT `sale_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `inv_items` (`item_id`),
  ADD CONSTRAINT `sale_items_sale_id_foreign` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`sale_id`),
  ADD CONSTRAINT `sale_items_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `inv_category` (`category_id`),
  ADD CONSTRAINT `sale_items_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `inv_unit` (`unit_id`),
  ADD CONSTRAINT `sale_items_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `sal_struct_map_staff`
--
ALTER TABLE `sal_struct_map_staff`
  ADD CONSTRAINT `sal_struct_map_staff_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `sal_struct_map_staff_salary_structure_id_foreign` FOREIGN KEY (`salary_structure_id`) REFERENCES `salary_structure` (`salary_structure_id`),
  ADD CONSTRAINT `sal_struct_map_staff_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `sal_struct_map_staff_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `sal_struct_map_staff_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `schedule_map`
--
ALTER TABLE `schedule_map`
  ADD CONSTRAINT `schedule_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `schedule_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `schedule_map_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `schedule_map_exam_schedule_id_foreign` FOREIGN KEY (`exam_schedule_id`) REFERENCES `exam_schedules` (`exam_schedule_id`),
  ADD CONSTRAINT `schedule_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `schedule_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `schedule_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `schedule_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `schedule_room_map`
--
ALTER TABLE `schedule_room_map`
  ADD CONSTRAINT `schedule_room_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `schedule_room_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `schedule_room_map_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `schedule_room_map_exam_schedule_id_foreign` FOREIGN KEY (`exam_schedule_id`) REFERENCES `exam_schedules` (`exam_schedule_id`),
  ADD CONSTRAINT `schedule_room_map_room_no_id_foreign` FOREIGN KEY (`room_no_id`) REFERENCES `room_no` (`room_no_id`),
  ADD CONSTRAINT `schedule_room_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `schedule_room_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `schedule_room_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `school`
--
ALTER TABLE `school`
  ADD CONSTRAINT `school_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `school_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `school_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `school_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `school_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `school_boards`
--
ALTER TABLE `school_boards`
  ADD CONSTRAINT `school_boards_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `school_boards_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `sections_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `sections_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `sections_shift_id_foreign` FOREIGN KEY (`shift_id`) REFERENCES `shifts` (`shift_id`),
  ADD CONSTRAINT `sections_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`),
  ADD CONSTRAINT `sections_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `sessions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `shifts`
--
ALTER TABLE `shifts`
  ADD CONSTRAINT `shifts_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `shifts_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`),
  ADD CONSTRAINT `staff_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`designation_id`),
  ADD CONSTRAINT `staff_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`),
  ADD CONSTRAINT `staff_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`),
  ADD CONSTRAINT `staff_staff_permanent_city_foreign` FOREIGN KEY (`staff_permanent_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `staff_staff_permanent_county_foreign` FOREIGN KEY (`staff_permanent_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `staff_staff_permanent_state_foreign` FOREIGN KEY (`staff_permanent_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `staff_staff_temporary_city_foreign` FOREIGN KEY (`staff_temporary_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `staff_staff_temporary_county_foreign` FOREIGN KEY (`staff_temporary_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `staff_staff_temporary_state_foreign` FOREIGN KEY (`staff_temporary_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `staff_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`),
  ADD CONSTRAINT `staff_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_attendance`
--
ALTER TABLE `staff_attendance`
  ADD CONSTRAINT `staff_attendance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_attendance_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `staff_attendance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_attend_details`
--
ALTER TABLE `staff_attend_details`
  ADD CONSTRAINT `staff_attend_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_attend_details_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `staff_attend_details_staff_attendance_id_foreign` FOREIGN KEY (`staff_attendance_id`) REFERENCES `staff_attendance` (`staff_attendance_id`),
  ADD CONSTRAINT `staff_attend_details_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `staff_attend_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  ADD CONSTRAINT `staff_class_allocations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_class_allocations_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `staff_class_allocations_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `staff_class_allocations_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `staff_class_allocations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_documents`
--
ALTER TABLE `staff_documents`
  ADD CONSTRAINT `staff_documents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`document_category_id`),
  ADD CONSTRAINT `staff_documents_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `staff_documents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD CONSTRAINT `staff_leaves_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_leaves_pay_leave_scheme_id_foreign` FOREIGN KEY (`pay_leave_scheme_id`) REFERENCES `pay_leave_scheme` (`pay_leave_scheme_id`),
  ADD CONSTRAINT `staff_leaves_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `staff_leaves_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `staff_leaves_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_leave_history_info`
--
ALTER TABLE `staff_leave_history_info`
  ADD CONSTRAINT `staff_leave_history_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_leave_history_info_pay_leave_scheme_id_foreign` FOREIGN KEY (`pay_leave_scheme_id`) REFERENCES `pay_leave_scheme` (`pay_leave_scheme_id`),
  ADD CONSTRAINT `staff_leave_history_info_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `staff_leave_history_info_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `staff_leave_history_info_staff_leave_id_foreign` FOREIGN KEY (`staff_leave_id`) REFERENCES `staff_leaves` (`staff_leave_id`),
  ADD CONSTRAINT `staff_leave_history_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD CONSTRAINT `staff_roles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_roles_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `staff_substitute`
--
ALTER TABLE `staff_substitute`
  ADD CONSTRAINT `staff_substitute_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `staff_substitute_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `staff_substitute_leave_staff_id_foreign` FOREIGN KEY (`leave_staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `staff_substitute_ref_leave_id_foreign` FOREIGN KEY (`ref_leave_id`) REFERENCES `staff_leaves` (`staff_leave_id`),
  ADD CONSTRAINT `staff_substitute_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `staff_substitute_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `staff_substitute_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `staff_substitute_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `state_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`);

--
-- Constraints for table `streams`
--
ALTER TABLE `streams`
  ADD CONSTRAINT `streams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `streams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `students_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`),
  ADD CONSTRAINT `students_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`),
  ADD CONSTRAINT `students_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `students_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`),
  ADD CONSTRAINT `students_student_parent_id_foreign` FOREIGN KEY (`student_parent_id`) REFERENCES `student_parents` (`student_parent_id`),
  ADD CONSTRAINT `students_student_permanent_city_foreign` FOREIGN KEY (`student_permanent_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `students_student_permanent_county_foreign` FOREIGN KEY (`student_permanent_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `students_student_permanent_state_foreign` FOREIGN KEY (`student_permanent_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `students_student_sibling_class_id_foreign` FOREIGN KEY (`student_sibling_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `students_student_temporary_city_foreign` FOREIGN KEY (`student_temporary_city`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `students_student_temporary_county_foreign` FOREIGN KEY (`student_temporary_county`) REFERENCES `country` (`country_id`),
  ADD CONSTRAINT `students_student_temporary_state_foreign` FOREIGN KEY (`student_temporary_state`) REFERENCES `state` (`state_id`),
  ADD CONSTRAINT `students_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`),
  ADD CONSTRAINT `students_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  ADD CONSTRAINT `student_academic_history_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_academic_history_info_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_academic_history_info_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_academic_history_info_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_academic_history_info_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`),
  ADD CONSTRAINT `student_academic_history_info_student_academic_info_id_foreign` FOREIGN KEY (`student_academic_info_id`) REFERENCES `student_academic_info` (`student_academic_info_id`),
  ADD CONSTRAINT `student_academic_history_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_academic_history_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  ADD CONSTRAINT `student_academic_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_academic_info_admission_class_id_foreign` FOREIGN KEY (`admission_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_academic_info_admission_section_id_foreign` FOREIGN KEY (`admission_section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_academic_info_admission_session_id_foreign` FOREIGN KEY (`admission_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_academic_info_current_class_id_foreign` FOREIGN KEY (`current_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_academic_info_current_section_id_foreign` FOREIGN KEY (`current_section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_academic_info_current_session_id_foreign` FOREIGN KEY (`current_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_academic_info_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`),
  ADD CONSTRAINT `student_academic_info_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`),
  ADD CONSTRAINT `student_academic_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_academic_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD CONSTRAINT `student_attendance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_attendance_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_attendance_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_attendance_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_attendance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_attend_details`
--
ALTER TABLE `student_attend_details`
  ADD CONSTRAINT `student_attend_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_attend_details_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_attend_details_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_attend_details_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_attend_details_student_attendance_id_foreign` FOREIGN KEY (`student_attendance_id`) REFERENCES `student_attendance` (`student_attendance_id`),
  ADD CONSTRAINT `student_attend_details_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_attend_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_cc`
--
ALTER TABLE `student_cc`
  ADD CONSTRAINT `student_cc_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_cc_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_cc_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_cc_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_cc_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_cc_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_cheques`
--
ALTER TABLE `student_cheques`
  ADD CONSTRAINT `student_cheques_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_cheques_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`),
  ADD CONSTRAINT `student_cheques_cheques_class_id_foreign` FOREIGN KEY (`cheques_class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_cheques_cheques_session_id_foreign` FOREIGN KEY (`cheques_session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_cheques_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_cheques_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD CONSTRAINT `student_documents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`document_category_id`),
  ADD CONSTRAINT `student_documents_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_documents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_health_info`
--
ALTER TABLE `student_health_info`
  ADD CONSTRAINT `student_health_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_health_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_health_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD CONSTRAINT `student_leaves_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_leaves_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_leaves_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_marksheet`
--
ALTER TABLE `student_marksheet`
  ADD CONSTRAINT `student_marksheet_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_marksheet_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_marksheet_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `student_marksheet_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_marksheet_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_marksheet_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_marksheet_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_parents`
--
ALTER TABLE `student_parents`
  ADD CONSTRAINT `student_parents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_parents_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_parents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `student_tc`
--
ALTER TABLE `student_tc`
  ADD CONSTRAINT `student_tc_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `student_tc_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `student_tc_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `student_tc_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `student_tc_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `student_tc_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `st_exam_result`
--
ALTER TABLE `st_exam_result`
  ADD CONSTRAINT `st_exam_result_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `st_exam_result_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `st_exam_result_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`),
  ADD CONSTRAINT `st_exam_result_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `st_exam_result_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `st_exclude_sub`
--
ALTER TABLE `st_exclude_sub`
  ADD CONSTRAINT `st_exclude_sub_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `st_exclude_sub_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `st_exclude_sub_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `st_exclude_sub_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `st_exclude_sub_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `st_exclude_sub_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `st_exclude_sub_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `st_parent_groups`
--
ALTER TABLE `st_parent_groups`
  ADD CONSTRAINT `st_parent_groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `st_parent_groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `st_parent_group_map`
--
ALTER TABLE `st_parent_group_map`
  ADD CONSTRAINT `st_parent_group_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `st_parent_group_map_st_parent_group_id_foreign` FOREIGN KEY (`st_parent_group_id`) REFERENCES `st_parent_groups` (`st_parent_group_id`),
  ADD CONSTRAINT `st_parent_group_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `subjects_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `subjects_co_scholastic_type_id_foreign` FOREIGN KEY (`co_scholastic_type_id`) REFERENCES `co_scholastic_types` (`co_scholastic_type_id`),
  ADD CONSTRAINT `subjects_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  ADD CONSTRAINT `subject_class_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `subject_class_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `subject_class_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `subject_class_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `subject_class_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `subject_class_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  ADD CONSTRAINT `subject_section_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `subject_section_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `subject_section_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `subject_section_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `subject_section_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `subject_section_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `system_config`
--
ALTER TABLE `system_config`
  ADD CONSTRAINT `system_config_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `system_config_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `tasks_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `tasks_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `tasks_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `tasks_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `task_response`
--
ALTER TABLE `task_response`
  ADD CONSTRAINT `task_response_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `task_response_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`task_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `task_response_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `tds_setting`
--
ALTER TABLE `tds_setting`
  ADD CONSTRAINT `tds_setting_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `tds_setting_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `tds_setting_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `term_exams`
--
ALTER TABLE `term_exams`
  ADD CONSTRAINT `term_exams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `term_exams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD CONSTRAINT `time_tables_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `time_tables_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `time_tables_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`),
  ADD CONSTRAINT `time_tables_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `time_tables_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `time_table_map`
--
ALTER TABLE `time_table_map`
  ADD CONSTRAINT `time_table_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `time_table_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `time_table_map_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `time_table_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`),
  ADD CONSTRAINT `time_table_map_time_table_id_foreign` FOREIGN KEY (`time_table_id`) REFERENCES `time_tables` (`time_table_id`),
  ADD CONSTRAINT `time_table_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `titles`
--
ALTER TABLE `titles`
  ADD CONSTRAINT `titles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `titles_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `transport_fees`
--
ALTER TABLE `transport_fees`
  ADD CONSTRAINT `transport_fees_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `transport_fees_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `transport_fees_map_student_staff_id_foreign` FOREIGN KEY (`map_student_staff_id`) REFERENCES `map_student_staff_vehicle` (`map_student_staff_id`),
  ADD CONSTRAINT `transport_fees_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `transport_fees_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `transport_fees_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `transport_fees_head`
--
ALTER TABLE `transport_fees_head`
  ADD CONSTRAINT `transport_fees_head_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transport_fees_head_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `transport_fee_details`
--
ALTER TABLE `transport_fee_details`
  ADD CONSTRAINT `transport_fee_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `transport_fee_details_transport_fee_id_foreign` FOREIGN KEY (`transport_fee_id`) REFERENCES `transport_fees` (`transport_fee_id`),
  ADD CONSTRAINT `transport_fee_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_details_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`task_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD CONSTRAINT `vehicles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `vehicles_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `vehicle_attendence`
--
ALTER TABLE `vehicle_attendence`
  ADD CONSTRAINT `vehicle_attendence_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `vehicle_attendence_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `vehicle_attendence_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `vehicle_attendence_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`);

--
-- Constraints for table `vehicle_attend_details`
--
ALTER TABLE `vehicle_attend_details`
  ADD CONSTRAINT `vehicle_attend_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `vehicle_attend_details_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `vehicle_attend_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `vehicle_attend_details_vehicle_attendence_id_foreign` FOREIGN KEY (`vehicle_attendence_id`) REFERENCES `vehicle_attendence` (`vehicle_attendence_id`),
  ADD CONSTRAINT `vehicle_attend_details_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`);

--
-- Constraints for table `vehicle_documents`
--
ALTER TABLE `vehicle_documents`
  ADD CONSTRAINT `vehicle_documents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `vehicle_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`document_category_id`),
  ADD CONSTRAINT `vehicle_documents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `vehicle_documents_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`);

--
-- Constraints for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  ADD CONSTRAINT `virtual_classes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `virtual_classes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `virtual_classes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  ADD CONSTRAINT `virtual_class_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `virtual_class_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `virtual_class_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `virtual_class_map_virtual_class_id_foreign` FOREIGN KEY (`virtual_class_id`) REFERENCES `virtual_classes` (`virtual_class_id`);

--
-- Constraints for table `visitors`
--
ALTER TABLE `visitors`
  ADD CONSTRAINT `visitors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `visitors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  ADD CONSTRAINT `wallet_ac_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `wallet_ac_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `wallet_ac_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`),
  ADD CONSTRAINT `wallet_ac_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `wallet_ac_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
