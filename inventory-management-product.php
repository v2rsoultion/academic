<!-- header include -->
<?php include('header.php') ?>
<!-- header close -->

<!-- second section start -->
 <div class="container-fulid inner-banner">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 Product-heading">
 				<h1>Inventory Management</h1>
 				<div class="Product-contant wow fadeInLeft"> <span> <a href="index.php" title="Home" title="Home"> Home /  </a> </span>Inventory Management</div>
 				
 			</div>
 		
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="container-fulid ">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12 about-contant wow fadeInUp">
 		       	<h1>Inventory Management</h1>
 		       	<p>Inventory module is a very important part for any school to maintain records for items or stock, this module keep track on total quantity of stock, how much it has consumed and what is the availability of stock. This module also record all the purchase and sales transaction of stock and maintain records of purchase and sales returns and stock register always get auto updated by this transaction.This module reduces time, requires minimum staff, no paperwork and it keeps a control over inventory transaction which helps school to take decision about stock management.</p><br>
 		       	<p>This module is important as with the help of this module all the items or stocks can be listed down in an organized way and it make easier for the management team to maintain track for furniture, equipment, books set, uniforms, stationery items, etc. and it requires less time. This tool maintains all the information which is beneficial for resource planning for next session.</p><br>
 		       	
 		        	<!-- <button class="Download-Brochure" title="Download Brochure"> Download Brochure</button> -->
 		        	<div class="Download-Brochure"><a  href="files/ccc_exam_form.pdf" download="" title="Download Brochure"> Download Brochure</a></div>
 	     	</div>
 	     	
 	     </div>	
 	</div>
 </div>
<!-- end -->
<div class="clear"></div>

 <!-- section admin portal start -->
<div class="container-fulid featur-protal">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 Features-contant wow fadeInUp">
 		       	<h1>Features & Benefits </h1>
 		       	<p>Inventory and Stock management provides easy and effective solution to maintain stock inward and outward without using paper system.</p>
 	     	</div>
 	     	<div class="col-md-7 col-sm-7 col-xs-12 feature-main-block">
 	     		
	 	     	<section id="demos2">
				    <div class="owl-carousel owl-theme">
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg1.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				        <div class="item">
				           <div class="row">
							<div class="col-xs-12 col-md-12 col-sm-12 featur--Portal-contant wow fadeInLeft">
								<img src="images/productimg2.png" class="img-responsive">	
							</div>
							<div class="clear"></div>
						</div>
				       </div>
				      </div> 
			   </section>
 	   	   			<div class="clear"></div>
 	     	</div>
 	     	<div class="col-md-5 col-sm-5 col-xs-12 feature-main-block feature-main-xs">
 	     		<div class="product-account-main1 wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Reconciliation"> <img src="images/fees.png" class="img-responsive" title="Fees Reconciliation"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Easy to manage Stock</h1>
 	     				<p>Inventory Module provide easy user interface to enter purchase and sale details.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product2"> <a href="#" title="Reports & Analytics"> <img src="images/report-analicy.png" class="img-responsive" alt="Reports & Analytics"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Fast and efficient system</h1>
 	     				<p>This module provide faster way to access to inventory of school and provide complete details of transaction.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product img-product3"> <a href="#" title="Offline Fees Collection"> <img src="images/offlin-fess.png" class="img-responsive" alt="Offline Fees Collection"></a> </div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Easy to manage returns</h1>
 	     				<p>This module allows school to make purchase and sale return transaction and auto update inventory.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>	
 	     		<div class="product-account-main wow fadeInUp">
 	     			<div class="img-product"> <a href="#" title="Fees Scheduling"> <img src="images/report.png" class="img-responsive" alt="Fees Scheduling"> </a></div>
 	     			<div class="contant-block contant-block-tab">
 	     				<h1>Reduce Burden of Store Manager</h1>
 	     				<p>Inventory module reduces burden as it require less time to enter transactions and generate invoices, update inventory.</p>
 	     				<div class="clear"></div>
 	     			</div>
 	     			<div class="clear"></div>
 	     		</div>		
 	     	</div><div class="clear"></div>
		</div>
	</div>
</div>
 <!-- end -->

 <!-- section why us start -->
 <div class="container-fulid">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 col-xs-12 col-sm-12 School-Fees  wow fadeInUp">
 					 <h1>More About Inventory Management</h1>
 		         	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.</p> -->
 			</div>

 		</div>
 		<div class="row School-Fees-main">
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
				  <a href="#" class="imge" title="Account Report">	<img src="images/cheque.png" alt="Account Report"></a>
					<h1><a href="#" title="Account Report">Vendor Details</a></h1>
					<p>Inventory Management allows school to maintain records of vendor/distributors with basic information i.e, name, contact, address, Gst number.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					<a href="#" class="imge" title="Facilities Fees">	<img src="images/cheque.png" alt="Facilities Fees"></a>
					<h1><a href="#" title="Facilities Fees">Purchase and Sales</a></h1>
					<p>Inventory management stores detail entry of purchase entry from vendors and entry of sales to staff, student with Tax and Discount details.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4   wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Prepaid Account"> <img src="images/cheque.png" alt="prepaid-account"></a>
					<h1><a href="#" title="Prepaid Account">Invoice of transaction</a> </h1>
					<p>Inventory management allows store manager to view all the invoice of purchase and sale transaction, invoice can be export as pdf or image file.</p>
				</div> 				
 			</div>
 			
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInLeft ">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="RTE Collection"><img src="images/cheque.png" alt="rtr"></a>
					<h1><a href="#" title="RTE Collection">Printing of Invoice</a></h1>
					<p>Inventory management allows store manager to print invoice of sales and purchase with complete details of tax, discount, net amount and gross amount.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4  wow fadeInUp ">
				<div class="why-us-block why-us-block-one">
					  <a href="#" class="imge" title="Fee Counter"><img src="images/cheque.png" alt="account"></a>
					<h1><a href="#" title="Fee Counter">Return Transaction</a> </h1>
					<p>This module also allows store manager to update purchase return and sale return transaction, and after that it will automatically create new invoice and update stock register.</p>
				</div> 				
 			</div>
 			<div class="col-md-4 col-xs-12 col-sm-4 wow fadeInRight">
				<div class="why-us-block why-us-block-one">
					 <a href="#" class="imge" title="Cheque Details"><img src="images/cheque.png" alt="cheque"></a>
					<h1> <a href="#" title="Cheque Details">Stock Register</a> </h1>
					<p>This module provide stock register to the store manager which gives completely details of stock inwards and stock outwards, with search filter of month wise, day wise, data ranges and it can be exported as pdf or csv files.</p>
				</div> 				
 			</div>
 			
 			
 		</div>
 	</div>
 </div>
 <div class="clear"></div>
 <!-- end -->
 <!-- footer  section start -->
<?php include('footer.php'); ?>
 <!-- end -->

